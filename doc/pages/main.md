@mainpage

<h1>Instrukcja jak uruchomi� demo gry FPS po pierwszym kamieniu milowym</h1>

1. W folderze ProjektGrafika_SilnikFPS znajduje si� archiwum EngineProjectFolder.zip . Nale�y je rozpakowa� gdy� zawiera ono pliki
asset�w z ca�ym demem. Plik�w domy�lnie nie ma w projekcie poniewa� .gitignore wyklucza pliki o rozszerzeniu .obj (w demie s� to modele)
</br>


2. Uruchom ProjektGrafika_SilnikFPS.sln przy pomocy Visual Studio 2019 (najlepiej w wersji 16.11.4) a nast�pnie uruchom projekt przyciskiem Debug->Start Debugging.</br>

3. W okienku asset explorer uruchom plik 3.scene (podw�jnym klikni�ciem) lub inny plik sceny (.scene)  �adowanie asset�w mo�e potrwa� kt�rk� chwilk� wi�c projekt mo�e przez chwil� si� zawiesi�.</br> Na koniec przyciskiem '`~' na klawiaturze zminimalizuj wszystkie okienka silnika.</br>

4. Ruch po scenie: </br></br>
Po odznaczeniu wszystkich okienek oraz przytrzymaniu prawego przycisku myszy, mo�na porusza� kamer� w nast�puj�cy spos�b:</br>
Klawisze / Mysz   | Akcje
------------|------------------
W,S,A,D,Q,E | ruch kamery
Ruch myszy  | rozgl�danie si� 
</br>



5. Ruch w grze (scena z gr� to: gra.scene): </br></br>
Po wpisaniu polecenia "play" w konsole, mo�na porusza� si� graczem w nast�puj�cy spos�b:</br>
Klawisze / Mysz   | Akcje
------------|------------------
W,S,A,D | ruch gracza
Ruch myszy  | rozgl�danie si� 
Lewy przycisk myszy | strza�
</br>

<b>W scenie 3.scene po uruchomieniu polecenia "play" w konsoli mo�na porusza� graczem  klawiszami W,S,A,D oraz skaka� spacj�.</b></br>

![](screen_z_gry3d.png)
