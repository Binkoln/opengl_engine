var class_directional_light_extension =
[
    [ "Cleanup", "class_directional_light_extension.html#aae5c5d0f3f1e47a2d7d954ebf76e5310", null ],
    [ "DrawImGui", "class_directional_light_extension.html#a6ca4005ad43ef4d4650098417694b478", null ],
    [ "Init", "class_directional_light_extension.html#aa9c5e1dab1abff8e77e1c7a3abeac898", null ],
    [ "MakeCopy", "class_directional_light_extension.html#a547dfac301896ced7377422955570138", null ],
    [ "Render", "class_directional_light_extension.html#aefd9457f6ced9afc71dea9eb9b49002d", null ],
    [ "SaveScene", "class_directional_light_extension.html#a388721b4dfea4acfef54945cac27eb53", null ],
    [ "Update", "class_directional_light_extension.html#ab60e139f64206008f4acc1d0b9e50379", null ]
];