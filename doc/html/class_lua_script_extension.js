var class_lua_script_extension =
[
    [ "AddLuaVariable", "class_lua_script_extension.html#a059abc39b59ea9617760173b7e5e2291", null ],
    [ "Cleanup", "class_lua_script_extension.html#a216c3d409ef1aac6f9d42dbe85dfd40c", null ],
    [ "DrawImGui", "class_lua_script_extension.html#a5efd04aa83024811a7dc7e9d16efc211", null ],
    [ "Init", "class_lua_script_extension.html#a37db92734e27df600426923efe73a5b0", null ],
    [ "LoadScript", "class_lua_script_extension.html#aa7a7cfe23c7885e4b8b125c1f9fef2fc", null ],
    [ "MakeCopy", "class_lua_script_extension.html#a2faadb2cbfaa53ee3cc7bce9d4a3a03f", null ],
    [ "SaveScene", "class_lua_script_extension.html#a7b0e953055ef5d388ccfea7e549ca1a2", null ],
    [ "Update", "class_lua_script_extension.html#acaded49251428dc2adc3000d1abcddba", null ]
];