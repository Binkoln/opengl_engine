var class_sprite_renderer_extension =
[
    [ "Cleanup", "class_sprite_renderer_extension.html#a2f68681211c7dfa252a3587c390d6540", null ],
    [ "DrawImGui", "class_sprite_renderer_extension.html#a279691aa1ecdf51be35610aee2f396e3", null ],
    [ "getColorMultiplier", "class_sprite_renderer_extension.html#ab8d667526d71abd652909d604a5a52fd", null ],
    [ "getPosSizVector", "class_sprite_renderer_extension.html#a6c870df950dda770f620074e54a5a342", null ],
    [ "getSpritePos", "class_sprite_renderer_extension.html#a9a774bfe0a218428a1a5fdbff2041749", null ],
    [ "getSpriteSiz", "class_sprite_renderer_extension.html#a5b905a92e12af4d2560a1d4feff3f4ea", null ],
    [ "Init", "class_sprite_renderer_extension.html#a5adb0527e655403340a79d9ece93fcfa", null ],
    [ "MakeCopy", "class_sprite_renderer_extension.html#a1fc2c8c72dc027281537290630f7ad1c", null ],
    [ "Render", "class_sprite_renderer_extension.html#a9c00f07d71530a72257b70376ee214f3", null ],
    [ "SaveScene", "class_sprite_renderer_extension.html#a341a0b18a71d61d5f78a2e09693a1602", null ],
    [ "setColorMultiplier", "class_sprite_renderer_extension.html#ad88b0842dd09fa3febc62b225ec51970", null ],
    [ "setPosSizeVector", "class_sprite_renderer_extension.html#a9e92da87b27c74d1a42654d7527eb49b", null ],
    [ "setSpritePos", "class_sprite_renderer_extension.html#a5044b212ae4d318991f459c8543fba35", null ],
    [ "setSpriteSize", "class_sprite_renderer_extension.html#ae194333c182dc1cebe79ee1e3ae70198", null ],
    [ "setTexture", "class_sprite_renderer_extension.html#a708abd515c8256ae2ec6bd9e9081e902", null ],
    [ "setTexture", "class_sprite_renderer_extension.html#acaeb1d5f7c041b6f1faba432bc031bb4", null ],
    [ "Update", "class_sprite_renderer_extension.html#aa9b17cef3917472a77025a3f71b732dc", null ]
];