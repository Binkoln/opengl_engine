var class_point_light_extension =
[
    [ "Cleanup", "class_point_light_extension.html#a247783c718d98ba6c0ade0c65dad2070", null ],
    [ "DrawImGui", "class_point_light_extension.html#a8876da7bf7148d5463dee35805f80d4f", null ],
    [ "Init", "class_point_light_extension.html#ac632093dc947bef7085e8af225d06cd1", null ],
    [ "MakeCopy", "class_point_light_extension.html#aa849713e3a3f821fa3229c62fd661d57", null ],
    [ "Render", "class_point_light_extension.html#aa094d7d25ecddea4ca4522c3f80e6334", null ],
    [ "SaveScene", "class_point_light_extension.html#acf798727119851401501b46f158d80b9", null ],
    [ "Update", "class_point_light_extension.html#ad1c10312dc884c7e8901819b719fdc76", null ]
];