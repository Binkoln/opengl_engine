var struct_mesh_vertex =
[
    [ "bitangent", "struct_mesh_vertex.html#ab208b3ff4e663bef0238a8700c130ec4", null ],
    [ "boneIDs", "struct_mesh_vertex.html#ab6611712cd020f389b8cb3b0706e8145", null ],
    [ "boneWeights", "struct_mesh_vertex.html#a33a7770b021e8fc5f63968413d0be172", null ],
    [ "normal", "struct_mesh_vertex.html#a9bd172f6367dd18d5a3537a0c1d4c72a", null ],
    [ "position", "struct_mesh_vertex.html#af424f4ac16e7aa6359862ee38fbaf309", null ],
    [ "tangent", "struct_mesh_vertex.html#a2c1a10de7273c817e279a5023a56c10d", null ],
    [ "UV", "struct_mesh_vertex.html#aa3a3229e8eaaf5e18501de25cd0c894a", null ]
];