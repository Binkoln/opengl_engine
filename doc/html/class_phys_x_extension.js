var class_phys_x_extension =
[
    [ "Cleanup", "class_phys_x_extension.html#a3a88de482e53d303b33795be06f23979", null ],
    [ "DrawImGui", "class_phys_x_extension.html#aa29cb4728057ca1f04ee03ee0ebb71b2", null ],
    [ "Init", "class_phys_x_extension.html#a8c8f36b0850c35f9accf20648edd6358", null ],
    [ "InitPhysX_Body", "class_phys_x_extension.html#a513f51c23f306cafdd7403e17b7f3787", null ],
    [ "MakeCopy", "class_phys_x_extension.html#acb73429f85f60cf013834200648878d8", null ],
    [ "Render", "class_phys_x_extension.html#ae1f518c7e9c0f67808e600d5ffebf580", null ],
    [ "SaveScene", "class_phys_x_extension.html#a83416a317a7848634e22d28b12ba0864", null ],
    [ "Update", "class_phys_x_extension.html#ab28a16d3ec0d76b52ab7b2b000a6aaf6", null ]
];