var class_extension =
[
    [ "Extension", "class_extension.html#a031885c19c0e73f5943862fb9842f2dd", null ],
    [ "Cleanup", "class_extension.html#ac0cbfaa5135a854f88538d2eb7a8bc34", null ],
    [ "DrawImGui", "class_extension.html#aeacc271dd7dd83a0f2707da96a3577fe", null ],
    [ "Init", "class_extension.html#a706d5671c811a169f89d438ff73271ec", null ],
    [ "LoadAdditionalData", "class_extension.html#a260e4e7239f633e167920fc966c4e1fb", null ],
    [ "MakeCopy", "class_extension.html#ae460a1a71db4c93f52b591da285b6b10", null ],
    [ "Render", "class_extension.html#a54b0038fd1965579a48fba2a5acbb661", null ],
    [ "SaveScene", "class_extension.html#a17fa0001a0f2684acc29cbfa4505d3f4", null ],
    [ "Update", "class_extension.html#a46ad7fb728830b979d67502861adaffe", null ]
];