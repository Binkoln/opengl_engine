var class_level =
[
    [ "DeleteLevel", "class_level.html#af749c2ebe095a697216d689b0d63eabf", null ],
    [ "deleteObject", "class_level.html#a5384d284b7e8f0cafb315b25df68c89d", null ],
    [ "EnablePlayMode", "class_level.html#ad2bd1e53d90a1a5a7bb9f7257df1a93f", null ],
    [ "findObjectInLevelByID", "class_level.html#a2dc3706ad5c64d2f6421d453ecb30f20", null ],
    [ "getActiveCamera", "class_level.html#a3bf47477b84bc5bfee9bc5624155270a", null ],
    [ "getRenderer2D", "class_level.html#a82c01e36d03b812b53028690c538b841", null ],
    [ "getSceneFilePath", "class_level.html#af8b34745e781100d6b53b80181d9364f", null ],
    [ "getSceneObject", "class_level.html#a04cb44826aea2fc0247e2e4d9bd5697f", null ],
    [ "isSceneCreated", "class_level.html#a0fcc9479e471ce84576e1f666fd71612", null ],
    [ "isThisLevelHasFileToSave", "class_level.html#ae6f3d49acafd729921535545e97f70ed", null ],
    [ "Load", "class_level.html#a0ed3c4744a3500f6a2fb22a04fc6793c", null ],
    [ "Render", "class_level.html#a3af06ef456180a68c9e9721161984206", null ],
    [ "Save", "class_level.html#aee444c1c724e2a4b35c9811d223cc79d", null ],
    [ "setActiveCamera", "class_level.html#a9602e988fa8a56929f6926b3d48c01e3", null ],
    [ "setRenderer2D", "class_level.html#a7ac18fc96ab3855443e9ad811dcbe86a", null ],
    [ "setupNewSceneObject", "class_level.html#a5afdafbc0ce72e2b92fea395672258c5", null ],
    [ "Update", "class_level.html#a348ad48f5fe0a4158751778735d88230", null ],
    [ "selectedObjectPointer", "class_level.html#a37f2fd2eb251a48cc2804720581090cc", null ]
];