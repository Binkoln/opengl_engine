var class_model_renderer_extension =
[
    [ "Cleanup", "class_model_renderer_extension.html#ae101f86b4c5de345fa7d1fa47f719dee", null ],
    [ "DrawImGui", "class_model_renderer_extension.html#af19226632e94ae18858fd4a73ca68882", null ],
    [ "getModel", "class_model_renderer_extension.html#a8deade24b84ab0ae2facc06c02673557", null ],
    [ "Init", "class_model_renderer_extension.html#a7bc70a6c3a77645ca5f36f48cfec92b2", null ],
    [ "LoadModel", "class_model_renderer_extension.html#a246104c1a81d0f40bc1ff46dba4beafb", null ],
    [ "MakeCopy", "class_model_renderer_extension.html#a05999f5d9372ca0a78f06873496db1ef", null ],
    [ "Render", "class_model_renderer_extension.html#a8c93a7d9792e2cf64ea462d602d1dcf0", null ],
    [ "SaveScene", "class_model_renderer_extension.html#a69cd317169a3a7034ccdd7732ec15378", null ],
    [ "Update", "class_model_renderer_extension.html#a0d3d201131a3583511ec78248cc77b92", null ]
];