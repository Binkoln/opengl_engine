Engine = {};

function Engine.getChildsObjects(hostObj)
	if hostObj == nil then 
		return nil 
	end
	ret = {_GetHostObjectsChilds(hostObj)}
	return ret
end

function Engine.getWorldPosition(hostObj)
	if hostObj == nil then 
		return nil 
	end


	px,py,pz = _GetWorldPosition(hostObj)
	pos = {}
	pos.x = px;
	pos.y = py;
	pos.z = pz;
	

	return pos
end

function Engine.getProfilerUpdateTime()
	return _GetProfilerUpdateTime()
end

function Engine.getTransform(hostObj)
	if hostObj == nil then 
		return nil 
	end

	ret = {}
	px,py,pz,rx,ry,rz,sx,sy,sz,for_x,for_y,for_z,rig_x,rig_y,rig_z = _GetTransform(hostObj)
	ret.pos = {}
	ret.pos.x = px;
	ret.pos.y = py;
	ret.pos.z = pz;
	
	ret.rot = {}
	ret.rot.x = rx;
	ret.rot.y = ry;
	ret.rot.z = rz;
	
	ret.siz = {}
	ret.siz.x = sx;
	ret.siz.y = sy;
	ret.siz.z = sz;

	ret.forward = {}
	ret.forward.x = for_x;
	ret.forward.y = for_y;
	ret.forward.z = for_z;

	ret.right = {}
	ret.right.x = rig_x;
	ret.right.y = rig_y;
	ret.right.z = rig_z;
	return ret
end

function Engine.setTransform(hostObj,t)
	if hostObj == nil then 
		return
	end
	if t == nil then 
		return 
	end
	
	_SetTransform(hostObj,t.pos.x,t.pos.y,t.pos.z,t.rot.x,t.rot.y,t.rot.z,t.siz.x,t.siz.y,t.siz.z)
end

function Engine.swapLevel(hostObj,levelPath)
	if hostObj == nil then 
		return
	end
	if levelPath == nil then 
		return 
	end
	
	_SwapLevel(hostObj,levelPath)
end

function Engine.Log(str)
	if str == nil then 
		return nil 
	end
	
	_LogToConsole(str);
end

function Engine.InstanceObject(parent,objectToCreate)
	if objectToCreate == nil then 
		return nil 
	end
	
	
	return _CreateNewInstanceOfObject(parent,objectToCreate)
end

function Engine.DeleteObject(object)
	if object == nil then 
		return nil 
	end
	
	_DeleteObject(object);
end

function Engine.setCameraFocusPoint(camera,object)
	if camera == nil or object == nil then 
		return nil 
	end
	_SetCameraFocusPoint(camera,object);
end

function Engine.GetObjectExtension(object,name)
	if object == nil then 
		return nil 
	end
	if name == nil then 
		return nil 
	end
	
	return _GetExtensionByName(object,name);
end

function Engine.getFirstObjectWithName(name)
	if name == nil then 
		return nil 
	end
	
	return _GetFirstHostWithName(name);
end

Extension = {};
Extension.SpriteRenderer = {};
function Extension.SpriteRenderer.SetPosition(extension,x,y)
	if extension == nil then 
		return nil 
	end
	if x == nil then 
		return nil 
	end
	if y == nil then 
		return nil 
	end
	
	_SetExtensionSpriteRenderer_SetPosition(extension,x,y);
end

function Extension.SpriteRenderer.SetSize(extension,w,h)
	
	if extension == nil then 
		return nil 
	end
	if w == nil then 
		return nil 
	end
	if h == nil then 
		return nil 
	end

	_SetExtensionSpriteRenderer_SetSize(extension,w,h);
end


Extension.PhysXCharacterController = {};
function Extension.PhysXCharacterController.Move(extension,x,y,z)
	if extension == nil then 
		return nil 
	end
	if x == nil then 
		return nil 
	end
	if y == nil then 
		return nil 
	end
	if z == nil then 
		return nil 
	end
	
	_ExtensionPhysXCharacterController_Move(extension,x,y,z);
end

function Extension.PhysXCharacterController.SetVelocity(extension,x,y,z)
	if extension == nil then 
		return nil 
	end
	if x == nil then 
		return nil 
	end
	if y == nil then 
		return nil 
	end
	if z == nil then 
		return nil 
	end
	
	_ExtensionPhysXCharacterController_SetVelocity(extension,x,y,z);
end

function Extension.PhysXCharacterController.getVelocity(extension)
	if extension == nil then 
		return nil 
	end

	vel = {}
	px,py,pz = _ExtensionPhysXCharacterController_GetVelocity(extension)

	vel.x = px;
	vel.y = py;
	vel.z = pz;

	return vel
end

function Extension.PhysXCharacterController.setOnGround(extension, value)
	if extension == nil then 
		return nil 
	end

	return _ExtensionPhysXCharacterController_SetOnGround(extension, value)
end

function Extension.PhysXCharacterController.getOnGround(extension)
	if extension == nil then 
		return nil 
	end

	return _ExtensionPhysXCharacterController_GetOnGround(extension)
end

Input = {};

function Input.getKeyPressed(keyname)
	return _GetKeyPressed(keyname);
end



--####    Other avalible functions   ####
--function Engine.Update(host)
--function Engine.OnTrigger(collisionHost)
