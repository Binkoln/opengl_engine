#pragma once
#include <string>
class SimpleMaterialEditor {
public:
	SimpleMaterialEditor();
	~SimpleMaterialEditor();

	void Init();
	void Cleanup();
	void onGui();

	bool isMaterialLoaded();
private:
	bool showWindow;
	std::string pathToOpenedMaterial;
};