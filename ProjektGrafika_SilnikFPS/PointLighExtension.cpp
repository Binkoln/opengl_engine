#include "Object.h"


PointLightExtension::PointLightExtension() :Extension("PointLightExtension")
{
	type = "PointLightExtension";
	lightColor = glm::vec3(1, 1, 1);
}

PointLightExtension::~PointLightExtension()
{
	Level::RemovePointLight(this);
}

tinyxml2::XMLElement* PointLightExtension::SaveScene(tinyxml2::XMLElement* e)
{
	using namespace tinyxml2;
	XMLElement* extension = Extension::SaveScene(e);
	extension->InsertNewChildElement("Color")->InsertNewText(StringTools::vec3ToString(lightColor).c_str());
	extension->InsertNewChildElement("CastShadows")->InsertNewText((castShadows)?"1.0":"0.0");
	extension->InsertNewChildElement("Intensity")->InsertNewText(StringTools::doubleToString(intensity).c_str());
	extension->InsertNewChildElement("Radius")->InsertNewText(StringTools::doubleToString(radius).c_str());


	return extension;
}


void PointLightExtension::Init()
{
	Level::AddPointLight(this);
}

void PointLightExtension::Cleanup()
{
	Level::RemovePointLight(this);
}

void PointLightExtension::Update(Object* o, Level* level)
{
	pos = o->getWorldPosition();
	
	Level::AddPointLight(this);
}

Extension* PointLightExtension::MakeCopy()
{
	PointLightExtension* ret = new PointLightExtension();
	ret->lightColor = lightColor;
	ret->castShadows = castShadows;
	ret->intensity = intensity;
	ret->radius = radius;
	return ret;
}




void PointLightExtension::Render(Object* o, Level* level)
{


}

void PointLightExtension::DrawImGui(Object* parent)
{

	if (ImGui::CollapsingHeader(name.c_str(), ImGuiTreeNodeFlags_DefaultOpen)) {


		Extension::DrawImGui(parent);


		float colorPicker[] = { lightColor.r,lightColor.g,lightColor.b };
		ImGui::ColorPicker3("Color", colorPicker, 0);
		lightColor.r = colorPicker[0];
		lightColor.g = colorPicker[1];
		lightColor.b = colorPicker[2];


		ImGui::DragFloat("Intensity", &intensity, 0.01f);
	}

	ImGui::Separator();
}