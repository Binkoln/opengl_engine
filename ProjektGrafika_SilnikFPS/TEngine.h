#ifndef TENGINE_H
#define TENGINE_H
#define TEGNGINE_STARTUP_OPTION_MODE_EDITOR 0
#define TEGNGINE_STARTUP_OPTION_MODE_GAME 1
#include <string>
#include <GL/glew.h>
#include <GL/GL.h>
#include <GLFW/glfw3.h>
#include <glm/vec3.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <math.h>
#include <assimp/Importer.hpp>
#include <string>
#include <sstream>
#include <iostream>
#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"
#include "ImGuizmo.h"
#include "InputManager.h"
#include "Object.h"
#include <portaudio.h>
#include <lua/lua.h>
#include "tinyxml2/tinyxml2.h"
#include <filesystem>
#include "EditorConsole.h"
#include "AssetsExplorerWindow.h"
#include "InspectorWindow.h"
#include "Model.h"
#include "Shader.h"
#include "DefferedRenderer.h"
#include "Texture.h"
#include "TextureManager.h"
#include "ModelManager.h"
#include "Renderer2D.h"
#include "SimpleMaterialEditor.h"

/// TEngineStartupOptions s�u�y do przechowywania informacji o uruchomieniu ca�ego programu do klasy singletonowej TEngine.
struct TEngineStartupOptions {
		int mode;
		std::string projectPath;
		int level_to_load;
};



///Klasa singleton uruchamiaj�ca ca�y silnik i gr� w zale�no�ci od informacji z TEngineStartupOptions.
class TEngine {
public:
	TEngine();
	~TEngine();
	/// S�u�y do inicjalizacji silnika gry oraz przekazania niezb�dnych parametr�w.
	///
	/// @param struct TEngineStartupOptions options - informacje o uruchamianym projekcie
	int Init(struct TEngineStartupOptions& options);

	/// Uruchamia silnik i g��wn� p�tle gry
	int Run();

	/// Zwalnia pami�� i zamyka silnik
	void Cleanup();

	/// Metoda renderuj�ca obiekty na ekran.
	///
	/// Dok�adnie to Odpowiedznie rozszerzenia (Extension) obiekt�r (Object) renderuj� si� na ekran.
	/// @see Extension Object
	void Render();

	/// Metoda aktualizuj�ca logik� gry/silnika
	void Update();

	///Metoda przetwarzaj�ca polecenia z wewn�trz silnikowej konsoli.
	static void ProcessCommand(std::string in);

	///Metoda zwrazaj�ca �cie�k� do aktualnie otwartego projektu.
	static std::string getProjectFolderPath();


	GLFWwindow* window;
	InputManager inputManager;
	Level* level;
	ModelManager modelManager;
	Renderer2D renderer2D;
	DefferedRenderer defferedRenderer;
	ForwardRenderer forwardRenderer;
	Profiler profiler;
	SimpleMaterialEditor simpleMaterialEditor;
	ProfilerTimerUnit mainLoopTimer;
	ProfilerTimerUnit mainRenderTimer;
private:

	void drawImGuiObjectTree(Object* o,int& iterator);
	void LevelWindowDrawCall(Object* o);
	
	EditorConsole editorConsoleWindow;
	AssetsExpolrerWindow assetsExpWin;
	InspectorWindow inspectorWindow;
	


	glm::vec3 getClickPosition();
	int getClickObjectID();
	//Rendr engine 
	Framebuffer* fb_deffered_components = nullptr;
	Framebuffer* fb_render_pass = nullptr;
	Framebuffer* fb_final_image = nullptr;
	Framebuffer* fb_final_before_tonemapped_image = nullptr;
	int selected_fb = 0;

	//Level

	
	//Object* sceneObject;
	Shader shad;
	Texture testTex;
	TextureManager textureManager;
	
	
};

#endif // !TENGINE_H

