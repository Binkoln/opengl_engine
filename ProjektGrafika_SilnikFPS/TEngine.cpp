#include "TEngine.h"

TEngine* c_engine;
namespace BLACKBOARD//todo: remove blackboard 
{
    bool hier;
    bool view_w = false;
    bool profiler = false;
    bool demo;
    bool proj3D = false;
    struct TEngineStartupOptions options;
    int bloom_id = BLOOM_DOWN_PASS_COUNT + BLOOM_UP_PASS_COUNT;
    bool motionblur_enabled = true;
    bool renderSceneAsMesh = false;
    bool applaySSAO = true;
    ImGuizmo::OPERATION GIZMO_OPERATION = ImGuizmo::OPERATION::TRANSLATE;
}


void LoadLevelFunctionCallback(std::string path) {
    c_engine->profiler.ResetData();
    c_engine->modelManager.deleteAllModels();
    c_engine->level->DeleteLevel();
    delete c_engine->level;
    c_engine->level = new Level(true);
    c_engine->level->setRenderer2D(&c_engine->renderer2D);
    c_engine->level->Load(path);

    std::cout << "Loadet Level\n";
    if (c_engine->level->mScene == NULL)
        std::cout << "And mScene in NULL right here\n";
    c_engine->level->Update();
}

void SaveLevelFunctionCallback(std::string path) {
    long curr_it_buff = c_engine->level->GetObjectID() - 1;
    Level* l = new Level(false);
    l->selectedObjectPointer = nullptr;

    l->Save(path + ".scene");
    delete l;
    std::cout << "Saved to:" << path << ".scene" << "\n";
    c_engine->level->setLevelObjectIDIterator(curr_it_buff);
}

TEngine::TEngine()
{
}

TEngine::~TEngine()
{

}

void TEngine::Render()
{
    mainRenderTimer.Reset();
    mainRenderTimer.StartTimer();
    level->defferedRenderer = &defferedRenderer;
    level->forwardRenderer = &forwardRenderer;
    level->finalFB = fb_final_image;
    Camera* c = level->getActiveCamera();

    if (c != nullptr)
            c->BakeCubemap();

    //=======================    Render pipeline    =========================

    //Light pass
    //1. get lights
    std::vector<DirectionalLightExtension*> lights = level->GetDirectionalLightsList();
    //2. loop for eatch light
    for (DirectionalLightExtension* dl : lights) {
        //1. prepare fbo
        if (!dl->UseDepthBuffer()) { continue; }
        glClear(GL_DEPTH_BUFFER_BIT);
        //2. Set light for whole level to render depth map
        level->tmp_dri_light = dl;
        //3. render scene with special shader
        level->lightPass = true;
        level->Render();
        level->lightPass = false;
        //4. end fbo
        dl->DontUseDepthBuffer();
    }
    
    level->tmp_dri_light = nullptr;

    if(BLACKBOARD::renderSceneAsMesh)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    //Standard pass
    level->defferedPass = true;
    fb_deffered_components->Bind(); 
    glClearColor(0,0,0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    level->Render();

    fb_deffered_components->Unbind();
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    //SSAO (no Forward rendered content included xd)
    if(BLACKBOARD::applaySSAO)
        defferedRenderer.RenderSSAO(fb_deffered_components, c);

    //End SSAO
    
    //Deffered rendering to final image

    defferedRenderer.RenderImage(fb_deffered_components, level->getActiveCamera(), fb_render_pass,defferedRenderer.getSSAOFramebuffer(),BLACKBOARD::applaySSAO);


    
    if (BLACKBOARD::renderSceneAsMesh)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    //forward renderer
    level->defferedPass = false;
    fb_render_pass->Bind();
    level->Render();
    forwardRenderer.SortQueuedModelsAndRender();
    


    //Render skybox
    level->cameraSkyboxPass = true;
    if (c != nullptr)
        c->Render(level);
    level->cameraSkyboxPass = false;
    fb_render_pass->Unbind();


    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    //Motion blour
    //defferedRenderer.RenderMotionBlur(&fb_render_pass,fb_deffered_components->textureVelocityID);
    
    float bloom_treshold =(c != nullptr) ? c->getBloomTresholdValue() : 0.85f;
    //Post processes
    if(BLACKBOARD::motionblur_enabled)
        defferedRenderer.RenderBloom(fb_render_pass, bloom_treshold);
    
    defferedRenderer.ApplayBloom(fb_render_pass, fb_final_before_tonemapped_image);

    glViewport(0, 0, inputManager.getWindowWidth(), inputManager.getWindowHeight());
    //Show buffer on scren
    defferedRenderer.ApplayTonemapping(fb_final_before_tonemapped_image, fb_final_image, BLACKBOARD::view_w);
    if(level->getActiveCamera() != nullptr)
    level->getActiveCamera()->prevCameraViewMatrix = level->getActiveCamera()->getViewMatrix();
    
    mainRenderTimer.StopTimer();
}

glm::vec3 TEngine::getClickPosition()
{
    fb_deffered_components->Bind();
    glReadBuffer(GL_COLOR_ATTACHMENT0);
    float costam[4];
    glm::vec2 mousePos = InputManager::getMousePos();
    double proc_x = mousePos.x / (double)InputManager::getWindowWidth();
    glReadPixels((int)mousePos.x, InputManager::getWindowHeight() - (int)mousePos.y, 1, 1, GL_RGBA, GL_FLOAT, &costam);

    glm::vec3 newPos = glm::vec3(costam[0], costam[1], costam[2]);
    fb_deffered_components->Unbind();
    //std::cout << "X:" << mousePos.x << "Y:" << mousePos.y   << "      A:" << costam[0] << " B:" << costam[1] << " C:" << costam[2] << " D:" << costam[3] << "\n";
    return newPos;
}

int TEngine::getClickObjectID()
{
    fb_deffered_components->Bind();
    glReadBuffer(GL_COLOR_ATTACHMENT3);
    float costam[4];
    glm::vec2 mousePos = InputManager::getMousePos();
    glReadPixels((int)mousePos.x, InputManager::getWindowHeight() - (int)mousePos.y, 1, 1, GL_RGBA, GL_FLOAT, &costam);

    glm::vec3 newPos = glm::vec3(costam[0], costam[1], costam[2]);
    fb_deffered_components->Unbind();
    int unit = 1024;
    int ret_id = 0;
    // Object id
    ret_id += costam[0]* unit;
    ret_id += costam[1] * unit * unit;
    //mesh id
    int meshid = costam[2] * 1024;

    std::cout << "Selected mesh id = " << meshid << "\n";
    std::cout << "FB Width = " << fb_deffered_components->getWidth() << "\n";
    return ret_id;
}

void TEngine::Update()
{

    if (level->swap_level_path != "") {
        //std::cout << level->swap_level_path << "\n";
        LoadLevelFunctionCallback(level->swap_level_path);
        level->EnablePlayMode();
    }
    level->Update();


    if (InputManager::getKeyOnce(GLFW_KEY_Y)) {
        BLACKBOARD::bloom_id--;
    }
    else if (InputManager::getKeyOnce(GLFW_KEY_U)) {
        BLACKBOARD::bloom_id++;
    }

    if (BLACKBOARD::bloom_id >= 16)BLACKBOARD::bloom_id = 15;
    else if (BLACKBOARD::bloom_id < 0)BLACKBOARD::bloom_id = 0;


    if (InputManager::getKeyOnce(GLFW_KEY_P) && !ImGui::IsWindowFocused(ImGuiFocusedFlags_AnyWindow)) {
        glm::vec3 newPos = getClickPosition();
        if (level->selectedObjectPointer != nullptr && glm::length(newPos) > 0) {
            level->selectedObjectPointer->setWorldPosition(newPos);
        }
    }

    if (InputManager::getKeyOnce(GLFW_KEY_D)) {
        if (InputManager::getKey(GLFW_KEY_LEFT_CONTROL) && InputManager::getKey(GLFW_KEY_LEFT_SHIFT)) {
            glm::vec3 newPos = getClickPosition();
            if (level->selectedObjectPointer != nullptr && glm::length(newPos) > 0) {//duplication and relocation
                Object* newO = level->selectedObjectPointer->MakeCopy();
                level->selectedObjectPointer->parent->put(newO);
                newO->setWorldPosition(newPos);
                level->selectedObjectPointer = newO;
            }
        }
        else if (InputManager::getKey(GLFW_KEY_LEFT_CONTROL)) {//just duplication
            if (level->selectedObjectPointer != nullptr) {
                Object* newO = level->selectedObjectPointer->MakeCopy();
                level->selectedObjectPointer->parent->put(newO);
                level->selectedObjectPointer = newO;
            }
        }
    }

    if (InputManager::getKeyOnce(GLFW_KEY_DELETE) && level->selectedObjectPointer != nullptr) {
        level->deleteObject(level->selectedObjectPointer);
    }

    if (InputManager::getKeyOnce(GLFW_KEY_F) && !ImGui::IsWindowFocused(ImGuiFocusedFlags_AnyWindow)) {
        int idd = getClickObjectID();
        Object* o = level->findObjectInLevelByID(idd);
        std::cout << "Object ID:" << idd << "\n";
        if (o != nullptr) {
            level->selectedObjectPointer = o;
        }
    }


    //GUIZMO
    if (InputManager::getKey(GLFW_KEY_Q)) BLACKBOARD::GIZMO_OPERATION = ImGuizmo::OPERATION::TRANSLATE;
    if (InputManager::getKey(GLFW_KEY_W)) BLACKBOARD::GIZMO_OPERATION = ImGuizmo::OPERATION::ROTATE;
    if (InputManager::getKey(GLFW_KEY_E)) BLACKBOARD::GIZMO_OPERATION = ImGuizmo::OPERATION::SCALE;
    

    if (level->isPlatModeEnabled() && InputManager::getKeyOnce(GLFW_KEY_ESCAPE)) {
        level->DisablePlayMode();
    }

}

std::string TEngine::getProjectFolderPath() {
    return BLACKBOARD::options.projectPath;
}


void TEngine::ProcessCommand(std::string in)
{
    if (in == "ol" || in == "lo")
        BLACKBOARD::hier = !BLACKBOARD::hier;

    if (in == "ae")
        c_engine->assetsExpWin.show();

    if (in == "pr" || in == "profiler" || in == "p")
        c_engine->profiler.ShowWindow();

    if(in == "demo")
        BLACKBOARD::demo = !BLACKBOARD::demo;

    if (in == "ins")
        c_engine->inspectorWindow.show();

    if (in == "save") {
        c_engine->level->Save(c_engine->level->getSceneFilePath());
    }

    //if (in == "load")
      //  c_engine->level->Load("testScene2d");
    
    if (in == "col" || in == "show colisions") {
        c_engine->level->showCollisions = !c_engine->level->showCollisions;
    }

    if (in == "ds") {
        c_engine->level->DeleteLevel();
    }

    if (in == "help" || in == "HELP" || in == "?")
        c_engine->editorConsoleWindow.showHelp();

    if (in == "play") {
        if (c_engine->editorConsoleWindow.isConsoleOpen())c_engine->editorConsoleWindow.show();
        c_engine->level->EnablePlayMode();
    }
    

    if (in == "fb_final") {
        c_engine->selected_fb = 0;
        std::cout << "Displayed framebuffer changet to final.\n";
    }

    if (in == "fb_albedo") {
        c_engine->selected_fb = 1;
        std::cout << "Displayed framebuffer changet to albedo.\n";
    }

    if (in == "fb_normal") {
        c_engine->selected_fb = 2;
        std::cout << "Displayed framebuffer changet to normal.\n";
    }
    if (in == "fb_position") {
        c_engine->selected_fb = 3;
        std::cout << "Displayed framebuffer changet to position.\n";
    }
    if (in == "fbb") {
        c_engine->selected_fb = 4;
        std::cout << "Displayed framebuffer changet to bloom preview.\n";
    }
    if (in == "fb_render") {
        c_engine->selected_fb = 5;
        std::cout << "Displayed framebuffer changet to render pass (without post processing).\n";
    }
    if (in == "fb_id") {
        c_engine->selected_fb = 6;
        std::cout << "Displayed framebuffer changet to objects ids\n";
    }
    if (in == "fb_depth") {
        c_engine->selected_fb = 7;
        std::cout << "Displayed framebuffer changet to depth\n";
    }
    if (in == "fb_velocity") {
        c_engine->selected_fb = 8;
        std::cout << "Displayed framebuffer changet to velocity\n";
    }
    if (in == "fb_ssao") {
        c_engine->selected_fb = 9;
        std::cout << "Displayed framebuffer changet to SSAO\n";
    }
    if (in == "fb_vp") {
        c_engine->selected_fb = 10;
        std::cout << "Displayed framebuffer changet to View Space Positions\n";
    }
    if (in == "fb_vn") {
        c_engine->selected_fb = 11;
        std::cout << "Displayed framebuffer changet to View Space Normals\n";
    }
    if (in == "fb_materials") {
        c_engine->selected_fb = 12;
        std::cout << "Displayed framebuffer changet to materias proporties.\n";
    }
    if (in == "ssao") {
        BLACKBOARD::applaySSAO = !BLACKBOARD::applaySSAO;
        std::cout << ((BLACKBOARD::applaySSAO)?"Enabled":"Disabled") << " SSAO \n";
    }
    if (in == "view") {
        BLACKBOARD::view_w = !BLACKBOARD::view_w;
    }
    if (in == "mesh") {
        BLACKBOARD::renderSceneAsMesh = !BLACKBOARD::renderSceneAsMesh;
    }

    if (in == "print max attachments count") {
        GLint maxAttach = 0;
        glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &maxAttach);
        std::cout << "MAX_ATTACHMENTS:" << maxAttach << "\n";
    }

    if (in == "reload") {
        c_engine->defferedRenderer.Destroy();
        c_engine->defferedRenderer.Init(c_engine->inputManager.getWindowWidth(), c_engine->inputManager.getWindowHeight(),c_engine->fb_deffered_components);
        c_engine->forwardRenderer.Destroy();
        c_engine->forwardRenderer.Init(c_engine->inputManager.getWindowWidth(), c_engine->inputManager.getWindowHeight());

        ModelRendererExtension::DeleteGlobalShader();
        ModelRendererExtension::LoadGlobalShader();
        TerrainExtension::DeleteGlobalShader();
        TerrainExtension::LoadGlobalShader();
        CubemapTexture::CleanupShader();
        CubemapTexture::InitShader();
    }

    //todo: repair this ghost effect
    if (in == "motionblur") {
        //BLACKBOARD::motionblur_enabled = !BLACKBOARD::motionblur_enabled;
        //std::cout << "Motionblur effect:" << ((BLACKBOARD::motionblur_enabled) ? "enabled" : "disabled") << "\n";
    }

    if (in == "emr") {
        Object* o = c_engine->level->selectedObjectPointer;
        if (o != nullptr) {
            for (Object* ch : o->childs) {
                for (Extension* ex : ch->extensions) {
                    if (ex->name == "Model Renderer") {
                        ex->enabled = !ex->enabled;
                    }
                }
            }
        }
    }
    
}

int TEngine::Init(struct TEngineStartupOptions& options) {
    BLACKBOARD::options = options;
    c_engine = this;
    BLACKBOARD::hier = true;
    BLACKBOARD::demo = false;


    if (!glfwInit())
        return -1;
    glewExperimental = GL_TRUE;

  

    //glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    const char* glsl_version = "#version 450";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    glfwSwapInterval(0); // Disable vsync
    //800, 580
    //1280, 720
    //1680, 920
    // 1920, 1080
    //PushTest
    window = glfwCreateWindow(1680, 920, "Silnik FPS", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);

    GLenum glewErr = glewInit();
    if (glewErr != GLEW_OK) {
        std::cout << "FATAL ERROR !!! GLEW initialization failed succesfully!\n" << glewGetErrorString(glewErr) << "\n";
        glfwTerminate();
        return -2;
    }
    glewExperimental = GL_TRUE;

    //glfwSwapInterval(1); // Enable vsync
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();

    ImGui::StyleColorsDark();

    inputManager.Init(window);
    double updateTimer = 0;

    ModelRendererExtension::LoadGlobalShader();
    CubemapTexture::InitShader();
    TerrainExtension::LoadGlobalShader();
    PhysXExtension::InitStaticData();
   
    forwardRenderer.Init(inputManager.getWindowWidth(), inputManager.getWindowHeight());
    renderer2D.Init();
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);
    bool showDemoWindow = true;
    bool showTimer = true;

    level = new Level(true);
    level->selectedObjectPointer = nullptr;
    level->setRenderer2D(&renderer2D);

    
    editorConsoleWindow.Init(ProcessCommand);
    assetsExpWin.Init(options.projectPath);
    assetsExpWin.BuildDB();
    assetsExpWin.setLoadSceneFunction(LoadLevelFunctionCallback);

    assetsExpWin.setSaveSceneFunction(SaveLevelFunctionCallback);
   

   
   shad = Shader(options.projectPath + "/assets/shaders/v_b.glsl", options.projectPath + "/assets/shaders/f_b.glsl");
   DirectionalLightExtension::InitShaders();
  
   testTex.Load(options.projectPath + "/assets/textures/stone_normal.jpg");
   SoundPlayer::Init();


       fb_deffered_components = new Framebuffer();
       fb_deffered_components->Init(inputManager.getWindowWidth(), inputManager.getWindowHeight(), FRAMEBUFFER_TYPE_DEFFERED_RENDERING_USAGE);
       fb_render_pass = new Framebuffer();
       fb_render_pass->Init(inputManager.getWindowWidth(), inputManager.getWindowHeight(),FRAMEBUFFER_TYPE_COLOR);
       fb_render_pass->SetDepthBufferSource(fb_deffered_components->GetDepthBufferSource());
       //fb_render_pass->SetVelocityBufferSource(fb_deffered_components->textureVelocityID);

       defferedRenderer.Init(inputManager.getWindowWidth(), inputManager.getWindowHeight(),fb_deffered_components);

       fb_final_image = new Framebuffer();
       fb_final_image->Init(inputManager.getWindowWidth(), inputManager.getWindowHeight(), FRAMEBUFFER_TYPE_COLOR_AND_DEPTH);
       fb_final_before_tonemapped_image = new Framebuffer();
       fb_final_before_tonemapped_image->Init(inputManager.getWindowWidth(), inputManager.getWindowHeight(), FRAMEBUFFER_TYPE_COLOR_AND_DEPTH);

       simpleMaterialEditor.Init();

       if (options.mode == TEGNGINE_STARTUP_OPTION_MODE_GAME) {
           level->EnablePlayMode();       
       }

    return 0;
  
}

int TEngine::Run() {

    double fpsCounterTimer = glfwGetTime();
    int fpsCounter = 0;
    int fps = 0;

    double ups = 60.0;
    double lastTime = 0;
    double frameTime = 1.0 / ups;
    glm::vec3 color(1, 0, 1);
    glViewport(0, 0, inputManager.getWindowWidth(), inputManager.getWindowHeight());
    while (!glfwWindowShouldClose(window))
    {
        mainLoopTimer.StartTimer();
        while (glfwGetTime() - lastTime >= frameTime) {
            glfwPollEvents();
            lastTime += frameTime;
            Update();
            inputManager.Update();
        }

        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
        ImGuizmo::BeginFrame();

        if (inputManager.getKeyOnce(GLFW_KEY_GRAVE_ACCENT))
            editorConsoleWindow.show();
        
        editorConsoleWindow.DrawImGui();
        if (editorConsoleWindow.isConsoleOpen()) {
            inspectorWindow.DrawImGui(level->selectedObjectPointer);
            assetsExpWin.DrawWindow();

            if (BLACKBOARD::demo)
                ImGui::ShowDemoWindow(&BLACKBOARD::demo);
            LevelWindowDrawCall(level->getSceneObject());

        }

        profiler.OnImGui(level->getSceneObject(), mainLoopTimer.GetTimeMesurement(),mainRenderTimer.GetTimeMesurement(), level->getPhysxProfilerTime());

        simpleMaterialEditor.onGui();
      
        //ImGui::ShowDemoWindow(&showDemoWindow);
        
        //Render
        if (BLACKBOARD::view_w) {
            ImGui::Begin("View", &BLACKBOARD::view_w);
            GLuint  textureID = fb_final_image->textureColorID;
            if (selected_fb == 1)textureID = fb_deffered_components->textureColorID;
            if (selected_fb == 2)textureID = fb_deffered_components->textureNormalID;
            if (selected_fb == 3)textureID = fb_deffered_components->texturePositionID;
            if (selected_fb == 4)textureID = defferedRenderer.getBloomBuffer(BLACKBOARD::bloom_id)->textureColorID;
            if (selected_fb == 5)textureID = fb_render_pass->textureColorID;
            if (selected_fb == 6)textureID = fb_deffered_components->textureObjectID;
            if (selected_fb == 7)textureID = fb_deffered_components->textureDepthID;
            if (selected_fb == 8)textureID = fb_deffered_components->textureVelocityID;
            if (selected_fb == 9)textureID = defferedRenderer.getSSAOFramebuffer()->textureColorID;
            if (selected_fb == 10)textureID = fb_deffered_components->textureViewSpacePositionID;
            if (selected_fb == 11)textureID = fb_deffered_components->textureViewSpaceNormalID;
            if (selected_fb == 12)textureID = fb_deffered_components->textureMaterialProportiesID;

            
            ImGui::Image((void*)textureID, ImGui::GetWindowSize(), { 0,1 }, { 1,0 });

            
            ImGui::End();

        }

        color = glm::vec3(0.12f, 0.12f, 0.12f);
        glClearColor(color.r, color.g, color.b, 1);
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);


        //Gizmo
        if (level != nullptr && level->getSceneObject() != nullptr && !level->isPlatModeEnabled()) {
            Object* o = level->selectedObjectPointer;
            Camera* camera = level->getActiveCamera();
            if (o != nullptr && camera != nullptr && o != level->getSceneObject()) {
                ImGuizmo::SetOrthographic(false);
                ImGuizmo::SetDrawlist(ImGui::GetBackgroundDrawList());
                float width = fb_deffered_components->getWidth();
                float height = fb_deffered_components->getHeight();

                ImGuizmo::SetRect(0, 0, width, height);
                glm::mat4 cameraProjection = camera->getProjection();
                glm::mat4 cameraView = (camera->getViewMatrix());
                glm::mat4 transform = (BLACKBOARD::GIZMO_OPERATION == ImGuizmo::OPERATION::SCALE)?o->getWorldSpaceTransformationMatrix():glm::translate(glm::mat4(1), o->getWorldPosition());
                

                ImGuizmo::Manipulate(glm::value_ptr(cameraView), glm::value_ptr(cameraProjection),
                    BLACKBOARD::GIZMO_OPERATION, ImGuizmo::LOCAL, glm::value_ptr(transform));

                glm::vec3 position;
                glm::vec3 rotation;
                glm::vec3 size;
                ImGuizmo::DecomposeMatrixToComponents(glm::value_ptr(transform), glm::value_ptr(position), glm::value_ptr(rotation), glm::value_ptr(size));

                
                if (ImGuizmo::IsUsing()) {
                    if (BLACKBOARD::GIZMO_OPERATION == ImGuizmo::OPERATION::SCALE) o->scale = size;
                    if (BLACKBOARD::GIZMO_OPERATION == ImGuizmo::OPERATION::TRANSLATE)o->setWorldPosition(position);
                    else if (BLACKBOARD::GIZMO_OPERATION == ImGuizmo::OPERATION::ROTATE) {
                        if (rotation.x > rotation.y && rotation.x > rotation.z || rotation.x < rotation.y && rotation.x < rotation.z)o->rotation.x += rotation.x;
                        if (rotation.y > rotation.x && rotation.y > rotation.z || rotation.y < rotation.x && rotation.y < rotation.z)o->rotation.y += rotation.y;
                        if (rotation.z > rotation.y && rotation.z > rotation.x || rotation.z < rotation.y && rotation.z < rotation.x)o->rotation.z += rotation.z;
                    }
                }
            }
        }
        
        
        
        Render();
        fpsCounter++;
        if (glfwGetTime() - fpsCounterTimer >= 1.0) {
            fpsCounterTimer = glfwGetTime();
            fps = fpsCounter;
            InputManager::setCurrentFPS(fps);
            fpsCounter = 0;
            glfwSetWindowTitle(window, (std::string("FPS:") + StringTools::intToString(fps)).c_str() );
        }
        
       
        
        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

      


        glfwSwapBuffers(window);

        InputManager::setMainLoopTimeProfiler(mainLoopTimer.GetTimeMesurement());
        profiler.Update(level->getSceneObject(),mainLoopTimer.GetTimeMesurement());
        mainLoopTimer.StopTimer();
    }
    return 0;
}

void TEngine::Cleanup()
{
    level->DeleteLevel();
    delete level;
    shad.Delete();
    ModelRendererExtension::DeleteGlobalShader();
    DirectionalLightExtension::CleanupShaders();
    CubemapTexture::CleanupShader();
    PhysXExtension::CleanupStaticData();

    renderer2D.Destroy();
    defferedRenderer.Destroy();
    forwardRenderer.Destroy();
    SoundPlayer::Clean();
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    if (fb_deffered_components != nullptr) {
        delete fb_deffered_components;
        fb_deffered_components = nullptr;
    }

    if (fb_render_pass != nullptr) {
        delete fb_render_pass;
        fb_render_pass = nullptr;
    }
    if (fb_final_image != nullptr) {
        delete fb_final_image;
        fb_final_image = nullptr;
    }
    if (fb_final_before_tonemapped_image != nullptr) {
        delete fb_final_before_tonemapped_image;
        fb_final_before_tonemapped_image = nullptr;
    }
    
    simpleMaterialEditor.Cleanup();

    glfwTerminate();
}

void TEngine::drawImGuiObjectTree(Object* o,int& iterator) {
    ImGuiIO& io = ImGui::GetIO();
    ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_OpenOnArrow;// | ImGuiTreeNodeFlags_DefaultOpen;
    if (!o->hasChilds())
        flags |= ImGuiTreeNodeFlags_Leaf;

    
    if (level->selectedObjectPointer == o)
        flags |= ImGuiTreeNodeFlags_Selected;
    ImGui::PushID(iterator++);
    bool treeNode = ImGui::TreeNodeEx(o->name.c_str(), flags);
   
    if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(0)) {
        level->selectedObjectPointer = o;
    }
  
    if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_None)) {

        ImGui::SetDragDropPayload("OBJECT", &o, sizeof(Object*), ImGuiCond_Once);
        std::string name_construction = "OBJECT:" + o->name;
        ImGui::Text(name_construction.c_str());

        ImGui::EndDragDropSource();
    }

    if (ImGui::BeginDragDropTarget()) {
        const ImGuiPayload* pyload = ImGui::AcceptDragDropPayload("OBJECT", ImGuiDragDropFlags_None);
        if (pyload != NULL) {

            Object* recived = *((Object**)(pyload->Data));
            if (!o->isChildOf(recived)) {
                recived->unchild();
                o->put(recived);
            }
        }

        ImGui::EndDragDropTarget();
    }
    
    
    if (treeNode) {

        for (Object* out : o->childs)
            drawImGuiObjectTree(out,iterator);


        ImGui::TreePop();
        

    }
    ImGui::PopID();
}


void TEngine::LevelWindowDrawCall(Object* o) {
    int idIterator = 0;
    //+ level->getSceneFilePath()).c_str()
    if (BLACKBOARD::hier) {
        ImGui::Begin("Level" , &BLACKBOARD::hier);
        ImGui::SetWindowSize(ImVec2(250, 400), ImGuiCond_Appearing);
        if (level->getSceneFilePath() == "") { ImGui::End(); return; }
        if(level->isSceneCreated())
            drawImGuiObjectTree(o,idIterator);
        
        if (ImGui::BeginPopupContextWindow("Context", ImGuiPopupFlags_MouseButtonRight)) {
            if (!level->isSceneCreated()) {//No scene
                if (ImGui::Button("Add Scene 2D"))
                {
                     level->setupNewSceneObject(new Object("Scene 2D", level->GetObjectID()));
                    ImGui::CloseCurrentPopup();
                }
                if (ImGui::Button("Add Scene 3D"))
                {
                    level->setupNewSceneObject(new Object("Scene 3D", level->GetObjectID()));
                    ImGui::CloseCurrentPopup();
                }
            }
            else {//Adding objects
                if (ImGui::Button("AddObject"))
                {
                    Object* target = level->getSceneObject();
                    if (level->selectedObjectPointer != nullptr)
                        target = level->selectedObjectPointer;

                    Object* newObject = new Object("Object",target,level->GetObjectID());
                    

                    ImGui::CloseCurrentPopup();
                }
                if (ImGui::Button("AddCamera"))
                {
                    Object* target = level->getSceneObject();
                    if (level->selectedObjectPointer != nullptr)
                        target = level->selectedObjectPointer;

                    Camera* newCamera = new Camera(level->GetObjectID());
                    target->put(newCamera);
                    level->setActiveCamera(newCamera);

                    ImGui::CloseCurrentPopup();
                }


                if (ImGui::Button("AddDirectionalLight"))
                {
                    Object* target = level->getSceneObject();
                    if (level->selectedObjectPointer != nullptr)
                        target = level->selectedObjectPointer;

                    Object* newObject = new Object("DirectionalLight", target, level->GetObjectID());
                    DirectionalLightExtension* dle = new DirectionalLightExtension();
                    newObject->addExtension(dle);
                    ImGui::CloseCurrentPopup();
                }

                if (ImGui::Button("AddModel"))
                {
                    Object* target = level->getSceneObject();
                    if (level->selectedObjectPointer != nullptr)
                        target = level->selectedObjectPointer;

                    Object* newObject = new Object("Model", target, level->GetObjectID());
                    ModelRendererExtension* mre = new ModelRendererExtension();
                    newObject->addExtension(mre);
                    ImGui::CloseCurrentPopup();
                }

                if (level->selectedObjectPointer != nullptr && !level->selectedObjectPointer->isScene()) {
                    if (ImGui::Button("Delete"))
                    {
                        level->deleteObject(level->selectedObjectPointer);
                        level->selectedObjectPointer = nullptr;
                        ImGui::CloseCurrentPopup();
                    }
                }
            }
            ImGui::EndPopup();
        }

        ImGui::End();
    }
}


