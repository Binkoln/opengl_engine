#ifndef INSPECTOR_WINDOW_H
#define INSPECTOR_WINDOW_H

#include <GL/glew.h>
#include "Object.h"
#include "imgui/imgui.h"
#include "ModelRendererExtension.h"

/// Klasa okienka inspektora, służącego do wyświetlania oraz edycji parametrów obiektów w silniku
class InspectorWindow {
public:
	InspectorWindow();
	~InspectorWindow();



	inline void show() {
		showWindow = !showWindow;
	}

	inline void show(bool val) {
		showWindow = val;
	}

	/// Metoda renderuje okienko przy pomocy biblioteki ImGui oraz aktualizuje parametru obiektu na te ustawione w inspektorze
	/// @param Object* selectedObject - wskaźnik na obiekt do edycji i inspecji
	void DrawImGui(Object* selectedObject);


private:
	char InputBuffer[256];
	bool showWindow;
};

#endif // !INSPECTOR_WINDOW_H
