#include "Object.h"

namespace Terrain_Blackboard {
    Shader shad;
    Shader forward_shader;
    Shader directionalLightDepthRenderShader;
}

TerrainExtension::TerrainExtension():Extension("TerrainExtension") {
	toDelete = false;
	type = "TerrainExtension";
    heightmap_path = "";
    texture = nullptr;
}
TerrainExtension::~TerrainExtension() {
    if (texture != nullptr)
        delete texture;

    if (model != nullptr)
        delete model;
}


tinyxml2::XMLElement* TerrainExtension::SaveScene(tinyxml2::XMLElement* e) {
    using namespace tinyxml2;
    XMLElement* extension = Extension::SaveScene(e);
    if(heightmap_path != "")
        extension->InsertNewChildElement("heightmap_path")->InsertNewText(heightmap_path.c_str());
    /*extension->InsertNewChildElement("IsModelSet")->InsertNewText((modelRecord == nullptr) ? "0.0" : "1.0");
    extension->InsertNewChildElement("ModelPath")->InsertNewText(modelPath.c_str());
    extension->InsertNewChildElement("Transparent")->InsertNewText((transparent) ? "1.0" : "0.0");
    */
    return extension;
}


void TerrainExtension::Init() {}
void TerrainExtension::Cleanup() {}
void TerrainExtension::Update(Object* o, Level* level) {}
void TerrainExtension::Render(Object* o, Level* level) 
{

    if (!(level->defferedPass || level->lightPass))return;

    Camera* camera = level->getActiveCamera();
    if (camera == nullptr)return;
    if (model == nullptr)return;



    //depends on material 
    glEnable(GL_CULL_FACE);


    //calc objectIdColor
    int id = o->getID();
    float r = id % 1024;
    float g = (id / 1024) % 1024;
    //float b = (id / (255 * 255)); now its for mesh id
    glm::vec3 objectIdColor = { r / 1024.0f,g / 1024.0f,0 };
   

    Model* model = getModel();

    glm::mat4 worldSpaceMatrix = o->getWorldSpaceTransformationMatrix();

  

    if (level->lightPass && level->tmp_dri_light != nullptr) {
        glm::mat4 testProj = camera->getProjection() * camera->getViewMatrix();
        //level->forwardRenderer->RenderModelToDepthMap(&Terrain_Blackboard::directionalLightDepthRenderShader, model, worldSpaceMatrix, level->tmp_dri_light, testProj);
    }
    else if(level->defferedPass){
        glDisable(GL_BLEND);
        glEnable(GL_DEPTH_TEST);
        //Deffered renderer
       
        prev_worldSpaceMatrix = o->getWorldSpaceTransformationMatrix();
        level->defferedRenderer->RenderModel(&Terrain_Blackboard::shad, model, worldSpaceMatrix, prev_worldSpaceMatrix, camera, objectIdColor, 1.0f);
    }
}


Extension* TerrainExtension::MakeCopy() {
    TerrainExtension* te = new TerrainExtension();
    te->GenerateTerrain(heightmap_path);
    //fill with other things
    return te;
}


void TerrainExtension::GenerateTerrain(std::string heightmap_texture)
{
    if (heightmap_texture == "")return;
    heightmap_path = heightmap_texture;
    //Load texture
    texture = new Texture();
    texture->BufferedLoad(heightmap_path);



    if (!texture->isLoaded()) {
        std::cout << "No heightmap texture file :" << heightmap_texture << "\n";
        delete texture;
        texture = nullptr;
        return;
    }//No texture file found handling

    
    //Generate terrain
    model = new Model();

   // Material* mat = new Material();
    //mat->AddTexture("EngineProjectFolder/terrain/textures/coast_sand_2k/coast_sand_02_diff_2k.png", aiTextureType_DIFFUSE);
   // mat->
   // model->materials.push_back(mat);
    
    
    Mesh* mesh = new Mesh("Terrain mesh");
    mesh->material.AddShadingVector3Value("u_albedoColor", { 0.3,0.5,1.0f });
    mesh->material.AddTexture("EngineProjectFolder/terrain/mask.png", "maskk",3);
    mesh->material.AddTexture("EngineProjectFolder/terrain/textures/coast_sand_2k/coast_sand_02_diff_2k.png", aiTextureType_DIFFUSE);
    mesh->material.AddTexture("EngineProjectFolder/terrain/textures/coast_sand_2k/coast_sand_02_nor_gl_2k.png", aiTextureType_NORMALS);
   // mesh->material.AddTexture("EngineProjectFolder/terrain/textures/coast_sand_2k/coast_sand_02_rough_2k.png", aiTextureType_SPECULAR);
    
    
    mesh->material.AddTexture("EngineProjectFolder/terrain/textures/rocks_ground_2k/rocks_ground_06_diff_2k.png", "tex2_diff", 6);
    mesh->material.AddTexture("EngineProjectFolder/terrain/textures/coast_sand_rocks_2k/coast_sand_rocks_02_diff_2k.png", "tex3_diff", 5);


    mesh->material.AddTexture("EngineProjectFolder/terrain/textures/rocks_ground_2k/rocks_ground_06_nor_gl_2k.png", "tex2_norm", 7);
    mesh->material.AddTexture("EngineProjectFolder/terrain/textures/coast_sand_rocks_2k/coast_sand_rocks_02_nor_gl_2k.png", "tex3_norm", 8);

    mesh->material.AddShadingVector3Value("terrainSize", { (float)texture->GetWidth(), (float)texture->GetHeight() ,0.0f });

    std::vector<struct MeshVertex> vertices;
    std::vector<unsigned int> indi;
    for (int z = 0; z < texture->GetHeight(); z++)
        for (int x = 0; x < texture->GetWidth(); x++) {
            glm::vec4 col = texture->GetPixel(x, z);
            
            glm::vec4 col_x =  (x == texture->GetWidth() - 1) ? glm::vec4(0, 0, 0, 0) : texture->GetPixel(x + 1, z);
            glm::vec4 col_z =  (z == texture->GetHeight() - 1) ? glm::vec4(0, 0, 0, 0) : texture->GetPixel(x, z + 1);
            glm::vec4 col_zx =  (x == texture->GetWidth() - 1 || z == texture->GetHeight() - 1) ? glm::vec4(0, 0, 0, 0) : texture->GetPixel(x + 1, z + 1);
            struct MeshVertex v1;
            struct MeshVertex v2;
            struct MeshVertex v3;
            struct MeshVertex v4;
            float height = 100.0f;
            float y = col.r* height;
            float y_x = col_x.r* height;
            float y_z = col_z.r* height;
            float y_xz = col_zx.r* height;


            v1.position = { x,y,z };
            v2.position = { x + 1,y_x,z };
            v3.position = { x + 1,y_xz,z + 1 };
            v4.position = { x,y_z,z + 1 };

            
            //1,3,2
            glm::vec3 tan1 = glm::normalize(v3.position - v1.position);
            glm::vec3 bitan1 = glm::normalize(v2.position - v1.position);
            glm::vec3 nrm1 = glm::cross(tan1,bitan1);
            //3,1,4
            glm::vec3 tan2 = glm::normalize(v3.position - v1.position);
            glm::vec3 bitan2 = glm::normalize(v3.position - v4.position);
            glm::vec3 nrm2 = glm::cross(tan2, bitan2);


            v1.normal = nrm1;
            v1.tangent = tan1;
            v1.bitangent = bitan1;
            v1.UV = { 0,0 };
            v1.boneIDs = { 0,0,0 };
            v1.boneWeights = { 0,0,0 };

            v2.normal = nrm1;
            v2.tangent = tan1;
            v2.bitangent = bitan1;
            v2.UV = { 1,0 };
            v2.boneIDs = { 0,0,0 };
            v2.boneWeights = { 0,0,0 };

            v3.normal = nrm1;
            v3.tangent = tan1;
            v3.bitangent = bitan1;
            v3.UV = { 1,1 };
            v3.boneIDs = { 0,0,0 };
            v3.boneWeights = { 0,0,0 };

            v4.normal = nrm2;
            v4.tangent = tan2;
            v4.bitangent = bitan2;
            v4.UV = { 0,1 };
            v4.boneIDs = { 0,0,0 };
            v4.boneWeights = { 0,0,0 };
            int siz = vertices.size();
            vertices.push_back(v1);
            vertices.push_back(v2);
            vertices.push_back(v3);
            vertices.push_back(v4);



            indi.push_back(siz + 0);
            indi.push_back(siz + 2);
            indi.push_back(siz + 1);
            indi.push_back(siz + 2);
            indi.push_back(siz + 0);
            indi.push_back(siz + 3);
        }



    mesh->LoadMesh(vertices, indi, 0);

    model->meshes.push_back(mesh);
    std::cout << "Terejn generajted\n";


}

void TerrainExtension::DrawImGui(Object* parent) {
    if (ImGui::CollapsingHeader(name.c_str(), ImGuiTreeNodeFlags_DefaultOpen)) {
        Extension::DrawImGui(parent);

        ImGui::TextWrapped(("Heightmap: " + heightmap_path).c_str(), model == nullptr, 0, ImVec2(300, 30));


        if (ImGui::BeginDragDropTarget()) {
            const ImGuiPayload* pyload = ImGui::AcceptDragDropPayload("ASSET", ImGuiDragDropFlags_None);
            if (pyload != NULL) {

                struct AssetData* recived = *((struct AssetData**)(pyload->Data));
                if (recived->type == ASSET_TYPE_TEXTURE) {
                    std::cout << "Recived texture:" << recived->path << "\nDirectory:" << recived->directory << "\nFilename:" << recived->filename << "\n";
                    GenerateTerrain(recived->path);
                }
            }

            ImGui::EndDragDropTarget();
        }



    }
}



 void TerrainExtension::LoadGlobalShader() 
 {
     Terrain_Blackboard::shad = Shader("EngineProjectFolder/assets/shaders/defferedTerrain_v_b_s1.glsl", "EngineProjectFolder/assets/shaders/defferedTerrain_f_b_s1.glsl");
     Terrain_Blackboard::forward_shader = Shader("EngineProjectFolder/assets/shaders/forward_v_b_s2.glsl", "EngineProjectFolder/assets/shaders/forward_f_b_s2.glsl");
     Terrain_Blackboard::directionalLightDepthRenderShader = Shader("EngineProjectFolder/assets/shaders/directional_light_depth_v_b.glsl", "EngineProjectFolder/assets/shaders/directional_light_depth_f_b.glsl");

 }
 void TerrainExtension::DeleteGlobalShader() 
 {
     Terrain_Blackboard::shad.Delete();
     Terrain_Blackboard::forward_shader.Delete();
     Terrain_Blackboard::directionalLightDepthRenderShader.Delete();
 }