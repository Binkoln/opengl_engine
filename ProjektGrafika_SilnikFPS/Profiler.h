#pragma once
#include "imgui/imgui.h"
#include <vector>
#include "ProfilerTimerUnit.h"


class Profiler {
public:
	Profiler();
	~Profiler();

	void Update(class Object* scene, double mainLoopTime);
	void OnImGui(Object* sceneObject, double mainLoopTime, double mainRenderTimer, double physXTimer);
	void ResetData();

	inline void ShowWindow() { window_opened = !window_opened; }

private:
	void drawImGuiObjectTree(Object* o, int& iterator);
	bool window_opened = false;

	double sec_timer = 0;

	bool ImGuiDetailOfObject(Object* object, int i);

	std::vector<float> renderTimes, updateTimes, frameTimes, mainLoopTimes;
	std::vector<Object*> details;
};