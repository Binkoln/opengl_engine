
tmr = 0
tmr_speed = math.random()*2
rotZ = math.random()
rotX = math.random()*3
rotY = math.random()
function Engine.Update(host)
    t = Engine.getTransform(host)

    tmr = tmr+tmr_speed

    speed = 0.23
    t.pos.y = t.pos.y - speed

    t.rot.y = tmr*rotY
    t.rot.z = tmr*rotZ

    if(t.pos.y < -13) then
        t.pos.y = 10
        t.pos.x = (math.random() -0.5) * 20.0
    end

    Engine.setTransform(host,t)
end