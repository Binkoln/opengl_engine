tmr = 0
function Engine.Update(host)
    ts = Engine.getChildsObjects(host)

    for i = 1,ts[1],1 do
        chHost = ts[i+1]
        t = Engine.getTransform(chHost)

        tmr = tmr+0.005
    
        speed = 0.13
        t.pos.y = t.pos.y - speed
        t.rot.y = tmr*0.02*i
        t.rot.z = tmr*0.4
    
        if(t.pos.y < -18) then
            t.pos.y = 45 + (math.random() * 20)
            t.pos.x = (math.random() -0.5) * 20.0
        end
    
        Engine.setTransform(chHost,t)
    end

    --print("Enemies count:",t[1])
end