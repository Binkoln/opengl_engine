tmr = 0
my_speed = 0.1
function anim(host)

    ext = Engine.GetObjectExtension(host,"SpriteRendererExtension")

    if(ext == nil) then
        print("No extension")
        return 
        end


        przesuniecie = 0
        if(tmr > 2) then
            przesuniecie = 0.5
        end
        if(tmr > 4) then
            tmr = 0
        end
        Extension.SpriteRenderer.SetPosition(ext,przesuniecie,0)
        Extension.SpriteRenderer.SetSize(ext,0.5,1.0)



end


function SpawnExplosion(hostPos)

    newObj = Engine.InstanceObject(scene,exp)
    newObT = Engine.getTransform(newObj)
    newObT.pos.x = hostPos.pos.x
    newObT.pos.y = hostPos.pos.y
    newObT.siz.x = 10
    newObT.siz.y = 10
    Engine.setTransform(newObj,newObT)

end

function Engine.Update(host)
    t = Engine.getTransform(host)

    tmr = tmr+0.7

    speed = 0.23
    my_speed = 1.04*my_speed + speed/45.0
    t.pos.y = t.pos.y + my_speed
    --t.rot.y = tmr/3
   -- t.rot.z = tmr/2

    anim(host)

    if(t.pos.x > -100 and t.pos.y > 20) then
        Engine.DeleteObject(host);
    end

    ts = Engine.getChildsObjects(enemy1)

    for i = 1,ts[1],1 do
        chHost = ts[i+1]
        if (chHost ~= nil) then

            enT = Engine.getTransform(chHost)

            a = enT.pos.x - t.pos.x
            b = enT.pos.y - t.pos.y
            c = 4

            if(a*a + b*b < c*c) then
                enT.pos.y = 30
                enT.pos.x = (math.random() -0.5) * 20.0
                Engine.setTransform(chHost,enT)
                SpawnExplosion(t)
                Engine.DeleteObject(host)
                break
            end

            
        end
    end

    Engine.setTransform(host,t)
end
