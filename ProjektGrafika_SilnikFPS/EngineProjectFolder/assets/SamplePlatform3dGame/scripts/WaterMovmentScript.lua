--@var Number amount
--@var Number timerSpeed

 timer = 0
function Engine.Update(host)
    timer = timer + timerSpeed
    t = Engine.getTransform(host)
    t.siz.y = 0.1 + (1 + math.sin(timer)) * amount
    t.rot.y = timer / 20
    Engine.setTransform(host,t)
end
