space_pressed = false

--@var Object playerCamera

lastPos = {}
lastPos.x = 0
lastPos.z = 0

lastPosDeltaNormalized = {}
lastPosDeltaNormalized.x = 0
lastPosDeltaNormalized.z = 0

faceRotationVector = {}
faceRotationVector.x = 1
faceRotationVector.y = 0
calculatedFaceRotation = 0

nextLevelPath = "11_quick_loading.scene"
currentLevelPath = "11_quick_loading.scene"

function StepFaceMove(x,y)
    div = 10.0
    div2 = 15.0
    faceRotationVector.x = faceRotationVector.x + ((x - faceRotationVector.x)/div)
    faceRotationVector.y = faceRotationVector.y + ((y - faceRotationVector.y)/div)
    rotCalc = math.atan(faceRotationVector.y, faceRotationVector.x)
    calculatedFaceRotation = calculatedFaceRotation + ((rotCalc - calculatedFaceRotation)/div2)
end

function calcFacingDir(playerPos)
    lastPosDeltaNormalized.x = playerPos.x - lastPos.x
    lastPosDeltaNormalized.y = playerPos.y- lastPos.y
    lastPos.y = playerPos.y
    lastPos.x = playerPos.x

    c = (lastPosDeltaNormalized.x*lastPosDeltaNormalized.x) + (lastPosDeltaNormalized.y*lastPosDeltaNormalized.y)
    if c ~= 0 then
        lastPosDeltaNormalized.x = lastPosDeltaNormalized.x/c
        lastPosDeltaNormalized.y = lastPosDeltaNormalized.y/c
    end
end

function Engine.Update(host)
    t = Engine.getTransform(host)
    t.rot.y = calculatedFaceRotation

    Engine.setTransform(host,t)
    speed = 0.017
    ext = Engine.GetObjectExtension(host,"PhysXCharacterControllerExtension")

    
    
    if(ext == nil or playerCamera==nil) then
        return
    else

            -- Loose condition
            if t.pos.y < -2 then
                Engine.swapLevel(host,"EngineProjectFolder/" .. currentLevelPath)
            end

            -- Win condition
            anyCoinOnMap = Engine.getFirstObjectWithName('CoinModel')
            if anyCoinOnMap == nil then
                print("You win!")
                Engine.swapLevel(host,"EngineProjectFolder/" .. nextLevelPath)
            end



            t_camera = Engine.getTransform(playerCamera)


            sin_value = t_camera.forward.x
            cos_value = t_camera.forward.z

            if (Input.getKeyPressed("forward")) then
                Extension.PhysXCharacterController.Move(ext,speed*sin_value,0,speed*cos_value)
                calcFacingDir(t.pos)
                StepFaceMove(sin_value,cos_value)
            end

            if(Input.getKeyPressed("backward")) then
                Extension.PhysXCharacterController.Move(ext,-speed*sin_value,0,-speed*cos_value)
                calcFacingDir(t.pos)
                StepFaceMove(-sin_value,-cos_value)
            end
            sin_value = t_camera.right.x
            cos_value = t_camera.right.z

            if(Input.getKeyPressed("right")) then
                Extension.PhysXCharacterController.Move(ext,speed*sin_value,0,speed*cos_value)
                calcFacingDir(t.pos)
                StepFaceMove(-sin_value,-cos_value)
            end

        
            if(Input.getKeyPressed("left")) then
                Extension.PhysXCharacterController.Move(ext,-speed*sin_value,0,-speed*cos_value)
                    calcFacingDir(t.pos)
                StepFaceMove(sin_value,cos_value)
            end

            --Extension.PhysXCharacterController.Move(ext,0,-0.0003,0)

            vel = Extension.PhysXCharacterController.getVelocity(ext)

            onGround = Extension.PhysXCharacterController.getOnGround(ext)

--print (calculatedFaceRotation)

            if(Input.getKeyPressed("space") ) then
                if(space_pressed == false and vel.y > -0.01 and vel.y < 0.01 and onGround) then
                    space_pressed = true
                    --for i=1,10,1 do
                    Extension.PhysXCharacterController.SetVelocity(ext,0,0.05,0)
                    Extension.PhysXCharacterController.setOnGround(ext,false)
                    --end
                end
            else
                space_pressed = false
            end
        --Gravity
       -- 
    end
    Engine.setTransform(host,t)
end

function Engine.onPickupCoinFunction(callingHost, args)
    print('Coin picked up')
end

--trigger_handler triggername
--function wyzyduj_pieniazka(Obiekt_pieniadz_host)
--end