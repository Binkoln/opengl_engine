--@var Number rotSpeed

function Engine.Update(host)

    sped = rotSpeed
    if rotSpeed == nil then
        sped = 0.1
    end
    tr = Engine.getTransform(host)
    tr.rot.y = tr.rot.y - sped
    Engine.setTransform(host,tr)

end