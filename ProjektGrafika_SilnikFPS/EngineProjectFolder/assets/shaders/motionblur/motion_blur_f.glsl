#version 330

out vec4 color;

uniform sampler2D input_image;
uniform sampler2D velocityMap;
uniform float motionblur_fps_adjustment;
in vec2 UV;


void main(){

  vec2 texelSize = 1.0 / vec2(textureSize(input_image, 0));
  vec2 screenTexCoords = gl_FragCoord.xy * texelSize;


  vec3 color_in = texture(input_image,UV).rgb;

  float dumper = 1.1f*motionblur_fps_adjustment;
  vec3 velocity = texture(velocityMap,UV).xyz*dumper;


  float speed = length(velocity.xyz / vec3(texelSize,texelSize.x));
  int nSamples = clamp(int(speed), 1, 50);


  for (int i = 1; i < nSamples; ++i) {
      vec2 offset = velocity.xy  * (float(i) / float(nSamples - 1) - 0.5);

      color_in += texture(input_image,UV+ offset).rgb;
   }
   color_in /= float(nSamples);

  color = vec4(color_in,1);
}