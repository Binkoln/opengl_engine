#version 330
layout (location = 0) in vec4 aPos;



out vec2 UV;



void main(){

gl_Position = vec4(aPos.x, aPos.y,0, 1.0);

UV = vec2(aPos.z,1.0-aPos.w);

}