#version 330

layout (location = 0)out vec4 color;
//layout (location = 3)out vec4 out_id;
uniform float time;
const int NR_DIR_LIGHTS = 5;

in vec3 in_nrm;
in vec3 in_pos;
in vec2 in_UV;
in mat3 in_normalSpace;
in vec4 in_directional_light_pos[NR_DIR_LIGHTS];
uniform int diffusemap_loadet;
uniform sampler2D diffuseTexture;//uni0
uniform int normalmap_loadet;
uniform sampler2D normalTexture;//uni1
uniform int specularmap_loadet;
uniform sampler2D specularTexture;//uni2

uniform vec3 camPos;



struct PointLight {
    vec3 Position;
    vec3 Color;
};
const int NR_LIGHTS = 32;
uniform PointLight point_lights[NR_LIGHTS];
uniform int active_point_light_count;

struct DirectionalLight {
    vec3 Direction;
    vec3 Color;
    mat4 LightProjection;
};

uniform sampler2D sampler_directional_depth[NR_DIR_LIGHTS];
uniform DirectionalLight directional_lights[NR_DIR_LIGHTS];
uniform int active_directional_light_count;

uniform sampler2D enviromentSampler;

const vec2 invAtan = vec2(0.1591, 0.3183);
vec2 SampleSphericalMap(vec3 v)
{
    vec2 uv = vec2(atan(v.z, v.x), asin(v.y));
    uv *= invAtan;
    uv += 0.5;
    return vec2(uv.x,1.0-uv.y);
}


vec3 processPointLight(vec3 lightPos,vec3 pos,vec3 normal_vec,vec3 lightColor,vec3 diffColor,vec3 specularMp){
 const float PI = 3.14159265359/2;
        float dist = length(lightPos-pos);
        vec3 lightPosDir = normalize(lightPos-pos);
        float intensity = max(  pow((dot(normal_vec,lightPosDir) + 1)*0.4,2),0.0);  


  // int intensity_i = int(intensity * 10);
  // intensity =intensity_i / 10.0;
  
    
    


    vec3 viewDir = normalize(camPos-pos);
    vec3 h = normalize(viewDir+lightPosDir);
    float specular = pow(max(dot(h,normal_vec),0),10 );
    //intensity=0;

  float cns = 0.05;
  float cns2 = 0.35;
    float equ = 1.0/(1 + dist*dist*cns) + 1.0/(1 + dist*cns2);
    //specular = 0;

  //fresnel
  vec3 fresnel_r = 2*(dot(normal_vec,lightPosDir))*normal_vec - lightPosDir;

    return ((lightColor*diffColor/PI)+ (specularMp*lightColor*specular/(PI*14)))*intensity*equ;
}

vec3 processDirectionalLight(vec3 lightDir,vec3 pos,vec3 normal_vec,vec3 lightColor,vec3 diffColor,vec3 specularMp,vec4 lightSpacePos,sampler2D shadowMap){
        const float PI = 3.14159265359/2;
        vec3 lightPosDir = normalize(lightDir);
    float intensity = max(dot(normal_vec,lightPosDir),0.0);    



    vec3 viewDir = normalize(camPos-pos);
    vec3 h = normalize(viewDir+lightPosDir);
    float specular = pow(max(dot(h,normal_vec),0),10 );
    //intensity=0;


//shadow
    vec3 projCoords = lightSpacePos.xyz/lightSpacePos.w;
    projCoords = projCoords * 0.5 + 0.5; 

    vec2 texel = vec2(1,1)/textureSize(shadowMap,0);
    float currentDepth = projCoords.z; 
    float bias = max(0.05 * (1.0 - dot(normal_vec, lightPosDir)), 0.005); 
    float shadow;
    for(int y = -3;y <= 3;y++){
      for(int x = -3;x <= 3;x++){
        float closestDepth = texture(shadowMap, projCoords.xy + (texel *vec2(x,y))).r; 
        shadow += (currentDepth - bias) > closestDepth  ? 1.0 : 0.0;  
      }
    }
    shadow = 1.0 - shadow/49.0;
    //shadow

    return ((lightColor*diffColor/PI)+ (specularMp*lightColor*specular/(PI*14)))*intensity*shadow;
}

void main(){

//Data feeding
  vec3 position_vec = in_pos;

  vec3 notmalMp = texture(normalTexture,in_UV).rgb;
    notmalMp = normalize(notmalMp * 2 - 1.0);

    if(normalmap_loadet < 1)
      notmalMp = vec3(0,0,1);
    vec3 normal_vec = normalize(in_normalSpace*notmalMp);

    vec4 colorr = texture(diffuseTexture,in_UV).rgba;
    vec3 diffColor = colorr.rgb;
    float alpha = colorr.a;
    
    if(diffusemap_loadet < 1)
      diffColor = vec3(1,1,1);


  vec3 specularMp = texture(specularTexture,in_UV).rgb;
    if(specularmap_loadet < 1)
      specularMp = vec3(1,1,1);


//Calculations


  vec3 viewDir = normalize(camPos-position_vec);


  vec3 finalColor = vec3(0,0,0);

    for(int i = 0;i < active_point_light_count;i++){
      PointLight p = point_lights[i];
      finalColor += processPointLight(p.Position,position_vec,normal_vec,p.Color,diffColor,specularMp);
    }

finalColor += vec3(0.1,0.1,0.1);

for(int i = 0;i < active_directional_light_count;i++){
      DirectionalLight p = directional_lights[i];
      finalColor += processDirectionalLight(p.Direction,position_vec,normal_vec,p.Color,diffColor,specularMp,in_directional_light_pos[i],sampler_directional_depth[i]);
    }
  //int iii = 4;
 //ivec3 intensity_i = ivec3(finalColor * iii);
 //  finalColor =intensity_i / float(iii);



    color = vec4(finalColor.rgb,alpha);
    //out_velocity = vec4(0,0,0,1);
    //out_id = vec4(objectIdColor.rgb,1);
}

