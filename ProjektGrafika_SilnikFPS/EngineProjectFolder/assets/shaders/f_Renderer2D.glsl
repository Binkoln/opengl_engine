#version 330

out vec4 color;


in vec2 UV;


uniform vec3 spriteColor;
uniform int diffusemap_loadet;
uniform sampler2D diffuseTexture;//uni0

void main(){
    vec3 finalColor = spriteColor;
    if(diffusemap_loadet > 0){
      vec4 texCol = texture(diffuseTexture,UV).rgba;
      if(texCol.a < 0.1)discard;
      finalColor *= texCol.rgb;
    }
    color = vec4(finalColor.rgb,1);
}