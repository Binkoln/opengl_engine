#version 330
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aUV;
layout (location = 2) in vec3 aNormal;
layout (location = 3) in vec3 aTangent;
layout (location = 4) in vec3 aBitanget;
layout (location = 5) in vec3 aBoneWeights;
layout (location = 6) in ivec3 aBoneIds;

uniform mat4 projection;
uniform mat4 viewMatrix;
uniform mat4 model;

out vec3 nrm;
out vec3 pos;
out vec2 UV;
out mat3 normalSpace;

void main(){

gl_Position = projection*viewMatrix*model*vec4(aPos.x, aPos.y, aPos.z, 1.0);
nrm = normalize((mat4(transpose(inverse(model)))*vec4(aNormal.xyz,0.0)).xyz);
pos = (model*vec4(aPos.xyz,1.0)).xyz;
UV = vec2(aUV.x,1.0-aUV.y);

mat4 invTransposedModel = transpose(inverse(model));
vec3 T = normalize(vec3(invTransposedModel * vec4(aTangent,   0.0)));
vec3 B = normalize(vec3(invTransposedModel * vec4(aBitanget, 0.0)));
vec3 N = normalize(vec3(invTransposedModel * vec4(aNormal,    0.0)));

normalSpace =(mat3(T,B,N));//transpose
}