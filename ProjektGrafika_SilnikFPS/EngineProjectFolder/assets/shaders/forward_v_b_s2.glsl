#version 330
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aUV;
layout (location = 2) in vec3 aNormal;
layout (location = 3) in vec3 aTangent;
layout (location = 4) in vec3 aBitanget;
layout (location = 5) in vec3 aBoneWeights;
layout (location = 6) in ivec3 aBoneIds;

const int NR_DIR_LIGHTS = 5;

uniform mat4 projection;
uniform mat4 viewMatrix;
uniform mat4 model;

out vec3 in_nrm;
out vec3 in_pos;
out vec2 in_UV;
out mat3 in_normalSpace;
out vec4 in_directional_light_pos[NR_DIR_LIGHTS];

struct DirectionalLight {
    vec3 Direction;
    vec3 Color;
    mat4 LightProjection;
};

uniform sampler2D sampler_directional_depth[NR_DIR_LIGHTS];
uniform DirectionalLight directional_lights[NR_DIR_LIGHTS];
uniform int active_directional_light_count;

void main(){

    gl_Position = projection*viewMatrix*model*vec4(aPos.x, aPos.y, aPos.z, 1.0);
    in_nrm = normalize((mat4(transpose(inverse(model)))*vec4(aNormal.xyz,0.0)).xyz);
    in_pos = (model*vec4(aPos.xyz,1.0)).xyz;
    in_UV = vec2(aUV.x,1.0-aUV.y);

    mat4 invTransposedModel = transpose(inverse(model));
    vec3 T = normalize(vec3(invTransposedModel * vec4(aTangent,   0.0)));
    vec3 B = normalize(vec3(invTransposedModel * vec4(aBitanget, 0.0)));
    vec3 N = normalize(vec3(invTransposedModel * vec4(aNormal,    0.0)));

    in_normalSpace =(mat3(T,B,N));//transpose

    for(int i = 0;i < active_directional_light_count;i++){//light pos pos xd
        in_directional_light_pos[i] = directional_lights[i].LightProjection * vec4(in_pos,1.0);
    }
}