#version 330

out vec4 color;
uniform float time;
uniform float noiseTreshold;

uniform vec3 camPos;
uniform sampler2D sampler_position;
uniform sampler2D sampler_diffuse;
uniform sampler2D sampler_normal;
uniform sampler2D sampler_material_protorties;
uniform sampler2D sampler_AO;
uniform bool applayAO;

in vec2 UV;
 const float PI = 3.14159265359;

struct PointLight {
    vec3 Position;
    vec3 Color;
};
const int NR_LIGHTS = 32;
uniform PointLight point_lights[NR_LIGHTS];
uniform int active_point_light_count;

struct DirectionalLight {
    vec3 Direction;
    vec3 Color;
    mat4 LightProjection;
};
const int NR_DIR_LIGHTS = 5;
uniform sampler2D sampler_directional_depth[NR_DIR_LIGHTS];
uniform DirectionalLight directional_lights[NR_DIR_LIGHTS];
uniform int active_directional_light_count;

uniform float enviromentStrength;
uniform samplerCube enviromentSampler;
uniform samplerCube enviromentIradianceSampler;


const float h_PI = 3.14159265359/2;
const vec2 invAtan = vec2(0.1591, 0.3183);
vec2 SampleSphericalMap(vec3 v)
{
    vec2 uv = vec2(atan(v.z, v.x), asin(v.y));
    uv *= invAtan;
    uv += 0.5;
    return vec2(uv.x,1.0-uv.y);
}


vec3 SchlickFresnel(vec3 F0,vec3 l,vec3 h){
  float e = 1.0 - dot(l,h);
  vec3 val = F0 + (vec3(1)-F0)*(e*e*e*e*e);
  return val;
}
vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(clamp(1.0 - cosTheta, 0.0, 1.0), 5.0);
}  

float NormalDistributionFunction(vec3 n,vec3 h,float alpha){
  float dt = max(dot(n,h),0);
  float alphaSQ = alpha*alpha;
  float sub = (dt*dt*(alphaSQ-1.0) + 1);
  sub *= sub;
  sub *= PI;
  return alphaSQ/sub;
}

float GeometryShlickGGX(float dotValue,float k){
  return dotValue/(dotValue*(1.0-k) + k);
}

float Geometry(vec3 normal,vec3 viewDir,vec3 lightDir,float k){
  float dotView = max(dot(normal,viewDir),0);
  float dotLight = max(dot(normal,lightDir),0);
  return GeometryShlickGGX(dotView,k)*GeometryShlickGGX(dotLight,k);
  
}

vec3 processPointLight(vec3 lightPos,vec3 pos,vec3 normal_vec,vec3 lightColor,vec3 diffColor,vec3 specularMp){

        float dist = length(lightPos-pos);
        vec3 lightPosDir = normalize(lightPos-pos);
        float intensity = max(  pow((dot(normal_vec,lightPosDir) + 1)*0.4,2),0.0);    
    vec3 viewDir = normalize(camPos-pos);
    vec3 h = normalize(viewDir+lightPosDir);
    float specular = pow(max(dot(h,normal_vec),0),10 );
    //intensity=0;
    float specularIntensity = 0.07;
  float cns = 0.05;
  float cns2 = 0.35;
    float equ = 1.0/(1 + dist*dist*cns) + 1.0/(1 + dist*cns2);
    //specular = 0;

  //fresnel
  vec3 fresnel_r = 2*(dot(normal_vec,lightPosDir))*normal_vec - lightPosDir;

    return ((lightColor*diffColor/(PI))+ (specularMp*lightColor*specular*(specularIntensity + 10)/(PI*14)))*intensity*equ;
}

vec3 processDirectionalLight(vec3 lightDir,vec3 pos,vec3 normal_vec,vec3 lightColor,vec3 diffColor,vec3 specularMp,vec4 lightSpacePos,sampler2D shadowMap,float roughness){

    vec3 lightPosDir = normalize(lightDir);
float cosTheta =  max(dot(normal_vec,lightPosDir),0.0);

    float intensity =cosTheta;    




    vec3 viewDir = normalize(camPos-pos);
    vec3 h = normalize(viewDir+lightPosDir);
    //float specular = pow(max(dot(h,normal_vec),0),roughness*1024 );
    //intensity=0;

//BRDF
  vec3 F0 = vec3(0.04);
F0 = mix(F0,diffColor,specularMp.r);
   vec3 F = SchlickFresnel(F0,lightPosDir,h);
    //vec3 F = fresnelSchlick(max(dot(h, viewDir), 0.0),F0);
   float NDF = NormalDistributionFunction(normal_vec,h,roughness);
   float G = Geometry(normal_vec,viewDir,lightPosDir,roughness);

   vec3 numerator    = NDF * G * F;
   float denominator = 4.0 * max(dot(normal_vec, viewDir), 0.0) * max(dot(normal_vec, lightPosDir), 0.0)  + 0.0001;
   vec3 specular     = numerator / denominator; 


//shadow
    vec3 projCoords = lightSpacePos.xyz/lightSpacePos.w;
    projCoords = projCoords * 0.5 + 0.5; 

    vec2 texel = vec2(1,1)/textureSize(shadowMap,0);
    float currentDepth = projCoords.z; 
    float bias = max(0.05 * (1.0 - dot(normal_vec, lightPosDir)), 0.005); 
    float shadow = 0;


    vec3 kdee_nwm_co_robie_proportion = vec3(1) - F;
    kdee_nwm_co_robie_proportion *= min(max(1.0 - specularMp.r,0.05),0.95);

    if(projCoords.x < 1 && projCoords.x > 0 && projCoords.y < 1 && projCoords.y > 0 && projCoords.z < 1 && projCoords.z > 0 && length(lightSpacePos) > 0){
        //diffColor = vec3(1,0,0);
      for(int y = -3;y <= 3;y++){
        for(int x = -3;x <= 3;x++){
          float closestDepth = texture(shadowMap, projCoords.xy + (texel *vec2(x,y))).r; 
          shadow += (currentDepth - bias) > closestDepth  ? 1.0 : 0.0;  
        }
      }
      shadow = 1.0 - shadow/49.0;
    }
    else{
      shadow = 1;
      //diffColor  = vec3(0.1,0.0,0.7);
    }
    //shadow

    return ((lightColor*diffColor)*intensity*(kdee_nwm_co_robie_proportion)+ (F*specularMp*lightColor*specular))  *shadow;
}

void main(){

  vec3 position_vec = texture(sampler_position,UV).rgb;
  vec3 normal_vec = normalize(texture(sampler_normal,UV).rgb);
  vec3 diffColor =  texture(sampler_diffuse,UV).rgb;
  float specVal = texture(sampler_diffuse,UV).a;
  vec4 materialProporties = texture(sampler_material_protorties,UV).rgba;
  float roughness = min(materialProporties.r + 0.001,0.9);
  //roughness = roughness*roughness;
  //vec3 specularMp = vec3(materialProporties.g);
 vec3 specularMp = vec3(1.0-roughness);

  vec3 viewDir = normalize(camPos-position_vec);


  vec3 finalColor = vec3(0,0,0);

  for(int i = 0;i < active_point_light_count;i++){
    PointLight p = point_lights[i];
    finalColor += processPointLight(p.Position,position_vec,normal_vec,p.Color,diffColor,specularMp);
  }



  for(int i = 0;i < active_directional_light_count;i++){
      DirectionalLight p = directional_lights[i];
      vec4 in_directional_light_pos = p.LightProjection * vec4(position_vec,1);
      finalColor += processDirectionalLight(p.Direction,position_vec,normal_vec,p.Color,diffColor,specularMp,in_directional_light_pos,sampler_directional_depth[i],roughness);
  }


  vec3 reflection = normalize(reflect(-viewDir,normal_vec));

  vec3 smpColor = texture(enviromentSampler,reflection,5).rgb;
  vec3 iradianceColor = texture(enviromentIradianceSampler,normal_vec,5).rgb;


  finalColor += processDirectionalLight(reflection,position_vec,normal_vec,smpColor,diffColor,vec3(0),vec4(0),sampler_directional_depth[0],roughness) * 0.8*specularMp*(1.0-roughness)*enviromentStrength;

   finalColor += processDirectionalLight(reflection,position_vec,normal_vec,iradianceColor,diffColor,vec3(0),vec4(0),sampler_directional_depth[0],roughness) *(roughness)*enviromentStrength;



  //finalColor += (texture(enviromentSampler,SampleSphericalMap(reflect(-viewDir,normal_vec)),7).rgb)*0.5;
    //finalColor += (texture(enviromentSampler,SampleSphericalMap(normal_vec),7).rgb)*0.1;
  if(applayAO){
    vec3 ao_color = texture(sampler_AO,UV- vec2(0.0015,0.002 )).rgb; 
    finalColor *= ao_color;
  }
  
  finalColor += vec3(0.1,0.1,0.1);

    color = vec4(finalColor.rgb,1);
}


// vec3 irradiance = vec3(0.0);  
// vec3 up    = vec3(0.0, 1.0, 0.0);
// vec3 right = normalize(cross(up, normal));
// up         = normalize(cross(normal, right));
// float sampleDelta = 0.025;
// float nrSamples = 0.0; 
// for(float phi = 0.0; phi < 2.0 * PI; phi += sampleDelta)
// {
//     for(float theta = 0.0; theta < 0.5 * PI; theta += sampleDelta)
//     {
//         //Obliczenia stosując swój ulubiony model BRDF (którego nie ma pokazanego na tej prezętacji)
//         vec3 tangentSample = vec3(sin(theta) * cos(phi),  sin(theta) * sin(phi), cos(theta));
//         vec3 sampleVec = tangentSample.x * right + tangentSample.y * up + tangentSample.z * N; 

//         //Sumujemy wynik
//         irradiance += texture(environmentMap, sampleVec).rgb * cos(theta) * sin(theta);
//         nrSamples++;
//     }
// }
// irradiance = PI * irradiance * (1.0 / float(nrSamples));


