#version 330

layout (location = 0)out vec4 out_position;
layout (location = 1)out vec4 out_normal;
layout (location = 2)out vec4 out_color;
layout (location = 3)out vec4 out_id;
layout (location = 4)out vec4 out_velocity;
layout (location = 5)out vec4 out_viewSpacePosition;
layout (location = 6)out vec4 out_viewSpaceNormals;
layout (location = 7)out vec4 out_materialProporties;
//layout (location = 5)out vec4 out_material_id;

uniform vec3 cubecolor;
uniform vec3 camPos;
uniform float time;

in vec3 nrm;
in vec4 pos;
in vec2 UV;
in mat3 normalSpace;
in vec4 posScreen;
in vec4 posScreenPrev;
in vec4 viewSpacePosition;
in mat3 viewSpaceNormalSpace;

uniform int diffusemap_loadet;
uniform sampler2D diffuseTexture;//uni0
uniform int normalmap_loadet;
uniform sampler2D normalTexture;//uni1
uniform int specularmap_loadet;
uniform sampler2D specularTexture;//uni2
//Material proporites
uniform vec3 u_albedoColor;
uniform float u_specular_value;
uniform float u_roughness_value;




uniform sampler2D noiseTexture;//uni3
uniform float noiseTreshold;



uniform vec3 objectIdColor;


void main(){

   float noiseValue = texture(noiseTexture,UV).r;
    if(noiseValue > noiseTreshold)
      discard;

    vec3 notmalMp = texture(normalTexture,UV).rgb;
    notmalMp = normalize(notmalMp * 2 - 1.0);

    if(normalmap_loadet < 1)
      notmalMp = vec3(0,0,1);
    vec3 normal_vec = normalize(normalSpace*notmalMp);
    vec3 ss_normal_vec = normalize(viewSpaceNormalSpace*notmalMp);


    vec4 diffCola = texture(diffuseTexture,UV).rgba;
    if(diffusemap_loadet < 1)
      diffCola = vec4(u_albedoColor.rgb,1);

    if(diffCola.a < 0.2) discard;
    vec3 diffColor = diffCola.rgb;

  vec3 roughnessMp = texture(specularTexture,UV).rgb;
    if(specularmap_loadet < 1)
      roughnessMp = vec3(1.0-u_roughness_value,1,1);


  
    out_position = pos;
    out_viewSpacePosition = viewSpacePosition;
    out_normal = vec4(normalize(normal_vec.xyz) ,1);
    out_color = vec4(diffColor.rgb,1.0 );
    out_id = vec4(objectIdColor.rgb,1);
    vec2 a = (posScreen.xy / posScreen.w) * 0.5 + 0.5;
    vec2 b = (posScreenPrev.xy / posScreenPrev.w) * 0.5 + 0.5;
    out_velocity = vec4(vec2(a-b),0,1.0);
    out_viewSpaceNormals = vec4(normalize(ss_normal_vec.xyz) ,1);
    out_materialProporties = vec4(1.0-roughnessMp.r,u_specular_value,1,1);

}