#version 330
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aUV;
layout (location = 2) in vec3 aNormal;
layout (location = 3) in vec3 aTangent;
layout (location = 4) in vec3 aBitanget;
layout (location = 5) in vec3 aBoneWeights;
layout (location = 6) in ivec3 aBoneIds;

uniform mat4 MPV;
uniform mat4 model;




void main(){

gl_Position = MPV*model*vec4(aPos.xyz, 1.0);


}