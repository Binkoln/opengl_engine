#version 330
layout (location = 0) in vec4 aPosSize;


uniform mat4 viewProjection;
uniform mat4 model;

uniform vec4 uvs_posSize;
out vec2 UV;


void main(){

gl_Position = viewProjection*model*vec4(aPosSize.x, aPosSize.y, 0, 1.0);

UV = vec2(uvs_posSize.x + uvs_posSize.z*aPosSize.z, uvs_posSize.y + uvs_posSize.w*aPosSize.w);

}