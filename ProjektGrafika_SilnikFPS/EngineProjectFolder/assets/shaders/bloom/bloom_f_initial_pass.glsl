#version 330

out vec4 color;

uniform sampler2D input_image;
in vec2 UV;
uniform vec2 fb_size;
uniform float bloom_treshold;

void main(){
  vec3 color_in = texture(input_image,UV).rgb;
  color_in -= vec3(bloom_treshold);
  color_in.rgb = max(color_in.rgb,vec3(0,0,0));
  //color_in.rgb = min(color_in.rgb,vec3(1));
  color = vec4(color_in.rgb  ,1);



}