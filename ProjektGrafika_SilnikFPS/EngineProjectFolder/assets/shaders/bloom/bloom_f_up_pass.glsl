#version 330

out vec4 color;

uniform sampler2D input_image;
uniform sampler2D input_image2;
in vec2 UV;
uniform vec2 fb_size;
uniform bool horizontal;
uniform float pass_dumper;



void main(){
  vec3 color_in =   max(texture(input_image2,UV).rgb,vec3(0,0,0));
  if(horizontal){
    vec2 pixel = 1/fb_size;
    color_in += max(texture(input_image,UV).rgb*pass_dumper,vec3(0,0,0))*4;
    
    color_in += max(texture(input_image,UV + vec2(pixel.x,0)).rgb*pass_dumper,vec3(0,0,0))*2;
    color_in += max(texture(input_image,UV + vec2(-pixel.x,0)).rgb*pass_dumper,vec3(0,0,0))*2;
    color_in += max(texture(input_image,UV + vec2(0,pixel.y)).rgb*pass_dumper,vec3(0,0,0))*2;
    color_in += max(texture(input_image,UV + vec2(0,-pixel.y)).rgb*pass_dumper,vec3(0,0,0))*2;

    color_in += max(texture(input_image,UV + vec2(pixel.x,pixel.y)).rgb*pass_dumper,vec3(0,0,0));
    color_in += max(texture(input_image,UV + vec2(-pixel.x,pixel.y)).rgb*pass_dumper,vec3(0,0,0));
    color_in += max(texture(input_image,UV + vec2(-pixel.x,-pixel.y)).rgb*pass_dumper,vec3(0,0,0));
    color_in += max(texture(input_image,UV + vec2(pixel.x,-pixel.y)).rgb*pass_dumper,vec3(0,0,0));

    color_in /= 16.0f;
  }else{
    color_in += max(texture(input_image,UV).rgb*pass_dumper,vec3(0,0,0));
  }
  
  color = vec4(color_in.rgb,1);
}
