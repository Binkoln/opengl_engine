#version 330

out vec4 color;

uniform sampler2D input_image;
in vec2 UV;
uniform vec2 fb_size;
uniform bool horizontal;




void main(){
  vec3 color_in = texture(input_image,UV).rgb;
  vec2 pixel = 1/fb_size;


  vec3 color_out = vec3(0);


  color_out +=texture(input_image,UV).rgb * 0.5;
  color_out +=texture(input_image,UV + pixel).rgb * 0.125;
  color_out +=texture(input_image,UV + vec2(-pixel.x,pixel.y)).rgb * 0.125;
  color_out +=texture(input_image,UV + vec2(pixel.x,-pixel.y)).rgb * 0.125;
  color_out +=texture(input_image,UV + vec2(-pixel.x,-pixel.y)).rgb * 0.125;
  
  


  color = vec4(color_out.rgb,1);
}
