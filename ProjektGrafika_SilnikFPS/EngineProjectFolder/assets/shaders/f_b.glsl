#version 330

out vec4 color;

uniform vec3 cubecolor;
uniform vec3 camPos;
uniform float time;

in vec3 nrm;
in vec3 pos;
in vec2 UV;
in mat3 normalSpace;
uniform int diffusemap_loadet;
uniform sampler2D diffuse;//uni0
uniform int normalmap_loadet;
uniform sampler2D normalTexture;//uni1
uniform int specularmap_loadet;
uniform sampler2D specularTexture;//uni2

struct PointLight {
    vec3 Position;
    vec3 Color;
};
const int NR_LIGHTS = 32;
uniform PointLight point_lights[NR_LIGHTS];
uniform int active_point_light_count;

vec3 processLight(vec3 lightPos,vec3 normal_vec,vec3 lightColor,vec3 diffColor,vec3 specularMp){
        const float PI = 3.14159265359/2;
        float dist = length(lightPos-pos);
        vec3 lightPosDir = normalize(lightPos-pos);
    float intensity = max(dot(normal_vec,lightPosDir),0.0);    

    vec3 viewDir = normalize(camPos-pos);
    vec3 h = normalize(viewDir+lightPosDir);
    float specular = pow(max(dot(h,normal_vec),0),10 );
    //intensity=0;
    float specularIntensity = 0.07;
  float cns = 0.05;
  float cns2 = 0.35;
    float equ = 1.0/(1 + dist*dist*cns) + 1.0/(1 + dist*cns2);
    //specular = 0;
    return (lightColor*diffColor/PI+ (specularMp*lightColor*specular*(specularIntensity + 10)/(PI*14)))*intensity*equ;
}


void main(){

    vec3 notmalMp = texture(normalTexture,UV).rgb;
    notmalMp = normalize(notmalMp * 2 - 1.0);

    if(normalmap_loadet < 1)
      notmalMp = vec3(0,0,1);
    vec3 normal_vec = normalize(normalSpace*notmalMp);
    vec4 diffCola = texture(diffuse,UV).rgba;
      if(diffusemap_loadet < 1)
      diffCola = vec4(1,1,1,1);

    if(diffCola.a < 0.2) discard;
    vec3 diffColor = diffCola.rgb;
    //diffColor = texture(normalTexture,UV).rgb;

  vec3 specularMp = texture(specularTexture,UV).rgb;
    if(specularmap_loadet < 1)
      specularMp = vec3(1,1,1);

    float anglt = time;
    vec3 lightColor = vec3(0.95,0.92,1);
    vec3 lightPos = vec3(0,1,-2);

    anglt += 2;
    vec3 lightColor2 = vec3(0.5333, 0.749, 1.0);
    vec3 lightPos2 = vec3(cos(anglt)*4,0,sin(anglt)*5);
    vec3 lightColor3 = vec3(1, 0.749, 0.5333);

const float col_dumper = 0.6;

  vec3 finalColor = vec3(0,0,0);
  // processLight(camPos,normal_vec,lightColor*col_dumper,diffColor,specularMp);
    //finalColor += processLight(lightPos2,normal_vec,lightColor2*col_dumper,diffColor,specularMp);
    //finalColor += processLight(-lightPos2,normal_vec,lightColor3*col_dumper,diffColor,specularMp);
    //finalColor += processLight(-lightPos,normal_vec,lightColor3*col_dumper,diffColor,specularMp);

    for(int i = 0;i < active_point_light_count;i++){
      PointLight p = point_lights[i];
      finalColor += processLight(p.Position,normal_vec,p.Color,diffColor,specularMp);
    }

finalColor += vec3(0.1,0.1,0.1);

  float gamma = 1.7;
    finalColor = pow(finalColor.rgb,vec3(gamma));

    color = vec4(finalColor.rgb,1);
}