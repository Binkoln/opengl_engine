#version 330

out vec4 color;

uniform sampler2D input_image;
in vec2 UV;

vec3 ACESFilm(vec3 x)
{
    float a = 2.51f;
    float b = 0.03f;
    float c = 2.43f;
    float d = 0.59f;
    float e = 0.14f;
    return ((x*(a*x+b))/(x*(c*x+d)+e));
}

void main(){
  vec3 color_in = texture(input_image,UV).rgb;
  float gamma = 1.7;
float exposure = 5.5;
// exposure tone mapping
    //color_in = vec3(1.0) - exp(-color_in * exposure);

  color_in = pow(ACESFilm(color_in),vec3(gamma));

    //color_in = color_in / (color_in + vec3(0.5));
    //color_in = pow(color_in, vec3(0.5)); 



  color = vec4(color_in.rgb,1);

  
 
}