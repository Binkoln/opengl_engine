#version 330

layout (location = 0)out vec4 color;

uniform float time;
uniform vec3 camPos;
uniform sampler2D enviromentSampler;

in vec3 in_pos;
in vec2 in_uv;
uniform float rotation;
uniform float strength;
uniform int mipmapLevel;



const vec2 invAtan = vec2(0.1591, 0.3183);
vec2 SampleSphericalMap(vec3 v)
{
    vec2 uv = vec2(atan(v.z, v.x), asin(v.y));
    uv *= invAtan;
    uv += 0.5;
    return vec2(uv.x,1.0-uv.y);
}


void main(){

//Data feeding
  vec3 position_vec = in_pos;
    position_vec.xy *= -1;
  position_vec = normalize(position_vec);
  //get color
 vec2 proj = SampleSphericalMap(position_vec);
 vec3 iradiance = vec3(0);
 int moc = 128;
  if(mipmapLevel == 0){
      vec3 smpColor = texture(enviromentSampler,vec2(proj.x  ,proj.y)).rgb;
          smpColor = min(smpColor,vec3(20));
          iradiance = smpColor;
    }
    else{
      for(int i = -moc;i < moc;i++)
          for(int j = -moc;j < moc;j++){
              float dmp = 1.0/(moc*2);
              vec2 differ = vec2(i,j)*0.3*dmp;
              vec3 smpColor = texture(enviromentSampler,vec2(proj.x + rotation ,proj.y)+differ,-1).rgb;
              smpColor = min(smpColor,vec3(20));
              iradiance += smpColor;
          }
        iradiance /= (moc*2)*(moc*2);
    }


    
    gl_FragDepth = 0.99999;
    color = vec4(iradiance.rgb,1);

}

