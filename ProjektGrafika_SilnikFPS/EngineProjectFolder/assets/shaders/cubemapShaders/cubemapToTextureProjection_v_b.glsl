#version 330
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aUV;
layout (location = 2) in vec3 aNormal;
layout (location = 3) in vec3 aTangent;
layout (location = 4) in vec3 aBitanget;
layout (location = 5) in vec3 aBoneWeights;
layout (location = 6) in ivec3 aBoneIds;


uniform mat4 projection;
uniform mat4 viewMatrix;
uniform mat4 model;

out vec3 in_pos;
out vec2 in_uv;


void main(){

    gl_Position = projection*viewMatrix*model*vec4(aPos.x, aPos.y, aPos.z, 1.0);
    in_pos = (model*vec4(aPos.xyz,1.0)).xyz;
    in_uv = aUV;
}