#version 330

layout (location = 0)out vec4 color;

uniform float time;
uniform vec3 camPos;

uniform samplerCube enviromentSampler;
in vec3 in_pos;
in vec2 in_uv;
uniform float rotation;
uniform float strength;






void main(){

//Data feeding
  vec3 position_vec = in_pos;

  position_vec = normalize(position_vec);
  //get color

    vec3 smpColor = texture(enviromentSampler,position_vec,4).rgb*strength;
    smpColor = min(smpColor,vec3(20));
    gl_FragDepth = 0.99999;
    //smpColor = vec3(3);
    color = vec4(smpColor.rgb,1);

}

