#version 330

out vec4 color;

uniform sampler2D ssaoInput;

in vec2 UV;
uniform vec2 fb_size;
uniform bool horizontal;
uniform float pass_dumper;



void main(){
  float color_in =  0;// max(texture(input_image2,UV).rgb,vec3(0,0,0));
  //if(horizontal){
    vec2 pixel = 1/fb_size;
    for(int y = 0;y < 4;y++)
    {
      for(int x = 0;x < 4;x++){
        color_in += texture(ssaoInput,UV + (pixel*vec2(x,y))).r;
      }
    }
    color_in /= 16.0f;
  // }else{
  //   color_in += max(texture(input_image,UV).rgb*pass_dumper,vec3(0,0,0));
  // }
  
  color = vec4(vec3(pow(color_in,1)),1);
}
