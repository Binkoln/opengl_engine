#version 330

out vec4 color;

uniform sampler2D normalsTex;
uniform sampler2D positionsTex;
uniform sampler2D depthTex;
uniform sampler2D noiseTex;

uniform mat4 camProjection;
uniform mat4 camView;


uniform vec3 uSampleKernel[64];
uniform vec2 texel;
in vec2 UV;

uniform int kernelSize;
uniform float radius;
uniform float bias;


void main(){
 // get input for SSAO algorithm
    vec3 fragPos = texture(positionsTex, UV).xyz;
    vec3 normal = (texture(normalsTex, UV).xyz);
    vec3 randomVec = (texture(noiseTex, UV * texel).xyz );
    // create TBN change-of-basis matrix: from tangent-space to view-space
   vec3 tangent = normalize(randomVec - normal * dot(randomVec, normal));
    vec3 bitangent = cross(normal, tangent);
    mat3 TBN = mat3(tangent, bitangent, normal);
    // iterate over the sample kernel and calculate occlusion factor
    float occlusion = 0.0;

    for(int i = 0;i < kernelSize;i++){

       vec3 samplePos = fragPos + ((TBN * uSampleKernel[i]) * radius);

       vec4 projCoords = camProjection * vec4(samplePos,1.0);
       projCoords.xyz /= projCoords.w;
       projCoords.xyz *= 0.5;  
       projCoords.xyz += 0.5;

       float sampleProjPos = texture(positionsTex, projCoords.xy).z;
      float rangeCheck = smoothstep(0.0, 1.0, radius / abs(fragPos.z - sampleProjPos));
        occlusion += ((sampleProjPos >= samplePos.z + bias) ? 1.0 : 0.0)* rangeCheck;      

    }

    occlusion = 1.0 - (occlusion / (kernelSize-3));
    

  color = vec4(vec3(occlusion),1.0);
}