#version 330
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aUV;
layout (location = 2) in vec3 aNormal;
layout (location = 3) in vec3 aTangent;
layout (location = 4) in vec3 aBitanget;
layout (location = 5) in vec3 aBoneWeights;
layout (location = 6) in ivec3 aBoneIds;

uniform mat4 projection;
uniform mat4 viewMatrix;
uniform mat4 prevViewMatrix;
uniform mat4 model;
uniform mat4 prev_model;

out vec3 nrm;
out vec4 pos;
out vec2 UV;
out vec2 SupUV;
out mat3 normalSpace;
out vec4 posScreen;
out vec4 posScreenPrev;
out vec4 viewSpacePosition;
out mat3 viewSpaceNormalSpace;
uniform vec3 terrainSize;

void main(){

SupUV = vec2(aPos.xz/terrainSize.xy);

gl_Position = projection*viewMatrix*model*vec4(aPos.x, aPos.y, aPos.z, 1.0);
nrm = normalize((mat4(transpose(inverse(model)))*vec4(aNormal.xyz,0.0)).xyz);
pos = (model*vec4(aPos.xyz,1.0));
viewSpacePosition = (viewMatrix*model*vec4(aPos.xyz,1.0));
posScreen = (projection*viewMatrix*model*vec4(aPos.xyz,1.0));
posScreenPrev =  (projection*prevViewMatrix*prev_model*vec4(aPos.xyz,1.0));
//posPrev = (projection*viewMatrix*model*vec4(aPos.xyz,1.0)).xyz - (projection*prevViewMatrix*prev_model*vec4(aPos.xyz,1.0)).xyz;
UV = vec2(aUV.x,1.0-aUV.y);

//For deffered shading
mat4 invTransposedModel = transpose(inverse(model));
vec3 T = normalize(vec3(invTransposedModel * vec4(aTangent,   0.0)));
vec3 B = normalize(vec3(invTransposedModel * vec4(aBitanget, 0.0)));
vec3 N = normalize(vec3(invTransposedModel * vec4(aNormal,    0.0)));

normalSpace =(mat3(T,B,N));//transpose

//For SSAO
invTransposedModel = transpose(inverse(viewMatrix*model));
T = normalize(vec3(invTransposedModel * vec4(aTangent,   0.0)));
B = normalize(vec3(invTransposedModel * vec4(aBitanget, 0.0)));
N = normalize(vec3(invTransposedModel * vec4(aNormal,    0.0)));

viewSpaceNormalSpace =(mat3(T,B,N));//transpose

}