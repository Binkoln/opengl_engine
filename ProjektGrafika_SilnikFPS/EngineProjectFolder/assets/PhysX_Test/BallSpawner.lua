ballObject = nil

timer = 0
counter = 0
function Engine.Update(host)
    t = Engine.getTransform(host)
    if ballObject == nil then
        ballObject = Engine.getFirstObjectWithName("Ball")
    end
    timer = timer +  60.0/60.0
    t.pos.x = t.pos.x + math.sin(counter/34.0)*3
    if timer > 1 then
        counter = counter + 1
        timer = 0
        newBall = Engine.InstanceObject(nil,ballObject)
        Engine.setTransform(newBall,t)
        print (counter.." "..Engine.getProfilerUpdateTime())
    end
end
