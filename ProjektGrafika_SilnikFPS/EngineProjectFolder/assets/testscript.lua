vx = 0.34
vy = 0.23

function clampp(x,min,max)
    ret = x
    if(ret < min) then
        return min
    end
    
        if(ret > max) then
        return max
    end
        return ret
    
end

function Engine.Update(host)
    t = Engine.getTransform(host)
    speed = 1.0
    podlogasize = 10;

    t.pos.x = t.pos.x + vx*speed
    if(t.pos.x > podlogasize or t.pos.x < -podlogasize) then
        vx = -vx
    end

    t.pos.z = t.pos.z + vy*speed
    if(t.pos.z > podlogasize or t.pos.z < -podlogasize) then
        
        vy = -vy
    end

    t.pos.z = clampp(t.pos.z,-podlogasize,podlogasize)
    t.pos.x = clampp(t.pos.x,-podlogasize,podlogasize)

    t.rot.y = t.rot.y - 0.1

    Engine.setTransform(host,t)
end