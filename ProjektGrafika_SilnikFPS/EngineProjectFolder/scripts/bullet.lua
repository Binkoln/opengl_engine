jumping = false
--@var Object level
--@var Object enemiesObjects

--@var Object camera
--@var Object celownik
lifetime = 1.0
initDirection = false
dirX = 0
dirY = 0
dirZ = 0
function InitDirectionOfMovment()
    initDirection = true
    p1 = Engine.getWorldPosition(camera)
    p2 = Engine.getWorldPosition(celownik)
    a = p2.x - p1.x
    b = p2.z- p1.z
    c = p2.y- p1.y
    len = math.sqrt(c*c + b*b + a*a)
    a = a/len
    b = b/len
    c = c/len
    dirX = a
    dirY = c
    dirZ = b
end

function Engine.Update(host)
    t = Engine.getTransform(host)
    speed = 1.9
    
    if(t.pos.y < -5000) then
        return
    end

    lifetime = lifetime - (1.0/60.0)
    if(lifetime < 0) then
        Engine.DeleteObject(host)
        return
    end


    if(initDirection == false) then
        InitDirectionOfMovment()
    end


    t.pos.x = t.pos.x + speed*dirX
    t.pos.y = t.pos.y + speed*dirY
    t.pos.z = t.pos.z + speed*dirZ
  
    if( enemiesObjects==nil) then
        return
    else
        ts = Engine.getChildsObjects(enemiesObjects)
        for i = 1,ts[1],1 do
            chHost = ts[i+1]
            if (chHost ~= nil) then
                chHostT = Engine.getTransform(chHost)
                a = chHostT.pos.x - t.pos.x
                b = chHostT.pos.z- t.pos.z
                c = chHostT.pos.y- t.pos.y
                if(c*c + b*b + a*a < 9.0) then
                    Engine.DeleteObject(chHost)
                    Engine.DeleteObject(host)
                    return
                end
            end
        end

      
    end
    Engine.setTransform(host,t)
end