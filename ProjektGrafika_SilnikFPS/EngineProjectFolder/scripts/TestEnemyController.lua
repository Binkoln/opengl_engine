jumping = false

--@var Object playerObject
jump_timer = 2
function Engine.Update(host)
    t = Engine.getTransform(host)
    speed = 0.1
    jump_timer =jump_timer - (1.0/60.0)
    ext = Engine.GetObjectExtension(host,"PhysXCharacterControllerExtension")
    t.rot.y = t.rot.y + (4.0/60)
  
    if(ext == nil or playerObject==nil) then
        return
    else

            t_camera = Engine.getTransform(playerObject)
            a = t_camera.pos.x - t.pos.x
            b = t_camera.pos.z- t.pos.z

            c = math.sqrt(a*a + b*b)
            if(c < 1.5) then
                --Kill player
                Engine.swapLevel(host,"EngineProjectFolder/GameOver.scene")
                return
            end

            a = a/c
            b = b/c
            --print (t_camera.rot.y)

            
            Extension.PhysXCharacterController.Move(ext,speed*a,0,speed*b)
            

           


            if(jump_timer < 0) then
                jump_timer = 2
                if(jumping == false) then
                    jumping = true
                    --for i=1,10,1 do
                    Extension.PhysXCharacterController.SetVelocity(ext,0,0.5,0)
                    --end
                end
            else
                jumping = false
            end
        --Gravity
       -- 
    end
    Engine.setTransform(host,t)
end