space_pressed = false

--@var Object playerCamera

function Engine.Update(host)
    t = Engine.getTransform(host)
    speed = 0.6
    ext = Engine.GetObjectExtension(host,"PhysXCharacterControllerExtension")

  
    if(ext == nil or playerCamera==nil) then
        return
    else

            t_camera = Engine.getTransform(playerCamera)
            sinn = t_camera.forward.x
            coss = t_camera.forward.z
            --print (t_camera.rot.y)

            if (Input.getKeyPressed("forward")) then
            Extension.PhysXCharacterController.Move(ext,speed*sinn,0,speed*coss)
            end

            if(Input.getKeyPressed("backward")) then
                Extension.PhysXCharacterController.Move(ext,-speed*sinn,0,-speed*coss)
            end
            sinn = t_camera.right.x
            coss = t_camera.right.z

            if(Input.getKeyPressed("right")) then
                Extension.PhysXCharacterController.Move(ext,speed*sinn,0,speed*coss)
            end

        
            if(Input.getKeyPressed("left")) then
                Extension.PhysXCharacterController.Move(ext,-speed*sinn,0,-speed*coss)
            end
            Extension.PhysXCharacterController.Move(ext,0,-0.1,0)



            if(Input.getKeyPressed("space") ) then
                if(space_pressed == false) then
                    space_pressed = true
                    --for i=1,10,1 do
                    Extension.PhysXCharacterController.SetVelocity(ext,0,0.5,0)
                    --end
                end
            else
                space_pressed = false
            end
        --Gravity
       -- 
    end
    Engine.setTransform(host,t)
end

--@trigger_handler triggername
function wyzyduj_pieniazka(Obiekt_pieniadz_host)
end