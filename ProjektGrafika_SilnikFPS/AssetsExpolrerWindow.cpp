#include "AssetsExplorerWindow.h"

AssetsExpolrerWindow::AssetsExpolrerWindow()
{
    
}


void AssetsExpolrerWindow::setSaveSceneFunction(void (*saveScene)(std::string))
{
    this->saveScene = saveScene;
}

void AssetsExpolrerWindow::setLoadSceneFunction(void (*loadScene)(std::string))
{
    this->loadScene = loadScene;
}

AssetsExpolrerWindow::~AssetsExpolrerWindow()
{
    icon_directory.~Texture();
    icon_file.~Texture();
    icon_model.~Texture();
    icon_shader.~Texture();
    icon_script.~Texture();
    image_icon_file.~Texture();
    icon_scene.~Texture();
    icon_material.~Texture();
}

void AssetsExpolrerWindow::Init(std::string projectPath)
{
	this->projectPath = projectPath;
	current_folder_path = projectPath;
    icon_directory.Load(InputManager::getTEngineExeFolderPath()+ "/EngineRes/gui/assets_explorer/dir_icon.png");
    icon_file.Load(InputManager::getTEngineExeFolderPath() + "/EngineRes/gui/assets_explorer/file_icon.png");
    icon_model.Load(InputManager::getTEngineExeFolderPath() + "/EngineRes/gui/assets_explorer/model_icon.png");
    icon_shader.Load(InputManager::getTEngineExeFolderPath() + "/EngineRes/gui/assets_explorer/shader_icon.png");
    icon_script.Load(InputManager::getTEngineExeFolderPath() + "/EngineRes/gui/assets_explorer/lua_script.png");
    image_icon_file.Load(InputManager::getTEngineExeFolderPath() + "/EngineRes/gui/assets_explorer/image_file_icon.png");
    icon_scene.Load(InputManager::getTEngineExeFolderPath() + "/EngineRes/gui/assets_explorer/scene_file_icon.png");
    icon_material.Load(InputManager::getTEngineExeFolderPath() + "/EngineRes/gui/assets_explorer/material_icon.png");
}
void AssetsExpolrerWindow::BuildDB()
{
    library.clear();
    std::string dirPath = "EngineProjectFolder";

    MoveToDirectory(dirPath);

    if (std::filesystem::exists(dirPath)) {
        for (const auto& entry : std::filesystem::recursive_directory_iterator(dirPath)) {
            std::string path = StringTools::bakcShlashedToSlashes(entry.path().string());
            bool isFile = !std::filesystem::is_directory(path);
            
            std::string re = "(?:/)";
            std::vector<std::string> sliced = StringTools::Split(path,re);
            std::string directory;
            std::string filename;
            for (int i = 0; i < sliced.size(); i++) {
                if (i == sliced.size() - 1)
                    filename = sliced[i];
                else
                    directory += sliced[i] + "/";
            }


            if (isFile) {
                struct AssetData asset;
                asset.directory = directory;
                asset.path = path;
                asset.filename = filename;
                asset.type = getAssetTypeByExtension(filename);
                library.push_back(asset);
                //if(asset.type == ASSET_TYPE_MODEL)
                //std::cout << directory << filename << "\n";
            }
            
        }
    }
    else {
        std::cout << "Assets folder do not exist\n";
    }
}


void AssetsExpolrerWindow::MoveToDirectory(std::string directoryPath)
{
    if (directoryPath == "")return;
    current_folder_path = directoryPath;
    RefreshCurentFolder();
}


void AssetsExpolrerWindow::RefreshCurentFolder()
{
    currentDirContent.clear();
    if (std::filesystem::exists(current_folder_path)) {
        for (const auto& entry : std::filesystem::directory_iterator(current_folder_path)) {
            std::string path = StringTools::bakcShlashedToSlashes(entry.path().string());
            bool isFile = !std::filesystem::is_directory(path);

            std::string re = "(?:/)";
            std::vector<std::string> sliced = StringTools::Split(path, re);
            std::string directory;
            std::string filename;
            for (int i = 0; i < sliced.size(); i++) {
                if (i == sliced.size() - 1)
                    filename = sliced[i];
                else
                    directory += sliced[i] + "/";
            }


             struct AssetData asset;
            if (isFile) {
                asset.directory = directory;
                asset.path = path;
                asset.filename = filename;
                asset.type = getAssetTypeByExtension(filename);
            }
            else {
                asset.directory = path;
                asset.path = path;
                asset.filename = filename;
                asset.type = ASSET_TYPE_DIRECTORY;
            }

            currentDirContent.push_back(asset);
        }
    }
    else {
        std::cout << "Assets folder do not exist\n";
    }
}

void AssetsExpolrerWindow::DrawWindow()
{

    if (newSceneFileWindowOpened) {
        ImGui::Begin("New Scene Creation",&newSceneFileWindowOpened);

        ImGui::Text("Insert file name:");
        ImGui::InputText(".scene", scene_filename, 256);
        ImGui::Separator();
        if (ImGui::Button("Ok")) {
            if (saveScene != nullptr) {
                saveScene((current_folder_path + "/") + scene_filename);
            }
            else {
                std::cout << "ERROR Asset Exprorer :Failed to invoke save scene function|  path:"<< (current_folder_path + "/") + scene_filename << "\n";
            }

            newSceneFileWindowOpened = false;
            RefreshCurentFolder();
        }
        ImGui::SameLine();
        if (ImGui::Button("Cancel")) {
            newSceneFileWindowOpened = false;
        }
        ImGui::End();
    }
    
    if (!opened)return;
    ImGui::SetWindowSize(ImVec2(700, 300), ImGuiCond_FirstUseEver);
	ImGui::Begin("Assets explorer", &opened, ImGuiWindowFlags_MenuBar);
    if (ImGui::BeginMenuBar())
    {
       
            if (ImGui::BeginMenu("File"))
            {
                if (ImGui::MenuItem("Refresh")) {
                    std::string curr_fol = current_folder_path;
                    BuildDB();
                    MoveToDirectory(curr_fol);
                }
                if (ImGui::MenuItem("Close")) opened = false;
                ImGui::EndMenu();
            }
            ImGui::EndMenuBar();
    }


    {//Context menu
        if (ImGui::BeginPopupContextWindow("Context", ImGuiPopupFlags_MouseButtonRight || ImGuiPopupFlags_NoOpenOverItems)) {
            if (ImGui::Button("New Scene File"))
            {
                memset(scene_filename, 0, 256);
                newSceneFileWindowOpened = true;
               
                ImGui::CloseCurrentPopup();
            }

            
            ImGui::EndPopup();
        }
    }//End of context menu


    //left side with folders
    { 
        if (ImGui::Button("<-")) {
            MoveToDirectory(StringTools::getUpperFolderPath(current_folder_path));
        }
        ImGui::SameLine();
        ImGui::Text(current_folder_path.c_str());
        ImGui::BeginChild("Folders", ImVec2(150, 0), true,ImGuiWindowFlags_HorizontalScrollbar);
        
       

        if (ImGui::Selectable("...", true, ImGuiSelectableFlags_AllowDoubleClick))
            if (ImGui::IsMouseDoubleClicked(0))
                MoveToDirectory(StringTools::getUpperFolderPath(current_folder_path));

        for (struct AssetData ad : currentDirContent) {
            
            if(ad.type == ASSET_TYPE_DIRECTORY)
            if(ImGui::Selectable(ad.filename.c_str(), false, ImGuiSelectableFlags_AllowDoubleClick))
                if (ImGui::IsMouseDoubleClicked(0)) {
                    MoveToDirectory(ad.path);
                    break;
                }
                
            
        }


        ImGui::EndChild();
    }

    int iterator = 0;
    {//Right side with files
        ImGui::SameLine();
        ImGui::BeginChild("Files");

        ImVec2 buttonSize (75,75);
        float window_visible_x2 =ImGui::GetContentRegionMax().x;
        int padding = 25;
        int collumn_size = buttonSize.x + padding;

        int column_num = window_visible_x2 / collumn_size;
        if (column_num < 1)column_num = 1;
        ImGui::Columns(column_num,"",false);

        for (int i = 0;i < currentDirContent.size();i++) {
            struct AssetData ad = currentDirContent[i];
           
            GLuint textureID = icon_file.getID();
            if (ad.type == ASSET_TYPE_DIRECTORY)textureID = icon_directory.getID();
            if (ad.type == ASSET_TYPE_MODEL)textureID = icon_model.getID();
            if (ad.type == ASSET_TYPE_SHADER)textureID = icon_shader.getID();
            if (ad.type == ASSET_TYPE_LUA_SCRIPT)textureID = icon_script.getID();
            if (ad.type == ASSET_TYPE_TEXTURE)textureID = image_icon_file.getID();
            if (ad.type == ASSET_TYPE_LEVEL)textureID = icon_scene.getID();
            if (ad.type == ASSET_TYPE_MATERIAL)textureID = icon_material.getID();

            ImGui::PushID(iterator++);
            ImGui::ImageButton((ImTextureID)textureID, buttonSize);
            if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(0)) {
                if (ad.type == ASSET_TYPE_DIRECTORY) {
                    MoveToDirectory(ad.path);
                    ImGui::PopID();
                    break;
                }
                if (ad.type == ASSET_TYPE_LEVEL) {
                    loadScene(ad.path);
                }
            }
            if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_None)) {
                struct AssetData* pointer = &currentDirContent[i];
                ImGui::SetDragDropPayload("ASSET", &pointer, sizeof(struct AssetData*), ImGuiCond_Once);
                std::string name_construction = "ASSET:" + ad.filename;
                ImGui::ImageButton((ImTextureID)textureID, buttonSize);
                ImGui::Text(name_construction.c_str());
                //std::cout << " send model:" << ad.path << "\n" << ad.directory << "\n" << ad.filename << "\n";

                ImGui::EndDragDropSource();
            }
            ImGui::PopID();
            
            
            ImGui::TextWrapped(ad.filename.c_str());
           
            
            ImGui::NextColumn();
        }
        ImGui::Columns(1);

        ImGui::EndChild();
    }

	ImGui::End();
}
/*
#define ASSET_TYPE_TEXT 0
#define ASSET_TYPE_TEXTURE 1
#define ASSET_TYPE_SHADER 2
#define ASSET_TYPE_AUDIOFILE 3
#define ASSET_TYPE_LEVEL 4
#define ASSET_TYPE_MODEL 5
#define ASSET_TYPE_MATERIAL 6
#define ASSET_TYPE_ANIMATION 7
#define ASSET_TYPE_PREFAB 8*/

unsigned int AssetsExpolrerWindow::getAssetTypeByExtension(std::string path)
{
    if (StringTools::endWith(path, ".txt"))return ASSET_TYPE_TEXT;
    if (StringTools::endWith(path, ".png") || StringTools::endWith(path, ".jpg") || StringTools::endWith(path, ".jpeg") || StringTools::endWith(path, ".bmp") || StringTools::endWith(path, ".hdr"))return ASSET_TYPE_TEXTURE;
    if (StringTools::endWith(path, ".glsl"))return ASSET_TYPE_SHADER;
    if (StringTools::endWith(path, ".wav") || StringTools::endWith(path, ".mp3") || StringTools::endWith(path, ".flac") || StringTools::endWith(path, ".ogg"))return ASSET_TYPE_AUDIOFILE;
    if (StringTools::endWith(path, ".lvldat") || StringTools::endWith(path, ".scene"))return ASSET_TYPE_LEVEL;
    if (StringTools::endWith(path, ".obj") || StringTools::endWith(path, ".fbx"))return ASSET_TYPE_MODEL;
    if (StringTools::endWith(path, ".mat"))return ASSET_TYPE_MATERIAL; 
    if (StringTools::endWith(path, ".anim"))return ASSET_TYPE_ANIMATION;
    if (StringTools::endWith(path, ".lua"))return ASSET_TYPE_LUA_SCRIPT;
    if (StringTools::endWith(path, ".prefobj"))return ASSET_TYPE_PREFAB;
    return ASSET_TYPE_OTHER;
}

