#pragma once
#ifndef INPUT_MANAGER_H
#define INPUT_MANAGER_H

#include <string>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

///Struktura przechowywuj�ca informacje o u�wyanych w projekcie klawiszach
///
/// @param string key - nazwa wej�cia
/// @param unsigned int push - id klawisza z biliotegi GLFW3
/// @see InputManager
struct KeyBinding {
	std::string key;
	unsigned int push;
};

/// Klasa singleton odczytuje wej�cia z klawiatury, myszy i pozwala na swobodny dost�p do informacji o stanie tych wej��
///
/// Dodatkowo klasa ta zawiera informacje z r�nych cz�ciach projekt�w np rozmiar okna , �cie�ka do projektu czy pliku exe silnika
class InputManager {
public:
	InputManager();
	~InputManager();

	void Init(GLFWwindow* window);
	void Cleanup();

	void Update();

	static bool pressMouseButton(int button);

	static glm::vec2 setMouseFPSMouseMode(bool fpsModeEnabled);
	static glm::vec2 getMousePos();
	static bool getMouseLPM();

	static bool getKey(int key);
	static bool getKeyOnce(int key);

	//blackboard ? TODO: definitly remove this !!! Now its just for test
	static int getWindowWidth();
	static int getWindowHeight();
	static std::string getTEngineExeFolderPath();
	void setTEngineExeFolderPath(std::string path);


	static int getCurrentFPS();
	static void setCurrentFPS(int fps_value);

	static double getMainLoopTimeProfiler();
	static void setMainLoopTimeProfiler(double mainLoopTimeProfiler);

private:
	
	

};
#endif // !INPUT_MANAGER_H
