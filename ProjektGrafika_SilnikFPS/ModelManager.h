#ifndef MODEL_MANAGER_H
#define MODEL_MANAGER_H

#include <vector>
#include "Model.h"

/// Struktura przechwywuj�ca informacje o za�adowanych modelach oraz obiektach kt�re go u�ywaj�
///
/// @param Model* model - wska�nik na za�adowany model
/// @param string path - �cie�ka do pliku modelu
/// @param std::vector<void*> usedIn - lista obiekt�w kt�re u�ywaj� modelu
/// @see ModelManager Model Mesh Texture TextureManager
struct ModelRecord {
	Model* model;
	std::string path;
	std::vector<void*> usedIn;
};

/// Zarz�dza �adowaniem modeli i zapobiega redundancji w �adowaniu. Oszcz�dza to czas �adowania jak i pami��
class ModelManager {
public:
	ModelManager();
	~ModelManager();

	/// Sprawdza czy model jest ju� za�adowany je�li tak zwraca struct ModelRecord, je�li nie to zwraca nullptr
	static struct ModelRecord* isModelAlreadyLoadet(std::string path,void* model_renderer_pointer);
	/// Dodaje model do rejestru
	static struct ModelRecord* addModelToRecord(std::string path, Model* t, void* model_renderer_pointer);

	///Powiadamia rejestr o usuni�ciu obiektu kt�ry u�ywa� tego modelu
	static void deleteNotification(struct ModelRecord* modelrec,void* model_renderer_pointer);
	/// Je�li �aden obiekt nie u�ywa ju� kt�rego� z modeli to zostaje on usuni�ty w tej funkcji
	static void deleteUnusedModels();
	/// Usuwa wszystkie modele
	void deleteAllModels();

};

#endif // !MODEL_MANAGER_H

