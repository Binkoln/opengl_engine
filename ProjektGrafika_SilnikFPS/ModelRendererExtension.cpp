#include "Object.h"

namespace ModelRenderer_Blackboard {
    Shader shad;
    Shader forward_shader;
    Shader directionalLightDepthRenderShader;
}
void ModelRendererExtension::LoadGlobalShader()
{
    ModelRenderer_Blackboard::shad           = Shader("EngineProjectFolder/assets/shaders/deffered_v_b_s1.glsl", "EngineProjectFolder/assets/shaders/deffered_f_b_s1.glsl");
    ModelRenderer_Blackboard::forward_shader = Shader("EngineProjectFolder/assets/shaders/forward_v_b_s2.glsl", "EngineProjectFolder/assets/shaders/forward_f_b_s2.glsl");
    ModelRenderer_Blackboard::directionalLightDepthRenderShader = Shader("EngineProjectFolder/assets/shaders/directional_light_depth_v_b.glsl", "EngineProjectFolder/assets/shaders/directional_light_depth_f_b.glsl");

}

void ModelRendererExtension::DeleteGlobalShader()
{
    ModelRenderer_Blackboard::shad.Delete();
    ModelRenderer_Blackboard::forward_shader.Delete();
    ModelRenderer_Blackboard::directionalLightDepthRenderShader.Delete();
}

ModelRendererExtension::ModelRendererExtension():Extension("Model Renderer")
{
    modelRecord = nullptr;
   

    //testTex.Load("EngineProjectFolder/assets/textures/stone_normal.jpg");
    /*testTex.Init(512, 512, 1.0f, 1.0f, 1.0f, 1.0f);

    for (int i = 0; i < 50; i++) {

        for (int y = 0; y < 512; y++) {
            testTex.Pixel(i * 10, y, 0, 0, 0, 1);
            testTex.Pixel(1+i * 10, y, 0, 0, 0, 1);
            testTex.Pixel(2 + i * 10, y, 0, 0, 0, 1);
        }

    }
    testTex.UploadToGPU();*/

    type = "ModelRendererExtension";
    toDelete = false;
}

ModelRendererExtension::~ModelRendererExtension()
{
    Cleanup();
}

Extension* ModelRendererExtension::MakeCopy()
{
    ModelRendererExtension* mre = new ModelRendererExtension();
    mre->enabled = enabled;
    mre->LoadModel(modelName, modelPath);

    return mre;
}

tinyxml2::XMLElement* ModelRendererExtension::SaveScene(tinyxml2::XMLElement* e)
{
    using namespace tinyxml2;
    XMLElement* extension = Extension::SaveScene(e);
    extension->InsertNewChildElement("IsModelSet")->InsertNewText((modelRecord == nullptr)?"0.0":"1.0");
    extension->InsertNewChildElement("ModelName")->InsertNewText(modelName.c_str());
    extension->InsertNewChildElement("ModelPath")->InsertNewText(modelPath.c_str());
    extension->InsertNewChildElement("Transparent")->InsertNewText((transparent) ? "1.0" : "0.0");

    return extension;
}

void ModelRendererExtension::Init()
{

}
void ModelRendererExtension::Cleanup()
{
    if (staticLoadedModel) {//delete previous loaded model
        DeleteStaticModel();
    }
    else if (modelRecord != nullptr) {
        ModelManager::deleteNotification(modelRecord, (void*)this);
        modelRecord = nullptr;
    }
}
void ModelRendererExtension::Update(Object* o, Level* level)
{
   
}
void ModelRendererExtension::Render(Object* o, Level* level)
{
    //if (level->showCollisions)return;
    if (((level->defferedPass && transparent) || (!level->defferedPass && !transparent)) && !level->lightPass)return;

    Camera* camera = level->getActiveCamera();
    if (camera == nullptr)return;
    if (modelRecord == nullptr)return;
    
    if (depthTest)
        glEnable (GL_DEPTH_TEST);
    else
        glDisable(GL_DEPTH_TEST);

    
    //depends on material 
    glEnable(GL_CULL_FACE);

   
    //calc objectIdColor
    int id = o->getID();
    float r = id % 1024;
    float g = (id / 1024)% 1024;
    //float b = (id / (255 * 255)); now its for mesh id
    glm::vec3 objectIdColor = { r / 1024.0f,g / 1024.0f,0 };


    Model* model = getModel();

    glm::mat4 worldSpaceMatrix = o->getWorldSpaceTransformationMatrix();

    if (level->lightPass && level->tmp_dri_light != nullptr) {
        glm::mat4 testProj = camera->getProjection() * camera->getViewMatrix();
        level->forwardRenderer->RenderModelToDepthMap( &ModelRenderer_Blackboard::directionalLightDepthRenderShader, model, worldSpaceMatrix, level->tmp_dri_light, testProj);
    }
    else if (transparent) {
        //Forward rendered
        level->forwardRenderer->QueueModelForRender(o,&ModelRenderer_Blackboard::forward_shader, model, worldSpaceMatrix, camera, objectIdColor);
    }
    else {
        glDisable(GL_BLEND);
        //Deffered renderer
        level->defferedRenderer->RenderModel(&ModelRenderer_Blackboard::shad, model, worldSpaceMatrix,prev_worldSpaceMatrix, camera, objectIdColor, sqrt(dissapear)* sqrt(dissapear));
        prev_worldSpaceMatrix = o->getWorldSpaceTransformationMatrix();
    }
    
    
}

void ModelRendererExtension::DeleteStaticModel() {
    if (!staticLoadedModel) return;
    if (modelRecord == nullptr) return;
    if (modelRecord->model == nullptr) return;
    delete modelRecord->model;
    delete modelRecord;
    modelRecord = nullptr;
}

void ModelRendererExtension::LoadModel(std::string modelName, std::string modelPath, bool staticLoad)
{
    if (staticLoadedModel) {//delete previous loaded model
        DeleteStaticModel();
    }


    staticLoadedModel = staticLoad;
    if (!staticLoadedModel) {
        if (modelRecord != nullptr) {
            ModelManager::deleteNotification(modelRecord, (void*)this);
            modelRecord = nullptr;
        }

        modelRecord = ModelManager::isModelAlreadyLoadet(modelPath, (void*)this);

        if (modelRecord == nullptr) {
            Model* model = new Model();

            if (!model->LoadModel(modelPath)) {
                std::cout << "Model loading failed!!!\n";
            }
            modelRecord = ModelManager::addModelToRecord(modelPath, model, (void*)this);
        }
    }
    else {
        //static load
        Model* model = new Model();

        if (!model->LoadModel(modelPath)) {
            std::cout << "Model loading failed!!!\n";
        }

        modelRecord = new ModelRecord;
        modelRecord->model = model;
        modelRecord->path = modelPath;
        modelRecord->usedIn.push_back((void*)this);
    }
        
    this->modelName = std::string(modelName);
    this->modelPath = std::string(modelPath);
}

void ModelRendererExtension::DrawImGui(Object* parent)
{
	if (ImGui::CollapsingHeader(name.c_str(), ImGuiTreeNodeFlags_DefaultOpen)) {
	Extension::DrawImGui(parent);
    
        
        

        if(modelRecord == nullptr)
            modelName = "Model: No model";

        ImGui::TextWrapped(("Model: " + modelName).c_str(), modelRecord == nullptr, 0,ImVec2(300,30));


            if (ImGui::BeginDragDropTarget()) {
                const ImGuiPayload* pyload = ImGui::AcceptDragDropPayload("ASSET", ImGuiDragDropFlags_None);
                if (pyload != NULL) {

                    struct AssetData* recived = *((struct AssetData**)(pyload->Data));
                    if (recived->type == ASSET_TYPE_MODEL) {
                        //std::cout << " recived model:" << recived->path<<"\n"<< recived->directory << "\n"<< recived->filename << "\n";
                        LoadModel(recived->filename, recived->path);
                        
                        
                    }
                }

                ImGui::EndDragDropTarget();
            }

			


        ImGui::Checkbox("DepthTest", &depthTest);
        ImGui::Checkbox("Transparent", &transparent);
        ImGui::SliderFloat("Dissolve", &dissapear, 0, 1);
        
      
        

        //meshed and Materials
        if(modelRecord != nullptr)
        if (ImGui::TreeNode("Meshes")) {
            Model* model = modelRecord->model;
            for (int mesh_i = 0; mesh_i < model->meshes.size(); mesh_i++) {
                Mesh* mesh = model->meshes[mesh_i];
                ImGui::PushID(mesh_i);
                if (ImGui::TreeNode((StringTools::intToString(mesh_i)+ " : " + mesh->getName()).c_str())) {

                    //Show Mesh material
                    Material* mat = &mesh->material;

                    if (ImGui::Button("Sejw")) {
                        mat->SaveToXML("EngineProjectFolder/testMaterjal.mat");
                    }

                    if (ImGui::Button("Lo�d")) {
                        mat->LoadFromXML("EngineProjectFolder/testMaterjal.mat");
                    }

                    for (int j = 0; j < mat->uniformList.size();j++) {
                        ImGui::PushID(j + (mesh_i+1)*2);//zapewniamy infywidualno�� sliderk�w
                        UniformValue* uv = mat->uniformList[j];
                        if (uv->type == UNIFORM_TYPE_TEXTURE) {

                            ImGui::Text("Uniform type: Texture");
                            ImGui::Text(("Name:" + uv->texture_name).c_str());
                            ImGui::Text(("SP name:" + uv->sh_pram_name).c_str());
                            Texture* tx = uv->tex;
                            if (tx != nullptr) {
                                if (tx->isLoaded()) {
                                    ImGui::Text(("Texture id:" + StringTools::intToString(tx->getID()) ).c_str() );
                                    ImGui::Image((ImTextureID)tx->getID(), { 64,64 }, { 0,0 }, { 1,1 });
                                }
                            }

                            if (ImGui::Button(("Remove " + StringTools::intToString(j) ).c_str())) {
                                delete uv;
                                mat->uniformList.erase(mat->uniformList.begin() + j);
                                j--;
                                //continue;
                            }
                            ImGui::Separator();
                        }else
                        if (uv->type == UNIFORM_TYPE_VECTOR3) {

                            ImGui::Text("Uniform type: Vector 3");
                            ImGui::Text(("Name:" + uv->sh_pram_name).c_str());
                            ImGui::DragFloat3("Value:", &uv->data[0],0.01f);

                            if (ImGui::Button("Remove")) {
                                ///HEHEHE
                            }
                            ImGui::Separator();
                        }
                        else
                        if (uv->type == UNIFORM_TYPE_FLOAT) {

                            ImGui::Text("Uniform type: Float");
                            ImGui::Text(("Name:" + uv->sh_pram_name).c_str());
                            ImGui::SliderFloat("Value:", &uv->data.x,0.0f,1.0f);

                            if (ImGui::Button("Remove")) {
                                ///HEHEHE
                            }
                            ImGui::Separator();
                        }

                        ImGui::PopID();
                    }


                    ImGui::Text("Drop diffuse texture here...");
                    if (ImGui::BeginDragDropTarget()) {
                        const ImGuiPayload* pyload = ImGui::AcceptDragDropPayload("ASSET", ImGuiDragDropFlags_None);
                        if (pyload != NULL) {

                            struct AssetData* recived = *((struct AssetData**)(pyload->Data));
                            if (recived->type == ASSET_TYPE_TEXTURE) {
                                std::cout << "Recived texture:" << recived->path << "\n" << recived->directory << "\n" << recived->filename << "\n";
                                //LoadModel(recived->filename, recived->path);
                                mat->AddTexture(recived->path, aiTextureType_DIFFUSE);
                            }
                        }
                        ImGui::EndDragDropTarget();
                    }

                    ImGui::Text("Drop normal texture here...");
                    if (ImGui::BeginDragDropTarget()) {
                        const ImGuiPayload* pyload = ImGui::AcceptDragDropPayload("ASSET", ImGuiDragDropFlags_None);
                        if (pyload != NULL) {

                            struct AssetData* recived = *((struct AssetData**)(pyload->Data));
                            if (recived->type == ASSET_TYPE_TEXTURE) {
                                std::cout << "Recived texture:" << recived->path<<"\n"<< recived->directory << "\n"<< recived->filename << "\n";
                                //LoadModel(recived->filename, recived->path);
                                mat->AddTexture(recived->path, aiTextureType_NORMALS);
                            }
                        }
                        ImGui::EndDragDropTarget();
                    }

                    ImGui::Text("Drop specular texture here...");
                    if (ImGui::BeginDragDropTarget()) {
                        const ImGuiPayload* pyload = ImGui::AcceptDragDropPayload("ASSET", ImGuiDragDropFlags_None);
                        if (pyload != NULL) {

                            struct AssetData* recived = *((struct AssetData**)(pyload->Data));
                            if (recived->type == ASSET_TYPE_TEXTURE) {
                                std::cout << "Recived texture:" << recived->path << "\n" << recived->directory << "\n" << recived->filename << "\n";
                                //LoadModel(recived->filename, recived->path);
                                mat->AddTexture(recived->path, aiTextureType_SPECULAR);
                            }
                        }
                        ImGui::EndDragDropTarget();
                    }

                    ImGui::TreePop();
                }
                ImGui::PopID();

            }//For per mesh

            ImGui::TreePop();
        }//Meshed tree

	}
	ImGui::Separator();
}