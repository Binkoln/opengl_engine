#include "Object.h"


SpriteRendererExtension::SpriteRendererExtension():Extension("SpriteRendererExtension")
{
	type = "SpriteRenderer";
	sprite_texture = nullptr;
	spriteRenderer_name = "SpriteRenderer";
	spritePosSiz = glm::vec4(0, 0, 1, 1);
	sprite_color = glm::vec3(1, 1, 1);
}

SpriteRendererExtension::~SpriteRendererExtension()
{}

tinyxml2::XMLElement* SpriteRendererExtension::SaveScene(tinyxml2::XMLElement* e)
{
	using namespace tinyxml2;
	XMLElement* extension = Extension::SaveScene(e);
	extension->InsertNewChildElement("SpriteRendererName")->InsertNewText(spriteRenderer_name.c_str());
	extension->InsertNewChildElement("IsTexture")->InsertNewText((sprite_texture == nullptr) ? "0.0" : "1.0");
	extension->InsertNewChildElement("TexturePath")->InsertNewText(texture_path.c_str());
	extension->InsertNewChildElement("SpritePosSiz")->InsertNewText(StringTools::vec4ToString(spritePosSiz).c_str());
	extension->InsertNewChildElement("SpriteColor")->InsertNewText(StringTools::vec3ToString(sprite_color).c_str());

	return extension;
}


void SpriteRendererExtension::Init()
{}

void SpriteRendererExtension::Cleanup()
{}

void SpriteRendererExtension::Update(Object* o, Level* level)
{

}

Extension* SpriteRendererExtension::MakeCopy()
{
	SpriteRendererExtension* ret = new SpriteRendererExtension();
	ret->setPosSizeVector(spritePosSiz);
	ret->setColorMultiplier(sprite_color);
	ret->setTexture(sprite_texture);
	return ret;
}


void SpriteRendererExtension::setTexture(std::string path)
{
	struct TextureRecord* tr = TextureManager::isTextureAlreadyLoadet(path);
	if (tr == nullptr) {
		sprite_texture = new Texture();
		sprite_texture->Load(path);
		TextureManager::addTextureToRecord(path, sprite_texture);
	}
	else {
		this->sprite_texture = tr->texture;
	}
	this->texture_path = path;
	this->texture_name = StringTools::getFileName(path);

}

void SpriteRendererExtension::Render(Object* o, Level* level)
{
	if (!level->forwardRenderer)return;
	if ( level->getActiveCamera() != nullptr) {
		glm::mat4 viewProj = level->getActiveCamera()->getProjection() * level->getActiveCamera()->getViewMatrix();
		if(cameraViewer)
			level->getRenderer2D()->RenderSprite(level->finalFB->textureColorID,true, glm::vec4(0,0, 1,1), sprite_color, o->getWorldSpaceTransformationMatrix(), viewProj);
		else
			level->getRenderer2D()->RenderSprite(sprite_texture, spritePosSiz,sprite_color,o->getWorldSpaceTransformationMatrix(), viewProj);
	
	}

}

void SpriteRendererExtension::DrawImGui(Object* parent)
{

	if (ImGui::CollapsingHeader(name.c_str(), ImGuiTreeNodeFlags_DefaultOpen)) {


		Extension::DrawImGui(parent);

		std::string texName = "No texture";
		if (sprite_texture != nullptr) 
			texName = texture_name;
		

		ImGui::TextWrapped(("Texture: " + texName).c_str(), sprite_texture == nullptr, 0, ImVec2(300, 30));


		if (ImGui::BeginDragDropTarget()) {
			const ImGuiPayload* pyload = ImGui::AcceptDragDropPayload("ASSET", ImGuiDragDropFlags_None);
			if (pyload != NULL) {

				struct AssetData* recived = *((struct AssetData**)(pyload->Data));
				if (recived->type == ASSET_TYPE_TEXTURE) {
					setTexture(recived->path);

				}
			}

			ImGui::EndDragDropTarget();
		}
	}

	float colorPicker[] = { sprite_color.r,sprite_color.g,sprite_color.b };
	ImGui::ColorPicker3("Color", colorPicker, 0);
	sprite_color.r = colorPicker[0];
	sprite_color.g = colorPicker[1];
	sprite_color.b = colorPicker[2];


	ImGui::Checkbox("Camera Viewer", &cameraViewer);

	ImGui::Separator();
}
