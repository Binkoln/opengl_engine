#include "Model.h"

Model::Model()
{

}

Model::~Model()
{
	for (Mesh* m:meshes) {
		delete m;
	}
	meshes.clear();
}

bool Model::LoadModel(std::string path)
{
	Assimp::Importer importer;

	const aiScene* scene = importer.ReadFile(path, aiProcess_CalcTangentSpace |
		aiProcess_Triangulate | aiProcess_JoinIdenticalVertices);

	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
		std::cout << importer.GetErrorString() << std::endl;
		return false;
	}

	aiNode* rootNode = scene->mRootNode;
	DoSceneProcessing(rootNode,scene, path);
	isloaded = true;
	return true;
}


void Model::DoSceneProcessing(aiNode* node,const aiScene* scene, std::string modelpath)
{
	

	if (scene->HasMeshes()) {
		int meshes_count = node->mNumMeshes;
		
		for (int ii = 0; ii < meshes_count;ii++) {
			aiMesh* mesh = scene->mMeshes[node->mMeshes[ii]];
			DoProcessMesh(mesh, scene, modelpath);


			
		}
		

		for (unsigned int i = 0; i < node->mNumChildren; i++)
		{
			DoSceneProcessing(node->mChildren[i], scene, modelpath);
		}

	}
}

void Model::DoProcessMesh(aiMesh* mesh, const aiScene *scene, std::string modelpath)
{
	
	std::vector<struct MeshVertex> meshVertices;
	std::vector<unsigned int> meshIndieces;

	for (unsigned int j = 0; j < mesh->mNumVertices; j++) {
		aiVector3D vertex = mesh->mVertices[j];
		aiVector3D nrm = mesh->mNormals[j];
		aiVector3D tan = mesh->mTangents[j];
		aiVector3D texCoords = mesh->mTextureCoords[0][j];


		glm::vec3 position(vertex.x, vertex.y, vertex.z);
		glm::vec3 normal(nrm.x, nrm.y, nrm.z);
		glm::vec3 tangent(tan.x, tan.y, tan.z);
		glm::vec3 bitangent = glm::cross(normal, tangent);
		glm::vec2 UV(texCoords.x, texCoords.y);


		struct MeshVertex mv;
		mv.position = position;
		mv.UV = UV;
		mv.normal = normal;
		mv.tangent = tangent;
		mv.bitangent = bitangent;
		//todo: Add support for bones
		mv.boneWeights = glm::vec3(0);
		mv.boneIDs = glm::ivec3(0);

		meshVertices.push_back(mv);

	}//end of adding Mesh vertex structs to meshVertices


	for (unsigned int j = 0; j < mesh->mNumFaces; j++) {
		aiFace face = mesh->mFaces[j];

		for (unsigned int i = 0; i < face.mNumIndices; i++)
			meshIndieces.push_back(face.mIndices[i]);

	}

	
	aiString name = mesh->mName;
	Mesh* newMesh = new Mesh(name.C_Str());

	newMesh->LoadMesh(meshVertices, meshIndieces, mesh->mMaterialIndex);

	if (mesh->mMaterialIndex >= 0) {
		aiMaterial* mat = scene->mMaterials[mesh->mMaterialIndex];
		std::string modelFolder = StringTools::getUpperFolderPath(modelpath);
		
		
		/* Foat-Vector values */
		

		//std::cout << "NRM CNT" << normal_material_count << "\n";


		aiColor3D color(0.f, 0.f, 0.f);
		auto rt = mat->Get(AI_MATKEY_COLOR_DIFFUSE, color);
		if (rt == aiReturn_SUCCESS) newMesh->material.AddShadingVector3Value("u_albedoColor", { color.r, color.g, color.b });

		rt = mat->Get(AI_MATKEY_COLOR_AMBIENT, color);
		if (rt == aiReturn_SUCCESS) newMesh->material.AddShadingFloatValue("u_roughness_value", (float)color.r);
		
		
		
		/*  Textures */
		
		int diffuse_material_count = mat->GetTextureCount(aiTextureType_DIFFUSE);

		for (int k = 0; k < diffuse_material_count;k++) {
			aiString path;
			mat->GetTexture(aiTextureType_DIFFUSE, k, &path);

			newMesh->material.AddTexture(modelFolder+"/" + StringTools::bakcShlashedToSlashes(path.C_Str()), aiTextureType_DIFFUSE);
		}




		int normal_material_count = mat->GetTextureCount(aiTextureType_HEIGHT);
		//std::cout << "NRM CNT" << normal_material_count << "\n";
		for (int k = 0; k < normal_material_count; k++) {
			aiString path;
			mat->GetTexture(aiTextureType_HEIGHT, k, &path);

			newMesh->material.AddTexture(modelFolder + "/" + StringTools::bakcShlashedToSlashes(path.C_Str()), aiTextureType_NORMALS);
		}

		//XD z INNEGO FBX
		normal_material_count = mat->GetTextureCount(aiTextureType_NORMALS);
		//std::cout << "NRM CNT" << normal_material_count << "\n";
		for (int k = 0; k < normal_material_count; k++) {
			aiString path;
			mat->GetTexture(aiTextureType_NORMALS, k, &path);

			newMesh->material.AddTexture(modelFolder + "/" + StringTools::bakcShlashedToSlashes(path.C_Str()), aiTextureType_NORMALS);
		}

		/*std::cout << "==========================\n";
		int Test_material_count = mat->GetTextureCount(aiTextureType_AMBIENT);
		std::cout << "aiTextureType_AMBIENT CNT" << Test_material_count << "\n";
		Test_material_count = mat->GetTextureCount(aiTextureType_AMBIENT_OCCLUSION);
		std::cout << "aiTextureType_AMBIENT_OCCLUSION  CNT" << Test_material_count << "\n";
		Test_material_count = mat->GetTextureCount(aiTextureType_BASE_COLOR);
		std::cout << "aiTextureType_BASE_COLOR CNT" << Test_material_count << "\n";
		Test_material_count = mat->GetTextureCount(aiTextureType_DIFFUSE);
		std::cout << "aiTextureType_DIFFUSE CNT" << Test_material_count << "\n";
		Test_material_count = mat->GetTextureCount(aiTextureType_DIFFUSE_ROUGHNESS);
		std::cout << "aiTextureType_DIFFUSE_ROUGHNESS CNT" << Test_material_count << "\n";
		Test_material_count = mat->GetTextureCount(aiTextureType_DISPLACEMENT);
		std::cout << "aiTextureType_DISPLACEMENT CNT" << Test_material_count << "\n";
		Test_material_count = mat->GetTextureCount(aiTextureType_EMISSION_COLOR);
		std::cout << "aiTextureType_EMISSION_COLOR CNT" << Test_material_count << "\n";
		Test_material_count = mat->GetTextureCount(aiTextureType_EMISSIVE);
		std::cout << "aiTextureType_EMISSIVE CNT" << Test_material_count << "\n";
		Test_material_count = mat->GetTextureCount(aiTextureType_HEIGHT);
		std::cout << "aiTextureType_HEIGHT CNT" << Test_material_count << "\n";
		Test_material_count = mat->GetTextureCount(aiTextureType_LIGHTMAP);
		std::cout << "aiTextureType_LIGHTMAP CNT" << Test_material_count << "\n";
		Test_material_count = mat->GetTextureCount(aiTextureType_METALNESS);
		std::cout << "aiTextureType_METALNESS CNT" << Test_material_count << "\n";
		Test_material_count = mat->GetTextureCount(aiTextureType_NONE);
		std::cout << "aiTextureType_NONE CNT" << Test_material_count << "\n";
		Test_material_count = mat->GetTextureCount(aiTextureType_NORMALS);
		std::cout << "aiTextureType_NORMALS CNT" << Test_material_count << "\n";
		Test_material_count = mat->GetTextureCount(aiTextureType_NORMAL_CAMERA);
		std::cout << "aiTextureType_NORMAL_CAMERA CNT" << Test_material_count << "\n";
		Test_material_count = mat->GetTextureCount(aiTextureType_OPACITY);
		std::cout << "aiTextureType_OPACITY CNT" << Test_material_count << "\n";
		Test_material_count = mat->GetTextureCount(aiTextureType_REFLECTION);
		std::cout << "aiTextureType_REFLECTION CNT" << Test_material_count << "\n";
		Test_material_count = mat->GetTextureCount(aiTextureType_SHININESS);
		std::cout << "aiTextureType_SHININESS CNT" << Test_material_count << "\n";
		Test_material_count = mat->GetTextureCount(aiTextureType_SPECULAR);
		std::cout << "aiTextureType_SPECULAR CNT" << Test_material_count << "\n";
		Test_material_count = mat->GetTextureCount(aiTextureType_UNKNOWN);
		std::cout << "aiTextureType_UNKNOWN CNT" << Test_material_count << "\n";*/
		//AI_MATKEY_PB

		int specular_material_count = mat->GetTextureCount(aiTextureType_AMBIENT);
		//std::cout << "SPC CNT" << specular_material_count << "\n";
		for (int k = 0; k < specular_material_count; k++) {
			aiString path;
			mat->GetTexture(aiTextureType_AMBIENT, k, &path);

			newMesh->material.AddTexture(modelFolder + "/" + StringTools::bakcShlashedToSlashes(path.C_Str()), aiTextureType_SPECULAR);
		}

		//XD z innego FBX
		specular_material_count = mat->GetTextureCount(aiTextureType_SHININESS);
		//std::cout << "SPC CNT" << specular_material_count << "\n";
		for (int k = 0; k < specular_material_count; k++) {
			aiString path;
			mat->GetTexture(aiTextureType_SHININESS, k, &path);

			newMesh->material.AddTexture(modelFolder + "/" + StringTools::bakcShlashedToSlashes(path.C_Str()), aiTextureType_SPECULAR);
		}

	}


	meshes.push_back(newMesh);

	meshVertices.clear();
	meshIndieces.clear();
}



physx::PxTriangleMesh* Model::getConsolidatedPhysxTriangleMeshes(physx::PxCooking* cooking, physx::PxScene* scene, glm::vec3 scale)
{
	//Consolidating meshes
	auto vertices = std::vector<physx::PxVec3>();
	auto indieces = std::vector<physx::PxU32>();
	unsigned int offset = 0;

	for (Mesh* mesh : meshes) {
		offset = vertices.size();
		auto meshVertices = mesh->getVertices();
		for (MeshVertex mv : meshVertices) {
			auto pos = mv.position;
			vertices.push_back(physx::PxVec3(pos.x * scale.x, pos.y * scale.y, pos.z * scale.z));
		}

		auto meshIndieces = mesh->getIndieces();
		for (unsigned int indiece : meshIndieces) {
			indieces.push_back(physx::PxU32(indiece + offset));
		}

	}

	//Triangles mesh description
	physx::PxTriangleMeshDesc meshDesc;
	meshDesc.points.count = vertices.size();
	meshDesc.points.stride = sizeof(physx::PxVec3);
	meshDesc.points.data = &vertices[0];

	meshDesc.triangles.count = indieces.size();
	meshDesc.triangles.stride = 3 * sizeof(physx::PxU32);
	meshDesc.triangles.data = &indieces[0];

	physx::PxTriangleMesh* retVal = cooking->createTriangleMesh(meshDesc, scene->getPhysics().getPhysicsInsertionCallback());

	return retVal;
}