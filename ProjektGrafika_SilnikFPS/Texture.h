#ifndef TEXTURE_H
#define TEXTURE_H

#include <GL/glew.h>
#include <GL/GL.h>
#include <string>
#include <iostream>
#include <glm/glm.hpp>

#define TEXTURE_FILTER_PIXEL 1
#define TEXTURE_FILTER_LINEAR 2
/// Klasa pozwala za�adowa� teksture z pliku b�d� wygenerowa� now� za pomoc� funkcji opisanych w instrukcjach laboratoryjnych

class Texture {
public:
	Texture();
	~Texture();

	inline bool isLoaded() { return loadet; }

	/// �aduje teksture z pliku
	void Load(std::string path,unsigned int filter_type);
	inline void Load(std::string path) { Load(path, TEXTURE_FILTER_LINEAR); }
	/// �aduje teksture z pliku o formacie HDR
	void LoadHDR(std::string path);
	///�aduje texture tylko do ram
	void BufferedLoad(std::string path);
	/// U�ywa tej tekstury
	inline void Bind() { glBindTexture(GL_TEXTURE_2D,texID); }
	/// Zwraca teksture
	inline GLuint getID() { return texID; }

	//drawing functions
	///Twozr bitmap� i inicjalizuje j� ca�� na zadany kolor
	void Init(int widt, int height, float r, float g, float b, float a);
	/// Czy�ci ca�� bitmap� na zadany kolor
	void ClearColor(float r, float g, float b, float a);
	/// Ustawia pixel o danym kolorze (r,g,b,a) w pozycji (x,y) na bitmapie
	void Pixel(int x, int y, float r, float g, float b, float a);
	glm::vec4 GetPixel(int x, int y);
	inline int GetWidth() { return width; }
	inline int GetHeight() { return height; }
	inline std::string GetPath() { return path; }
	/// Wywy�a bitmap� do GUP by da�o si� jej u�yc
	void UploadToGPU();


	/// Rysuje linie wed�ug algorytmu z instrukcji laboratoryjnej
	void DrawLine(int x1, int y1, int x2, int y2, float r, float g, float b, float a);
	/// Rysuje elipse wed�ug algorytmu z instrukcji laboratoryjnej
	void DrawElipse(int x1, int y1, int r1, int r2, float r, float g, float b, float a);
	/// Rysuje ko�o wed�ug algorytmu z instrukcji laboratoryjnej
	inline void DrawCircle(int x1, int y1, int radius, float r, float g, float b, float a) { DrawElipse(x1, y1, radius, radius, r, g, b,a); }
private:
	unsigned char* data = nullptr;
	int width, height,comp;
	bool loadet = false;
	std::string path;
	GLuint texID;
};
#endif // !TEXTURE_H

