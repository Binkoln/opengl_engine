#ifndef CUBEMAPTEXTURE_H
#include "Shader.h"
#include "TextureManager.h"
#include "Texture.h"
#include "Framebuffer.h"
#include "Model.h"
#include <GLFW/glfw3.h>

class CubemapTexture {
public:
	CubemapTexture(std::string path_to_shybox_texture);
	~CubemapTexture();

	void RenderCubemap(glm::mat4 projection, glm::mat4 cameraView,float strenhthParameter,float rotation);
	void BakeCubemap(float strenhthParameter, float rotation);
	inline bool IsCubemapRendered() { return isCubemapRendered; }
	inline GLuint GetCubemapTextureSamplerID() { return cubemapID; }
	inline GLuint GetIradiacneTextureSamplerID() { return iradianceID; }
	void Bind();
	void Unbind();
	static void InitShader();
	static void CleanupShader();
private:
	void RenderCubemapIntoFramebuffer(GLuint cubemap, int mipmap_level, float strenhthParameter, float rotation, Framebuffer* fb_baker);
	
	void ProjectionSphericalRenderCubemap(glm::mat4 projection, glm::mat4 cameraView, float strenhthParameter, float rotation,int mipmapLevel);
	GLuint GenCubemap(int resolution);
	
	bool isCubemapRendered = false;
	GLuint cubemapID, iradianceID;
	Framebuffer* fb_baker, *fb_ir;
	Texture* sphericalProjectedTexture;
};
#endif // !CUBEMAPTEXTURE_H
