#include "StringTools.h"



bool StringTools::endWith(std::string const& fullString, std::string const& ending) {
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
    }
    else {
        return false;
    }
}
bool StringTools::startWith(std::string const& fullString, std::string const& starting) {
    if (fullString.length() >= starting.length()) {
        return (0 == fullString.compare(0, starting.length(), starting));
    }
    else {
        return false;
    }
}

std::vector<std::string> StringTools::Split(const std::string& subject, std::string splitChars) {
    static const std::regex re{ splitChars.c_str() };
    std::vector<std::string> container{
        std::sregex_token_iterator(subject.begin(), subject.end(), re, -1),
        std::sregex_token_iterator()
    };
    return container;
}

std::string StringTools::bakcShlashedToSlashes(std::string in)
{
    std::ostringstream out;
    char lastC = '0';
    for (char c : in) 
        if (c == '\\') {
            if (lastC != '/') {
                lastC = '/';
                out << '/';
            }
        }
        else {
            lastC = c;
            out << c;
        }
    return out.str();
}

std::string StringTools::intToString(int i) {
    std::ostringstream out;
    out << i;
    return out.str();
}


std::string StringTools::getUpperFolderPath(std::string path)
{
    std::string out;
    std::vector<std::string> split = StringTools::Split(path, "(?:/)");
    for (int i = 0; i < split.size() - 1; i++)
    {
        out += split[i];
        if (i != split.size() - 2)
            out += "/";
    }

    return out;

}

std::string StringTools::getFileName(std::string path)
{
    std::vector<std::string> list = StringTools::Split(path, "(?:/)");
    if (list.size() == 0)return nullptr;
    return (std::string)(list[list.size() - 1]);
}

double StringTools::stringToDouble(std::string in) {
    double d;
    std::istringstream b(in);
    b >> d;
    return d;
}

int StringTools::stringToInt(std::string in)
{
    int d;
    std::istringstream b(in);
    b >> d;
    return d;
}

std::string StringTools::doubleToString(double in) {
    std::ostringstream b;
    b << in;
    return b.str();
}

long StringTools::stringToLong(std::string in) {
    long d;
    std::istringstream b(in);
    b >> d;
    return d;
}

std::string StringTools::longToString(long in) {
    std::ostringstream b;
    b << in;
    return b.str();
}

std::string StringTools::vec3ToString(glm::vec3 v) {
    std::ostringstream b;
    b << v.x << " " << v.y << " " << v.z;
    return b.str();
}

glm::vec3 StringTools::stringToVec3(std::string in) {
    glm::vec3 v;
    std::istringstream b(in);
    b >> v.x;
    b >> v.y;
    b >> v.z;
    return v;
}

std::string StringTools::vec4ToString(glm::vec4 v)
{
    std::ostringstream b;
    b << v.x << " " << v.y << " " << v.z << " " << v.w;
    return b.str();
}

glm::vec4 StringTools::stringToVec4(std::string in)
{
    glm::vec4 v;
    std::istringstream b(in);
    b >> v.x;
    b >> v.y;
    b >> v.z;
    b >> v.w;
    return v;
}


std::string StringTools::vec2ToString(glm::vec2 v)
{
    std::ostringstream b;
    b << v.x << " " << v.y;
    return b.str();
}

glm::vec2 StringTools::stringToVec2(std::string in)
{
    glm::vec2 v;
    std::istringstream b(in);
    b >> v.x;
    b >> v.y;
    return v;
}