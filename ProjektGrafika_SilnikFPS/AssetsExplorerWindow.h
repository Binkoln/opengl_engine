#ifndef ASSETS_EXPOLRER_WINDOW_H
#define ASSETS_EXPOLRER_WINDOW_H

#include <vector>
#include <string>
#include <iostream>
#include <filesystem>
#include "imgui/imgui.h"
#include "StringTools.h"
#include "Texture.h"
#include "InputManager.h"
#include "Object.h"

#define ASSET_TYPE_TEXT 0
#define ASSET_TYPE_TEXTURE 1
#define ASSET_TYPE_SHADER 2
#define ASSET_TYPE_AUDIOFILE 3
#define ASSET_TYPE_LEVEL 4
#define ASSET_TYPE_MODEL 5
#define ASSET_TYPE_MATERIAL 6
#define ASSET_TYPE_ANIMATION 7
#define ASSET_TYPE_PREFAB 8
#define ASSET_TYPE_DIRECTORY 9
#define ASSET_TYPE_LUA_SCRIPT 10
#define ASSET_TYPE_OTHER 100

/// Struktura przechowywuj�ca informacje o assecie
///
/// @param string path - �cie�ka do asetu
/// @param string filename - nazwa pliku
/// @param string directory - folder w kt�rym znajduje si� asset
/// @param int type - typ assetu
/// @see AssetsExpolrerWindow
struct AssetData {
	std::string path;
	std::string filename;
	std::string directory;
	int type;
};

/// Klasa przeszukuje rekursywnie folder projektu (struct TEngineStartupOptions) i buduje biblioteke asset�w (struct AssetData)
class AssetsExpolrerWindow {
public:
	AssetsExpolrerWindow();
	~AssetsExpolrerWindow();
	
	/// Inicjalizuje klas�
	///
	/// @param string projectPath - �ciezka do projektu
	void Init(std::string projectPath);
	/// Buduje baz� plik�w struct AssetData i klasyfikuje je na podstawie rozszerze�
	void BuildDB();
	/// Rysuje okienko przegl�darki plik�w przy u�yciu biblioteki ImGui
	void DrawWindow();
	/// Ustawia wska�nik funkcji do tworzenia plik�w nowych scen
	void setSaveSceneFunction(void (*saveScene)(std::string));
	/// Ustawia wska�nik funkcji do �adowania scen
	void setLoadSceneFunction(void (*loadScene)(std::string));
	///Metoda sprawdza rozszerzenie pliku i zwraca odpowiadaj�cy mu typ
	static unsigned int getAssetTypeByExtension(std::string extension);

	/// Metoda wywo�aniu na zmien� ukrywa lub pokazuje okienko.
	inline void show() { opened = !opened; }

private:
	/// Pchechodzi do innego filderu
	void MoveToDirectory(std::string directoryPath);
	/// Od�wierza pliki w tym folderze i sprawdza czy nie pojawi�y si� nowe
	void RefreshCurentFolder();

	void (*saveScene)(std::string) = nullptr;
	void (*loadScene)(std::string) = nullptr;

	std::string projectPath;
	std::vector<struct AssetData> library;
	std::vector<struct AssetData> currentDirContent;
	std::string current_folder_path;
	bool opened = true;

	//New Scene Name mini window
	bool newSceneFileWindowOpened = false;
	char scene_filename[256];

	Texture icon_directory, image_icon_file, icon_file, icon_model, icon_shader, icon_script, icon_scene, icon_material;
};

#endif // !ASSET_EXPOLRER_WINDOW_H
