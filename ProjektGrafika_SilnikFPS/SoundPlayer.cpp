#include "SoundPlayer.h"

#define TINY_WAV_IMPLEMENTATION
#include "tinywav.h"

bool is_playing = false;
ALuint audio_buffer;
ALuint source;
ALCdevice* device;
ALCcontext* context;
void SoundPlayer::Init()
{
    device = alcOpenDevice(NULL);
    if (!device)printf("Error:Nie udalo sie zinicjalizowac niezbendnych bibliotek(OpenAL)\n");
    context = alcCreateContext(device, NULL);
    alcMakeContextCurrent(context);
    if (!context)printf("Error:Nie udalo sie zinicjalizowac kontekstu OpenAl\n");

    alListener3f(AL_POSITION, 0, 0, 0);
    alListener3f(AL_VELOCITY, 0, 0, 0);
    alListener3f(AL_ORIENTATION, 0, 0, 0);


    std::string path = "EngineProjectFolder/explosion.wav";
    bool a_type = false;
    std::ifstream stream(path, std::ios::binary);
    if (!stream.good() || stream.eof())
    {
        std::cout<< "Failed to open wav file " <<  path.c_str() << "\n";
        return;
    }
    std::vector<char> m_FileData;

    stream.seekg(0, stream.end);
    size_t length = stream.tellg();
    stream.seekg(0, stream.beg);

    m_FileData.reserve(length);
    std::for_each(std::istreambuf_iterator<char>(stream), std::istreambuf_iterator<char>(), [&](const char c) {
        m_FileData.push_back(c);
        });

    int channels;
    int samplerate;
    int bps;
    int alloc_size;
    char* data = tw_load_mem(m_FileData.data(), m_FileData.size(), &channels, &samplerate, &bps, &alloc_size);
    if (!data)
    {
        std::cout << "TinyWav Error: " << tw_get_error() << "\n";
        return;
    }


    ALuint format = 0;

    if (bps == 8) { if (channels == 1)format = AL_FORMAT_MONO8; else format = AL_FORMAT_STEREO8; }
    else if (bps == 16) { if (channels == 1)format = AL_FORMAT_MONO16; else format = AL_FORMAT_STEREO16; }

    ALuint buffer;
    ALuint u_samplerate = samplerate;
    alGenBuffers(1, &buffer);
    alBufferData(buffer, format, data, (ALuint)alloc_size, u_samplerate);


    audio_buffer = buffer;

    ALuint source_id;
    alGenSources(1, &source_id);
    alSourcei(source_id, AL_BUFFER, buffer);
    alSourcef(source_id, AL_PITCH, 1.0f);
    alSourcef(source_id, AL_GAIN, 0.3f);
    alSourcei(source_id, AL_LOOPING, (a_type) ? AL_TRUE : AL_FALSE);
    alSource3f(source_id, AL_POSITION, 0, 0, 0);
    alSource3f(source_id, AL_VELOCITY, 0, 0, 0);
    source = source_id;

    tw_free(data);
}
void SoundPlayer::Clean()
{
    alDeleteBuffers(1, &audio_buffer);
    alDeleteSources(1, &source);

    alcCloseDevice(device);
    alcMakeContextCurrent(NULL);
    alcDestroyContext(context);
}

void SoundPlayer::PlaySound()
{
    alSourcePlay(source);
}