#ifndef RENDERER_2D_H
#define RENDERER_2D_H
#include <GL/glew.h>
#include "Texture.h"
#include "Shader.h"

/// Klasa s�u�y do renderowania sprit�w 2D
class Renderer2D {
public:
	Renderer2D();
	~Renderer2D();

	/// Inicjalizuje renderer 2D
	void Init();
	/// Zwalnia pami�� i usuwa
	void Destroy();
	/// Renderuje sprite na framebuffer (domy�lnie ekran)
	///
	/// @param Texture* texture - tekstura z kt�rego b�dzie wyci�ty sprite
	/// @param glm::vec4 posSiz - pozycja na teksturze oraz rozmiar wyci�cia sprita
	/// @param glm::vec3 spriteColor - kolor sprita
	/// @param glm::mat4 model - macierz transfromacji spita
	/// @param glm::mat4 viewProjection - macierz projekcji sprita
	void RenderSprite(Texture* texture, glm::vec4 posSiz, glm::vec3 spriteColor, glm::mat4 model, glm::mat4 viewProjection);
	void RenderSprite(GLuint texture, bool isTextureLoaded, glm::vec4 posSiz, glm::vec3 spriteColor, glm::mat4 model, glm::mat4 viewProjection);

	

private:
	GLuint VAO, VBO,EBO;
	Shader shader;

};

#endif // !RENDERER_2D_H
