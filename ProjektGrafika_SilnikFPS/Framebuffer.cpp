#include "Framebuffer.h"

Framebuffer::Framebuffer()
{
	framebufferID = NULL;
	textureColorID = NULL;
	textureDepthID = NULL;
	textureObjectID = NULL;
	textureVelocityID = NULL;
	textureViewSpacePositionID = NULL;
	textureViewSpaceNormalID = NULL;
	textureMaterialProportiesID = NULL;
}

void Framebuffer::DeleteFB_TEXTURE(GLuint& id)
{
	if (id != NULL) {
		glDeleteTextures(1, &id);
		id = NULL;
	}
}

Framebuffer::~Framebuffer() 
{
	
	
	DeleteFB_TEXTURE(textureColorID);
	DeleteFB_TEXTURE(textureDepthID);
	DeleteFB_TEXTURE(textureNormalID);
	DeleteFB_TEXTURE(texturePositionID);
	DeleteFB_TEXTURE(textureObjectID);
	DeleteFB_TEXTURE(textureVelocityID);
	DeleteFB_TEXTURE(textureViewSpacePositionID);
	DeleteFB_TEXTURE(textureViewSpaceNormalID);
	DeleteFB_TEXTURE(textureMaterialProportiesID);

	if (framebufferID != NULL) {
		glDeleteFramebuffers(1, &framebufferID);
		framebufferID = NULL;
	}

}

void Framebuffer::Bind() 
{
	glViewport(0, 0, width, height);


	glBindFramebuffer(GL_FRAMEBUFFER,framebufferID);

	if (framebyffer_type == FRAMEBUFFER_TYPE_DEFFERED_RENDERING_USAGE) {
		unsigned int attachments[8] = { GL_COLOR_ATTACHMENT0 ,GL_COLOR_ATTACHMENT1,GL_COLOR_ATTACHMENT2 ,GL_COLOR_ATTACHMENT3,GL_COLOR_ATTACHMENT4,GL_COLOR_ATTACHMENT5,GL_COLOR_ATTACHMENT6,GL_COLOR_ATTACHMENT7 };
		glDrawBuffers(8, attachments);

	}
	else if (framebyffer_type == FRAMEBUFFER_TYPE_COLOR_AND_DEPTH) {
		if (textureVelocityID == NULL) {
			unsigned int attachments[1] = { GL_COLOR_ATTACHMENT0 };
			glDrawBuffers(1, attachments);
		}
		else {
			unsigned int attachments[2] = { GL_COLOR_ATTACHMENT0,GL_COLOR_ATTACHMENT1 };
			glDrawBuffers(2, attachments);
		}

	}else if (framebyffer_type == FRAMEBUFFER_TYPE_DEPTH) {
		if (textureDepthID != NULL) {
			unsigned int attachments[1] = { GL_DEPTH_ATTACHMENT };
			glDrawBuffers(1, attachments);
		}
		
	}
	else if (framebufferID == FRAMEBUFFER_TYPE_DEPTH_ONLY) {
		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);
	}
	else if(framebufferID == FRAMEBUFFER_TYPE_COLOR || framebufferID == FRAMEBUFFER_TYPE_COLOR_HDR){
		unsigned int attachments[1] = { GL_COLOR_ATTACHMENT0 };
		glDrawBuffers(1, attachments);
	}
}

void Framebuffer::Unbind() 
{
	glBindFramebuffer(GL_FRAMEBUFFER,0);
	unsigned int attachments[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, attachments);
	glViewport(0, 0, InputManager::getWindowWidth(), InputManager::getWindowHeight());
}


void Framebuffer::SetDepthBufferSource(GLuint framebufferDepthID)
{
	glBindFramebuffer(GL_FRAMEBUFFER, framebufferID);
	glBindTexture(GL_TEXTURE_2D, framebufferDepthID);
	framebyffer_type = FRAMEBUFFER_TYPE_COLOR_AND_DEPTH;
	//TODO:?????????????? coment beow ?
	//textureDepthID = framebufferDepthID;
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, framebufferDepthID, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Framebuffer::SetVelocityBufferSource(GLuint framebufferVelocityID)
{
	glBindFramebuffer(GL_FRAMEBUFFER, framebufferID);
	glBindTexture(GL_TEXTURE_2D, framebufferVelocityID);


	textureVelocityID = framebufferVelocityID;
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, textureVelocityID, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Framebuffer::Init(unsigned int width, unsigned int height, unsigned int framebuffer_type)
{
	this->framebyffer_type = framebuffer_type;
	this->width = width;
	this->height = height;

	glGenFramebuffers(1, &framebufferID);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		std::cout << "Error Framebuffer(1)-Init: Cannot create framebuffer.\n";
	}


	Bind();
	if (framebuffer_type == FRAMEBUFFER_TYPE_COLOR_HDR) {
		
		glGenTextures(1, &textureColorID);
		glBindTexture(GL_TEXTURE_2D, textureColorID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);


		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureColorID, 0);


	}
	if (framebuffer_type == FRAMEBUFFER_TYPE_COLOR || framebuffer_type == FRAMEBUFFER_TYPE_COLOR_AND_DEPTH) {
		glGenTextures(1, &textureColorID);
		glBindTexture(GL_TEXTURE_2D, textureColorID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);


		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureColorID, 0);
		

	}
	
	if (framebuffer_type == FRAMEBUFFER_TYPE_DEPTH || framebuffer_type == FRAMEBUFFER_TYPE_COLOR_AND_DEPTH) {
		glGenTextures(1, &textureDepthID);
		glBindTexture(GL_TEXTURE_2D, textureDepthID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, width, height, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);



		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, textureDepthID, 0);
	}

	if (framebuffer_type == FRAMEBUFFER_TYPE_DEPTH_ONLY) {
		glGenTextures(1, &textureDepthID);
		glBindTexture(GL_TEXTURE_2D, textureDepthID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, width, height, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
		//float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		//glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, textureDepthID, 0);
		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);
	}

	if (framebuffer_type == FRAMEBUFFER_TYPE_DEFFERED_RENDERING_USAGE) {
		//ObjectID buffer
		glGenTextures(1, &textureObjectID);
		glBindTexture(GL_TEXTURE_2D, textureObjectID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, textureObjectID, 0);


		//Color buffer
		glGenTextures(1, &textureColorID);
		glBindTexture(GL_TEXTURE_2D, textureColorID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, textureColorID, 0);
		
		//position buffer
		glGenTextures(1, &texturePositionID);
		glBindTexture(GL_TEXTURE_2D, texturePositionID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texturePositionID, 0);


		//Normal buffer
		glGenTextures(1, &textureNormalID);
		glBindTexture(GL_TEXTURE_2D, textureNormalID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, textureNormalID, 0);


		//textureViewSpacePositionID SSAO
		glGenTextures(1, &textureViewSpacePositionID);
		glBindTexture(GL_TEXTURE_2D, textureViewSpacePositionID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT5, GL_TEXTURE_2D, textureViewSpacePositionID, 0);

		//textureViewSpaceNormalID SSAO
		glGenTextures(1, &textureViewSpaceNormalID);
		glBindTexture(GL_TEXTURE_2D, textureViewSpaceNormalID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT6, GL_TEXTURE_2D, textureViewSpaceNormalID, 0);

		//Velocity buffer
		glGenTextures(1, &textureVelocityID);
		glBindTexture(GL_TEXTURE_2D, textureVelocityID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D, textureVelocityID, 0);


		//Depth buffer
		glGenTextures(1, &textureDepthID);
		glBindTexture(GL_TEXTURE_2D, textureDepthID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, width, height, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, textureDepthID, 0);


		//Material Proporties buffer
		glGenTextures(1, &textureMaterialProportiesID);
		glBindTexture(GL_TEXTURE_2D, textureMaterialProportiesID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT7, GL_TEXTURE_2D, textureMaterialProportiesID, 0);

		unsigned int attachments[8] = { GL_COLOR_ATTACHMENT0 ,GL_COLOR_ATTACHMENT1,GL_COLOR_ATTACHMENT2 ,GL_COLOR_ATTACHMENT3,GL_COLOR_ATTACHMENT4,GL_COLOR_ATTACHMENT5,GL_COLOR_ATTACHMENT6,GL_COLOR_ATTACHMENT7 };
		glDrawBuffers(8, attachments);



	}

	Unbind();

}