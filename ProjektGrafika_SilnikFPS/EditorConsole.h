#pragma once
#ifndef EDITOR_CONSOLE_H
#define EDITOR_CONSOLE_H

#include <string>
#include "imgui/imgui.h"

/// Klasa okienka(ImGui) konsoli pozwala wpisywa� polecenia kt�re przekazywane s� do wykonania do TEngine:ProcessCommand
class EditorConsole {
public:
	EditorConsole();
	~EditorConsole();

	void Init(void (*ProcessCommand)(std::string in));

	inline void show() {
		consoleWindow = !consoleWindow;
	}

	/// Metoda wywo�aniu na zmien� ukrywa lub pokazyje okienko.
	inline void show(bool val) {
		consoleWindow = val;
	}

	///Zwraca czy Konsola jest otwarta
	inline bool isConsoleOpen() { return consoleWindow; }

	/// Metoda wywo�aniu na zmien� ukrywa lub pokazuje okienko pomocy z poleceniami.
	inline void showHelp() {
		showHelpWindow = !showHelpWindow;
	}

	/// Metoda pozwala ustawi� widoczo�� okienka pomocy z poleceniami.
	/// @param bool val - czy okienko help ma by� widoczne
	inline void showHelp(bool val) {
		showHelpWindow = val;
	}

	/// Rysuje okienko na ekran przy wykorzystaniu biblioteki ImGui
	void DrawImGui();

private:
	void pushHelpNode(const char* title,const char* content);
	bool consoleWindow = true;
	bool showHelpWindow = false;
	void (*ProcessCommand)(std::string in);

	char InputBuffer[256];

};
#endif // !EDITOR_CONSOLE_H
