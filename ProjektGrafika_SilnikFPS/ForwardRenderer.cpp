#include "ForwardRenderer.h"


void ForwardRenderer::Init(int framebuffer_width, int framebuffer_height)
{


}


void ForwardRenderer::Destroy()
{

}

void ForwardRenderer::QueueModelForRender(Object*o,Shader* shad, Model* model, glm::mat4 transform, Camera* camera, glm::vec3 objectIDColor) {
	RenderForwardQueueObject* qo = new RenderForwardQueueObject;
	qo->shad = shad;
	qo->model = model;
	qo->transform = transform;
	qo->camera = camera;
	qo->objectIDColor = objectIDColor;
	
	glm::vec3 vtc = (camera->getWorldPosition() - o->getWorldPosition());
	qo->distanceToCamera = glm::length(vtc);
	
	renderqueue.push_back(qo);
}

bool compareLenght(RenderForwardQueueObject* a, RenderForwardQueueObject* b) {
	return a->distanceToCamera > b->distanceToCamera;
}

void ForwardRenderer::SortQueuedModelsAndRender() {
	std::sort(renderqueue.begin(), renderqueue.end(),compareLenght);
	glEnable(GL_BLEND);
	for (RenderForwardQueueObject* qo : renderqueue) {
		RenderModel(qo->shad, qo->model, qo->transform, qo->camera, qo->objectIDColor);
		delete qo;
	}

	renderqueue.clear();
}

void ForwardRenderer::RenderModelToDepthMap(Shader* shad, Model* model, glm::mat4 transform, DirectionalLightExtension* dl,glm::mat4 d)
{
	shad->Use();
	for (int i = 0; i < (*model).meshes.size(); i++) {
		glBindVertexArray((*model).meshes[i]->VAO);
		shad->SetMatrix4("MPV", dl->getLightProjection());
		shad->SetMatrix4("model", transform);
		//(*model).meshes[i]->material.Use(shad);
		glDrawElements(GL_TRIANGLES, (*model).meshes[i]->indi_count, GL_UNSIGNED_INT, 0);
	}
}


void ForwardRenderer::RenderModel(Shader* shad, Model* model, glm::mat4 transform, Camera* camera, glm::vec3 objectIDColor)
{
	shad->Use();
	for (int i = 0; i < (*model).meshes.size(); i++) {

		glBindVertexArray((*model).meshes[i]->VAO);

		shad->SetMatrix4("projection", camera->getProjection());
		shad->SetMatrix4("viewMatrix", camera->getViewMatrix());
		glm::vec3 cameraPos = camera->getWorldPosition();
		shad->SetVector3f("camPos", cameraPos);
		shad->SetFloat("time", (float)glfwGetTime());
		glm::mat4 modelMat = transform;

		shad->SetVector3f("objectIdColor", objectIDColor);
		shad->SetMatrix4("model", modelMat);

		shad->SetVector3f("cubecolor", glm::vec3(1, 1, 1));


		std::vector<PointLightExtension*> point_lights = Level::GetPointLightsList();
		int max_lights = 32;
		int active_lights_count = 0;
		for (int i = 0; i < point_lights.size(); i++) {
			if (i >= max_lights)break;
			PointLightExtension* p = point_lights[i];
			if (!p->enabled)continue;
			shad->SetVector3f(("point_lights[" + StringTools::intToString(active_lights_count) + "].Color").c_str(), p->lightColor * p->intensity);
			shad->SetVector3f(("point_lights[" + StringTools::intToString(active_lights_count) + "].Position").c_str(), p->getPosition());
			active_lights_count++;
		}
		shad->SetInt("active_point_light_count", active_lights_count);


		std::vector<DirectionalLightExtension*> directional_lights = Level::GetDirectionalLightsList();
		max_lights = 5;
		active_lights_count = 0;
		for (int i = 0; i < directional_lights.size(); i++) {
			if (i >= max_lights)break;
			DirectionalLightExtension* p = directional_lights[i];
			if (!p->enabled)continue;
			shad->SetVector3f(("directional_lights[" + StringTools::intToString(active_lights_count) + "].Color").c_str(), p->lightColor * p->intensity);
			shad->SetVector3f(("directional_lights[" + StringTools::intToString(active_lights_count) + "].Direction").c_str(), p->getDirection());
			glm::mat4 lmp = p->getLightProjection();
			//lmp = glm::scale(lmp, glm::vec3(0.5f, 0.5f, 0.5f));
			//lmp = glm::translate(lmp, glm::vec3(0.5f, 0.5f, 0.5f - p->bias));
			shad->SetMatrix4(("directional_lights[" + StringTools::intToString(active_lights_count) + "].LightProjection").c_str(), lmp);
			glActiveTexture(GL_TEXTURE10+i);
			glBindTexture(GL_TEXTURE_2D, p->getDepthTextureID());
			shad->SetTextureSampler(("sampler_directional_depth[" + StringTools::intToString(active_lights_count) + "]").c_str(),10+i);
			active_lights_count++;
		}

		shad->SetInt("active_directional_light_count", active_lights_count);


		shad->SetFloat("time", (float)glfwGetTime() * 3);


		(*model).meshes[i]->material.Use(shad);

		glDrawElements(GL_TRIANGLES, (*model).meshes[i]->indi_count, GL_UNSIGNED_INT, 0);
	}

}