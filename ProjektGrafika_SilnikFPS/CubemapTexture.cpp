#include "CubemapTexture.h"

namespace CubemapTexture_Blackboard {
	Shader sh_projectionCubemapRenderer;
    Shader sh_cubemapRenderer;
	Mesh* cubeMesh;
};

CubemapTexture::CubemapTexture(std::string path_to_shybox_texture)
{
    sphericalProjectedTexture = new Texture();
    sphericalProjectedTexture->LoadHDR(path_to_shybox_texture.c_str());
}
CubemapTexture::~CubemapTexture()
{
    delete sphericalProjectedTexture;

    if (isCubemapRendered) {
        delete fb_baker;
        delete fb_ir;
        glDeleteTextures(1, &cubemapID);
        glDeleteTextures(1, &iradianceID);
    }
}


GLuint CubemapTexture::GenCubemap(int resolution)
{
    GLuint retVal;
    glGenTextures(1, &retVal);
    glBindTexture(GL_TEXTURE_CUBE_MAP, retVal);

    for (unsigned int i = 0; i < 6; ++i)
    {
        //HDR GL_RGB16F beybe!
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB16F,
            resolution, resolution, 0, GL_RGB, GL_FLOAT, nullptr);
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    return retVal;
}

void CubemapTexture::BakeCubemap(float strenhthParameter, float rotation)
{
    //How this work? Its simple, just read this simple breif and anjoy your live !
    //1. create framebuffer
    //2. create cubemap textures.
    //3. create matrices for baking cubemap into texture.
    //4. render into textures
    //5. go and kill yourself
    // Thats all, thank you for wasting your life and reading this!


    if (isCubemapRendered)return; //if rendered then go wawy from this function !
    int texture_size = 1024;//Resolution - todo: delete this
  
    //AD.1 create framebuffer
    if (!isCubemapRendered){
        fb_baker = new Framebuffer();
        fb_baker->Init(texture_size, texture_size, FRAMEBUFFER_TYPE_COLOR_HDR); 
        fb_ir = new Framebuffer();
        fb_ir->Init(64, 64, FRAMEBUFFER_TYPE_COLOR_HDR);
    }
   
    //AD.2 create cubemap textures
    glm::mat4 projection = glm::perspective(glm::radians(90.0f),1.0f,0.1f,1000000.0f);
    glm::mat4 cameraViewMatrix(1);

    //AD.3 Generate cubemap texture
    if (!isCubemapRendered) {
        cubemapID = GenCubemap(texture_size);
        iradianceID = GenCubemap(64);


    }
    //AD4
    
    
    RenderCubemapIntoFramebuffer(cubemapID, 0,strenhthParameter,rotation, fb_baker);
    RenderCubemapIntoFramebuffer(iradianceID, 1,strenhthParameter,rotation,fb_ir);
    
   

    //AD.5 - https://www.youtube.com/watch?v=dQw4w9WgXcQ
 
  



    isCubemapRendered = true;
}

void CubemapTexture::RenderCubemapIntoFramebuffer(GLuint cubemap, int mipmap_level, float strenhthParameter, float rotation,Framebuffer* fb_baker)
{
    fb_baker->Bind();
    glm::mat4 projection = glm::perspective(glm::radians(90.0f), 1.0f, 0.1f, 1000000.0f);
    glm::mat4 cameraViewMatrix(1);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap);
    glTexParameteri(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    glTexParameteri(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    glTexParameteri(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    glTexParameteri(GL_TEXTURE_CUBE_MAP_POSITIVE_X, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP_POSITIVE_X, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    glTexParameteri(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    glTexParameteri(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    cameraViewMatrix = glm::mat4(1);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
        GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, cubemap, 0);
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    ProjectionSphericalRenderCubemap(projection, cameraViewMatrix, strenhthParameter, rotation, mipmap_level);

    cameraViewMatrix = glm::rotate(cameraViewMatrix, glm::radians(90.0f), glm::vec3(0, 1, 0));
    // right->Bind();
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
        GL_TEXTURE_CUBE_MAP_NEGATIVE_X, cubemap, 0);
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    ProjectionSphericalRenderCubemap(projection, cameraViewMatrix, strenhthParameter, rotation, mipmap_level);

    cameraViewMatrix = glm::rotate(cameraViewMatrix, glm::radians(90.0f), glm::vec3(0, 1, 0));
    // back->Bind();
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
        GL_TEXTURE_CUBE_MAP_POSITIVE_Z, cubemap, 0);
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    ProjectionSphericalRenderCubemap(projection, cameraViewMatrix, strenhthParameter, rotation, mipmap_level);

    cameraViewMatrix = glm::rotate(cameraViewMatrix, glm::radians(90.0f), glm::vec3(0, 1, 0));
    // left->Bind();
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
        GL_TEXTURE_CUBE_MAP_POSITIVE_X, cubemap, 0);
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    ProjectionSphericalRenderCubemap(projection, cameraViewMatrix, strenhthParameter, rotation, mipmap_level);

    cameraViewMatrix = glm::mat4(1);
    cameraViewMatrix = glm::rotate(cameraViewMatrix, glm::radians(90.0f), glm::vec3(1, 0, 0));
    cameraViewMatrix = glm::rotate(cameraViewMatrix, glm::radians(180.0f), glm::vec3(0, 1, 0));
    // up->Bind();
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
        GL_TEXTURE_CUBE_MAP_POSITIVE_Y, cubemap, 0);
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    ProjectionSphericalRenderCubemap(projection, cameraViewMatrix, strenhthParameter, rotation, mipmap_level);

    cameraViewMatrix = glm::mat4(1);
    cameraViewMatrix = glm::rotate(cameraViewMatrix, glm::radians(-90.0f), glm::vec3(1, 0, 0));
    cameraViewMatrix = glm::rotate(cameraViewMatrix, glm::radians(180.0f), glm::vec3(0, 1, 0));
    //down->Bind();
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
        GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, cubemap, 0);
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    ProjectionSphericalRenderCubemap(projection, cameraViewMatrix, strenhthParameter, rotation, mipmap_level);
    fb_baker->Unbind();
}


void CubemapTexture::ProjectionSphericalRenderCubemap(glm::mat4 projection,glm::mat4 cameraView, float strenhthParameter,float rotation, int mipmapLevel)
{
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glDepthFunc(GL_LESS);
    Shader* shad = &CubemapTexture_Blackboard::sh_projectionCubemapRenderer;
    shad->Use();
	
	glBindVertexArray(CubemapTexture_Blackboard::cubeMesh->VAO);

	shad->SetMatrix4("projection", projection);
	shad->SetMatrix4("viewMatrix", cameraView);
    shad->SetFloat("rotation", rotation);
    shad->SetFloat("strength", strenhthParameter);
	shad->SetMatrix4("model", glm::mat4(1));
    shad->SetInt("mipmapLevel", mipmapLevel);

    glActiveTexture(GL_TEXTURE0);
    sphericalProjectedTexture->Bind();
    shad->SetTextureSampler("enviromentSampler", 0);


	glDrawElements(GL_TRIANGLES, CubemapTexture_Blackboard::cubeMesh->indi_count, GL_UNSIGNED_INT, 0);
    glDepthMask(GL_TRUE);
    glDepthFunc(GL_LESS);
}

void CubemapTexture::RenderCubemap(glm::mat4 projection, glm::mat4 cameraView, float strenhthParameter, float rotation)
{
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
    glDepthFunc(GL_LESS);
    Shader* shad = &CubemapTexture_Blackboard::sh_cubemapRenderer;
    shad->Use();

    glBindVertexArray(CubemapTexture_Blackboard::cubeMesh->VAO);

    shad->SetMatrix4("projection", projection);
    shad->SetMatrix4("viewMatrix", cameraView);
    shad->SetFloat("rotation", rotation);
    shad->SetFloat("strength", strenhthParameter);
    shad->SetMatrix4("model", glm::mat4(1));

    glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapID);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapID);
    shad->SetTextureSampler("enviromentSampler", 0);



    //glDisable(GL_BLEND);
    //glDisable(GL_CULL_FACE);
    glDrawElements(GL_TRIANGLES, CubemapTexture_Blackboard::cubeMesh->indi_count, GL_UNSIGNED_INT, 0);
    
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
    glDepthMask(GL_TRUE);
    glDepthFunc(GL_LESS);
}


void CubemapTexture::Bind()
{}

void CubemapTexture::Unbind()
{}


struct MeshVertex getMeshVertex(float x, float y, float z,float u,float v) {
    struct MeshVertex v1;
  
    v1.position = {x,y,z };
    v1.normal = { 0,0,0 };
    v1.tangent = { 0,0,0 };
    v1.bitangent = { 0,0,0 };
    v1.UV = { u,v };
    v1.boneIDs = { 0,0,0 };
    v1.boneWeights = { 0,0,0 };
    return v1;
}

void CubemapTexture::InitShader()
{
	CubemapTexture_Blackboard::sh_projectionCubemapRenderer = Shader("EngineProjectFolder/assets/shaders/cubemapShaders/cubemapToTextureProjection_v_b.glsl", "EngineProjectFolder/assets/shaders/cubemapShaders/cubemapToTextureProjection_f_b.glsl");
	CubemapTexture_Blackboard::sh_cubemapRenderer = Shader("EngineProjectFolder/assets/shaders/cubemapShaders/cubemapSkybox_v_b.glsl", "EngineProjectFolder/assets/shaders/cubemapShaders/cubemapSkybox_f_b.glsl");
	CubemapTexture_Blackboard::cubeMesh = new Mesh("Cube");



    std::vector<struct MeshVertex> vertices;
    std::vector<unsigned int> indi;

        
        //Down
        int siz = vertices.size();
        vertices.push_back(getMeshVertex(-1, -1, -1, 0, 0));
        vertices.push_back(getMeshVertex( 1, -1, -1, 1, 0));
        vertices.push_back(getMeshVertex( 1, -1,  1, 1, 1));
        vertices.push_back(getMeshVertex(-1, -1,  1, 0, 1));
        
        indi.push_back(siz + 0);
        indi.push_back(siz + 2);
        indi.push_back(siz + 1);
        indi.push_back(siz + 2);
        indi.push_back(siz + 0);
        indi.push_back(siz + 3);


        //UP
        siz = vertices.size();
        vertices.push_back(getMeshVertex(-1,1, -1, 0, 0));
        vertices.push_back(getMeshVertex(1, 1, -1, 1, 0));
        vertices.push_back(getMeshVertex(1, 1, 1, 1, 1));
        vertices.push_back(getMeshVertex(-1, 1, 1, 0, 1));

        indi.push_back(siz + 0);
        indi.push_back(siz + 2);
        indi.push_back(siz + 1);
        indi.push_back(siz + 2);
        indi.push_back(siz + 0);
        indi.push_back(siz + 3);
    

        //Front
        siz = vertices.size();
        vertices.push_back(getMeshVertex(-1, -1, -1, 0, 0));
        vertices.push_back(getMeshVertex( -1, 1, -1, 1, 0));
        vertices.push_back(getMeshVertex( 1, 1,  -1, 1, 1));
        vertices.push_back(getMeshVertex(1, -1,  -1, 0, 1));
        indi.push_back(siz + 0);
        indi.push_back(siz + 2);
        indi.push_back(siz + 1);
        indi.push_back(siz + 2);
        indi.push_back(siz + 0);
        indi.push_back(siz + 3);

        //Back
        siz = vertices.size();
        vertices.push_back(getMeshVertex(-1, -1, 1, 0, 0));
        vertices.push_back(getMeshVertex(-1, 1, 1, 1, 0));
        vertices.push_back(getMeshVertex(1, 1, 1, 1, 1));
        vertices.push_back(getMeshVertex(1, -1, 1, 0, 1));
        indi.push_back(siz + 0);
        indi.push_back(siz + 2);
        indi.push_back(siz + 1);
        indi.push_back(siz + 2);
        indi.push_back(siz + 0);
        indi.push_back(siz + 3);

        //Left
        siz = vertices.size();
        vertices.push_back(getMeshVertex(-1, -1, -1, 0, 0));
        vertices.push_back(getMeshVertex(-1, 1, -1, 1, 0));
        vertices.push_back(getMeshVertex(-1, 1, 1, 1, 1));
        vertices.push_back(getMeshVertex(-1, -1, 1, 0, 1));

        indi.push_back(siz + 0);
        indi.push_back(siz + 2);
        indi.push_back(siz + 1);
        indi.push_back(siz + 2);
        indi.push_back(siz + 0);
        indi.push_back(siz + 3);

        //Right
        siz = vertices.size();
        vertices.push_back(getMeshVertex(1, -1, -1, 0, 0));
        vertices.push_back(getMeshVertex(1, 1, -1, 1, 0));
        vertices.push_back(getMeshVertex(1, 1, 1, 1, 1));
        vertices.push_back(getMeshVertex(1, -1, 1, 0, 1));

        indi.push_back(siz + 0);
        indi.push_back(siz + 2);
        indi.push_back(siz + 1);
        indi.push_back(siz + 2);
        indi.push_back(siz + 0);
        indi.push_back(siz + 3);

        
            
        

    CubemapTexture_Blackboard::cubeMesh->LoadMesh(vertices, indi, 0);

}

void CubemapTexture::CleanupShader()
{
	CubemapTexture_Blackboard::sh_projectionCubemapRenderer.Delete();
	CubemapTexture_Blackboard::sh_cubemapRenderer.Delete();
    delete CubemapTexture_Blackboard::cubeMesh;

}