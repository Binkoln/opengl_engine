#ifndef SHADER_H
#define SHADER_H

#include <iostream>
#include <string>
#include <fstream>
#include <stdio.h>
#include <sstream>
#include <GL/glew.h>
#include <GL/GL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

/// Klasa przechowuje informacje o shaderze, oraz zawiera metody u�atwiaj�ce jego urzywanie
class Shader {
public:
	/// �aduje dany shader
	///
	/// @param string vertex_path - �cie�ka do pliku vertex shadera
	/// @param string fragment_path - �cie�ka do pliku fragment shadera
	Shader(std::string vertex_path, std::string fragment_path);
	Shader();
	~Shader();
	/// Zwraca spreparowany kod shadera z pliku o �cie�ce path
	std::string getShaderSource(std::string path);
	/// U�ywa tego shadera
	void Use();
	///Ustawia uniform shader o naziwe name na wektor 4d glm::vec4 
	void SetVector4f(const GLchar* name, glm::vec4 v);
	///Ustawia uniform shader o naziwe name na wektor 3d glm::vec3 
	void SetVector3f(const GLchar* name, glm::vec3 v);
	///Ustawia uniform shader o naziwe name na wektor 2d glm::vec2 
	void SetVector2f(const GLchar* name, glm::vec2 v);
	///Ustawia uniform shader o naziwe name na macierz 4x4 glm::mat4 
	void SetMatrix4(const GLchar* name, glm::mat4 mat);
	///Ustawia uniform shader o naziwe name na macierz 4x4 float[16]
	void SetMatrix4(const GLchar* name, float mat[16]);
	///Ustawia uniform shader o naziwe name na warto�� float
	void SetFloat(const GLchar* name, GLfloat value);
	///Ustawia uniform shader o naziwe name na warto�� int
	void SetInt(const GLchar* name, GLint value);
	///Ustawia uniform shader o naziwe name na warto�� int
	void SetBool(const GLchar* name, GLboolean value);

	///Ustawia uniform shader o naziwe name na warto�� danego samplera 2D
	void SetTextureSampler(const GLchar* name, GLfloat value);
	/// Usuwa shader
	void Delete();
	/// Zwraca ID shadera wygenerowany przez OpenGL
	inline GLuint getProgram() { return m_Program; }
	/// Przestaje u�ywa� tego shadera
	inline void stop() { glUseProgram(0); }
	/// Przypisuje dany atrybut wej�ciowy do konkretnego id (okre�lane przy �adowaniu siatki Mesh)
	inline void bindAtributeLocation(int id, std::string name) { glBindAttribLocation(m_Program, id, name.c_str()); }

private:
	GLuint m_Program;
	std::string name;
};
#endif // !SHADER_H
