#include "Texture.h"
#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>
Texture::Texture() {
	texID = 0;
}

Texture::~Texture()
{
	if (data == nullptr)
		delete data;

	if (loadet)
		glDeleteTextures(1, &texID);
	loadet = false;

}


void Texture::Init(int width, int height, float r, float g, float b, float a)
{
	if (loadet)
		Texture::~Texture();
	this->width = width;
	this->height = height;
	this->comp = 4;
	//data = new int[width*height];
	ClearColor(r,g,b,a);

}

void Texture::ClearColor(float r, float g, float b, float a)
{
	for(int y = 0;y < height;y++)
		for (int x = 0; x < width; x++) {
			Pixel(x, y, r,g,b,a);
		}
}

void Texture::Pixel(int x, int y, float r, float g, float b, float a)
{
	/*char clr[4];
	clr[3] = (char)(a * 255);
	clr[0] = (char)(r * 255);
	clr[1] = (char)(g * 255);
	clr[2] = (char)(b * 255);
	int* color = (int*)clr;
	if (x < 0 || x >= width || y < 0 || y >= height)return;
	data[x + y * width] = *color;*/
	//TODO: przerobic z int* na unsigned char*
}

glm::vec4 Texture::GetPixel(int x, int y)
{
	if (x < 0 || x >= width || y < 0 || y >= height)return {0,0,0,0};
	unsigned char cr = (data[(x + y * width)*4]);
	unsigned char cg = (data[1+(x + y * width)* 4]);
	unsigned char cb = (data[2 + (x + y * width)* 4]);
	unsigned char ca = (comp == 4)?(3 + data[(x + y * width)* 4]):255;

	glm::vec4 retColor(0,0,0,1);
	if (comp == 4)
		retColor.a = ((float)ca)/255.0f;
	retColor.r = ((float)cr) / 255.0f;
	retColor.g = ((float)cg) / 255.0f;
	retColor.b = ((float)cb) / 255.0f;
	return retColor;
}

void Texture::BufferedLoad(std::string path)
{
	 data = stbi_load(path.c_str(), &width, &height, &comp, 4);


	if (!data) {
		std::cout << "Loading texture failed! " << path << "Stb says:'" << stbi_failure_reason() << "'\n";
		return;
	}
	this->path = path;

	/*std::cout << "cmp:" << comp << "\n";
	std::cout << "width:" << width<< "\n";
	std::cout << "height:" << height<< "\n";

	for (int j = 0; j < height; j++) {
		for (int i = 0; i < width; i++) {
			unsigned char col = data[(i + j * width) * 4];
			std::cout << ((col > 200) ? '1' : '0');
		}
		std::cout << std::endl;
	}*/


	loadet = true;
	
}

void Texture::UploadToGPU()
{
	glGenTextures(1, &texID);
	glBindTexture(GL_TEXTURE_2D, texID);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_NEAREST);

	stbi_image_free(data);
	loadet = true;
}

void Texture::Load(std::string path, unsigned int filter_type)
{
	unsigned char* data = stbi_load(path.c_str(), &width, &height, &comp, 4);
	
	if (!data) {
		std::cout << "Loading texture failed! " << path <<" Stb says: '"<< stbi_failure_reason() << "' \n";
		return;
	}
	this->path = path;
	
	glGenTextures(1, &texID);
	glBindTexture(GL_TEXTURE_2D, texID);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);

	GLuint filter = (filter_type == TEXTURE_FILTER_LINEAR) ? GL_LINEAR : GL_NEAREST;
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);
	
	GLuint filterMipMap = (filter_type == TEXTURE_FILTER_LINEAR) ? GL_NEAREST_MIPMAP_LINEAR : GL_NEAREST_MIPMAP_NEAREST;

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filterMipMap);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filterMipMap);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 3);
	glGenerateMipmap(GL_TEXTURE_2D);


	stbi_image_free(data);
	loadet = true;
}

void Texture::LoadHDR(std::string path)
{
	float* data = stbi_loadf(path.c_str(), &width, &height, &comp, 4);

	if (!data) {
		std::cout << "Loading texture failed! " << path << "Stb says: '" << stbi_failure_reason() << "'\n";
		return;
	}
	this->path = path;

	glGenTextures(1, &texID);
	glBindTexture(GL_TEXTURE_2D, texID);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	bool genMipmaps = false;
	if(genMipmaps){
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	glGenerateMipmap(GL_TEXTURE_2D);
	}
	stbi_image_free(data);
	loadet = true;
}

void Texture::DrawLine(int x1, int y1, int x2, int y2, float r, float g, float b, float a)
{

	int dx = x2 - x1; if (dx < 0) dx = -dx;
	int dy = y2 - y1; if (dy < 0) dy = -dy;
	int m = dy / dx;
	int y = y1;
	for (int x = x1; x < x2; x++)
	{
		Pixel(x, y, r, b, g, a);
		y = +m;
	}

}

void Texture::DrawElipse(int x1, int y1, int r1, int r2, float r, float g, float b, float a)
{
	int n = 100;
	int R = 100;
	int p1;
	int p2;
	for (int i = 1; i <= n; i++)
	{
		int x =  R * cos(6.28318 * i / n);
		int y =  R * sin(6.28318 * i / n);
		Pixel(x1 + x, y1 + y, r, b, g, a);
		Pixel(x1 - x, y1 + y, r, b, g, a);
		Pixel(x1 + x, y1 - y, r, b, g, a);
		Pixel(x1 - x, y1 - y, r, b, g, a);

	}
}