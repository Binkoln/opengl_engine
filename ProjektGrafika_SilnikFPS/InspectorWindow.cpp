#include "InspectorWindow.h"

InspectorWindow::InspectorWindow() {
	showWindow = true;
}

InspectorWindow::~InspectorWindow() {

}



void InspectorWindow::DrawImGui(Object* selectedObject)
{
	ImGui::SetWindowSize(ImVec2(400, 600), ImGuiCond_Appearing);
	if (showWindow) {
		ImGui::Begin("Object inspector",&showWindow);

		
		if (selectedObject == nullptr) 
		{//Object is not selected
			ImGui::Text("No selected object to inspect.");
			ImGui::Separator();
			
		}
		else if (selectedObject -> isScene()) {

		}
		else //Object is selected
		{


			ImGui::Text((selectedObject->name + std::string(" ") + StringTools::doubleToString(selectedObject->getID())).c_str());

			memset(InputBuffer, 0, 256);
			selectedObject->name.copy(InputBuffer, 256);

			ImGuiInputTextFlags input_text_flags = ImGuiInputTextFlags_EnterReturnsTrue | ImGuiInputTextFlags_AlwaysInsertMode;
			if (!selectedObject->isScene())
				if (ImGui::InputText("Name", InputBuffer, IM_ARRAYSIZE(InputBuffer), input_text_flags, 0, (void*)this)) {
					std::string in(InputBuffer);
					selectedObject->name = in;
				}
			

			ImGui::Separator();
			selectedObject->drawImGuiInspector();

			{//Context menu
				if (ImGui::BeginPopupContextWindow("Context", ImGuiPopupFlags_MouseButtonRight|| ImGuiPopupFlags_NoOpenOverItems)) {
					if (ImGui::Button("Add mesh renderer"))
					{
						//Do smthing
						Extension* e = new ModelRendererExtension();
						selectedObject->extensions.push_back(e);
						ImGui::CloseCurrentPopup();
					}

					if (ImGui::Button("Add sprite renderer"))
					{
						//Do smthing
						Extension* e = new SpriteRendererExtension();
						selectedObject->extensions.push_back(e);
						ImGui::CloseCurrentPopup();
					}

					if (ImGui::Button("Add lua script"))
					{
						//Do smthing
						Extension* e = new LuaScriptExtension();
						selectedObject->extensions.push_back(e);
						ImGui::CloseCurrentPopup();
					}
					if (ImGui::Button("Add PhysX"))
					{
						//Do smthing
						PhysXExtension* e = new PhysXExtension();
						selectedObject->extensions.push_back(e);
						ImGui::CloseCurrentPopup();
					}
					if (ImGui::Button("Add PhysX Character Controller"))
					{
						//Do smthing
						PhysXCharacterControllerExtension* e = new PhysXCharacterControllerExtension();
						selectedObject->extensions.push_back(e);
						ImGui::CloseCurrentPopup();
					}
					if (ImGui::Button("Add Simple Trigger"))
					{
						//Do smthing
						SimpleTriggerExtension* e = new SimpleTriggerExtension();
						selectedObject->extensions.push_back(e);
						ImGui::CloseCurrentPopup();
					}
					if (ImGui::Button("Add Point Light Extension"))
					{
						//Do smthing
						PointLightExtension* e = new PointLightExtension();
						selectedObject->extensions.push_back(e);
						ImGui::CloseCurrentPopup();
					}
					if (ImGui::Button("Add Directional Light Extension"))
					{
						//Do smthing
						DirectionalLightExtension* e = new DirectionalLightExtension();
						selectedObject->extensions.push_back(e);
						ImGui::CloseCurrentPopup();
					}
					if (ImGui::Button("Add Terrain Extension"))
					{
						//Do smthing
						TerrainExtension* e = new TerrainExtension();
						selectedObject->extensions.push_back(e);
						ImGui::CloseCurrentPopup();
					}
					ImGui::EndPopup();
				}
			}//End of context menu
		}
		
		ImGui::End();
	}
}