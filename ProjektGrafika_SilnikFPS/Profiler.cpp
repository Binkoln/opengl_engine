#include "Profiler.h"
#include "imgui/imgui.h"
#include "Object.h"
#include <GLFW/glfw3.h>


Profiler::Profiler() {

}

Profiler::~Profiler() {


}

void Profiler::ResetData()
{
    mainLoopTimes.clear();
    frameTimes.clear();
    renderTimes.clear();
    updateTimes.clear();
    details.clear();
}

void Profiler::Update(Object* scene, double mainLoopTime) {
    if (scene == nullptr) {
        ResetData();
        return;
    }
	
    if (glfwGetTime() - sec_timer > 1.0) {
        sec_timer = glfwGetTime();
        updateTimes.push_back(scene->getRecurentUpdateTimeSum());
        renderTimes.push_back(scene->getRecurentRenderTimeSum());
        frameTimes.push_back(scene->getRecurentRenderTimeSum() + scene->getRecurentUpdateTimeSum());
        mainLoopTimes.push_back(mainLoopTime);
    }
}



void Profiler::OnImGui(Object* sceneObject, double mainLoopTime, double mainRenderTimer, double physXTimer) {
	if (!window_opened)return;
	ImGui::Begin("Profiler", &window_opened);
	ImGui::SetWindowSize( { 300, 200 },ImGuiCond_Appearing);
	

    ImGui::BeginChild("Scene object tree list", ImVec2(250, 0), true, ImGuiWindowFlags_HorizontalScrollbar);
    ImGui::Text("Level:");
    int iterator = 0;
    if (sceneObject != nullptr)
        drawImGuiObjectTree(sceneObject, iterator);

    ImGui::EndChild();

    ImGui::SameLine();
    ImGui::BeginChild("Statistics");

    //Main statistics
        ImGui::Text("Frame statistics:");
        ImGui::SameLine();
        if (ImGui::Button("Reset data")) {
            ResetData();
        }
        if (sceneObject != nullptr) {
            ImGui::Text((std::string("Engine Loop time :") + StringTools::doubleToString(mainLoopTime) + std::string(" ms")).c_str());
            ImGui::Text((std::string("Master Render time :") + StringTools::doubleToString(mainLoopTime) + std::string(" ms")).c_str());
            ImGui::Text((std::string("PhysX simpulation time :") + StringTools::doubleToString(physXTimer) + std::string(" ms")).c_str());
            ImGui::Text((std::string("Frame time :") + StringTools::doubleToString(sceneObject->getRecurentRenderTimeSum() + sceneObject->getRecurentUpdateTimeSum()) + std::string(" ms")).c_str());
            ImGui::Text((std::string("Render time:") + StringTools::doubleToString(sceneObject->getRecurentRenderTimeSum() ) + std::string(" ms")).c_str());
            ImGui::Text((std::string("Update time:") + StringTools::doubleToString(sceneObject->getRecurentUpdateTimeSum()) + std::string(" ms")).c_str());

            ImGui::PlotHistogram("Main loop", (mainLoopTimes.size() > 0) ? &mainLoopTimes[0] : NULL, mainLoopTimes.size(), 0, "Mail loops times  [ms]", 0.0f, 40.0f, ImVec2(0, 100.0f));
            ImGui::PlotHistogram("Frame", (frameTimes.size() > 0) ? &frameTimes[0] : NULL, frameTimes.size(), 0, "Frame times  [ms]", 0.0f, 40.0f, ImVec2(0, 100.0f));
            ImGui::PlotHistogram("Update", (updateTimes.size() > 0)? &updateTimes[0]:NULL, updateTimes.size(), 0, "Update times [ms]", 0.0f, 40.0f, ImVec2(0, 100.0f));
            ImGui::PlotHistogram("Object Render calls", (renderTimes.size() > 0)? &renderTimes[0]:NULL, renderTimes.size(), 0, "Render times [ms]", 0.0f, 40.0f, ImVec2(0, 100.0f));
        }

        if (ImGui::CollapsingHeader("Selected object statistics")) {
            for (int i = 0; i < details.size(); i++)
                if (ImGuiDetailOfObject(details[i], i))
                {
                    details.erase(details.begin() + i);
                    i--;
                }
        }

    ImGui::EndChild();
	ImGui::End();
}

bool Profiler::ImGuiDetailOfObject(Object* object,int i) {
    bool retval = false;
    ImGui::Text(object->name.c_str());
    ImGui::SameLine();
    ImGui::PushID(i);
    if (ImGui::Button("X")) {
        retval = true;
    }
    ImGui::PopID();
    ImGui::Text((std::string("Frame time :") + StringTools::doubleToString(object->getRecurentRenderTimeSum() + object->getRecurentUpdateTimeSum()) + std::string(" ms")).c_str());
    ImGui::Text((std::string("Render time:") + StringTools::doubleToString(object->getRecurentRenderTimeSum()) + std::string(" ms")).c_str());
    ImGui::Text((std::string("Update time:") + StringTools::doubleToString(object->getRecurentUpdateTimeSum()) + std::string(" ms")).c_str());
    ImGui::Separator();
    return retval;
}



void Profiler::drawImGuiObjectTree(Object* o, int& iterator) {
    ImGuiIO& io = ImGui::GetIO();
    ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_DefaultOpen;
    if (!o->hasChilds())
        flags |= ImGuiTreeNodeFlags_Leaf;

    ImGui::PushID(iterator++);
    std::string name = o->name + " ";
    name += StringTools::doubleToString(o->getRecurentRenderTimeSum() + o->getRecurentUpdateTimeSum());
    bool treeNode = ImGui::TreeNodeEx(name.c_str(), flags);

    if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(0)) {
        std::cout << "Open stats\n";
        if(o != nullptr)
        details.push_back(o);
    }

    if (treeNode) {
        for (Object* out : o->childs)
            drawImGuiObjectTree(out, iterator);

        ImGui::TreePop();
    }
    ImGui::PopID();
}