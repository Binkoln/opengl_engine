#ifndef STRING_TOOLS_H
#define STRING_TOOLS_H

#include <vector>
#include <sstream>
#include <string>
#include <glm/glm.hpp>
#include <regex>

/// Klasa zawiera zestaw przydatnych metod do przetwarzania i operacji na tekstach string
class StringTools {
public:
    static bool startWith(std::string const& fullString, std::string const& ending);
    static bool endWith(std::string const& fullString, std::string const& ending);
    static std::vector<std::string> Split(const std::string& subject, std::string splitChars);
    static std::string bakcShlashedToSlashes(std::string in);
    static std::string intToString(int i);
    static std::string getUpperFolderPath(std::string path);
    static std::string getFileName(std::string path);

    static double stringToDouble(std::string in);
    static int stringToInt(std::string in);
    static long stringToLong(std::string in);
    static std::string doubleToString(double in);
    static std::string longToString(long in);
    static std::string vec4ToString(glm::vec4 v);
    static glm::vec4 stringToVec4(std::string in);
    static std::string vec3ToString(glm::vec3 v);
    static glm::vec3 stringToVec3(std::string in);
    static std::string vec2ToString(glm::vec2 v);
    static glm::vec2 stringToVec2(std::string in);
};

#endif // !STRING_TOOLS_H
