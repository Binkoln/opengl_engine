#include "Object.h"

namespace sd {//PhysXExtensionStaticData
    ModelRendererExtension* cubeModelRendererExtension;
    ModelRendererExtension* sphereModelRendererExtension;
}

void PhysXExtension::InitStaticData()
{
    sd::cubeModelRendererExtension = new ModelRendererExtension();
    sd::cubeModelRendererExtension->LoadModel("PhysXCollisionGeometryVisualizationCube", InputManager::getTEngineExeFolderPath() + "/EngineRes/physx/visualizationModels/cube.obj",true);
     sd::sphereModelRendererExtension = new ModelRendererExtension();
    sd::sphereModelRendererExtension->LoadModel("PhysXCollisionGeometryVisualizationCube", InputManager::getTEngineExeFolderPath() + "/EngineRes/physx/visualizationModels/sphere.obj",true);

}

void PhysXExtension::CleanupStaticData()
{
    delete sd::cubeModelRendererExtension;
    delete sd::sphereModelRendererExtension;
}

PhysXExtension::PhysXExtension():Extension("PhysXExtension") {
	type = "PhysX";

}

PhysXExtension::~PhysXExtension() {

}

void PhysXExtension::Init() {
    
}


tinyxml2::XMLElement* PhysXExtension::SaveScene(tinyxml2::XMLElement* e)
{
    using namespace tinyxml2;
    XMLElement* extension = Extension::SaveScene(e);
    extension->InsertNewChildElement("IsTrigger")->InsertNewText((isTrigger) ? "1.0" : "0.0");
    extension->InsertNewChildElement("IsDynamic")->InsertNewText((isDynamic) ? "1.0" : "0.0");
    extension->InsertNewChildElement("CollisionShapeType")->InsertNewText(StringTools::doubleToString(collisionShapeType).c_str());
    extension->InsertNewChildElement("Mass")->InsertNewText(StringTools::doubleToString(mass).c_str());
    extension->InsertNewChildElement("ColliderSize")->InsertNewText(StringTools::vec3ToString(collider_size).c_str());
    extension->InsertNewChildElement("Material")->InsertNewText(StringTools::vec3ToString(material_properties).c_str());
    extension->InsertNewChildElement("Origin")->InsertNewText(StringTools::vec3ToString(shape_origin).c_str());
    if(!collisionModelName.empty()) extension->InsertNewChildElement("collisionModelName")->InsertNewText(collisionModelName.c_str());
    if (!collisionModelName.empty())extension->InsertNewChildElement("collisionModelPath")->InsertNewText(collisionModelPath.c_str());

    return extension;

}

Extension* PhysXExtension::MakeCopy()
{
    PhysXExtension* mre = new PhysXExtension();
    mre->enabled = enabled;
    mre->name = name;

    //Copy
    mre->isTrigger = isTrigger;
    mre->isDynamic = isDynamic;
    mre->mass = mass;
    mre->collisionShapeType = collisionShapeType;
    mre->collider_size = glm::vec3(collider_size);
    mre->material_properties = glm::vec3(material_properties);
    mre->shape_origin = glm::vec3(shape_origin);
    mre->collisionModelName = collisionModelName;
    mre->collisionModelPath = collisionModelPath;
    return mre;
}

void PhysXExtension::OnPlay() {
    initialized = false;
}

void PhysXExtension::Cleanup() {
    if (actor != nullptr)
        actor->release();
    
    if (shape != nullptr)
        shape->release();
    
    

    if (px_mat != nullptr)
        px_mat->release();
    actor = nullptr;
    shape = nullptr;
    px_mat = nullptr;
    if (collisionShapeModel != nullptr) {
        delete collisionShapeModel;
        collisionShapeModel = nullptr;
    }
}

void PhysXExtension::Update(Object* o, Level* level) {
    if (!level->isPlatModeEnabled())return;
    if (!initialized) {
        initialized = true;
        InitPhysX_Body(level->mScene, level, o);
    }
    else {
        //fetch position
        physx::PxTransform pose = actor->getGlobalPose();
        
        glm::vec4 kuten = { pose.q.x ,pose.q.y,pose.q.z,pose.q.w };

        //o->qrot.y = pose.q.y;
       // o->qrot.z = pose.q.z;
       // o->qrot.w = pose.q.w;
       // o->qrot.x = pose.q.x;

        auto pxRotGlobal = glm::eulerAngles(glm::quat(kuten.w, kuten.x, kuten.y, kuten.z));
        
        //o->getWorld
        //if (level->selectedObjectPointer == o) {
          //  auto rt = o->getWorldRotation();
          //  std::cout << "Qrot x:" << rt.x << "y:" << rt.y << "z:" << rt.z << "w:" << rt.w << "\n";
        //}

        kuten = glm::inverse(o->parent->getWorldSpaceRotationMatrix()) * kuten;
        glm::quat newRotation = glm::quat( kuten.w,kuten.x,kuten.y,kuten.z );



        if (isDynamic) {
            o->setWorldPosition({ pose.p.x,pose.p.y,pose.p.z });
            o->rotation = glm::eulerAngles(newRotation);
        }
    }
}

void  PhysXExtension::Render(Object* o, Level* level) {
    //std::cout << "Im alive function\n";
    if (level->showCollisions) {
        level->showCollisions = false;
        if(collisionShapeType == PHYSX_EXTENSION_COLLISION_SHAPE_SPHERE)
            sd::sphereModelRendererExtension->Render(o, level);
        else if (collisionShapeType == PHYSX_EXTENSION_COLLISION_SHAPE_MESH_COLLIDER && collisionShapeModel != nullptr) {
            collisionShapeModel->Render(o, level);
        }
        else
            sd::cubeModelRendererExtension->Render(o, level);
        level->showCollisions = true;
    }
}

void PhysXExtension::InitPhysX_Body(physx::PxScene* scene, Level* level, Object* parent)
{
    Cleanup();
    using namespace physx;
    PxTransform pose;
    auto pos = parent->getWorldPosition();
    pose.p.x = pos.x;
    pose.p.y = pos.y;
    pose.p.z = pos.z;

    glm::quat qrot = parent->getDecomposedWorldRotation(); //parent->getWorldRotation();
    

    pose.q.x = qrot.x;
    pose.q.y = qrot.y;
    pose.q.z = qrot.z;
    pose.q.w = qrot.w;

    glm::vec3 parentScale = parent->getWorldScale();

    if (isDynamic) {
        PxRigidDynamic* rd = scene->getPhysics().createRigidDynamic(pose);
        rd->setMass(mass);
        actor = rd->is<PxRigidActor>();
    }
    else
        actor = scene->getPhysics().createRigidStatic(pose);

    px_mat = scene->getPhysics().createMaterial(material_properties.x, material_properties.y, material_properties.b);



    if (collisionShapeType == PHYSX_EXTENSION_COLLISION_SHAPE_CUBE) {
        PxBoxGeometry geom = PxBoxGeometry(parentScale.x * collider_size.x, parentScale.y * collider_size.y, parentScale.z * collider_size.z);
        PxShape* boxShape = scene->getPhysics().createShape(geom, *px_mat, true);
        PxTransform relativePose(PxVec3(shape_origin.x, shape_origin.y, shape_origin.z));
        boxShape->setLocalPose(relativePose);
        
        actor->attachShape(*boxShape);
    }else if (collisionShapeType == PHYSX_EXTENSION_COLLISION_SHAPE_SPHERE) {
        PxSphereGeometry geom = PxSphereGeometry(glm::max(glm::max(parentScale.x, parentScale.y), parentScale.z) * collider_size.x);
        PxShape* sphereShape = scene->getPhysics().createShape(geom, *px_mat, true);
        PxTransform relativePose(PxVec3(shape_origin.x, shape_origin.y, shape_origin.z));
        sphereShape->setLocalPose(relativePose);
        actor->attachShape(*sphereShape);
    }
    else if (collisionShapeType == PHYSX_EXTENSION_COLLISION_SHAPE_CAPSULE) {
        PxCapsuleGeometry geom = PxCapsuleGeometry(parentScale.x * collider_size.x, parentScale.y * collider_size.y);
        PxShape* capsuleShape = scene->getPhysics().createShape(geom, *px_mat, true);
        PxTransform relativePose(PxVec3(shape_origin.x, shape_origin.y, shape_origin.z), PxQuat(PxHalfPi, PxVec3(0, 0, 1)));
        capsuleShape->setLocalPose(relativePose);
        actor->attachShape(*capsuleShape);
    }
    else if (collisionShapeType == PHYSX_EXTENSION_COLLISION_SHAPE_MESH_COLLIDER && !collisionModelPath.empty()) {
        LoadCollisionModel(collisionModelName, collisionModelPath);
        auto cooking = level->getPhysxCooking();
        auto scale = parent->getWorldScale();
        auto mesh = collisionShapeModel->getModel()->getConsolidatedPhysxTriangleMeshes(cooking, scene, scale);

        auto geom = PxTriangleMeshGeometry(mesh);
        PxShape* meshShape = scene->getPhysics().createShape(geom, *px_mat);
        actor->attachShape(*meshShape);

    }
    

    scene->addActor(*actor);
}

void PhysXExtension::LoadCollisionModel(std::string filename, std::string filePath) {
    collisionModelName = filename;
    collisionModelPath = filePath;

    if (collisionShapeModel != nullptr) {
        delete collisionShapeModel;
        collisionShapeModel = nullptr;
    }

    collisionShapeModel = new ModelRendererExtension();
    collisionShapeModel->LoadModel(filename,filePath,true);


    auto model = collisionShapeModel->getModel();
    for (Mesh* mesh : model->meshes) {
        mesh->material.AddTexture(InputManager::getTEngineExeFolderPath() + "/EngineRes/physx/visualizationModels/box_albedo.png", aiTextureType_DIFFUSE);
    }
}

void  PhysXExtension::DrawImGui(Object* parent) {
    if (ImGui::CollapsingHeader(name.c_str(), ImGuiTreeNodeFlags_DefaultOpen)) {
        Extension::DrawImGui(parent);

        ImGui::Checkbox("Is Trigger", &isTrigger);

        if (isTrigger) {
            isDynamic = false;
        }

        ImGui::Combo("Collider type", &collisionShapeType, "Cube\0Sphere\0Capsule\0Mesh");

        ImGui::DragFloat3("Origin", &shape_origin[0], 0.1f);
        if (collisionShapeType == PHYSX_EXTENSION_COLLISION_SHAPE_CUBE) {
            ImGui::DragFloat3("Box size", &collider_size[0], 0.1f);
        }
        if (collisionShapeType == PHYSX_EXTENSION_COLLISION_SHAPE_SPHERE) {
            ImGui::DragFloat("Sphere radius", &collider_size.x, 0.1f);
        }
        if (collisionShapeType == PHYSX_EXTENSION_COLLISION_SHAPE_CAPSULE) {
            ImGui::DragFloat("Capsule radius", &collider_size.x, 0.1f);
            ImGui::DragFloat("Capsule height", &collider_size.y, 0.1f);
        }
        if (collisionShapeType == PHYSX_EXTENSION_COLLISION_SHAPE_MESH_COLLIDER) {

            ImGui::TextWrapped((collisionShapeModel==nullptr?"Put collision model here": collisionModelName).c_str(), collisionShapeModel == nullptr, 0, ImVec2(300, 30));

            if (ImGui::BeginDragDropTarget()) {
                const ImGuiPayload* pyload = ImGui::AcceptDragDropPayload("ASSET", ImGuiDragDropFlags_None);
                if (pyload != NULL) {

                    struct AssetData* recived = *((struct AssetData**)(pyload->Data));
                    if (recived->type == ASSET_TYPE_MODEL) {
                        //std::cout << " recived model:" << recived->path<<"\n"<< recived->directory << "\n"<< recived->filename << "\n";
                        LoadCollisionModel(recived->filename, recived->path);
                    }
                }

                ImGui::EndDragDropTarget();
            }

            //TODO: parametry mesh collidera
            //ImGui::DragFloat("Capsule radius", &collider_size.x, 0.1f);
            //ImGui::DragFloat("Capsule height", &collider_size.y, 0.1f);
        }

        if(!isTrigger)
            ImGui::Checkbox("Is Dynamic", &isDynamic);
        
        ImGui::DragFloat("Static friction", &material_properties.x, 0.01, 0, 1);
        ImGui::DragFloat("Dynamic friction", &material_properties.y, 0.01, 0, 1);
        ImGui::DragFloat("Restitution", &material_properties.z, 0.01, 0, 1);
      

        if (isDynamic) {
            ImGui::DragFloat("Mass", &mass);
        }



        if (ImGui::Button("InitMeeeee!")) {
            initialized = false;
        }
        


        ImGui::Text("PhysX Extension");

    }
    ImGui::Separator();
}