extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}
#include "TEngine.h"
#include "tinyxml2/tinyxml2.h"
#include "StringTools.h"
#include "ProfilerTimerUnit.h"



/*wyci�te problematyczne liby z linkera
HACD_Debug.lib
gtest_Debug.lib
gwen_Debug.lib
*/
void pseudoTest()
{
    for (int k = 0; k < 5; k++) {
        int i;
        std::cin >> i;

        long it = 0;
        Object* parnt = new Object("Main", it++);

        Object* op = parnt;
        for (int i = 0; i < 999; i++) {
            Object* on = new Object("Hello fello world!", it++);
            for (int j = 0; j < 3999; j++) {
                LuaScriptExtension* ex = new LuaScriptExtension();
                on->addExtension(ex);
            }
            op->put(on);
            op = on;
        }
        std::cout << "!!!!Utworzono\n\n\n";

        std::cin >> i;
        parnt->Cleanup();
        delete parnt;
        std::cout << k <<" : Usunieto!!!!\n";
        std::cin >> i;
    }
}


int main(int argc,char** argv)
{

    
    //pseudoTest();
    //return 0;

    //btDiscreteDynamicsWorld* bt_world;
   
    struct TEngineStartupOptions run_options;
    run_options.mode = 0;
    run_options.projectPath = "EngineProjectFolder";
    run_options.level_to_load = 0;

    if(false){
        using namespace tinyxml2;
        XMLDocument doc;
       
        XMLElement* e = doc.NewElement("TEngineProjectVersion");
        doc.InsertFirstChild(e);
        e->InsertNewText("1.0");
        e = doc.NewElement("ProjectPath");
        doc.InsertEndChild(e);
        e->InsertNewText("EngineProjectFolder");
        doc.SaveFile("TestProject.tengproj");
    }

    for (int i = 0; i < argc; i++)
        std::cout << argv[i] << "\n";

        if (argc >= 2) {
    std::string project_file( argv[1]);
            if (StringTools::endWith(project_file, "tengproj")) {
                using namespace tinyxml2;
                XMLDocument doc;
                XMLError eResult = doc.LoadFile(project_file.c_str());
                
                if (eResult != XML_SUCCESS)
                {
                    std::cout << "Error:Can't not open project file.\n";
                    std::getchar();
                    return -1;
                }

                std::string projFolderPath (doc.FirstChildElement("ProjectPath")->GetText());
                //std::cout << projFolderPath << "\n";
                run_options.projectPath = StringTools::bakcShlashedToSlashes(projFolderPath);
                
            }
            else {
                std::cout << "Error:Opened not a TEngine project file!!!\n";
                std::getchar();
                return -1;
            }
        }
    
    ///*lua testing
        lua_State* L = luaL_newstate();
        luaL_openlibs(L);
        std::string luaSmthing = "a = 10+16 + math.sin(22232)";
        int r = luaL_dostring(L,luaSmthing.c_str());
        r = luaL_dostring(L, "paint a");

           // if (isLuaOk(L,r)) {
          //  lua_getglobal(L, "a");
         //   if (lua_isnumber(L, -1)) {
        //        float numbr = (float)lua_tonumber(L, -1);
       //         std::cout << "A:" << numbr << "\n";
      //      }
     //   }


    //end of lua testing*/


    TEngine engineInstance;
    engineInstance.inputManager.setTEngineExeFolderPath(StringTools::getUpperFolderPath(StringTools::bakcShlashedToSlashes( argv[0])));
    //std::cout << "EXEFolderPath:" << StringTools::getUpperFolderPath(StringTools::bakcShlashedToSlashes(argv[0])) << "\n";


    int retval = engineInstance.Init(run_options);

    if (retval != 0) {
        engineInstance.Cleanup();
        return -1;
    }
    
    retval = engineInstance.Run();
    engineInstance.Cleanup();



    return 0;
}

