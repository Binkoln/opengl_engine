#include "InputManager.h"
#define MAX_KEYS 300
static GLFWwindow* windowForInputBindings;
static bool prevKeyboardStates[MAX_KEYS];
namespace INPUTMANAGER_BLACKBARD{
	static int width,height;
	static bool fpsModeEnabled = false;
	static glm::vec2 mouse_move(0, 0);
	static std::string tengine_folder_path;
	static int fps;
	static double mainLoopTimer = 0;
}

double InputManager::getMainLoopTimeProfiler() { return INPUTMANAGER_BLACKBARD::mainLoopTimer; }
void InputManager::setMainLoopTimeProfiler(double mainLoopTimeProfiler) { INPUTMANAGER_BLACKBARD::mainLoopTimer = mainLoopTimeProfiler; }

int InputManager::getCurrentFPS()
{
	return INPUTMANAGER_BLACKBARD::fps;
}
void InputManager::setCurrentFPS(int fps_value)
{
	INPUTMANAGER_BLACKBARD::fps = fps_value;
}

InputManager::InputManager() {

}

InputManager::~InputManager()
{

}

void window_size_callback(GLFWwindow* window, int width, int height)
{
	if (width == 0 || height == 0)return;
	INPUTMANAGER_BLACKBARD::width = width;
	INPUTMANAGER_BLACKBARD::height = height;
}


void InputManager::Init(GLFWwindow* window)
{
	windowForInputBindings = window;
	glfwGetWindowSize(windowForInputBindings, &INPUTMANAGER_BLACKBARD::width, &INPUTMANAGER_BLACKBARD::height);
	glfwSetWindowSizeCallback(window, window_size_callback);
	for (int i = 0; i < MAX_KEYS; i++)
		prevKeyboardStates[i] = false;
}

void InputManager::Cleanup()
{

}

void FPS_CAMERA_CALC() {
	int w, h;
	double xpos, ypos;
	INPUTMANAGER_BLACKBARD::mouse_move = glm::vec2(0, 0);
	glfwGetCursorPos(windowForInputBindings, &xpos, &ypos);
	if (!INPUTMANAGER_BLACKBARD::fpsModeEnabled) {
		glfwSetInputMode(windowForInputBindings, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		INPUTMANAGER_BLACKBARD::mouse_move = glm::vec2(xpos, ypos);
	}
	else {

		glfwGetWindowSize(windowForInputBindings, &w, &h);
		glfwSetInputMode(windowForInputBindings, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		double Xmove = xpos - (w / 2);
		double Ymove = ypos - (h / 2);
		INPUTMANAGER_BLACKBARD::mouse_move.x -= Ymove;
		INPUTMANAGER_BLACKBARD::mouse_move.y += Xmove;

		glfwSetCursorPos(windowForInputBindings, (w / 2), (h / 2));

	}
}

void InputManager::Update()
{

	FPS_CAMERA_CALC();

	for (int i = 0; i < MAX_KEYS; i++)
		if (prevKeyboardStates[i])
			if (!getKey(i))
				prevKeyboardStates[i] = false;
}
bool InputManager::getMouseLPM()
{
	return glfwGetMouseButton(windowForInputBindings, GLFW_MOUSE_BUTTON_1);
}

bool InputManager::getKey(int key)
{
	return glfwGetKey(windowForInputBindings, key)==GLFW_PRESS;
}



bool InputManager::getKeyOnce(int key)
{
	if (key >= MAX_KEYS) { printf("INPUT MANAGER ERROR, out of bound key:%d",key); return false; }

	if (getKey(key) && !prevKeyboardStates[key]) {
		prevKeyboardStates[key] = true;
		return true;
	}


	return false;
}



int InputManager::getWindowWidth() {
	 return INPUTMANAGER_BLACKBARD::width;
}


int InputManager::getWindowHeight() {
	return INPUTMANAGER_BLACKBARD::height;
}

glm::vec2 InputManager::setMouseFPSMouseMode(bool fpsModeEnabled)
{
	INPUTMANAGER_BLACKBARD::fpsModeEnabled = fpsModeEnabled;
	return INPUTMANAGER_BLACKBARD::mouse_move;
}


glm::vec2 InputManager::getMousePos()
{
	if (INPUTMANAGER_BLACKBARD::fpsModeEnabled)return { getWindowWidth() / 2,getWindowHeight() / 2 };
	return INPUTMANAGER_BLACKBARD:: mouse_move;
}

bool InputManager::pressMouseButton(int button)
{
	return glfwGetMouseButton(windowForInputBindings, button) == GLFW_PRESS;
}

std::string InputManager::getTEngineExeFolderPath()
{
	return INPUTMANAGER_BLACKBARD::tengine_folder_path;
}

void InputManager::setTEngineExeFolderPath(std::string path)
{
	INPUTMANAGER_BLACKBARD::tengine_folder_path = path;
}