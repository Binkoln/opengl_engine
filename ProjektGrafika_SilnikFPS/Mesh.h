#ifndef MESH_H
#define MESH_H

#include <iostream>
#include <vector>
#include <string>
#include <glm/glm.hpp>
#include <gl/glew.h>
#include <GL/GL.h>
#include "Material.h"


/// Struktura przechowuje informacje o wierzcho�ku siatki Mesh
struct MeshVertex {
	glm::vec3 position;
	glm::vec2 UV;
	glm::vec3 normal;
	glm::vec3 tangent;
	glm::vec3 bitangent;
	//vertex can be transformed by max 3 bones
	glm::vec3 boneWeights;
	glm::ivec3 boneIDs;
};

/// Klasa przechowuje informacje o za�adowanej siatce Mesh kt�ra jest cz�ci� modelu Model
/// @see Model
class Mesh {
public:
	Mesh(std::string mesh_name);
	~Mesh();

	///�aduje siatk� do GPU
	///
	/// @param std::vector<struct MeshVertex> vertices - lista wierzcho�k�w do za�adowania
	/// @param std::vector<unsigned int> indiecies - lista element�w do za�adowania (patrz dokumentacja OpenGl glDrawElements(...))
	/// @param unsigned int material_index - id materia�u w li�cie materia��w Material w obiekcie Model
	/// @see Model
	bool LoadMesh(std::vector<struct MeshVertex> vertices, std::vector<unsigned int> indiecies, unsigned int material_index);
	/// Zwraca czy model zosta� ju� za�adowany
	inline bool isLoaded() { return loaded; }
	inline std::string getName() { return mesh_name; }
	GLuint VAO, VBO, EBO;
	unsigned int indi_count;
	Material material;
	inline std::vector<struct MeshVertex> getVertices() { return vertices; }
	inline std::vector<unsigned int> getIndieces() { return indiecies; }

private:
	bool loaded = false;
	std::vector<struct MeshVertex> vertices;
	std::vector<unsigned int> indiecies;
	std::string mesh_name;
};
#endif // !MESH_H
