#include "Object.h"

Object::Object(std::string name,long id) {
	this->name = name;
	parent = nullptr;
	this->id = id;
	
}

Object::Object(std::string name, Object* parent,long id) {
	this->name = name;
	this->parent = parent;
	parent->put(this);
	this->id = id;
}


Object* Object::MakeCopy()
{
	Object* o = new Object(name, Level::GetObjectID());
	o->position = { position.x,position.y,position.z };
	o->rotation = { rotation.x,rotation.y,rotation.z };
	o->qrot = getRotation();
	o->scale = { scale.x,scale.y,scale.z };
	o->type = type;
	for (Object* ch : childs)
		o->put(ch->MakeCopy());

	for (Extension* e : extensions)
		o->addExtension(e->MakeCopy());

	return o;

}


Object::~Object()
{
	for (Object* o : childs) {
		if (o == nullptr)continue;
		o->Cleanup();
		delete o;
		o = nullptr;
	}
	childs.clear();


}
void Object::Cleanup()
{
	for (Extension* e : extensions) {
		

		if (e->type == "ModelRendererExtension") {
			ModelRendererExtension* me = (ModelRendererExtension*)e;
			e->Cleanup();
			delete me;
		}
		else if (e->type == "SpriteRenderer") {
			SpriteRendererExtension* le = (SpriteRendererExtension*)e;
			e->Cleanup();
			delete le;
		}
		else if (e->type == "LuaScript") {
			LuaScriptExtension* le = (LuaScriptExtension*)e;
			e->Cleanup();
			delete le;
		}
		else {
			e->Cleanup();
			delete e;
		}

		e = nullptr;
	}
}



tinyxml2::XMLElement* Object::SaveScene(tinyxml2::XMLElement* e)
{
	using namespace tinyxml2;
	XMLElement* objNode = e->InsertNewChildElement("Object");
	objNode->InsertNewChildElement("ID")->InsertNewText(StringTools::longToString(id).c_str());
	objNode->InsertNewChildElement("Type")->InsertNewText(type.c_str());
	objNode->InsertNewChildElement("Name")->InsertNewText(name.c_str());

	XMLElement* positionNode = objNode->InsertNewChildElement("Position");
	positionNode->InsertNewText(StringTools::vec3ToString(position).c_str());
	XMLElement* rotNode = objNode->InsertNewChildElement("Rotation");
	rotNode->InsertNewText(StringTools::vec3ToString(rotation).c_str());
	XMLElement* scaleNode = objNode->InsertNewChildElement("Scale");
	scaleNode->InsertNewText(StringTools::vec3ToString(scale).c_str());


	XMLElement* extensions = objNode->InsertNewChildElement("Extensions");
	for (Extension* e : this->extensions)
		e->SaveScene(extensions);

	XMLElement* childs = objNode->InsertNewChildElement("Childs");

	for (Object* chld : this->childs)
		chld->SaveScene(childs);

	return objNode;
}

Object* Object::LoadScene(tinyxml2::XMLNode* obj)
{
	using namespace tinyxml2;


	if (obj == nullptr)return nullptr;
	//data fetching
	XMLElement* s_id = obj->FirstChildElement("ID");
	XMLElement* name = obj->FirstChildElement("Name");
	XMLElement* pos = obj->FirstChildElement("Position");
	XMLElement* rot = obj->FirstChildElement("Rotation");
	XMLElement* sca = obj->FirstChildElement("Scale");
	

	std::string objType = obj->FirstChildElement("Type")->GetText();

	long id = StringTools::stringToLong(s_id->GetText());

	Object* retO;
	if (objType == "Camera") {
		Camera* c = new Camera(id);
		c->LoadAdditionalData(obj);
		retO = c;
		retO->name = name->GetText();
	}
	else {
		retO = new Object(name->GetText(),id);
	}

	retO->position = StringTools::stringToVec3(pos->GetText());
	retO->rotation = StringTools::stringToVec3(rot->GetText());
	retO->scale = StringTools::stringToVec3(sca->GetText());

	XMLElement* extE = obj->FirstChildElement("Extensions");
	if (!extE->NoChildren()) {
		XMLNode* exObj;

		exObj = extE->FirstChildElement("Extension");
		while (true) {
			retO->extensions.push_back(Extension::LoadScene(exObj));
			if (exObj == extE->LastChild()) {
				break;
			}
			else
				exObj = exObj->NextSiblingElement("Extension");
		}
	}


	//rekurencja
	XMLElement* childsE = obj->FirstChildElement("Childs");
	if (!childsE->NoChildren()) {
		XMLNode* chldObj;
		
		chldObj = childsE->FirstChildElement("Object");
		while (true) {
			retO->put(LoadScene(chldObj));
			if (chldObj == childsE->LastChild()) {
				break;
			}else
				chldObj = chldObj->NextSiblingElement("Object");
		}

	}
	

	return retO;
}

void Object::OnPlay()
{
	for (Extension* e : extensions)
		e->OnPlay();

	for (Object* o : childs)
		o->OnPlay();
}

void Object::PushScriptingVariables()
{
	for (Extension* e : extensions) {
		if (e->type == "LuaScript") {
			LuaScriptExtension* lse = (LuaScriptExtension *) e;
			lse->PushVarsToLua();
		}
	}

	for (Object* o : childs)
		o->PushScriptingVariables();
}

void Object::objectActionNotify(Object* o, unsigned int action)
{
	for (Object* c : childs)
		c->objectActionNotify(o, action);


}

void Object::Init()
{

}

void Object::Update(Level* level)
{
	updateTimer.StartTimer();
	this->level = level;

	qrot = glm::quat(rotation);

	for (Extension* e : extensions)
		if(e->enabled)
			e->Update(this,level);

	for (int i = 0; i < childs.size();i++) {
		Object* o = childs[i];
		o->Update(level);
	}
	updateTimer.StopTimer();
}

void Object::Render(Level* level)
{
	renderTimer.StartTimer();
	for (Extension* e : extensions)
		if (e->enabled)
			e->Render(this,level);

	for (Object* o : childs)
		o->Render(level);
	renderTimer.StopTimer();
}

double Object::getRecurentRenderTimeSum()
{
	double sum = renderTimer.GetTimeMesurement();
	//for (Object* child : childs)
		//sum += child->renderTimer.GetAvreageTimeMS();
	return sum;
}

double Object::getRecurentUpdateTimeSum()
{
	double sum = updateTimer.GetTimeMesurement();
	//for (Object* child : childs)
		//sum += child->updateTimer.GetAvreageTimeMS();
	return sum;
}

void Object::ResetTimers() {
	renderTimer.Reset();
	updateTimer.Reset();
	for (Object* child : childs)
		child->ResetTimers();
}

//Functions for hierarchy managment
void Object::put(Object* o) {
	childs.push_back(o);
	o->parent = this;
}

void Object::DeleteObject() {
	SimpleTriggerExtension::DeleteNotification(this);
	Level::RegisterObjectToDeletion(this);
}

void Object::deleteChild(Object* o) {
	SimpleTriggerExtension::DeleteNotification(this);
	for (int i = 0; i < childs.size();i++) {
		if (o == childs[i]) {
			childs.erase(childs.begin() + i);
			break;
		}

	}
}

bool Object::isChildOf(Object* par) {
	if (isScene())return false;
	if (par == parent)return true;
	return parent->isChildOf(par);
}

bool Object::unchild() {
	if (parent == nullptr)return false;
	parent->deleteChild(this);
	parent = nullptr;
	return true;
}

Extension* Object::getExtensionByName(std::string name)
{
	Extension* retEx = nullptr;

	for (Extension* e : extensions) {
		if (e->name == name) {
			return e;
		}
	}

	return nullptr;
}

glm::mat4 Object::getViewMatrix()
{
	

		glm::mat4 transformMatrix = glm::mat4(1.0f);
		//transformMatrix = glm::scale(transformMatrix, glm::vec3(1.0/scale.x, 1.0 / scale.y, 1.0 / scale.z));

		qrot = glm::quat(rotation);
		transformMatrix = transformMatrix * glm::mat4_cast(glm::conjugate(qrot));
		transformMatrix = glm::translate(transformMatrix, -position);

		glm::mat4 parentTransform = glm::mat4(1.0f);

		if (!isScene())
			parentTransform = parent->getViewMatrix();

		//forward = parentTransform*transformMatrix * forward;

		return transformMatrix*parentTransform;


}

glm::quat makeQ(glm::vec3 axis, float angl)
{
	float sn = sin(angl*0.5f);
	glm::quat e;
	e.x = axis.x * sn;
	e.y = axis.y * sn;
	e.z = axis.z * sn;
	e.w = cos(angl * 0.5f);
	return e;
}


glm::mat4 Object::getWorldSpaceRotationMatrix()
{
	glm::mat4 transformMatrix = glm::mat4(1.0f);
	qrot = getRotation();
	transformMatrix = transformMatrix * glm::mat4_cast(qrot);

	//todo: remove recurency
	glm::mat4 parentTransform = glm::mat4(1.0f);

	if (!isScene())
		parentTransform = parent->getWorldSpaceRotationMatrix();

	transformMatrix = parentTransform * transformMatrix;

	return transformMatrix;
}

glm::mat4 Object::getWorldSpaceTransformationMatrix()
{
	glm::mat4 transformMatrix = glm::mat4(1.0f);
	transformMatrix = glm::translate(transformMatrix, position);
	qrot = getRotation();
	transformMatrix = transformMatrix * glm::mat4_cast(qrot);
	
	transformMatrix = glm::scale(transformMatrix, scale);
	//todo: remove recurency
	glm::mat4 parentTransform = glm::mat4(1.0f);
	
	if (!isScene())
		parentTransform = parent->getWorldSpaceTransformationMatrix();

	transformMatrix = parentTransform* transformMatrix;

	return transformMatrix;
}

Object* Object::findObjectInLevelByID(long id)
{
	if (getID() == id)return this;

	Object* ret = nullptr;

	for (Object* o : childs) {
		ret = o->findObjectInLevelByID(id);
		if (ret != nullptr)
			return ret;
	}

	return ret;
}

Object* Object::findFirstObjectWithName(std::string name)
{
	if (name == this->name)return this;

	Object* ret = nullptr;

	for (Object* o : childs) {
		ret = o->findFirstObjectWithName(name);
		if (ret != nullptr)
			return ret;
	}

	return ret;
}

void Object::drawImGuiInspector() {
	drawImGuiTransformation();

	//Extensions
	int i = 0;
	for (Extension* e : extensions) {
		ImGui::PushID(i++);
		e->DrawImGui(this);
		ImGui::PopID();
		if (e->toDelete) {
			deleteExtension(e);
			break;
			//Todo: notify level ?
		}
	}
}

void Object::addExtension(Extension* e)
{
	extensions.push_back(e);
}

void Object::deleteExtension(Extension* e)
{
	for (int i = 0; i < extensions.size(); i++) {
		if (extensions[i] == e) {
			extensions.erase(extensions.begin() + i);
			e->Cleanup();
			delete e;
			break;
		}
	}

}

void  Object::drawImGuiTransformation() {
	if(ImGui::CollapsingHeader("Transformation:",ImGuiTreeNodeFlags_DefaultOpen)){
		float slideSpeed = 0.01f;
		float vec3[] = { position.x,position.y,position.z };
		ImGui::DragFloat3("Position", vec3, slideSpeed);
		for (int i = 0; i < 3; i++)
			position[i] = vec3[i];

		for (int i = 0; i < 3; i++)
			vec3[i] = rotation[i];
		ImGui::DragFloat3("Rotation", vec3, slideSpeed);
		for (int i = 0; i < 3; i++)
			rotation[i] = vec3[i];

		for (int i = 0; i < 3; i++)
			vec3[i] = scale[i];
		ImGui::DragFloat3("Scale", vec3, slideSpeed);
		for (int i = 0; i < 3; i++)
			scale[i] = vec3[i];

		ImGui::Separator();
		//for debuging only todo: remove after creating tests
		/*if (!isScene() & false) {
			Object* req = this;
			while (!req->isScene()) {
				glm::vec4 pos4(req->position.x, req->position.y, req->position.z, 1);


				glm::vec4 cosik = req->parent->getWorldSpaceTransformationMatrix() * pos4;

				for (int i = 0; i < 3; i++)
					vec3[i] = cosik[i];
				ImGui::DragFloat3("WorldSpace pos", vec3, 0);




				ImGui::Separator();
				req = req->parent;
			}
		}*/

	}
}