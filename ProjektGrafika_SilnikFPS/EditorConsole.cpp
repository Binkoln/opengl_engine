#include "EditorConsole.h"

EditorConsole::EditorConsole() {

}


EditorConsole::~EditorConsole() {

}

void EditorConsole::Init(void (*ProcessCommand)(std::string in)) {
	this->ProcessCommand = ProcessCommand;
	
}


void EditorConsole::pushHelpNode(const char* title, const char* content) {
	if (ImGui::TreeNode(title)) {
		ImGui::Text(content);
		ImGui::TreePop();
	}
}


void EditorConsole::DrawImGui() {
	if (consoleWindow) {
		ImGui::Begin("Console", &consoleWindow);
		ImGui::SetWindowSize(ImVec2(700, 1000), ImGuiCond_Appearing);
		ImGuiInputTextFlags input_text_flags = ImGuiInputTextFlags_EnterReturnsTrue| ImGuiInputTextFlags_AlwaysInsertMode;
		bool reclaim_focus = false;
		if (ImGui::InputText("Input", InputBuffer, IM_ARRAYSIZE(InputBuffer), input_text_flags, 0, (void*)this)) {
			std::string in(InputBuffer);
			memset(InputBuffer, 0, 256);
			ProcessCommand(in);
			reclaim_focus = true;
		}
		ImGui::SetItemDefaultFocus();
		if (reclaim_focus)
			ImGui::SetKeyboardFocusHere(-1); // Auto focus previous widget
		ImGui::SameLine();
		ImGui::TextDisabled("(?)");
		if (ImGui::IsItemHovered())
		{
			ImGui::BeginTooltip();
			ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
			ImGui::TextUnformatted("Type 'HELP' or '?' for help.");
			ImGui::PopTextWrapPos();
			ImGui::EndTooltip();
		}

		ImGui::End();
	}


	if (showHelpWindow) {
		ImGui::Begin("Console Help",&showHelpWindow);

		ImGui::Text("HELP welcome text. Format is like: What it is : command [arg1] [arg2] ... [argn]");
		ImGui::Separator();
		pushHelpNode("Help window : help", "Shows informations about commands.");
		pushHelpNode("Demo window : demo", "Shows ImGui demo window. TODO: Remove this function\n after fully explored.");
		pushHelpNode("Asset explorer : ae", "Opens Asset Explorer of project, that allows you to asigns\n diffrent project resources to varient of things.");
		pushHelpNode("Level objects : lo", "Shows hierarchy of objects plced on level. Allows you to find\n and select object for modification its parameters in object inspector.");
		pushHelpNode("Object inspectot : ins", "Shows object inspector that allows you to \nmodify objects parameters and extend its functionality");

		ImGui::End();
	}

}