#include "DefferedRenderer.h"


float lerp(float a, float b, float t) {
	return ((1.0f - t) * a) + (t * b);
}

void DefferedRenderer::Init(int framebuffer_width, int framebuffer_height, Framebuffer* defferedComponents)
{
	const float vertices[] = { -1.0f,1.0f,0.0f,0.0f,
								1.0f,1.0f,1.0f,0.0f ,
							  -1.0f,-1.0f,0.0f,1.0f,
								1.0f,-1.0f,1.0f,1.0f };

	const unsigned int indieces[] = { 0,1,2,1,3,2 };

	//OpenGl Sprite Mesh Init
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indieces), indieces, GL_STATIC_DRAW);

	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4, (void*)0);

	deffered_shader = Shader("EngineProjectFolder/assets/shaders/deffered_v_b_s2.glsl", "EngineProjectFolder/assets/shaders/deffered_f_b_s2.glsl");
	motionBlurShader = Shader("EngineProjectFolder/assets/shaders/motionblur/motion_blur_v.glsl", "EngineProjectFolder/assets/shaders/motionblur/motion_blur_f.glsl");
	bloomInitPassShader = Shader("EngineProjectFolder/assets/shaders/bloom/bloom_v_initial_pass.glsl", "EngineProjectFolder/assets/shaders/bloom/bloom_f_initial_pass.glsl");
	bloomDownPassShader = Shader("EngineProjectFolder/assets/shaders/bloom/bloom_v_down_pass.glsl", "EngineProjectFolder/assets/shaders/bloom/bloom_f_down_pass.glsl");
	bloomUpPassShader = Shader("EngineProjectFolder/assets/shaders/bloom/bloom_v_up_pass.glsl", "EngineProjectFolder/assets/shaders/bloom/bloom_f_up_pass.glsl");
	AcesTonemappingShader = Shader("EngineProjectFolder/assets/shaders/tonemapping/aces_tonemapping_v.glsl", "EngineProjectFolder/assets/shaders/tonemapping/aces_tonemapping_f.glsl");
	SSAOshader = Shader("EngineProjectFolder/assets/shaders/ssao/ssao_v.glsl", "EngineProjectFolder/assets/shaders/ssao/ssao_f.glsl");
	SSAOBlurShader = Shader("EngineProjectFolder/assets/shaders/ssao/ssao_blur_v.glsl", "EngineProjectFolder/assets/shaders/ssao/ssao_blur_f.glsl");
 

	t_enviroment = new Texture();
	t_enviroment->LoadHDR("EngineProjectFolder/IBL/HDR1.hdr");

	t_noise = new Texture();
	t_noise->Load("EngineProjectFolder/noise.png");

	bloomPasses = new Framebuffer*[BLOOM_DOWN_PASS_COUNT + BLOOM_UP_PASS_COUNT];
	//Down pass setup
	for (int i = 0; i < BLOOM_DOWN_PASS_COUNT; i++) {
		bloomPasses[i] = new Framebuffer();
		int width  = framebuffer_width  / 2;
		int height = framebuffer_height / 2;
		if (i > 0) {
			width  = bloomPasses[i - 1]->getWidth()  / 2;
			height = bloomPasses[i - 1]->getHeight() / 2;
		}
		bloomPasses[i]->Init(width, height, FRAMEBUFFER_TYPE_COLOR_AND_DEPTH);
		//std::cout << "FB bloom i:" << i <<"   width=" << width << "  height=" << height << "\n";
	}

	//Up pass setup
	for (int i = BLOOM_DOWN_PASS_COUNT; i < BLOOM_DOWN_PASS_COUNT + BLOOM_UP_PASS_COUNT; i++) {
		bloomPasses[i] = new Framebuffer();
		int width = bloomPasses[i - 1]->getWidth()   * 2;
		int height = bloomPasses[i - 1]->getHeight() * 2;

		bloomPasses[i]->Init(width, height, FRAMEBUFFER_TYPE_COLOR_AND_DEPTH);
	}


	blour_buffor = new Framebuffer();
	blour_buffor->Init(framebuffer_width, framebuffer_height, FRAMEBUFFER_TYPE_COLOR_AND_DEPTH);
	blour_buffor->SetDepthBufferSource(defferedComponents->GetDepthBufferSource());


	//SSAO - Init
	//1. Generate samples
	std::random_device rd;
	std::default_random_engine eng(rd());
	std::uniform_real_distribution<double> dist1(-1, 1);
	std::uniform_real_distribution<double> dist2(0, 1);

	SSAO_SAMPLES_COUNT = 64;
	InitSamples();
	//2.Generate random texture
	std::vector<glm::vec3> noiseTextureValues;
	int noiseTextureSize = 4;
	for (int i = 0; i < noiseTextureSize * noiseTextureSize; i++) {
		glm::vec3 dir(dist1(eng), dist1(eng), 0.0f);
		//dir *= 0.5f;
		//dir += 0.5f;
		dir = glm::normalize(dir);

		noiseTextureValues.push_back(dir);
	}

	glGenTextures(1, &noiseTexID);
	glBindTexture(GL_TEXTURE_2D, noiseTexID);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, noiseTextureSize, noiseTextureSize, 0, GL_RGB,GL_FLOAT, &noiseTextureValues[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBindTexture(GL_TEXTURE_2D, 0);
	
	//3. Generate framebuffers
	ssao_fb = new Framebuffer();
	ssao_fb->Init((int)(framebuffer_width), (int)(framebuffer_height), FRAMEBUFFER_TYPE_COLOR);

	ssao_blur_fb = new Framebuffer();
	ssao_blur_fb->Init(framebuffer_width, framebuffer_height, FRAMEBUFFER_TYPE_COLOR);

	//End of SSAO init
}

void DefferedRenderer::InitSamples()
{
	std::random_device rd;
	std::default_random_engine eng(rd());
	std::uniform_real_distribution<double> dist1(-1, 1);
	std::uniform_real_distribution<double> dist2(0, 1);
	ssaoSamples.clear();
	for (int i = 0; i < SSAO_SAMPLES_COUNT; i++) {
		//std::cout << "plu:" << dist1(eng)<< "\n";
		glm::vec3 sample(dist1(eng), dist1(eng), dist2(eng));
		sample = glm::normalize(sample);
		sample *= dist2(eng);
		float s = (float)i / (float)SSAO_SAMPLES_COUNT;
		float scale = lerp(0.1f, 1.0f, s * s);

		sample *= scale;


		ssaoSamples.push_back(sample);
	}
}

void DefferedRenderer::RenderSSAO(Framebuffer* deffered_components,Camera* c)
{
	if (c == nullptr)return;
	ssao_fb->Bind();
 	//glClearColor(0.5, 0, 0, 1);
	//glClear(GL_COLOR_BUFFER_BIT );
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glDisable(GL_CULL_FACE);
	//Use ssaoShader

	glBindVertexArray(VAO);//Wysy�amy wierzcho�ki
	SSAOshader.Use();


	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, deffered_components->textureViewSpaceNormalID);
	SSAOshader.SetTextureSampler("normalsTex", 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, deffered_components->textureViewSpacePositionID);
	SSAOshader.SetTextureSampler("positionsTex", 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, deffered_components->textureDepthID);
	SSAOshader.SetTextureSampler("depthTex", 2);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, noiseTexID);
	SSAOshader.SetTextureSampler("noiseTex", 3);

	SSAOshader.SetVector2f("texel", glm::vec2(ssao_fb->getWidth() * 0.25f, ssao_fb->getHeight() * 0.25f));

	SSAOshader.SetMatrix4("camProjection",  c->getProjection());
	SSAOshader.SetMatrix4("camView", c->getViewMatrix());

	
	if (c->kernelSize != SSAO_SAMPLES_COUNT) {
		SSAO_SAMPLES_COUNT = c->kernelSize;
		InitSamples();
	}

	for (int i = 0; i < ssaoSamples.size(); i++) {

		SSAOshader.SetVector3f(("uSampleKernel[" + StringTools::intToString(i) + "]").c_str(), ssaoSamples[i]);
	}



	SSAOshader.SetInt("kernelSize", c->kernelSize);
	SSAOshader.SetFloat("radius", c->radius);
	SSAOshader.SetFloat("bias", c->bias);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);


	ssao_fb->Unbind();

	//=================  Bluring result ==================================
	ssao_blur_fb->Bind();
	glBindVertexArray(VAO);

	glClearColor(0.5, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT);

	SSAOBlurShader.Use();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, ssao_fb->textureColorID);
	SSAOBlurShader.SetTextureSampler("ssaoInput", 0);

	SSAOBlurShader.SetVector2f("fb_size", glm::vec2(ssao_fb->getWidth(), ssao_fb->getHeight()));
	SSAOBlurShader.SetFloat("pass_dumper", 1.0f);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);


	ssao_blur_fb->Unbind();
	//End of BLUR=========================

	glEnable(GL_DEPTH_TEST);
}

void DefferedRenderer::RenderMotionBlur(Framebuffer** in_out_image, GLuint velocityTextureID)
{
	Framebuffer* input = *in_out_image;
	blour_buffor->Bind();
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_BLEND);//W��czamy blendowanie aplha
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_DEPTH_TEST);// W��czamy rendering 2D
	glDisable(GL_CULL_FACE);


	motionBlurShader.Use();
	glBindVertexArray(VAO);//Wysy�amy wierzcho�ki


	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, input->textureColorID);
	motionBlurShader.SetTextureSampler("input_image", 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, velocityTextureID);
	motionBlurShader.SetTextureSampler("velocityMap", 1);

	motionBlurShader.SetFloat("motionblur_fps_adjustment", (float)InputManager::getCurrentFPS() / 60.0f);


	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);


	blour_buffor->Unbind();

	//Swap buffers :-) .... Nasty nasty hehe
	*in_out_image = blour_buffor;
	blour_buffor = input;

}


void DefferedRenderer::RenderModel(Shader* shad,Model* model, glm::mat4 transform, glm::mat4 prevTransform, Camera* camera, glm::vec3 objectIDColor,float noise_treshold)
{
	


	shad->Use();

	
	
	

	for (int i = 0; i < (*model).meshes.size(); i++) {
		glBindVertexArray((*model).meshes[i]->VAO);
		objectIDColor.b = (float)i / 1024.0f;

		shad->SetMatrix4("projection", camera->getProjection());
		shad->SetMatrix4("viewMatrix", camera->getViewMatrix());
		shad->SetMatrix4("prevViewMatrix", camera->prevCameraViewMatrix);
		glm::vec3 cameraPos = camera->getWorldPosition();
		shad->SetVector3f("camPos", cameraPos);
		shad->SetFloat("time", (float)glfwGetTime());
		glm::mat4 modelMat = transform;

		shad->SetVector3f("objectIdColor", objectIDColor);
		shad->SetMatrix4("model", modelMat);
		shad->SetMatrix4("prev_model", prevTransform);

		shad->SetVector3f("cubecolor", glm::vec3(1, 1, 1));

		
		(*model).meshes[i]->material.Use(shad);

		glActiveTexture(GL_TEXTURE4);
		shad->SetTextureSampler("noiseTexture", 4);
		t_noise->Bind();
		shad->SetFloat("noiseTreshold", noise_treshold);

		glDrawElements(GL_TRIANGLES, (*model).meshes[i]->indi_count, GL_UNSIGNED_INT, 0);
	}
}

void DefferedRenderer::RenderImage(Framebuffer* fb, Camera* camera, Framebuffer* output,Framebuffer* AO,bool applayAO)
{
	if (camera == nullptr)return;



	output->Bind();
	glClearColor(0, 0, 0, 1);
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_BLEND);//W��czamy blendowanie aplha
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_DEPTH_TEST);// W��czamy rendering 2D
	glDisable(GL_CULL_FACE);

	deffered_shader.Use();
	glBindVertexArray(VAO);//Wysy�amy wierzcho�ki

	//Wk�adamy dane do shadera

	if (camera->cubemap != nullptr)
		if (camera->cubemap->IsCubemapRendered()) {
			glActiveTexture(GL_TEXTURE5);
			glBindTexture(GL_TEXTURE_CUBE_MAP, camera->cubemap->GetCubemapTextureSamplerID());
			deffered_shader.SetTextureSampler("enviromentSampler", 5);
			deffered_shader.SetFloat("enviromentStrength", camera->skybox_strength);
			glActiveTexture(GL_TEXTURE6);
			glBindTexture(GL_TEXTURE_CUBE_MAP, camera->cubemap->GetIradiacneTextureSamplerID());
			deffered_shader.SetTextureSampler("enviromentIradianceSampler", 6);
		}
	
	std::vector<PointLightExtension*> point_lights = Level::GetPointLightsList();
	int max_lights = 32;
	int active_lights_count = 0;
	for (int i = 0; i < point_lights.size(); i++) {
		if (i >= max_lights)break;
		PointLightExtension* p = point_lights[i];
		if (!p->enabled)continue;
		deffered_shader.SetVector3f(("point_lights[" + StringTools::intToString(active_lights_count) + "].Color").c_str(), p->lightColor * p->intensity);
		deffered_shader.SetVector3f(("point_lights[" + StringTools::intToString(active_lights_count) + "].Position").c_str(), p->getPosition());
		active_lights_count++;
	}
	deffered_shader.SetInt("active_point_light_count", active_lights_count);


	std::vector<DirectionalLightExtension*> directional_lights = Level::GetDirectionalLightsList();
	max_lights = 5;
	active_lights_count = 0;
	for (int i = 0; i < directional_lights.size(); i++) {
		if (i >= max_lights)break;
		DirectionalLightExtension* p = directional_lights[i];
		if (!p->enabled)continue;
		deffered_shader.SetVector3f(("directional_lights[" + StringTools::intToString(active_lights_count) + "].Color").c_str(), p->lightColor * p->intensity);
		deffered_shader.SetVector3f(("directional_lights[" + StringTools::intToString(active_lights_count) + "].Direction").c_str(), p->getDirection());
		glm::mat4 lmp = p->getLightProjection();
		//lmp = glm::scale(lmp, glm::vec3(0.5f, 0.5f, 0.5f));
		//lmp = glm::translate(lmp, glm::vec3(0.5f, 0.5f, 0.5f));
		deffered_shader.SetMatrix4(("directional_lights[" + StringTools::intToString(active_lights_count) + "].LightProjection").c_str(),lmp);
		glActiveTexture(GL_TEXTURE10 + i);
		glBindTexture(GL_TEXTURE_2D, p->getDepthTextureID());
		deffered_shader.SetTextureSampler(("sampler_directional_depth[" + StringTools::intToString(active_lights_count) + "]").c_str(), 10 + i);
		active_lights_count++;
	}

	deffered_shader.SetInt("active_directional_light_count", active_lights_count);


	glm::vec3 cameraPos = camera->position;
	deffered_shader.SetVector3f("camPos", cameraPos);

	glActiveTexture(GL_TEXTURE0);
	deffered_shader.SetTextureSampler("sampler_position", 0);
	glBindTexture(GL_TEXTURE_2D, fb->texturePositionID);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, fb->textureColorID);
	deffered_shader.SetTextureSampler("sampler_diffuse", 1);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, fb->textureNormalID);
	deffered_shader.SetTextureSampler("sampler_normal", 2);


	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, fb->textureMaterialProportiesID);
	deffered_shader.SetTextureSampler("sampler_material_protorties", 3);

	

	deffered_shader.SetBool("applayAO", applayAO);
	if (applayAO) {
		glActiveTexture(GL_TEXTURE4);
		glBindTexture(GL_TEXTURE_2D, AO->textureColorID);
		deffered_shader.SetTextureSampler("sampler_AO", 4);
	}

	glActiveTexture(GL_TEXTURE5);
	deffered_shader.SetTextureSampler("enviromentSampler", 5);
	t_enviroment->Bind();

	deffered_shader.SetFloat("time", (float)glfwGetTime()*3);

	
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	output->Unbind();

}

Framebuffer* DefferedRenderer::getBloomBuffer(int id)
{
	int i = id;
	if (i > BLOOM_DOWN_PASS_COUNT + BLOOM_UP_PASS_COUNT - 1) i = BLOOM_DOWN_PASS_COUNT + BLOOM_UP_PASS_COUNT - 1;
	else if (i < 0) i = 0;
	return bloomPasses[i];
}

Framebuffer* DefferedRenderer::getBloomBuffer()
{
	return bloomPasses[BLOOM_DOWN_PASS_COUNT + BLOOM_UP_PASS_COUNT-1];
}

void DefferedRenderer::RenderBloom(Framebuffer* input,float bloom_treshold)
{
	float pass_dumper = 1.0f;
	bloomInitPassShader.Use();
	bloomInitPassShader.SetFloat("bloom_treshold", bloom_treshold);
	RenderBloomUpAndDownSampling(input, bloomPasses[0], &bloomInitPassShader,false, nullptr, pass_dumper);

	for(int i = 1;i < BLOOM_DOWN_PASS_COUNT;i++)
		RenderBloomUpAndDownSampling(bloomPasses[i-1], bloomPasses[i], &bloomDownPassShader,(i&1), nullptr, pass_dumper);

	for(int i = 0;i < BLOOM_UP_PASS_COUNT;i++)
		RenderBloomUpAndDownSampling(bloomPasses[BLOOM_DOWN_PASS_COUNT-1 + i], bloomPasses[BLOOM_DOWN_PASS_COUNT + i], &bloomUpPassShader, true, bloomPasses[BLOOM_DOWN_PASS_COUNT-1 - i] , pass_dumper);
		

}

void DefferedRenderer::ApplayBloom(Framebuffer* input, Framebuffer* output)
{
	RenderBloomUpAndDownSampling(getBloomBuffer(), output, &bloomUpPassShader, false, input, 1.0f);

}

void  DefferedRenderer::RenderBloomUpAndDownSampling(Framebuffer* input, Framebuffer* output, Shader* shad,bool horizontal, Framebuffer* input2,float pass_dumper) {
	output->Bind();
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_BLEND);//W��czamy blendowanie aplha
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_DEPTH_TEST);// W��czamy rendering 2D
	glDisable(GL_CULL_FACE);

	glViewport(0, 0, output->getWidth(), output->getHeight());

	shad->Use();
	glBindVertexArray(VAO);//Wysy�amy wierzcho�ki


	glActiveTexture(GL_TEXTURE0);
	shad->SetTextureSampler("input_image", 0);
	glBindTexture(GL_TEXTURE_2D, input->textureColorID);
	shad->SetVector2f("fb_size", {input->getWidth(), input->getHeight()});
	shad->SetBool("horizontal", horizontal);
	if (input2 != nullptr) {
		glActiveTexture(GL_TEXTURE1);
		shad->SetTextureSampler("input_image2", 1);
		glBindTexture(GL_TEXTURE_2D, input2->textureColorID);
		shad->SetFloat("pass_dumper", pass_dumper);
	}

	

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	output->Unbind();
}





void DefferedRenderer::ApplayTonemapping(Framebuffer* input, Framebuffer* output, bool bindOutput)
{
	if(bindOutput)
		output->Bind();
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_BLEND);//W��czamy blendowanie aplha
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_DEPTH_TEST);// W��czamy rendering 2D
	glDisable(GL_CULL_FACE);

	
	AcesTonemappingShader.Use();
	glBindVertexArray(VAO);//Wysy�amy wierzcho�ki


	glActiveTexture(GL_TEXTURE0);
	AcesTonemappingShader.SetTextureSampler("input_image", 0);
	glBindTexture(GL_TEXTURE_2D, input->textureColorID);
	

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	output->Unbind();
}

void DefferedRenderer::Destroy()
{
	if (EBO != NULL) {
		glDeleteBuffers(1, &EBO);
		EBO = NULL;
	}

	if (VBO != NULL) {
		glDeleteBuffers(1, &VBO);
		VBO = NULL;
	}

	if (VAO != NULL) {
		glDeleteVertexArrays(1, &VAO);
		VAO = NULL;
	}

	deffered_shader.Delete();
	bloomInitPassShader.Delete();
	bloomDownPassShader.Delete();
	bloomUpPassShader.Delete();
	AcesTonemappingShader.Delete();
	motionBlurShader.Delete();
	SSAOshader.Delete();
	SSAOBlurShader.Delete();
	delete t_enviroment;
	delete t_noise;

	for (int i = 0; i < BLOOM_DOWN_PASS_COUNT + BLOOM_UP_PASS_COUNT; i++)
		delete bloomPasses[i];
	delete bloomPasses;

	delete blour_buffor;

	delete ssao_fb;
	delete ssao_blur_fb;
	ssaoSamples.clear();
	glDeleteTextures(1, &noiseTexID);
}