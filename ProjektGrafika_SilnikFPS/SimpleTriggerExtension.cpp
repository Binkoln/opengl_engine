#include "Object.h"

namespace ste {
    std::vector<SimpleTriggerEmiter_s> emiters;
}

void SimpleTriggerExtension::AfterPlay() 
{
    ste::emiters.clear();
}

void SimpleTriggerExtension::DeleteNotification(Object* o)
{
    auto e = ste::emiters;
    for (int i = 0; i < e.size(); i++) {
        if (e[i].emiter == o) {
            e.erase(e.begin() + i);
            i--;
        }
    }
}

SimpleTriggerExtension::SimpleTriggerExtension(): Extension("SimpleTriggerExtension") {
    type = "SimpleTriggerExtension";
}
SimpleTriggerExtension::~SimpleTriggerExtension() {}
tinyxml2::XMLElement* SimpleTriggerExtension::SaveScene(tinyxml2::XMLElement* e) {
    using namespace tinyxml2;
    XMLElement* extension = Extension::SaveScene(e);
    extension->InsertNewChildElement("triggerType")->InsertNewText(StringTools::intToString(triggerType).c_str());
    extension->InsertNewChildElement("radius")->InsertNewText(StringTools::doubleToString(radius).c_str());
    if(!tag.empty())             extension->InsertNewChildElement("tag")->InsertNewText(tag.c_str());
    if(!luaFunctionName.empty()) extension->InsertNewChildElement("luaFunctionName")->InsertNewText(luaFunctionName.c_str());

    return extension;
}


void SimpleTriggerExtension::InitStaticData() {}
void SimpleTriggerExtension::CleanupStaticData() {}

void SimpleTriggerExtension::Init() {}
void SimpleTriggerExtension::Cleanup() {}
int hjh = 0;
void SimpleTriggerExtension::Update(Object* o, Level* level) 
{
    if (triggerType == SIMPLE_TRIGGER_EMITER && !isEmiterRegistered) {
        isEmiterRegistered = true;
        struct SimpleTriggerEmiter_s s;
        s.emiter = o;
        s.tag = std::string(tag.c_str());
        ste::emiters.push_back(s);
    }

    if (triggerType == SIMPLE_TRIGGER_LISTNENER && level->isPlatModeEnabled()) {
        auto emiters = ste::emiters;
        for (SimpleTriggerEmiter_s e : emiters) {
            if (glm::length(e.emiter->getWorldPosition() - o->getWorldPosition()) <= radius && (e.tag == tag || tag.empty())) {
                //std::cout << "Bajlando !!!! " << (hjh++) << "\n";
                //o->DeleteObject();
                if (!luaFunctionName.empty()) {
                   
                    for (Extension* e : o->extensions) {
                        if (e->type == "LuaScript")
                        {
                            LuaScriptExtension* lse = (LuaScriptExtension*)e;
                            lse->TriggerFunction(luaFunctionName, o, tag, level);
                        }
                    }
                }
            }
        }
    }

}


Extension* SimpleTriggerExtension::MakeCopy() {
    auto copy = new SimpleTriggerExtension();
    copy->tag = std::string(tag.c_str());
    copy->luaFunctionName = std::string(luaFunctionName.c_str());
    copy->triggerType = triggerType;
    copy->radius = radius;
    return copy;
}
void SimpleTriggerExtension::OnPlay() { 
    ste::emiters = std::vector<SimpleTriggerEmiter_s>();
    ste::emiters.clear();
    isEmiterRegistered = false;
}


void SimpleTriggerExtension::Render(Object* o, Level* level) {}

void SimpleTriggerExtension::DrawImGui(Object* parent)
{
    if (ImGui::CollapsingHeader(name.c_str(), ImGuiTreeNodeFlags_DefaultOpen)) {
        Extension::DrawImGui(parent);
        ImGui::Text("Simple Trigger Extension ");
        ImGui::Combo("Adjust ", &triggerType, "Lisnener\0Emiter\0");
        
        char buf[256];
        memset(buf, 0, 256);
        tag.copy(buf, 256);
        ImGui::InputText("Tag name", buf, IM_ARRAYSIZE(buf));
        tag = std::string(buf);

        if (triggerType == SIMPLE_TRIGGER_LISTNENER) {
            memset(buf, 0, 256);
            luaFunctionName.copy(buf, 256);
            ImGui::InputText("Lua handler name", buf, IM_ARRAYSIZE(buf));
            luaFunctionName = std::string(buf);
        }
    }
    ImGui::Separator();
}