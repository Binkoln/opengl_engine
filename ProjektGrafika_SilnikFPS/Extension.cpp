#include "Object.h"

Extension::Extension(std::string name) {
	type = "Extension";
	this->name = name;
	enabled = true;
	toDelete = false;
}
Extension::~Extension() {

}

tinyxml2::XMLElement* Extension::SaveScene(tinyxml2::XMLElement* e)
{
	using namespace tinyxml2;
	XMLElement* extension = e->InsertNewChildElement("Extension");

	extension->InsertNewChildElement("Name")->InsertNewText(name.c_str());
	extension->InsertNewChildElement("Type")->InsertNewText(type.c_str());
	extension->InsertNewChildElement("Enabled")->InsertNewText(enabled ? "1.0" : "0.0");


	return extension;
}
Extension* Extension::LoadScene(tinyxml2::XMLNode* obj)
{
	using namespace tinyxml2;
	if (obj == nullptr)return nullptr;
	//data fetching
	XMLElement* name = obj->FirstChildElement("Name");

	std::string objType = obj->FirstChildElement("Type")->GetText();
	std::string enabled = obj->FirstChildElement("Enabled")->GetText();

	if (objType == "ModelRendererExtension") {

		ModelRendererExtension* ex = new ModelRendererExtension();
		std::string isModSet = obj->FirstChildElement("IsModelSet")->GetText();
		std::string modName = obj->FirstChildElement("ModelName")->GetText();
		std::string modPath = "";
		
		if(obj->FirstChildElement("ModelPath") != nullptr && isModSet == "1.0")
			modPath = obj->FirstChildElement("ModelPath")->GetText();

		XMLElement* xml_transparent_element = obj->FirstChildElement("Transparent");
		if (xml_transparent_element != nullptr) {
			std::string transparent= xml_transparent_element->GetText();
			ex->setTransparent(transparent == "1.0");
		}

		if(isModSet == "1.0")
			ex->LoadModel(modName, modPath);
		ex->enabled = (enabled == "1.0");
		ex->name = name->GetText();
		return ex;

	}

	if (objType == "SimpleTriggerExtension") {
		SimpleTriggerExtension* ex = new SimpleTriggerExtension();
		
		ex->triggerType = StringTools::stringToInt(obj->FirstChildElement("triggerType")->GetText());
		ex->radius = StringTools::stringToDouble(obj->FirstChildElement("radius")->GetText());

		auto xml_tag = obj->FirstChildElement("tag");
		auto xml_luaFunctionName = obj->FirstChildElement("luaFunctionName");

		if (xml_tag != NULL) ex->tag = xml_tag->GetText();
		if (xml_luaFunctionName != NULL) ex->luaFunctionName = xml_luaFunctionName->GetText();

		ex->enabled = (enabled == "1.0");
		ex->name = name->GetText();
		return ex;

	}

	if (objType == "PhysX") {

		PhysXExtension* ex = new PhysXExtension();
		
		bool isTrigger = (std::string(obj->FirstChildElement("IsTrigger")->GetText()) == "1.0");
		bool isDynamic = (std::string(obj->FirstChildElement("IsDynamic")->GetText()) == "1.0");
		float mass =				 (float)StringTools::stringToDouble(obj->FirstChildElement("Mass")->GetText());
		int collisionShapeType =	   (int)StringTools::stringToDouble(obj->FirstChildElement("CollisionShapeType")->GetText());
		glm::vec3 collider_size =			StringTools::stringToVec3(  obj->FirstChildElement("ColliderSize")->GetText());
		glm::vec3 material_properties =		StringTools::stringToVec3(  obj->FirstChildElement("Material")->GetText());
		glm::vec3 shape_origin =			StringTools::stringToVec3(  obj->FirstChildElement("Origin")->GetText());
		auto collisionModelName =			(obj->FirstChildElement("collisionModelName"));
		auto collisionModelPath =			(obj->FirstChildElement("collisionModelPath"));

		ex->isTrigger = isTrigger;
		ex->isDynamic = isDynamic;
		ex->mass = mass;
		ex->collisionShapeType = collisionShapeType;
		ex->collider_size = glm::vec3(collider_size);
		ex->material_properties = glm::vec3(material_properties);
		ex->shape_origin = glm::vec3(shape_origin);

		if (collisionModelName != NULL && collisionModelName->GetText() != NULL) ex->collisionModelName = std::string(collisionModelName->GetText());
		if (collisionModelPath != NULL && collisionModelPath->GetText() != NULL) ex->collisionModelPath = std::string(collisionModelPath->GetText());
		if (collisionModelName != NULL && collisionModelPath != NULL) {
			ex->LoadCollisionModel(ex->collisionModelName, ex->collisionModelPath);
		}

		ex->enabled = (enabled == "1.0");
		ex->name = name->GetText();
		return ex;

	}

	if (objType == "PhysXCharacterControllerExtension") {

		PhysXCharacterControllerExtension* ex = new PhysXCharacterControllerExtension();

		
		
		float mass = (float)StringTools::stringToDouble(obj->FirstChildElement("Mass")->GetText());
		glm::vec3 collider_size = StringTools::stringToVec3(obj->FirstChildElement("ColliderSize")->GetText());
		glm::vec3 material_properties = StringTools::stringToVec3(obj->FirstChildElement("Material")->GetText());
		glm::vec3 shape_origin = StringTools::stringToVec3(obj->FirstChildElement("Origin")->GetText());

		ex->mass = mass;
		ex->collider_size = glm::vec3(collider_size);
		ex->material_properties = glm::vec3(material_properties);
		ex->shape_origin = glm::vec3(shape_origin);


		ex->enabled = (enabled == "1.0");
		ex->name = name->GetText();
		return ex;

	}

	if (objType == "SpriteRenderer") {

		SpriteRendererExtension* ex = new SpriteRendererExtension();
		std::string spriteRenderer_name = obj->FirstChildElement("SpriteRendererName")->GetText();
		std::string isTextureSet = obj->FirstChildElement("IsTexture")->GetText();
		std::string texturePath = "";
		if (isTextureSet == "1.0") {
			texturePath = obj->FirstChildElement("TexturePath")->GetText();
			ex->setTexture(texturePath);
		}
		std::string posSiz = obj->FirstChildElement("SpritePosSiz")->GetText();
		std::string spriteColor = obj->FirstChildElement("SpriteColor")->GetText();

			

		ex->setPosSizeVector(StringTools::stringToVec4(posSiz));
		ex->setColorMultiplier(StringTools::stringToVec3(spriteColor));


		ex->enabled = (enabled == "1.0");
		ex->name = name->GetText();
		return ex;

	}

	if (objType == "PointLightExtension") {

		PointLightExtension* ex = new PointLightExtension();
		std::string spriteColor = obj->FirstChildElement("Color")->GetText();
		std::string CastShadows = obj->FirstChildElement("CastShadows")->GetText();
		std::string Intensity = obj->FirstChildElement("Intensity")->GetText();
		std::string Radius = obj->FirstChildElement("Radius")->GetText();

		ex->castShadows = (CastShadows == "1.0");
		ex->setColor(StringTools::stringToVec3(spriteColor));
		ex->intensity = StringTools::stringToDouble(Intensity);
		ex->radius = StringTools::stringToDouble(Radius);

		ex->enabled = (enabled == "1.0");
		ex->name = name->GetText();
		return ex;

	}
	if (objType == "DirectionalLightExtension") {

		DirectionalLightExtension* ex = new DirectionalLightExtension();
		std::string spriteColor = obj->FirstChildElement("Color")->GetText();
		std::string CastShadows = obj->FirstChildElement("CastShadows")->GetText();
		std::string Intensity = obj->FirstChildElement("Intensity")->GetText();
		if (obj->FirstChildElement("DepthMapSize") != NULL) {
			std::string e = obj->FirstChildElement("DepthMapSize")->GetText();
			ex->dm_size = StringTools::stringToDouble(e);
		}
		if (obj->FirstChildElement("DepthMapDepth") != NULL) {
			std::string e = obj->FirstChildElement("DepthMapDepth")->GetText();
			ex->dm_depth = StringTools::stringToDouble(e);
		}
		if (obj->FirstChildElement("DepthMapResolution") != NULL) {
			std::string e = obj->FirstChildElement("DepthMapResolution")->GetText();
			ex->dm_resolution = StringTools::stringToInt(e);
		}

		ex->castShadows = (CastShadows == "1.0");
		ex->setColor(StringTools::stringToVec3(spriteColor));
		ex->intensity = StringTools::stringToDouble(Intensity);

		ex->enabled = (enabled == "1.0");
		ex->name = name->GetText();
		return ex;

	}
	if (objType == "TerrainExtension") {

		TerrainExtension* ex = new TerrainExtension();
		std::string heightmap_path = (obj->FirstChildElement("heightmap_path") != nullptr)?obj->FirstChildElement("heightmap_path")->GetText():"";
		


		ex->GenerateTerrain(heightmap_path);
		

		ex->enabled = (enabled == "1.0");
		ex->name = name->GetText();
		return ex;

	}
	if (objType == "LuaScript") {
		LuaScriptExtension* ex = new LuaScriptExtension();
		std::string isModSet = obj->FirstChildElement("IsSctiptSet")->GetText();
		std::string modName = obj->FirstChildElement("ScriptName")->GetText();
		std::string modPath = (modName == "No script")?"":obj->FirstChildElement("ScriptPath")->GetText();


		//Load script variables
		XMLElement* params = obj->FirstChildElement("Parameters");
		if (!params->NoChildren()) {
			XMLNode* chldObj;

			chldObj = params->FirstChildElement("Parameter");
			while (true) {
				struct LuaScriptVariable* v = new struct LuaScriptVariable;
				v->in_script_name = std::string(chldObj->FirstChildElement("Name")->GetText());
				v->type = StringTools::stringToInt(chldObj->FirstChildElement("Type")->GetText());

				if (v->type == LUASCRIPT_VAR_TYPE_NUMBER) {
					v->val = StringTools::stringToDouble(chldObj->FirstChildElement("Value")->GetText());
				}
				if (v->type == LUASCRIPT_VAR_TYPE_OBJECT) {
					int objID = StringTools::stringToInt(chldObj->FirstChildElement("Value")->GetText());
					if (objID == -1) 
						v->ptr = nullptr;
					else 
						Level::RegisterObjectSwapService({v,objID});
				}

				ex->AddLuaVariable(v);


				if (chldObj == params->LastChild()) {
					break;
				}
				else
					chldObj = chldObj->NextSiblingElement("Parameter");
			}
		}

		if(modPath != "")
			ex->LoadScript(modPath, modName);
		ex->enabled = (enabled == "1.0");
		ex->name = name->GetText();
		return ex;

	}

	return nullptr;
}


void Extension::Init() {}
void Extension::Cleanup() {}
void Extension::Update(Object* o, Level* level) {}
void Extension::Render(Object* o, Level* level) {}
void Extension::OnPlay() {};

void Extension::DrawImGui(Object* parent) {
	ImGui::Checkbox("Extension Enabled", &enabled);
	//ImGui::SameLine();


	

	if (ImGui::Button("  Delete extension  "))
	{
		ImGui::OpenPopup("DeleteExtensionRylyQ");
	}

	if (ImGui::BeginPopup("DeleteExtensionRylyQ")) {

		ImGui::Text("U sure ?");
		ImGui::Separator();
		if (ImGui::Button("  No   ")) {
			ImGui::CloseCurrentPopup();
		}
		if (ImGui::Button("  Yes  ")) {
			toDelete = true;
			ImGui::CloseCurrentPopup();
		}

		ImGui::EndPopup();
	}
}