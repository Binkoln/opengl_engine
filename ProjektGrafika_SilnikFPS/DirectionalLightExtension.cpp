#include "Object.h"

namespace DirectionalLightExtension_Blackboard {
	Shader shad;
}

void DirectionalLightExtension::InitShaders()
{
	DirectionalLightExtension_Blackboard::shad = Shader("EngineProjectFolder/assets/shaders/directional_light_depth_v_b.glsl", "EngineProjectFolder/assets/shaders/directional_light_depth_f_b.glsl");

}

void DirectionalLightExtension::CleanupShaders()
{
	DirectionalLightExtension_Blackboard::shad.Delete();
}

DirectionalLightExtension::DirectionalLightExtension() :Extension("DirectionalLightExtension")
{
	type = "DirectionalLightExtension";
	lightColor = glm::vec3(1, 1, 1);
}

DirectionalLightExtension::~DirectionalLightExtension()
{
	Level::RemoveDirectionalLight(this);
	if (depthBuffer != nullptr) {
		delete depthBuffer;
		depthBuffer = nullptr;
	}
}

tinyxml2::XMLElement* DirectionalLightExtension::SaveScene(tinyxml2::XMLElement* e)
{
	using namespace tinyxml2;
	XMLElement* extension = Extension::SaveScene(e);
	extension->InsertNewChildElement("Color")->InsertNewText(StringTools::vec3ToString(lightColor).c_str());
	extension->InsertNewChildElement("CastShadows")->InsertNewText((castShadows) ? "1.0" : "0.0");
	extension->InsertNewChildElement("Intensity")->InsertNewText(StringTools::doubleToString(intensity).c_str());
	extension->InsertNewChildElement("DepthMapSize")->InsertNewText(StringTools::doubleToString(dm_size).c_str());
	extension->InsertNewChildElement("DepthMapDepth")->InsertNewText(StringTools::doubleToString(dm_depth).c_str());
	extension->InsertNewChildElement("DepthMapResolution")->InsertNewText(StringTools::intToString(dm_resolution).c_str());


	return extension;
}


void DirectionalLightExtension::Init()
{
	Level::AddDirectionalLight(this);
}

void DirectionalLightExtension::Cleanup()
{
	Level::RemoveDirectionalLight(this);
	if (depthBuffer != nullptr) {
		delete depthBuffer;
		depthBuffer = nullptr;
	}
}

void DirectionalLightExtension::Update(Object* o, Level* level)
{
	glm::mat4 obrot(1);
	obrot = glm::rotate(obrot, o->rotation.y, { 0,1,0 });
	obrot = glm::rotate(obrot, o->rotation.x, { 1,0,0 });
	obrot = glm::rotate(obrot, o->rotation.z, { 0,0,1 });

	dir = obrot * glm::vec4( 0,1,0,0 );

	glm::vec3 w_pos = o->getWorldPosition();

	glm::mat4 lookAtMat = glm::lookAt({0,0,0}, -glm::vec3(dir.x, dir.y, dir.z), glm::vec3(0, 1, 0));
	glm::mat4 proj = glm::ortho(-dm_size, dm_size, -dm_size, dm_size, -dm_depth, dm_depth);
	glm::mat4 modelMat(1);
	modelMat = glm::translate(modelMat, {-w_pos.x,0,-w_pos.z });

	lightProjection = proj  *lookAtMat* modelMat;

	Level::AddDirectionalLight(this);

	if(depthBuffer == nullptr)
		InitFramebuffer();
}

void DirectionalLightExtension::InitFramebuffer() {
	if (depthBuffer != nullptr)return;
	depthBuffer = new Framebuffer();
	depthBuffer->Init(dm_resolution, dm_resolution, FRAMEBUFFER_TYPE_DEPTH_ONLY);

}

Extension* DirectionalLightExtension::MakeCopy()
{
	DirectionalLightExtension* ret = new DirectionalLightExtension();
	ret->lightColor = lightColor;
	ret->castShadows = castShadows;
	ret->intensity = intensity;
	return ret;
}




void DirectionalLightExtension::Render(Object* o, Level* level)
{
	
}


bool DirectionalLightExtension::UseDepthBuffer() {
	if (depthBuffer == nullptr)return false;
	depthBuffer->Bind();
	return true;
}

void DirectionalLightExtension::DontUseDepthBuffer() {
	if (depthBuffer == nullptr)return;
	depthBuffer->Unbind();
}

void DirectionalLightExtension::DrawImGui(Object* parent)
{

	if (ImGui::CollapsingHeader(name.c_str(), ImGuiTreeNodeFlags_DefaultOpen)) {


		Extension::DrawImGui(parent);


		float colorPicker[] = { lightColor.r,lightColor.g,lightColor.b };
		ImGui::ColorPicker3("Color", colorPicker, 0);
		lightColor.r = colorPicker[0];
		lightColor.g = colorPicker[1];
		lightColor.b = colorPicker[2];


		ImGui::DragFloat("Intensity", &intensity, 0.01f);
		ImGui::DragFloat("Depth Map Size", &dm_size, 0.01f);
		ImGui::DragFloat("Depth Map Depth", &dm_depth, 0.01f);
		ImGui::InputInt("Depth Map Resolution", &dm_resolution);

		if (ImGui::Button("Reinit framebuffer")) {
			if (depthBuffer != nullptr) {
				delete depthBuffer;
			}
			InitFramebuffer();
		}

		if (depthBuffer != nullptr) {
			if(ImGui::Button("Show depth framebuffer"))
				showImGuiFramebuffer = true;
		}
	}

	if (showImGuiFramebuffer) {
		ImGui::Begin("Depth shower", &showImGuiFramebuffer);
		ImGui::Image((ImTextureID)getDepthTextureID(), { 512,512 }, { 0,1 }, { 1,0 });
		
		ImGui::End();
	}

	ImGui::Separator();
}