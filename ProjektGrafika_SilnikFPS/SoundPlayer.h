#ifndef SOUND_PLAYER_H
#define SOUND_PLAYER_H
#include <string>
#include <AL/al.h>
#include <AL/alc.h>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>

class SoundPlayer {
public:

	static void Init();
	static void Clean();
	static void PlaySound();

private:
	
};

#endif // !SOUND_PLAYER_H
