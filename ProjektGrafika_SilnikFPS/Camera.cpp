#include "Object.h"

Camera::Camera(long id):Object("Camera",id)
{
	type = "Camera";
	cubemap = nullptr;
}

Camera::~Camera()
{
	if (cubemap != nullptr) {
		delete cubemap;
		cubemap = nullptr;
	}
}

tinyxml2::XMLElement* Camera::SaveScene(tinyxml2::XMLElement* e)
{
	using namespace tinyxml2;

	XMLElement* obj = Object::SaveScene(e);
	obj->InsertNewChildElement("ProjectionType")->InsertNewText((projectionType == CAMERA_PROJECTION_PERSPECTIVE)?"Perspective": "Ortho");
	obj->InsertNewChildElement("Fov")->InsertNewText(StringTools::doubleToString(fov).c_str());
	obj->InsertNewChildElement("NearPlane")->InsertNewText(StringTools::doubleToString(near_plane).c_str());
	obj->InsertNewChildElement("FarPlane")->InsertNewText(StringTools::doubleToString(far_plane).c_str());
	obj->InsertNewChildElement("ProjectionSize")->InsertNewText(StringTools::doubleToString(projection_size).c_str());
	obj->InsertNewChildElement("Sensitivity")->InsertNewText(StringTools::doubleToString(sensitivity).c_str());
	obj->InsertNewChildElement("Speed")->InsertNewText(StringTools::doubleToString(speed).c_str());
	obj->InsertNewChildElement("Fastspeed")->InsertNewText(StringTools::doubleToString(fastspeed).c_str());
	obj->InsertNewChildElement("AdjustType")->InsertNewText((ajdust_type == CAMERA_PROJECTION_ORTHO_ADJUST_TO_HEIGHT)? "CAMERA_PROJECTION_ORTHO_ADJUST_TO_HEIGHT" : "CAMERA_PROJECTION_ORTHO_ADJUST_TO_WIDTH");

	obj->InsertNewChildElement("EulerCamRot")->InsertNewText(StringTools::vec3ToString(glm::vec3(pich, yaw, 0)).c_str());

	//Bloom
	obj->InsertNewChildElement("BloomTreshold")->InsertNewText(StringTools::doubleToString(bloom_treshold).c_str());

	//SSAO
	obj->InsertNewChildElement("SSAOSamplesCount")->InsertNewText(StringTools::intToString(kernelSize).c_str());
	obj->InsertNewChildElement("SSAOBais")->InsertNewText(StringTools::doubleToString(bias).c_str());
	obj->InsertNewChildElement("SSAORadius")->InsertNewText(StringTools::doubleToString(radius).c_str());


	//Skybox
	obj->InsertNewChildElement("SkyboxStrength")->InsertNewText(StringTools::doubleToString(skybox_strength).c_str());
	obj->InsertNewChildElement("SkyboxRotation")->InsertNewText(StringTools::doubleToString(skybox_rotation).c_str());
	//Path to default skybox
	//"EngineProjectFolder/assets/skyboxes/belfast_sunset_2k.hdr"
	if(skybox_texture_path != "")
		obj->InsertNewChildElement("SkyboxImagePath")->InsertNewText(skybox_texture_path.c_str());
	
	return obj;
}

void Camera::BakeCubemap() {
	if (cubemap != nullptr)
		cubemap->BakeCubemap(skybox_strength, skybox_rotation);
}

void Camera::LoadAdditionalData(tinyxml2::XMLNode* e)
{
	using namespace tinyxml2;
	glm::vec3 eulerRot = StringTools::stringToVec3(e->FirstChildElement("EulerCamRot")->GetText());

	pich = eulerRot.x;
	yaw = eulerRot.y;

	setAsActiveCamera = true;

	std::string projType = e->FirstChildElement("ProjectionType")->GetText();
	if (projType == "Perspective")
		projectionType = CAMERA_PROJECTION_PERSPECTIVE;
	else
		projectionType = CAMERA_PROJECTION_ORTHO;

	fov = StringTools::stringToDouble(e->FirstChildElement("Fov")->GetText());
	projection_size = StringTools::stringToDouble(e->FirstChildElement("ProjectionSize")->GetText());
	far_plane = StringTools::stringToDouble(e->FirstChildElement("FarPlane")->GetText());
	near_plane = StringTools::stringToDouble(e->FirstChildElement("NearPlane")->GetText());
	sensitivity = StringTools::stringToDouble(e->FirstChildElement("Sensitivity")->GetText());
	speed = StringTools::stringToDouble(e->FirstChildElement("Speed")->GetText());
	fastspeed = StringTools::stringToDouble(e->FirstChildElement("Fastspeed")->GetText());
	std::string adjType = e->FirstChildElement("AdjustType")->GetText();
	if (adjType == "CAMERA_PROJECTION_ORTHO_ADJUST_TO_HEIGHT")
		ajdust_type = CAMERA_PROJECTION_ORTHO_ADJUST_TO_HEIGHT;
	else
		ajdust_type = CAMERA_PROJECTION_ORTHO_ADJUST_TO_WIDTH;

	//Bloom
	if (e->FirstChildElement("BloomTreshold") != nullptr)
		bloom_treshold = StringTools::stringToDouble(e->FirstChildElement("BloomTreshold")->GetText());

	//SSAO settings
	if(e->FirstChildElement("SSAOSamplesCount") != nullptr)
		kernelSize = StringTools::stringToInt(e->FirstChildElement("SSAOSamplesCount")->GetText());
	if (e->FirstChildElement("SSAOBais") != nullptr)
		bias = StringTools::stringToDouble(e->FirstChildElement("SSAOBais")->GetText());
	if (e->FirstChildElement("SSAORadius") != nullptr)
		radius = StringTools::stringToDouble(e->FirstChildElement("SSAORadius")->GetText());


	//Skybox
	if (e->FirstChildElement("SkyboxStrength") != nullptr)
		skybox_strength = StringTools::stringToDouble(e->FirstChildElement("SkyboxStrength")->GetText());
	if (e->FirstChildElement("SkyboxRotation") != nullptr)
		skybox_rotation = StringTools::stringToDouble(e->FirstChildElement("SkyboxRotation")->GetText());
	if (e->FirstChildElement("SkyboxImagePath") != nullptr) {
		skybox_texture_path = std::string(e->FirstChildElement("SkyboxImagePath")->GetText());
		if (cubemap != nullptr)delete cubemap;
		cubemap = new CubemapTexture(skybox_texture_path);
	}



}
void Camera::drawImGuiInspector()
{
	Object::drawImGuiInspector();
	if (ImGui::CollapsingHeader("Camera Settings",ImGuiTreeNodeFlags_DefaultOpen)) {
		ImGui::Checkbox("Editor camera", &editorCamera);

		ImGui::Combo("Perspective", &projectionType, "Projection\0Ortho");
		if (projectionType == CAMERA_PROJECTION_PERSPECTIVE) {
			ImGui::DragFloat("FOV:", &fov, 0.1f);
			ImGui::DragFloat("Near:", &near_plane, 0.1f);
			ImGui::DragFloat("Far:", &far_plane, 0.1f);
		}
		else if (projectionType == CAMERA_PROJECTION_ORTHO) {
			ImGui::Combo("Adjust ", &ajdust_type, "to window height\0to window width");
			ImGui::DragFloat("Constant view size:", &projection_size, 0.1f);
			ImGui::DragFloat("Near:", &near_plane, 0.1f);
			ImGui::DragFloat("Far:", &far_plane, 0.1f);
		}
		ImGui::Separator();


		if (ImGui::Button("Set As Active Camera"))
			setAsActiveCamera = true;

		ImGui::InputFloat("Sensitivity", &sensitivity);
		ImGui::InputFloat("Fly speed", &speed);
		ImGui::InputFloat("Fast fly speed", &fastspeed);
		ImGui::DragFloat("pich", &pich,0.01f);
		ImGui::DragFloat("yaw", &yaw, 0.01f);

		if (ImGui::Button(((objectToLookAt == nullptr) ? "Set ObjectToLookAt" : objectToLookAt->name.c_str())))
		{
			if (InputManager::getKey(GLFW_KEY_LEFT_SHIFT))
				objectToLookAt = nullptr;
		}
		if (ImGui::BeginDragDropTarget()) {
			const ImGuiPayload* pyload = ImGui::AcceptDragDropPayload("OBJECT", ImGuiDragDropFlags_None);
			if (pyload != NULL) {

				Object* recived = *((Object**)(pyload->Data));
				objectToLookAt = recived;
			}

			ImGui::EndDragDropTarget();
		}

		ImGui::DragFloat("Bloom treshold:", &bloom_treshold, 0.01f);

		//SSAO
		ImGui::Text("SSAO Params:");
		ImGui::DragFloat("Bais", &bias, 0.001f);
		ImGui::DragFloat("Radius", &radius, 0.001f);
		ImGui::DragInt("Samples count", &kernelSize, 0.25f, 1, 64);
		//Shybox
		ImGui::Text("Skybox Params:");
		ImGui::DragFloat("Skybox strength", &skybox_strength, 0.001f);
		ImGui::DragFloat("Skybox rotation", &skybox_rotation, 0.001f);
		{//Selection of skybox
			ImGui::TextWrapped(("Skybox texture: " + skybox_texture_path).c_str(), cubemap == nullptr, 0, ImVec2(300, 30));
			if (ImGui::BeginDragDropTarget()) {
				const ImGuiPayload* pyload = ImGui::AcceptDragDropPayload("ASSET", ImGuiDragDropFlags_None);
				if (pyload != NULL) {

					struct AssetData* recived = *((struct AssetData**)(pyload->Data));
					if (recived->type == ASSET_TYPE_TEXTURE) {
						if (cubemap != nullptr)delete cubemap;
						cubemap = new CubemapTexture(recived->path);
						skybox_texture_path = recived->path;
					}
				}

				ImGui::EndDragDropTarget();
			}
		}//End of selection 
	}
}

void Camera::Init()
{

}

glm::mat4  Camera::getWorldSpaceTransformationMatrix() {
	glm::mat4 transformMatrix = glm::mat4(1.0f);
	
	transformMatrix = glm::translate(transformMatrix, position);
	glm::vec3 front;
	front.z = cos(yaw) * cos(pich);
	front.y = cos(pich);
	front.x = sin(yaw) * cos(pich);
	front = glm::normalize(front);

	//qrot = glm::quatLookAt(front, {0,1,0});



	//transformMatrix = transformMatrix * glm::mat4_cast(qrot);

	qrot = glm::quat(rotation);
	transformMatrix = transformMatrix * glm::mat4_cast(qrot);
	//transformMatrix = glm::rotate(transformMatrix, rotation.x, glm::vec3(1, 0, 0));
	//transformMatrix = glm::rotate(transformMatrix, rotation.y, glm::vec3(0, 1, 0));
	//transformMatrix = glm::rotate(transformMatrix, rotation.z, glm::vec3(0, 0, 1));
	
	transformMatrix = glm::scale(transformMatrix, scale);
	
	
	//todo: remove recurency
	glm::mat4 parentTransform = glm::mat4(1.0f);

	if (!isScene())
		parentTransform = parent->getWorldSpaceTransformationMatrix();

	transformMatrix = parentTransform * transformMatrix;

	return transformMatrix;
}

glm::mat4 Camera::getViewMatrix()
{
	if (objectToLookAt != nullptr)
		direction = glm::normalize(objectToLookAt->getWorldPosition() - getWorldPosition());
	else {
		direction.z = cos(yaw) * cos(pich);
		direction.y = sin(pich);
		direction.x = sin(yaw) * cos(pich);
		direction = glm::normalize(direction);
	}


		glm::mat4 transformMatrix = glm::mat4(1.0f);
		glm::quat rot;
		
		

		qrot = glm::quatLookAt(direction, { 0,1,0 });
		rotation = glm::eulerAngles(glm::normalize(qrot));
		transformMatrix = glm::mat4_cast(glm::conjugate(qrot));

		//transformMatrix = glm::rotate(transformMatrix, -rotation.x, glm::vec3(1, 0, 0));
		//transformMatrix = glm::rotate(transformMatrix, -rotation.z, glm::vec3(0, 0, 1));
		//transformMatrix = glm::rotate(transformMatrix, -rotation.y, glm::vec3(0, 1, 0));
		//transformMatrix = glm::lookAt({ 0,0,0 }, direction, { 0,1,0 }) * transformMatrix;
		transformMatrix = glm::translate(transformMatrix, -position);
		//transformMatrix = glm::scale(transformMatrix, scale);
		glm::vec4 forward(1, 0, 0, 0);
		glm::mat4 parentTransform = glm::mat4(1.0f);

		//rotation = glm::axis(rot);
		
		
		if (!isScene())
			parentTransform = parent->getViewMatrix();

		//forward = parentTransform*transformMatrix * forward;

		return transformMatrix* parentTransform;
		
	

}

glm::mat4 Camera::getViewRotationOnlyMatrix()
{
	if (objectToLookAt != nullptr)
		direction = glm::normalize(objectToLookAt->getWorldPosition() - getWorldPosition());
	else {
		direction.z = cos(yaw) * cos(pich);
		direction.y = sin(pich);
		direction.x = sin(yaw) * cos(pich);
		direction = glm::normalize(direction);
	}

	glm::mat4 transformMatrix = glm::mat4(1.0f);
	glm::quat rot;


	qrot = glm::quatLookAt(direction, { 0,1,0 });
	rotation = glm::eulerAngles(glm::normalize(qrot));
	transformMatrix = glm::mat4_cast(glm::conjugate(qrot));


	return transformMatrix ;


}


void Camera::Update(Level* level)
{
	float width = InputManager::getWindowWidth();
	float height = InputManager::getWindowHeight();
	if (projectionType == CAMERA_PROJECTION_PERSPECTIVE) {
		
		projection = glm::perspective(glm::radians(fov), (float)width / (float)height, near_plane, far_plane);

	}
	else if (CAMERA_PROJECTION_ORTHO) {

		float p_width = (ajdust_type == CAMERA_PROJECTION_ORTHO_ADJUST_TO_WIDTH) ? projection_size : projection_size * (width / height);
		float p_height = (ajdust_type == CAMERA_PROJECTION_ORTHO_ADJUST_TO_HEIGHT) ? projection_size : projection_size * (height / width);

		projection = glm::ortho(-p_width, p_width, -p_height, p_height, near_plane, far_plane);
	}
	glViewport(0, 0, width, height);


	if (setAsActiveCamera) {
		level->setActiveCamera(this);
		setAsActiveCamera = false;
	}

	if (editorCamera) {
		if ((InputManager::pressMouseButton(GLFW_MOUSE_BUTTON_RIGHT) && !ImGui::IsWindowFocused(ImGuiFocusedFlags_AnyWindow)) || level->isPlatModeEnabled()) {
			glm::vec2 ddd = InputManager::setMouseFPSMouseMode(true);
			firstEditCameraFrame++;
			if (firstEditCameraFrame < 3 ) {
				ddd = glm::vec2(0, 0);
			}
			

			//std::cout << "x:"<<ddd.x << " y:" << ddd.y << "\n";

				//rotation of camera
				pich += glm::radians(ddd.x * sensitivity);

				if (pich > 1.562) pich = 1.562;
				else if (pich < -1.562) pich = -1.562;
				yaw += -glm::radians(ddd.y * sensitivity);

			

				if (!level->isPlatModeEnabled()) {
					float c_cpeed = (InputManager::getKey(GLFW_KEY_LEFT_SHIFT)) ? fastspeed : speed;


					glm::vec3  right_dir = glm::normalize(glm::cross(direction, { 0,1,0 }));
					if (InputManager::getKey(GLFW_KEY_W)) {
						position += direction * c_cpeed;
					}
					if (InputManager::getKey(GLFW_KEY_S)) {
						position -= direction * c_cpeed;
					}


					double length = glm::length(position);
					if (InputManager::getKey(GLFW_KEY_D)) {
						position += right_dir * c_cpeed;
						if (objectToLookAt != nullptr) {
							position = glm::normalize(position);
							position *= length;
						}
					}
					if (InputManager::getKey(GLFW_KEY_A)) {
						position -= right_dir * c_cpeed;
						if (objectToLookAt != nullptr) {
							position = glm::normalize(position);
							position *= length;
						}
					}

					glm::vec3 up_dir = glm::vec3(0, 1, 0);// glm::cross(direction, right_dir);

					if (InputManager::getKey(GLFW_KEY_E)) {
						position += up_dir * c_cpeed;
					}
					if (InputManager::getKey(GLFW_KEY_Q)) {
						position -= up_dir * c_cpeed;
					}
				}//end camera editor movment
		}
		else {
			InputManager::setMouseFPSMouseMode(false);
			firstEditCameraFrame = 0;

		}
	}


	Object::Update(level);
	
}

void Camera::Render(Level* level)
{
	if (level->cameraSkyboxPass)//Needs to be last thing because of transparency
		if(cubemap != nullptr)
		cubemap->RenderCubemap(getProjection(), getViewRotationOnlyMatrix(),skybox_strength,skybox_rotation);
	else
		Object::Render(level);
	
}

void Camera::Cleanup()
{

}

void Camera::objectActionCallback(Object* o, unsigned int action)
{
	if(action == OBJECT_ACTION_DELETE)
		if (o == objectToLookAt)
			objectToLookAt == nullptr;

	Object::objectActionNotify(o, action);

}