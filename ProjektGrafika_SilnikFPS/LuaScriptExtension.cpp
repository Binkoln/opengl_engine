#include "Object.h"

void LuaScriptExtension::LoadScript(std::string path, std::string name)
{

    this->scriptName = name;
    this->scriptPath = path;
    L = luaL_newstate();
    luaL_openlibs(L);

    lua_register(L, "_LogToConsole", wrap_log);
    lua_register(L, "_GetTransform", wrap_getTransform);
    lua_register(L, "_SetTransform", wrap_setTransform);
    lua_register(L, "_GetKeyPressed", wrap_getKeyPressed);
    lua_register(L, "_CreateNewInstanceOfObject", wrap_CreateNewInstanceOfObject);
    lua_register(L, "_DeleteObject", wrap_DeleteObject); 
    lua_register(L, "_GetHostObjectsChilds", wrap_getHostObjectsChilds);
    lua_register(L, "_GetExtensionByName", wrap_GetExtensionByName);
    lua_register(L, "_SetExtensionSpriteRenderer_SetPosition", wrap_ExtensionSpriteRenderer_SetPosition);
    lua_register(L, "_SetExtensionSpriteRenderer_SetSize", wrap_ExtensionSpriteRenderer_SetSize);
    lua_register(L, "_ExtensionPhysXCharacterController_Move", wrap_ExtensionPhysXCharacterController_Move);
    lua_register(L, "_ExtensionPhysXCharacterController_SetVelocity", wrap_ExtensionPhysXCharacterController_SetVelocity);
    lua_register(L, "_ExtensionPhysXCharacterController_GetVelocity", wrap_ExtensionPhysXCharacterController_GetVelocity);
    lua_register(L, "_ExtensionPhysXCharacterController_GetOnGround", wrap_ExtensionPhysXCharacterController_GetOnGround);
    lua_register(L, "_ExtensionPhysXCharacterController_SetOnGround", wrap_ExtensionPhysXCharacterController_SetOnGround);
    lua_register(L, "_SwapLevel", wrap_swapLevel);
    lua_register(L, "_GetWorldPosition", wrap_getWorldPosition);
    lua_register(L, "_PlaySound", wrap_playSound);
    lua_register(L, "_GetFirstHostWithName", wrap_GetFirstHostWithName);
    lua_register(L, "_SetCameraFocusPoint", wrap_SetCameraFocusPoint);
    lua_register(L, "_GetProfilerUpdateTime", wrap_GetProfilerUpdateTime);
    

    

    
    //luaL_dostring(L, "print 4");
    std::string engineDefaultLibsPath = InputManager::getTEngineExeFolderPath() + "/EngineRes/ScriptingEngines/Lua/std.lua";
    if (!isLuaOk(L, luaL_dofile(L, engineDefaultLibsPath.c_str())))
        std::cout << "FATAL ERROR: Scripting engine ftal error in standard lua engine library\n";

    if (!isLuaOk(L, luaL_dofile(L, path.c_str()))) {
        std::cout << "ERROR: Lua stript loading error (" << path << " ) !\n";
    }


    //Analyze for variables
    SearchForVariablesInScriptAndAddThemIfFound(path);

    PushVarsToLua();
}

void LuaScriptExtension::SearchForVariablesInScriptAndAddThemIfFound(std::string path_to_script) {
    std::fstream script;
    script.open(path_to_script);

    std::string in;
    if (script.is_open()) {
        while (getline(script, in)) {
            if (StringTools::startWith(in,"--@var")) {
                std::istringstream in_stream(in);
                std::string var_type;
                std::string var_name;
                if (!in_stream.eof())
                    in_stream >> var_type;//--@var
                else
                    continue;
                if (!in_stream.eof())
                    in_stream >> var_type;//type
                else
                    continue;
                if (!in_stream.eof())
                    in_stream >> var_name;//name
                else
                    continue;

                if (!isLuaVariableExisting(var_name)) {
                    if (var_type == "Number") {
                        struct LuaScriptVariable* var = new struct LuaScriptVariable;
                        var->type = LUASCRIPT_VAR_TYPE_NUMBER;
                        var->in_script_name = var_name;
                        var->val = 0;
                        vars.push_back(var);
                    }
                    if (var_type == "Object" ) {
                        struct LuaScriptVariable* var = new struct LuaScriptVariable;
                        var->type = LUASCRIPT_VAR_TYPE_OBJECT;
                        var->in_script_name = var_name;
                        var->ptr = nullptr;

                        vars.push_back(var);
                    }
                }

                //std::cout << in << "\n";

            }
        }
    }
    else {
        //error
    }


    script.close();
}


bool LuaScriptExtension::isLuaVariableExisting(std::string name)
{
    for (struct LuaScriptVariable* lv : vars) {
        if (lv->in_script_name == name)
            return true;
    }
    return false;
}

LuaScriptExtension::LuaScriptExtension():Extension("Lua Script")
{
    Init();
    type = "LuaScript";
}
LuaScriptExtension::~LuaScriptExtension()
{}



tinyxml2::XMLElement* LuaScriptExtension::SaveScene(tinyxml2::XMLElement* e)
{
    using namespace tinyxml2;
    XMLElement* extension = Extension::SaveScene(e);
    extension->InsertNewChildElement("IsSctiptSet")->InsertNewText((L == nullptr) ? "0.0" : "1.0");
    extension->InsertNewChildElement("ScriptName")->InsertNewText(scriptName.c_str());
    extension->InsertNewChildElement("ScriptPath")->InsertNewText(scriptPath.c_str());
    XMLElement* parameters = extension->InsertNewChildElement("Parameters");
    for (LuaScriptVariable* v : vars) {
        XMLElement* paramater = parameters->InsertNewChildElement("Parameter");
        paramater->InsertNewChildElement("Type")->InsertNewText(StringTools::intToString(v->type).c_str());
        paramater->InsertNewChildElement("Name")->InsertNewText(v->in_script_name.c_str());
        switch (v->type) {
            case LUASCRIPT_VAR_TYPE_NUMBER:
                paramater->InsertNewChildElement("Value")->InsertNewText(StringTools::doubleToString(v->val).c_str());
            break;
            case LUASCRIPT_VAR_TYPE_OBJECT:
                long idd = -1;
                if (v->ptr != nullptr)
                    idd = ((Object*)v->ptr)->getID();
                paramater->InsertNewChildElement("Value")->InsertNewText(StringTools::longToString(idd).c_str());
            break;
        }
    }

    return extension;

}

void LuaScriptExtension::Init()
{
   // std::cout << "Init ls\n";
   
}

void LuaScriptExtension::Cleanup()
{
   // std::cout << "Cleanup ls\n";
    while (vars.size() > 0)
    {
        delete vars[0];
        vars.erase(vars.begin());
    }

    if (L != nullptr) {
        lua_close(L);
    }
}

void LuaScriptExtension::Update(Object* o, Level* level)
{

   // std::cout << "Update ls\n";
    if (L != nullptr && level->isPlatModeEnabled()) {
        lua_getglobal(L, "Engine");
        if (lua_istable(L, -1)) {
            lua_pushstring(L, "Update");
            lua_gettable(L, -2);
            if (lua_isfunction(L, -1)) {
                lua_pushlightuserdata(L, o);
                if (!isLuaOk(L, lua_pcall(L, 1, 0, 0))) {
                    //some error
                }
            }
            lua_pop(L, -1);
        }
    }
}

void LuaScriptExtension::TriggerFunction(std::string functionName, Object* hostEmiter, std::string emiterTag, Level* level)
{
    if (L != nullptr && level->isPlatModeEnabled()) {
        if (L != nullptr && level->isPlatModeEnabled()) {
            lua_getglobal(L, "Engine");
            if (lua_istable(L, -1)) {
                lua_pushstring(L, functionName.c_str());
                lua_gettable(L, -2);
                if (lua_isfunction(L, -1)) {
                    lua_pushlightuserdata(L, hostEmiter);
                    lua_pushstring(L, emiterTag.c_str());
                    if (!isLuaOk(L, lua_pcall(L, 2, 0, 0))) {
                        //some error
                    }
                }
                lua_pop(L, -1);
            }
        }
    }
}


bool LuaScriptExtension::isLuaOk(lua_State* L, int r) {
    if (r != LUA_OK) {
        std::cout << std::string(lua_tostring(L, -1)) << "\n";
        return false;
    }
    return true;
}



void LuaScriptExtension::PushVarsToLua()
{

    //Analyze for in_script defined variables



    //Push variable list
    for (struct LuaScriptVariable* s :vars) {
        if (s->type == LUASCRIPT_VAR_TYPE_NUMBER) {
            lua_pushnumber(L, s->val);
            lua_setglobal(L, s->in_script_name.c_str());
        }else if (s->type == LUASCRIPT_VAR_TYPE_OBJECT) {
            if (s->ptr == nullptr)
                lua_pushnil(L);
            else
                lua_pushlightuserdata(L, s->ptr);
                lua_setglobal(L, s->in_script_name.c_str());
        }
    }
}

int LuaScriptExtension::wrap_DeleteObject(lua_State* L)
{
    if (lua_gettop(L) != 1)return -1;
    Object* o = (Object*)(lua_touserdata(L, 1));
    if(o != nullptr) o->DeleteObject();
    return 0;
}

int LuaScriptExtension::wrap_getHostObjectsChilds(lua_State* L)
{
    if (lua_gettop(L) != 1)return -1;
    Object* o = (Object*)(lua_touserdata(L, 1));
    lua_pushnumber(L,o->childs.size());

    for (Object* ch : o->childs)
        lua_pushlightuserdata(L, ch);

    return o->childs.size() + 1;
}

int LuaScriptExtension::wrap_GetExtensionByName(lua_State* L)
{
    if (lua_gettop(L) != 2)return -1;
    Object* o = (Object*)(lua_touserdata(L, 1));
    std::string extensionName = lua_tostring(L, 2);
    Extension* e = o->getExtensionByName(extensionName);
    if (e == nullptr)
        lua_pushnil(L);
    else
        lua_pushlightuserdata(L, e);

    return 1;
}
int LuaScriptExtension::wrap_ExtensionSpriteRenderer_SetPosition(lua_State* L)
{
    if (lua_gettop(L) != 3)return -1;
    SpriteRendererExtension* ex = (SpriteRendererExtension*)(lua_touserdata(L, 1));
    float x = lua_tonumber(L, 2);
    float y = lua_tonumber(L, 3);

    ex->setSpritePos({ x,y });

    return 0;
}

int LuaScriptExtension::wrap_getWorldPosition(lua_State* L)
{
    if (lua_gettop(L) != 1)return -1;
    Object* o = (Object*)(lua_touserdata(L, 1));
    glm::vec3 world_pos = o->getWorldPosition();
    lua_pushnumber(L, world_pos.x);
    lua_pushnumber(L, world_pos.y);
    lua_pushnumber(L, world_pos.z);

    return 3;
}

int LuaScriptExtension::wrap_ExtensionSpriteRenderer_SetSize(lua_State* L)
{
    if (lua_gettop(L) != 3)return -1;
    SpriteRendererExtension* ex = (SpriteRendererExtension*)(lua_touserdata(L, 1));
    float x = lua_tonumber(L, 2);
    float y = lua_tonumber(L, 3);

    ex->setSpriteSize({ x,y });

    return 0;
}

int LuaScriptExtension::wrap_ExtensionPhysXCharacterController_SetVelocity(lua_State* L)
{
    if (lua_gettop(L) != 4)return -1;
    PhysXCharacterControllerExtension* ex = (PhysXCharacterControllerExtension*)(lua_touserdata(L, 1));
    float x = lua_tonumber(L, 2);
    float y = lua_tonumber(L, 3);
    float z = lua_tonumber(L, 4);

    ex->velocity = { x,y,z };

    return 0;
}

int LuaScriptExtension::wrap_ExtensionPhysXCharacterController_GetVelocity(lua_State* L) {
    if (lua_gettop(L) != 1)return -1;
    PhysXCharacterControllerExtension* ex = (PhysXCharacterControllerExtension*)(lua_touserdata(L, 1));

    auto v = ex->velocity;

    lua_pushnumber(L, v.x);
    lua_pushnumber(L, v.y);
    lua_pushnumber(L, v.z);

    return 3;
}

int LuaScriptExtension::wrap_ExtensionPhysXCharacterController_Move(lua_State* L)
{
    if (lua_gettop(L) != 4)return -1;
    PhysXCharacterControllerExtension* ex = (PhysXCharacterControllerExtension*)(lua_touserdata(L, 1));
    float x = lua_tonumber(L, 2);
    float y = lua_tonumber(L, 3);
    float z = lua_tonumber(L, 4);

    ex->Move({ x,y,z });

    return 0;
}


int LuaScriptExtension::wrap_CreateNewInstanceOfObject(lua_State* L)
{
    if (lua_gettop(L) != 2)return -1;
    Object* parent = (Object*)(lua_touserdata(L, 1));
    Object* childObj = (Object*)(lua_touserdata(L, 2));

    Object* newInstance = childObj->MakeCopy();
    if (parent != nullptr)parent->put(newInstance);
    else childObj->getLevel()->getSceneObject()->put(newInstance);

    lua_pushlightuserdata(L, newInstance);

    return 1;//returns new object pointer
}

int LuaScriptExtension::wrap_getTransform(lua_State* L)
{
    if (lua_gettop(L) != 1)return -1;
    Object* o = (Object*)(lua_touserdata(L, 1));
    lua_pushnumber(L, o->position.x);
    lua_pushnumber(L, o->position.y);
    lua_pushnumber(L, o->position.z);

    lua_pushnumber(L, o->rotation.x);
    lua_pushnumber(L, o->rotation.y);
    lua_pushnumber(L, o->rotation.z);

    lua_pushnumber(L, o->scale.x);
    lua_pushnumber(L, o->scale.y);
    lua_pushnumber(L, o->scale.z);

    glm::vec3 forward = o->getWorldForwardVector();
    lua_pushnumber(L, forward.x);
    lua_pushnumber(L, forward.y);
    lua_pushnumber(L, forward.z);


    glm::vec3 right = o->getWorldRightVector();
    lua_pushnumber(L, right.x);
    lua_pushnumber(L, right.y);
    lua_pushnumber(L, right.z);

    return 3*5;
}

int LuaScriptExtension::wrap_setTransform(lua_State* L)
{
    if (lua_gettop(L) != 10)return -1;
    Object* o = (Object*)(lua_touserdata(L, 1));
    o->position.x = lua_tonumber(L, 2);
    o->position.y = lua_tonumber(L, 3);
    o->position.z = lua_tonumber(L, 4);


    o->rotation.x = lua_tonumber(L, 5);
    o->rotation.y = lua_tonumber(L, 6);
    o->rotation.z = lua_tonumber(L, 7);

    o->scale.x = lua_tonumber(L, 8);
    o->scale.y = lua_tonumber(L, 9);
    o->scale.z = lua_tonumber(L, 10);

    return 0;
}

int LuaScriptExtension::wrap_swapLevel(lua_State* L)
{
    if (lua_gettop(L) != 2)return -1;
    Object* o = (Object*)(lua_touserdata(L, 1));
    if (o->getLevel() != nullptr) {
        o->getLevel()->swap_level_path = lua_tostring(L, 2);
    }
    return 0;
}


int LuaScriptExtension::wrap_log(lua_State* L)
{
    if (lua_gettop(L) != 1)return -1;
    if (lua_isstring(L, 1)) {
        std::string in = lua_tostring(L, 1);
        std::cout << in << std::endl;
    }
    return 0;
}

int LuaScriptExtension::wrap_getKeyPressed(lua_State* L)
{

    if (lua_gettop(L) != 1)return -1;
    if (lua_isstring(L, 1)) {
        std::string key = lua_tostring(L, 1);
        if (key == "forward"&& InputManager::getKey(GLFW_KEY_W))    
                lua_pushboolean(L, 1);
        else if (key == "backward" && InputManager::getKey(GLFW_KEY_S))
            lua_pushboolean(L, 1);
        else if (key == "right" && InputManager::getKey(GLFW_KEY_D))
            lua_pushboolean(L, 1);
        else if (key == "left" && InputManager::getKey(GLFW_KEY_A))
            lua_pushboolean(L, 1);
        else if (key == "space" && InputManager::getKey(GLFW_KEY_SPACE))
            lua_pushboolean(L, 1);
        else if (key == "LPM" && InputManager::getMouseLPM())
            lua_pushboolean(L, 1);
        else
            lua_pushboolean(L, -0);
    }
    return 1;
}

int LuaScriptExtension::wrap_playSound(lua_State* L)
{
    SoundPlayer::PlaySound();
    return 0;
}

Extension* LuaScriptExtension::MakeCopy()
{
    LuaScriptExtension* lse = new LuaScriptExtension();
    lse->enabled = enabled;
    
    for (LuaScriptVariable* lsv : vars) {
        struct LuaScriptVariable* v = new struct LuaScriptVariable;
        v->in_script_name = std::string(lsv->in_script_name);
        v->str = std::string(lsv->str);
        v->ptr = lsv->ptr;
        v->type = lsv->type;
        v->val = lsv->val;
        lse->AddLuaVariable(v);
    }
    
    lse->LoadScript(scriptPath, scriptName);

    return lse;
}

//void LuaScriptExtension::Render(Object* o, Level* level);

void LuaScriptExtension::DrawImGui(Object* parent)
{
    if (ImGui::CollapsingHeader(name.c_str(), ImGuiTreeNodeFlags_DefaultOpen)) {

        
        Extension::DrawImGui(parent);

        if (L == nullptr)
            scriptName = "No script";

        ImGui::TextWrapped(("Script: " + scriptName).c_str(), L == nullptr, 0, ImVec2(300, 30));


        if (ImGui::BeginDragDropTarget()) {
            const ImGuiPayload* pyload = ImGui::AcceptDragDropPayload("ASSET", ImGuiDragDropFlags_None);
            if (pyload != NULL) {

                struct AssetData* recived = *((struct AssetData**)(pyload->Data));
                if (recived->type == ASSET_TYPE_LUA_SCRIPT) {
                    std::cout << " recived script:" << recived->path<<"\n"<< recived->directory << "\n"<< recived->filename << "\n";
                    //LoadModel(, );
                    LoadScript(recived->path, recived->filename);


                }
            }

            ImGui::EndDragDropTarget();
        }

        ImGui::Separator();
        
        //Variabes
        if (ImGui::TreeNode("Variables")) {
            int iid = 0;
            for (int i = 0; i < vars.size();i++) {
                LuaScriptVariable* v = vars[i];
                ImGui::PushID(iid++);
                    if (v->type == LUASCRIPT_VAR_TYPE_NUMBER) {
                        float f = v->val;
                        ImGui::DragFloat("Value", &f, 0.1);
                        v->val = f;
                    }
                    if (v->type == LUASCRIPT_VAR_TYPE_OBJECT){
                        std::string objName = "No Object Assigned";
                        Object* o = (Object*)v->ptr;
                        if (o != nullptr)
                            objName = o->name;

                        ImGui::Text(objName.c_str());
                        if (ImGui::BeginDragDropTarget()) {
                            const ImGuiPayload* pyload = ImGui::AcceptDragDropPayload("OBJECT", ImGuiDragDropFlags_None);
                            if (pyload != NULL) {

                                Object* recived = *((Object**)(pyload->Data));
                                v->ptr = (void*)recived;
                            }

                            ImGui::EndDragDropTarget();
                        }
                        

                   }
                   
                


                char buf[256];
                memset(buf, 0, 256);
                v->in_script_name.copy(buf, 256);
                ImGui::InputText("Var name", buf, IM_ARRAYSIZE(buf));
                v->in_script_name = std::string(buf);
                if (ImGui::Button("-")) {
                    lua_pushnil(L);
                    lua_setglobal(L, v->in_script_name.c_str());
                    delete v;
                    vars.erase(vars.begin() + i);
                    i--;
                    ImGui::PopID();
                    continue;
                }

                ImGui::Separator();
                ImGui::PopID();
            }

            if (ImGui::Button("+")) {
                ImGui::OpenPopup("WhatToAddPopupMenu");
            }

            if (ImGui::BeginPopup("WhatToAddPopupMenu")) {

                ImGui::Text("Variable Type");
                ImGui::Separator();
                if (ImGui::Button("Number")) {
                    struct LuaScriptVariable* var = new struct LuaScriptVariable;
                    var->type = LUASCRIPT_VAR_TYPE_NUMBER;
                    var->in_script_name = "v";
                    var->val = 0;
                    vars.push_back(var);
                    ImGui::CloseCurrentPopup();
                }
                if (ImGui::Button("Object")) {
                    struct LuaScriptVariable* var = new struct LuaScriptVariable;
                    var->type = LUASCRIPT_VAR_TYPE_OBJECT;
                    var->in_script_name = "o";
                    var->ptr = nullptr;
                        
                    vars.push_back(var);
                    ImGui::CloseCurrentPopup();
                }

                ImGui::EndPopup();
            }

            if (ImGui::Button("Push to script")) {
                PushVarsToLua();
            }
            if (ImGui::Button("Reload script")) {
                LoadScript(scriptPath, scriptName);
            }

            ImGui::TreePop();
        }



    }
    ImGui::Separator();

}

int LuaScriptExtension::wrap_ExtensionPhysXCharacterController_SetOnGround(lua_State* L)
{
    if (lua_gettop(L) != 2)return -1;
    PhysXCharacterControllerExtension* ex = (PhysXCharacterControllerExtension*)(lua_touserdata(L, 1));
    bool x = lua_toboolean(L, 2);
    ex->onGround = x;

    return 0;
}

int LuaScriptExtension::wrap_ExtensionPhysXCharacterController_GetOnGround(lua_State* L)
{
    if (lua_gettop(L) != 1)return -1;
    PhysXCharacterControllerExtension* ex = (PhysXCharacterControllerExtension*)(lua_touserdata(L, 1));

    lua_pushboolean(L, ex->onGround);

    return 1;
}

namespace luaScriptsGlobalSpace {
    Level* level = nullptr;
};

void LuaScriptExtension::SetGlobalLevel(Level* level) {
    luaScriptsGlobalSpace::level = level;
}

int LuaScriptExtension::wrap_GetFirstHostWithName(lua_State* L)
{
    if (lua_gettop(L) != 1)return -1;
    auto name = std::string(lua_tostring(L, 1));

    auto founded = luaScriptsGlobalSpace::level->getSceneObject()->findFirstObjectWithName(name);

    if (founded == nullptr) {
        //std::cout << "ERROR | LuaScriptExtension: Object with name '"<<name<<"' was not found.";
        lua_pushnil(L);
    }
    else
        lua_pushlightuserdata(L, founded);

    return 1;
}


int LuaScriptExtension::wrap_SetCameraFocusPoint(lua_State* L)
{
    if (lua_gettop(L) != 2)return -1;
    Camera* camera = (Camera*)(lua_touserdata(L, 1));
    Object* o = (Object*)(lua_touserdata(L, 2));

    if(camera != nullptr && o != nullptr)
    camera->SetObjectToFocus(o);

    return 0;
}

int LuaScriptExtension::wrap_GetProfilerUpdateTime(lua_State* L) {

    auto sceneO = luaScriptsGlobalSpace::level->getSceneObject();
    if (sceneO != nullptr)
        lua_pushnumber(L, InputManager::getMainLoopTimeProfiler());
    else
        lua_pushnil(L);

    return 1;
}