#pragma once
#ifndef OBJECT_H
#define OBJECT_H
extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include <GL/glew.h>
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include "imgui/imgui.h"
#include <glm/glm.hpp>
#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>
#include <glm/ext/quaternion_common.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include "InputManager.h"
#include "tinyxml2/tinyxml2.h"
#include "ModelRendererExtension.h"
#include "StringTools.h"
#include <GLFW/glfw3.h>
#include "ModelManager.h"
#include "Renderer2D.h"
#include <PxPhysics.h>
#include <PxPhysicsAPI.h>
#include "Framebuffer.h"
#include "SoundPlayer.h"
#include "CubemapTexture.h"
#include "Profiler.h"
#include "ProfilerTimerUnit.h"



#define OBJECT_ACTION_CREATE 0
#define OBJECT_ACTION_DELETE 1
#define OBJECT_ACTION_EDITED 2

#define CAMERA_PROJECTION_PERSPECTIVE 0
#define CAMERA_PROJECTION_ORTHO 1

#define CAMERA_PROJECTION_ORTHO_ADJUST_TO_HEIGHT 0
#define CAMERA_PROJECTION_ORTHO_ADJUST_TO_WIDTH 1

class Level;
class Extension;
class ModelRendererExtension;
class TerrainExtension;
class PointLightExtension;
class DirectionalLightExtension;
class Model;
class Camera;
class LuaScriptExtension;


class DefferedRenderer;
class ForwardRenderer;

/// Klasa Object jest podstaw� dla wszystkich obiekt�w w grze.
///
/// Sama w sobie nie robi nic poza istnieniem, posiada jedynie transformacje i mo�liwo�� przechowywania innych obiekt�w (Object).
/// Jej funkcjonalno�� mo�e by� rozszerzona poprzez dodanie odpowiednich rozszerze� (Extension).
/// Przyk�ad: Po dodaniu do obiektu Object rozszerzenia LuaScriptExtension obiekt zaczyna zachowywa� si� w spos�b zdefiniowany w skrypcie.
class Object {
public:
	///Konstruktor dla obkietu sceny (jedyny obiekt bez rodzica)
	///
	/// @param string name - nazwa obiektu
	/// @param long id - unikatowe ID obiektu generowanie z Level::GetObjectID()
	Object(std::string name,long id);

	///Konstruktor dla obkietu w scenie
	///
	/// @param string name - nazwa obiektu
	/// @param Object* parent - rodzic
	/// @param long id - unikatowe ID obiektu generowanie z Level::GetObjectID()
	Object(std::string name, Object* parent,long id);
	~Object();

	//Object functions
	///Inicjalizuje obiekt
	virtual void Init();
	/// Aktualizuje obiekt (Object), jego dzieci (Object) oraz rozszerzenia (Extension)
	virtual void Update(Level* level);
	/// Renderuje obiekt (Object), jego dzieci (Object) oraz rozszerzenia (Extension)
	virtual void Render(Level* level);

	/// Aktualizuje parametry rozszerze� skrypt�w lua LuaScriptExtension dla tego obiektu (Object) oraz jego dzieci (Object)
	virtual void PushScriptingVariables();
	
	/// Zwalnia pami�� z rozszerze� (Extension)
	/// @see Level:deleteObject(Object* o)
	virtual void Cleanup();

	/// Zapisuje wszystkie parametry obiektu Object do tinyxml2::XMLElement* e
	virtual tinyxml2::XMLElement* SaveScene(tinyxml2::XMLElement* e);
	/// Odczytuje parametry obiektu Object z tinyxml2::XMLNode* e
	static Object* LoadScene(tinyxml2::XMLNode* obj);
	/// Odczytuje parametry specjalne obiektu Object z tinyxml2::XMLNode* e
	virtual void LoadAdditionalData(tinyxml2::XMLNode* e) {}

	virtual void OnPlay();

	///Przyj�scie informacji o jakim� zdarzeniu.
	///
	/// Przyk�adowo, je�eli jakie� z rozszerze� u�ywa�o innego obiektu i ten zosta� usuni�ty to by unikn�� odwo�ywania si� do usuni�tego obiektu.
	virtual void objectActionNotify(Object* o,unsigned int action);

	/// Zwraca kopi� obiektu (Object) z nowym ID
	Object* MakeCopy();
	/// Zwraca ID obiektu
	inline long getID() { return id; }


	//Functions for hierarchy managment
	/// Dodaje nowy obiekt o (Object) jako dziecko do tego obiektu (Object)
	void put(Object* o);

	/// Usuwa obiekt
	void DeleteObject();
	/// Usuwa obiekt o (Object) z listy dzieci, lecz nie zwalnia jego pami�ci.
	void deleteChild(Object* o);
	/// Szuka obiektu po�r�d swoich dzieci i samego siebie o danym ID i zwraca go je�li go znajdzie lub zwraca nullptr w przypadku niepowodzenia.
	Object* findObjectInLevelByID(long id);
	Object* findFirstObjectWithName(std::string name);

	///Testuje czy dany obiekt jest dzieckiem innego obiektu.
	bool isChildOf(Object* par);
	/// Usuwa powi�zanie rodzic-dziecko tego obiektu
	bool unchild();
	/// Zwraca czy obiekt ma dzieci czy nie
	inline bool hasChilds() { return childs.size() > 0; }

	///Dodaje rozszerzenie(Extension) do obiektu(Object)
	void addExtension(Extension* e);
	///Usuwa rozszerzenie(Extension) z obiektu(Object)
	void deleteExtension(Extension* e);
	///Zwraza rozszerzenie(Extension) o danej nazwie z obiektu(Object)
	/// @param std::string name - nazwa rozszerzenia
	Extension* getExtensionByName(std::string name);

	///Zwraza macierz widoku dla tego obiektu.
	virtual glm::mat4 getViewMatrix();
	///Zwraza macierz transformacji dla tego obiektu.
	virtual glm::mat4 getWorldSpaceTransformationMatrix();

	///Zwraza macierz transformacji dla tego obiektu.
	virtual glm::mat4 getWorldSpaceRotationMatrix();

	inline Level* getLevel() { return level; }

	glm::vec3 getParentScale() {
		if (parent == nullptr)return glm::vec3(1);
		return scale*parent->getParentScale();
	}

	glm::quat getParentRotation() {
		if (parent == nullptr)return glm::quat(glm::vec3(0,0,0));
		return parent->qrot*parent->getParentRotation();
	}

	glm::quat getWorldRotation() {
		return qrot * getParentRotation();
	}

	glm::quat getDecomposedWorldRotation() {
		glm::mat4 transformation = getWorldSpaceTransformationMatrix();
		glm::vec3 scale;
		glm::quat rotation;
		glm::vec3 translation;
		glm::vec3 skew;
		glm::vec4 perspective;
		glm::decompose(transformation, scale, rotation, translation, skew, perspective);
		return rotation;
	}

	glm::vec3 getWorldScale() {
		return getParentScale();
	}

	glm::vec3 getWorldPosition() {
		if (parent == nullptr)return glm::vec3(0);
		glm::vec4 worldPos = (parent->getWorldSpaceTransformationMatrix() * glm::vec4(position.x, position.y, position.z, 1.0));
		return glm::vec3(worldPos.x, worldPos.y, worldPos.z);
	}

	void setWorldPosition(glm::vec3 newWorldPosition) {
		if (parent == nullptr)return;
		glm::vec4 np = glm::inverse(parent->getWorldSpaceTransformationMatrix())* glm::vec4(newWorldPosition.x, newWorldPosition.y, newWorldPosition.z, 1.0f);
		position = glm::vec3(np.x, np.y, np.z);
	}

	glm::vec3 getWorldForwardVector() {
		glm::mat4 rot_mat = getWorldSpaceRotationMatrix();
		glm::vec4 fm = { 0,0,-1,0 };
		fm = rot_mat * fm;
		fm.y = 0;
		glm::vec4 result = glm::normalize(fm);
		return { result.x,result.y,result.z };
	}

	glm::vec3 getWorldRightVector() {
		glm::mat4 rot_mat = getWorldSpaceRotationMatrix();
		glm::vec4 fm = { 1,0,0,0 };
		fm = rot_mat * fm;
		fm.y = 0;
		glm::vec4 result = glm::normalize(fm);
		return { result.x,result.y,result.z };
	}

	glm::vec3 getWorldUpVector() {
		glm::mat4 rot_mat = getWorldSpaceRotationMatrix();
		glm::vec4 fm = { 0,1,0,0 };
		glm::vec4 result = glm::normalize(rot_mat * fm);
		return { result.x,result.y,result.z };
	}

	///Rysuje GUI dla silnika z wykorzystaniem bibliotegi ImGui
	virtual void drawImGuiInspector();
	/// Zwraca czy obiekt jest scen�.
	inline bool isScene() { return parent == nullptr; }

	inline glm::quat getRotation() { qrot = glm::quat(rotation); return qrot; }


	std::string name;
	Object* parent;
	std::vector<Object*> childs;
	std::vector<Extension*> extensions;

	//transform
	glm::vec3 position;
	glm::vec3 rotation;
	glm::quat qrot;
	glm::vec3 scale = glm::vec3(1,1,1);
	glm::mat4 transform_matrix;

	ProfilerTimerUnit renderTimer, updateTimer;

	double getRecurentRenderTimeSum();
	double getRecurentUpdateTimeSum();
	void ResetTimers();
protected:
	long id;
	void drawImGuiTransformation();
	std::string type = "Object";
	Level* level = nullptr;
};


/// Rozszerzenie (Extension) s�u�y do rozszerzania funckjonalno�ci obiekt�w (Object)
/// 
/// Extension jest klas� bazow� dla rozszerze�. Samo Extension nie robi nic.
/// @see Object
class Extension {
public:
	/// Konstruktor dla Extension
	/// @param string name - nazwa rozszerzenia
	Extension(std::string name);
	~Extension();

	/// Odpowiednik SaveScene z Object
	virtual tinyxml2::XMLElement* SaveScene(tinyxml2::XMLElement* e);
	/// Odpowiednik LoadScene z Object
	static Extension* LoadScene(tinyxml2::XMLNode* obj);
	/// Odpowiednik LoadAdditionalData z Object
	virtual void LoadAdditionalData(tinyxml2::XMLNode* e) {}

	/// Odpowiednik Init z Object
	virtual void Init();
	/// Odpowiednik Cleanup z Object
	virtual void Cleanup();
	/// Cz�� z logik� rozszerzenia (nie u�ywana przez ka�de rozszerzenie)
	virtual void Update(Object* o, Level* level);
	/// Cz�� renderuj�ca rozszerzenia (nie u�ywana przez ka�de rozszerzenie)
	virtual void Render(Object* o, Level* level);

	virtual void OnPlay();

	/// Odpowiednik MakeCopy z Object
	virtual Extension* MakeCopy() { return nullptr; }

	/// Odpowiednik DrawImGui z Object
	virtual void DrawImGui(Object* parent);

	std::string name,type;
	bool enabled;
	bool toDelete;

};



#define SIMPLE_TRIGGER_LISTNENER 0
#define SIMPLE_TRIGGER_EMITER 1


struct SimpleTriggerEmiter_s {
	Object* emiter;
	std::string tag;
};

class SimpleTriggerExtension :public Extension {
public:
	SimpleTriggerExtension();
	~SimpleTriggerExtension();
	tinyxml2::XMLElement* SaveScene(tinyxml2::XMLElement* e);


	static void InitStaticData();
	static void CleanupStaticData();
	static void AfterPlay();
	static void DeleteNotification(Object* o);

	virtual void Init();
	virtual void Cleanup();
	virtual void Update(Object* o, Level* level);
	virtual Extension* MakeCopy();
	virtual void OnPlay();
	virtual void Render(Object* o, Level* level);

	virtual void DrawImGui(Object* parent);

	std::string tag;
	std::string luaFunctionName;
	int triggerType = SIMPLE_TRIGGER_LISTNENER;
	double radius = 0.3f;
private:
	bool isEmiterRegistered = false;

};

/// Specjalna klasa dziedzicz�ca po Object, pe�ni rol� kamery.
class Camera : public Object {
public:
	Camera(long id);
	~Camera();

	virtual tinyxml2::XMLElement* SaveScene(tinyxml2::XMLElement* e);
	virtual void LoadAdditionalData(tinyxml2::XMLNode* e);

	/// Opis taki sam jak w Object
	void Init();
	/// Opis taki sam jak w Object
	void Update(Level* level);
	/// Opis taki sam jak w Object
	void Render(Level* level);
	/// Opis taki sam jak w Object
	void Cleanup();
	/// Opis taki sam jak w Object
	glm::mat4 getWorldSpaceTransformationMatrix();
	/// Opis taki sam jak w Object
	void drawImGuiInspector();
	/// Opis taki sam jak w Object
	glm::mat4 getViewMatrix();
	glm::mat4 getViewRotationOnlyMatrix();
	/// Zwraca macierz projekcji
	inline glm::mat4 getProjection() { return projection; }
	/// Opis taki sam jak w Object
	void objectActionCallback(Object* o, unsigned int action);

	inline float getBloomTresholdValue() { return bloom_treshold; }

	glm::mat4 prevCameraViewMatrix;

	void BakeCubemap();

	inline void SetObjectToFocus(Object* o) { if (o != nullptr) objectToLookAt = o; }

	//SSAO
	int kernelSize = 32;
	float radius = 10.3f;
	float bias = 0.015;
	float skybox_strength = 1.0f;
	CubemapTexture* cubemap; 
private:
	/// Je�li ustawione, kamera przelicza macierz widoku z getViewMatrix na tak� kt�ra patrzy w kierunku wybranego obiektu
	Object* objectToLookAt = nullptr;
	glm::mat4 projection;
	glm::vec3 direction;
	int projectionType = CAMERA_PROJECTION_PERSPECTIVE;
	//3d projection
	float fov = 45.0f, near_plane = 0.25f, far_plane = 100.5f;
	//2d projection
	float projection_size = 10.0f;
	int ajdust_type = CAMERA_PROJECTION_ORTHO_ADJUST_TO_HEIGHT;


	bool editorCamera = true;
	bool setAsActiveCamera = false;
	float sensitivity = 0.25;
	float speed = 0.2;
	float fastspeed = 1.2;
	double firstEditCameraFrame = 0;
	float pich=0, yaw=0;
	float bloom_treshold = 0.85f;

	//Skybox params
	//float skybox_strength = 1.0f;
	float skybox_rotation = 0.0f;
	std::string skybox_texture_path = "";

	//float exposition = DXcostamXD;

};

///Struktua s�u��ca do przekazywania rozkazu zamiany ID obiektu na jego wska�nik
/// @param void* ptr - wska�nik do kt�rego b�dzie zapisany wska�nik obiektu z ID == obj_id
/// @param long obj_id - id szukanego obiektu
struct SwapObjectPointer {
	void* ptr;
	long obj_id;
};

/// Klasa singleton przechowywyuj�ca informacje od danym poziomie.
class Level {
public:
	Level() { Level(true); }
	Level(bool init_physx);
	~Level();

	/// Opis taki sam jak w TEngine Update()
	void Update();
	/// Opis taki sam jak w TEngine Render()
	void Render();

	///Zapisuje dan� scene wraz z jej wszystkimi parametrami do pliku
	/// @param string path - �cie�ka do pliku
	void Save(std::string path);
	///Odczytuj� scene wraz z jej wszystkimi parametrami z pliku
	/// @param string path - �cie�ka do pliku
	void Load(std::string path);
	/// Zwraca obiekt Object sceny
	inline Object* getSceneObject() { return sceneObject; }
	/// Ustawia nowy obiekt sceny
	inline void setupNewSceneObject(Object* newScene) { if (!isSceneCreated())sceneObject = newScene; }
	/// Zwraca informacje czy scena jest zainicjalizowana
	inline bool isSceneCreated() { return sceneObject != nullptr; }
	/// Zwraca obiekt aktywnej kamery Camera - na raz mo�e by� aktywna tylko jedna kamera
	inline Camera* getActiveCamera() { return camera; }
	/// Ustawia obiekt aktywnej kamery Camera - na raz mo�e by� aktywna tylko jedna kamera
	inline void setActiveCamera(Camera* cam) { camera=cam; }

	/// Czy scena posiada plik do kt�rego mo�na zapisa� zmiany
	inline bool isThisLevelHasFileToSave() { return sceneFilePath != ""; }
	/// Zwraca �cie�k� do pliku sceny
	inline std::string getSceneFilePath() { return sceneFilePath; }
	/// Zwraca Renderer2D sceny
	inline Renderer2D* getRenderer2D() { return renderer2D; }
	/// Ustawia Renderer2D sceny
	inline void setRenderer2D(Renderer2D* renderer2D) { this->renderer2D = renderer2D; }

	/// Bezpiecznie usuwa dany obiekt Object ze sceny
	void deleteObject(Object *o);
	/// Usuwa ca�� scene
	void DeleteLevel();

	//static methods
	/// Generuje nowy unikatowy ID dla obiektu
	static long GetObjectID();
	/// przy �adowaniu sceny z pliku przywraca iteratorID dla sceny
	static void setLevelObjectIDIterator(long new_id);

	/// Usuwa bezpiecznie zarejestrowany w kolejce obiekt Object
	/// @see LuaScriptExtension:wrap_DeleteObject
	static void RegisterObjectToDeletion(Object* o);
	/// Szuka w scenie obiektu o wskazanym w (struct SwapObjectPointer) ID i ustawia odpowiedni wska�nik (u�ywane przy �adowaniu sceny z pliku)
	static void RegisterObjectSwapService(struct SwapObjectPointer s);
	/// Szuka w scenie obiektu o podanym ID
	Object* findObjectInLevelByID(long id);
	Object* findFirstObjectWithName(std::string name, Object* start);

	static void AddPointLight(PointLightExtension* pl);
	static void RemovePointLight(PointLightExtension* pl);
	static std::vector<PointLightExtension*> GetPointLightsList();
	
	static void AddDirectionalLight(DirectionalLightExtension* dl);
	static void RemoveDirectionalLight(DirectionalLightExtension* dl);
	static std::vector<DirectionalLightExtension*> GetDirectionalLightsList();

	///W��cza tryb rozgrywki, czyli symulacje i skrypty zaczynaj� dzia�a�
	void EnablePlayMode();
	inline void DisablePlayMode() { SimpleTriggerExtension::AfterPlay(); playMode = false; /*Przywroc stan sceny*/ }
	bool isPlatModeEnabled() { return playMode; }

	///Wska�nik na wybrany obiekt, u�ywane przez inspektor obiet�w w silniku (InspectorWindow)
	Object* selectedObjectPointer;

	bool cameraSkyboxPass = false;
	bool defferedPass = false;
	bool lightPass = false;
	bool showCollisions = false;
	DirectionalLightExtension* tmp_dri_light = nullptr;

	DefferedRenderer* defferedRenderer;
	ForwardRenderer* forwardRenderer;
	Framebuffer* finalFB;
	

	physx::PxScene* mScene = NULL;
	physx::PxControllerManager* characterControllersManager;
	std::string swap_level_path = "";//if filled then swap level to this path

	inline physx::PxCooking* getPhysxCooking() { return gPxCooking; }
	inline double getPhysxProfilerTime() { return physxProfilerTimer.GetTimeMesurement(); }

private:
	Renderer2D* renderer2D;
	std::string sceneFilePath = "";
	void AssignObjectPointersToLuaScriptVariables();
	Object* sceneObject = nullptr;
	Camera* camera = nullptr;

	bool playMode = false;

	//PhysX
	physx::PxDefaultAllocator mDefaultAllocatorCllback;
	physx::PxDefaultErrorCallback mDefaulrErrorCallback;
	physx::PxDefaultCpuDispatcher* mDispatcher = NULL;
	physx::PxTolerancesScale mTolerancesScale;
	physx::PxFoundation* mFoundation = NULL;
	physx::PxCooking* gPxCooking = NULL;
	physx::PxPhysics* mPhysics = NULL;
	physx::PxMaterial* mMaterial = NULL;
	physx::PxPvd* pvd = nullptr;
	physx::PxPvdTransport* transport = nullptr;
	ProfilerTimerUnit physxProfilerTimer;
};


/// Rozszerzenie pozwalaj�ce renderowa� modele 3D
class ModelRendererExtension :public Extension {
public:
	ModelRendererExtension();
	~ModelRendererExtension();
	tinyxml2::XMLElement* SaveScene(tinyxml2::XMLElement* e);
	virtual void Init();
	virtual void Cleanup();
	virtual void Update(Object* o, Level* level);
	virtual void Render(Object* o, Level* level);
	virtual Extension* MakeCopy();

	virtual void DrawImGui(Object* parent);
	/// �aduje model je�li nie zosta� jeszcze za�adowany
	/// @param string modelName - nazwa modelu
	/// @param string modelPath - �cie�ka do pliku modelu
	/// @see ModelManager
	void LoadModel(std::string modelName, std::string modelPath, bool staticLoad = false);
	/// Zwraca wska�nik na Model model
	inline Model* getModel() { return modelRecord->model; }

	static void LoadGlobalShader();
	static void DeleteGlobalShader();

	inline void setTransparent(bool v) { transparent = v; }
	inline bool isTransparent() { return transparent; }
	
private:
	void DeleteStaticModel();

	ModelRecord* modelRecord;
	std::string modelName;
	std::string modelPath;
	glm::mat4 prev_worldSpaceMatrix;


	bool drawToStencilBuffor = false;
	bool depthTest = true;
	bool staticLoadedModel = false;

	bool transparent = false;
	float dissapear = 1.0f;
};



class TerrainExtension: public Extension {
public:
	TerrainExtension();
	~TerrainExtension();
	tinyxml2::XMLElement* SaveScene(tinyxml2::XMLElement* e);
	virtual void Init();
	virtual void Cleanup();
	virtual void Update(Object* o, Level* level);
	virtual void Render(Object* o, Level* level);
	virtual Extension* MakeCopy();

	virtual void DrawImGui(Object* parent);
	/// �aduje model je�li nie zosta� jeszcze za�adowany
	/// @param string modelName - nazwa modelu
	/// @param string modelPath - �cie�ka do pliku modelu
	/// @see ModelManager
	void GenerateTerrain(std::string heightmap_texture);
	/// Zwraca wska�nik na Model model
	inline Model* getModel() { return model; }

	static void LoadGlobalShader();
	static void DeleteGlobalShader();

	

private:
	Model* model;
	glm::mat4 prev_worldSpaceMatrix;
	Texture* texture;

	std::string heightmap_path;




};



#define LUASCRIPT_VAR_TYPE_OBJECT 0
#define LUASCRIPT_VAR_TYPE_TEXTURE 1
#define LUASCRIPT_VAR_TYPE_NUMBER 2
#define LUASCRIPT_VAR_TYPE_STRING 3

/// Struktura przechowywuj�ce dane przekazywane do skrypt�w lua w LuaScriptExtension
/// @param int type - typ parametru, czy to obiekt czy warto�c albo asset itd.
/// @param void* ptr - wska�nik na co� co definiuje LuaScriptVariable.type  (je�li typ == LUASCRIPT_VAR_TYPE_OBJECT lub LUASCRIPT_VAR_TYPE_TEXTURE)
/// @param double val - warto�� przekazywana do skryptu (je�li typ == LUASCRIPT_VAR_TYPE_NUMBER)
/// @param string str - tekst przekazywany do skryptu (je�li typ == LUASCRIPT_VAR_TYPE_STRING)
/// @param string in_script_name - nazwa zmiennej globalnej w skrypcie kt�ra ma by� ustawiona na podan� warto��
struct LuaScriptVariable {
	int type;
	void* ptr;
	double val;
	std::string str;
	std::string in_script_name;
};

/// Rozszerzenie wykonuj�ce skrypt lua
class LuaScriptExtension :public Extension {
public:
	LuaScriptExtension();
	~LuaScriptExtension();
	tinyxml2::XMLElement* SaveScene(tinyxml2::XMLElement* e);
	/// �aduje i inicjaliuje skrypt lua
	void LoadScript(std::string path, std::string name);
	
	virtual void Init();
	virtual void Cleanup();
	virtual void Update(Object* o, Level* level);
	virtual Extension* MakeCopy();

	static void SetGlobalLevel(Level* level);

	void PushVarsToLua();
	///Dodaje zmienn� struct LuaScriptVariable* v do listy zmiennych przekazywanych skryptu po wuwo�aniu PushVarsToLua()
	inline void AddLuaVariable(struct LuaScriptVariable* v) { vars.push_back(v); }
	//virtual void Render(Object* o, Level* level);
	bool isLuaVariableExisting(std::string name);

	virtual void DrawImGui(Object* parent);

	void TriggerFunction(std::string functionName, Object* hostEmiter, std::string emiterTag, Level* level);

private:
	void SearchForVariablesInScriptAndAddThemIfFound(std::string path_to_script);
	/// Metoda sprawdza czy wykonanie akcji si� powiod�o.
	bool isLuaOk(lua_State* L, int r);
	/// Motoda podpi�ta pod skypt, kt�ra wy�wietla text w konsoli
	static int wrap_log(lua_State* L);
	/// Motoda podpi�ta pod skypt, kt�ra przekazuje do skryptu aktualn� transformacje obiektu
	static int wrap_getTransform(lua_State* L);
	/// Motoda podpi�ta pod skypt, kt�ra ustawia transformacje obiektu na t� przekazan� z skryptu
	static int wrap_setTransform(lua_State* L);
	/// Motoda podpi�ta pod skypt, kt�ra tworzy nowy obiekt i dodaje go do sceny
	static int wrap_CreateNewInstanceOfObject(lua_State* L);
	/// Motoda podpi�ta pod skypt, kt�ra usuwa dany obiekt
	static int wrap_DeleteObject(lua_State* L);

	/// Motoda podpi�ta pod skypt, kt�ra zwraza do skryptu wska�nik do danego rozszerzenia obiektu
	static int wrap_GetExtensionByName(lua_State* L);
	/// Motoda podpi�ta pod skypt, kt�ra ustawia pozycj� wycinania sprita w danym SpriteRendererExtension
	static int wrap_ExtensionSpriteRenderer_SetPosition(lua_State* L);
	/// Motoda podpi�ta pod skypt, kt�ra ustawia rozmiar wycinania sprita w danym SpriteRendererExtension
	static int wrap_ExtensionSpriteRenderer_SetSize(lua_State* L);

	static int wrap_ExtensionPhysXCharacterController_Move(lua_State* L);
	static int wrap_ExtensionPhysXCharacterController_SetVelocity(lua_State* L);
	static int wrap_ExtensionPhysXCharacterController_GetVelocity(lua_State* l);
	static int wrap_ExtensionPhysXCharacterController_SetOnGround(lua_State* L);
	static int wrap_ExtensionPhysXCharacterController_GetOnGround(lua_State* L);

	/// Motoda podpi�ta pod skypt, kt�ra zwraca czy dany klawisz zosta� wci�ni�ty.
	static int wrap_getKeyPressed(lua_State* L);
	/// Motoda podpi�ta pod skypt, kt�ra zwraca do skryptu list� wska�nik�w Object dzieci obiektu wskazanego wska�nikiem
	static int wrap_getHostObjectsChilds(lua_State* L);

	static int wrap_swapLevel(lua_State* L);

	static int wrap_getWorldPosition(lua_State* L);
	static int wrap_playSound(lua_State* L);
	static int wrap_GetFirstHostWithName(lua_State* L);
	static int wrap_SetCameraFocusPoint(lua_State* L);
	static int wrap_GetProfilerUpdateTime(lua_State* L);

	lua_State* L = nullptr;
	std::string scriptName, scriptPath;
	std::vector<struct LuaScriptVariable*> vars;
};

/// Klasa rozszerzenia pozwala renderowa� wyci�te z tekstury (Texture) sprity 2D 
class SpriteRendererExtension :public Extension {
public:
	SpriteRendererExtension();
	~SpriteRendererExtension();
	tinyxml2::XMLElement* SaveScene(tinyxml2::XMLElement* e);


	virtual void Init();
	virtual void Cleanup();
	virtual void Update(Object* o, Level* level);
	virtual Extension* MakeCopy();

	/// Ustawia textur�
	/// 
	/// @param Texture* t - tekstura
	inline void setTexture             (Texture* t)			{ sprite_texture = t; }
	/// Ustawia pozycj� i rozmiar wycinania z tekstury
	inline void setPosSizeVector       (glm::vec4 posSiz)	{ spritePosSiz = posSiz; }
	/// Ustawia pozycj� wycinania z tekstury
	inline void setSpritePos           (glm::vec2 pos)		{ spritePosSiz.x = pos.x; spritePosSiz.y = pos.y; }
	/// Ustawia rozmiar wycinania z tekstury
	inline void setSpriteSize          (glm::vec2 size)		{ spritePosSiz.z = size.x;spritePosSiz.w = size.y;}
	/// Ustawia kolor tekstury
	inline void setColorMultiplier(glm::vec3 sprite_color)  { this->sprite_color = sprite_color; }

	/// Zwraca pozycj� i rozmiar wycinania z tekstury
	inline glm::vec4 getPosSizVector   () { return spritePosSiz;							  }
	/// Zwraca pozycj�  wycinania z tekstury
	inline glm::vec2 getSpritePos	   () { return glm::vec2(spritePosSiz.x, spritePosSiz.y); }
	/// Zwraca  rozmiar wycinania z tekstury
	inline glm::vec2 getSpriteSiz	   () { return glm::vec2(spritePosSiz.z, spritePosSiz.w); }
	/// Zwraca kolor tekstury
	inline glm::vec3 getColorMultiplier() { return sprite_color; }

	/// Ustawia textur�
	/// 
	/// @param string path - �cie�ka do tekstury
	void setTexture(std::string path);

	///Renderuje sprite wyci�ty z textury za pomoc� Level -> getRenderer2D()
	virtual void Render(Object* o, Level* level);

	virtual void DrawImGui(Object* parent);


private:
	std::string texture_path, texture_name,spriteRenderer_name;
	Texture* sprite_texture;//used texture
	glm::vec4 spritePosSiz;
	glm::vec3 sprite_color;
	bool cameraViewer = false;
	
};



#define PHYSX_EXTENSION_COLLISION_SHAPE_CUBE 0
#define PHYSX_EXTENSION_COLLISION_SHAPE_SPHERE 1
#define PHYSX_EXTENSION_COLLISION_SHAPE_CAPSULE 2
#define PHYSX_EXTENSION_COLLISION_SHAPE_MESH_COLLIDER 3
#define PHYSX_EXTENSION_COLLISION_SHAPE_TERRAIN_COLLIDER 4

/// Klasa rozszerzenia pozwala doda� fizyk� do obiektow 
class PhysXExtension :public Extension {
public:
	PhysXExtension();
	~PhysXExtension();
	tinyxml2::XMLElement* SaveScene(tinyxml2::XMLElement* e);


	static void InitStaticData();
	static void CleanupStaticData();

	virtual void Init();
	virtual void Cleanup();
	virtual void Update(Object* o, Level* level);
	virtual Extension* MakeCopy();
	virtual void OnPlay();

	
	virtual void Render(Object* o, Level* level);

	virtual void DrawImGui(Object* parent);

	///Inicjalizuje aktora physx
	void InitPhysX_Body(physx::PxScene* scene, Level* level, Object* parent);
	void LoadCollisionModel(std::string filename, std::string filePath);


	bool isTrigger = false;
	bool isDynamic = false;
	float mass = 10;
	int collisionShapeType = PHYSX_EXTENSION_COLLISION_SHAPE_CUBE;
	glm::vec3 collider_size = { 1,1,1 };
	glm::vec3 material_properties = { 0.5f, 0.5f, 0.2f };
	glm::vec3 shape_origin = { 0,0,0 };
	std::string collisionModelName, collisionModelPath;
private:

	


	ModelRendererExtension* collisionShapeModel = nullptr;


	physx::PxShape* shape = nullptr;
	physx::PxRigidActor* actor = nullptr;
	physx::PxMaterial* px_mat = nullptr;

	bool initialized = false;
};


/// Klasa rozszerzenia pozwala doda� CCT do obiekt�w 
class PhysXCharacterControllerExtension :public Extension {
public:
	PhysXCharacterControllerExtension();
	~PhysXCharacterControllerExtension();
	tinyxml2::XMLElement* SaveScene(tinyxml2::XMLElement* e);


	virtual void Init();
	virtual void Cleanup();
	virtual void Update(Object* o, Level* level);
	virtual Extension* MakeCopy();


	virtual void Render(Object* o, Level* level);

	virtual void DrawImGui(Object* parent);

	///Inicjalizuje aktora physx
	void InitPhysX_Body(physx::PxScene* scene, Level* level, Object* parent);

	void Move(glm::vec3 displacment);
	
	float mass = 10;
	float gravity = 0.002;
	glm::vec3 collider_size = { 0.5f,1,1 };
	glm::vec3 material_properties = { 0.5f, 0.5f, 0.2f };
	glm::vec3 shape_origin = { 0,0,0 };
	
	glm::vec3 velocity = { 0,0,0 };
	bool onGround = false;

private:

	
	physx::PxMaterial* px_mat = nullptr;

	physx::PxController* controller = nullptr;
	bool initialized = false;
};


class PointLightExtension :public Extension {
public:
	PointLightExtension();
	~PointLightExtension();
	tinyxml2::XMLElement* SaveScene(tinyxml2::XMLElement* e);


	virtual void Init();
	virtual void Cleanup();
	virtual void Update(Object* o, Level* level);
	virtual Extension* MakeCopy();

	
	inline void setColor(glm::vec3 lightColor) { this->lightColor = lightColor; }
	inline glm::vec3 getPosition() { return pos; }

	virtual void Render(Object* o, Level* level);

	virtual void DrawImGui(Object* parent);


	glm::vec3 lightColor;
	bool castShadows = true;
	float intensity = 1.0f;
	float radius = 1.0f;
private:
	glm::vec3 pos;

};

class DirectionalLightExtension :public Extension {
public:
	DirectionalLightExtension();
	~DirectionalLightExtension();
	tinyxml2::XMLElement* SaveScene(tinyxml2::XMLElement* e);


	virtual void Init();
	virtual void Cleanup();
	virtual void Update(Object* o, Level* level);
	virtual Extension* MakeCopy();


	inline void setColor(glm::vec3 lightColor) { this->lightColor = lightColor; }
	inline glm::vec3 getDirection() { return { dir.x,dir.y,dir.z}; }

	virtual void Render(Object* o, Level* level);

	virtual void DrawImGui(Object* parent);

	static void InitShaders();
	static void CleanupShaders();

	glm::vec3 lightColor;
	bool castShadows = true;
	float intensity = 1.0f;
	
	bool UseDepthBuffer();
	void DontUseDepthBuffer();
	inline glm::mat4 getLightProjection() { return lightProjection; }
	inline GLuint getDepthTextureID() { return (depthBuffer != nullptr) ? depthBuffer->textureDepthID : 0; }

	float dm_size = 10;
	float dm_depth = 10;
	int dm_resolution = 2048;
	float bias = 0.005f;

private:
	void InitFramebuffer();
	glm::vec4 dir;
	
	Framebuffer* depthBuffer = nullptr;
	glm::mat4 lightProjection;

	bool showImGuiFramebuffer = false;

};

#include "DefferedRenderer.h"
#include "ForwardRenderer.h"

#endif // !OBJECT_H