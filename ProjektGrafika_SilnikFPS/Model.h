#ifndef MODEL_H
#define MODEL_H

#include "Mesh.h"
#include "Material.h"
#include <iostream>
#include <vector>
#include <string>
#include <glm/glm.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/pbrmaterial.h>
#include <PxPhysics.h>
#include <PxPhysicsAPI.h>

/// Klasa przechowuje informacje o modelu
///
/// Model sk�ada� si� mo�e z wielu siatek Mesh kt�re s� przechowywane w std::vector<Mesh*> meshes w tej klasie.
/// Model mo�e u�ywa� wielu materia��w Material kt�re s� przechowywane w std::vector<Material*> materials w tej klasie.
class Model {
public:
	Model();
	~Model();

	///�aduje model z pliku
	bool LoadModel(std::string path);


	std::vector<Mesh*> meshes;
	std::vector<Material*> materials;

	physx::PxTriangleMesh* getConsolidatedPhysxTriangleMeshes(physx::PxCooking* cooking, physx::PxScene* scene, glm::vec3 scale);

private:
	bool isloaded = false;
	///Metoda u�ywana w LoadModel kt�ra przetwarza przy pomocy biblioteki Assimp kolejne elementy modelu
	void DoSceneProcessing(aiNode* node,const aiScene* scene, std::string modelpath);
	///Metoda u�ywana w DoSceneProcessing kt�ra przetwarza przy pomocy biblioteki Assimp kolejne elementy siatki Mesh
	void DoProcessMesh(aiMesh* mesh, const aiScene* scene, std::string modelpath);
};
#endif // !MODEL_H
