#ifndef DEFFERED_RENDERER_H
#define DEFFERED_RENDERER_H

#define BLOOM_DOWN_PASS_COUNT 8
#define BLOOM_UP_PASS_COUNT 8

#include "Shader.h"
#include "Framebuffer.h"
#include "Object.h"
#include <random>

class DefferedRenderer {
public:
	void Init(int framebuffer_width, int framebuffer_height,Framebuffer* defferedComponents);
	void RenderImage(Framebuffer* fb,Camera* c, Framebuffer* output, Framebuffer* AO, bool applayAO);
	void RenderModel(Shader* shad,Model* model, glm::mat4 transform, glm::mat4 prevTransform, Camera* camera, glm::vec3 objectIDColor , float noise_treshold);
	
	void RenderSSAO(Framebuffer* deffered_components,Camera* c);
	
	inline Framebuffer* getSSAOFramebuffer() { return ssao_blur_fb; }
	inline Framebuffer* getRawSSAOFramebuffer() { return ssao_fb; }

	Framebuffer* getBloomBuffer();
	Framebuffer* getBloomBuffer(int id);
	void RenderBloom(Framebuffer* input, float bloom_treshold);
	void ApplayBloom(Framebuffer* input, Framebuffer* output);

	void RenderMotionBlur(Framebuffer** in_out_image, GLuint velocityTextureID);

	void ApplayTonemapping(Framebuffer* input, Framebuffer* output, bool bindOutput);


	void Destroy();
private:
	void RenderBloomUpAndDownSampling(Framebuffer* input, Framebuffer* output, Shader* shad, bool horizontal, Framebuffer* input2,float pass_dumper);

	

	GLuint VAO, VBO, EBO;
	Shader deffered_shader,
		   bloomInitPassShader, bloomDownPassShader, bloomUpPassShader
		  ,AcesTonemappingShader
		  ,motionBlurShader
		  ,SSAOshader,SSAOBlurShader;
	Framebuffer** bloomPasses;
	Framebuffer* blour_buffor;

	Texture* t_enviroment;
	Texture* t_noise;

	//SSAO
	GLuint noiseTexID;
	std::vector<glm::vec3> ssaoSamples;
	Framebuffer* ssao_fb;
	Framebuffer* ssao_blur_fb;
	int SSAO_SAMPLES_COUNT;
	void InitSamples();
};

#endif // !DEFFERED_RENDERER_H
