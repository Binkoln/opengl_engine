#ifndef FORWARD_RENDERER_H
#define FORWARD_RENDERER_H


#include "Shader.h"
#include "Framebuffer.h"
#include "Object.h"
#include <algorithm>


typedef struct s_RenderForwardQueueObject {
	Shader* shad;
	Model* model;
	glm::mat4 transform;
	Camera* camera;
	glm::vec3 objectIDColor;
	float distanceToCamera;
}RenderForwardQueueObject;


class ForwardRenderer {
public:

	void QueueModelForRender(Object* o,Shader* shad, Model* model, glm::mat4 transform, Camera* camera, glm::vec3 objectIDColor);
	
	//Render Shadow
	void SortQueuedModelsAndRender();
	void RenderModel(Shader* shad, Model* model, glm::mat4 transform, Camera* camera, glm::vec3 objectIDColor);
	void RenderModelToDepthMap(Shader* shad, Model* m, glm::mat4 transform, DirectionalLightExtension* dl,glm::mat4 d);

	void Init(int framebuffer_width, int framebuffer_height);
	void Destroy();

private:
	std::vector<RenderForwardQueueObject*> renderqueue;
};


#endif // FORWARD_RENDERER_H