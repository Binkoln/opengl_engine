#ifndef MODEL_RENDERER_EXTENSION_H
#define MODEL_RENDERER_EXTENSION_H


#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Shader.h"
#include "Texture.h"
#include "Object.h"
#include "Model.h"
#include "AssetsExplorerWindow.h"
#include "InputManager.h"
#include "Framebuffer.h"



#endif // !MODEL_RENDERER_EXTENSION_H