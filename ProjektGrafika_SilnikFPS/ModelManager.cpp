#include "ModelManager.h"
namespace ModelManagerBlackboard {
	std::vector<struct ModelRecord* > model_list;
}

ModelManager::ModelManager()
{}
ModelManager::~ModelManager()
{
	
}

void ModelManager::deleteAllModels()
{
	for (struct ModelRecord* mm : ModelManagerBlackboard::model_list) {
		delete mm->model;
		delete mm;
	}
	ModelManagerBlackboard::model_list.clear();
}

struct ModelRecord* ModelManager::isModelAlreadyLoadet(std::string path, void* model_renderer_pointer)
{
	for (struct ModelRecord* mm :ModelManagerBlackboard::model_list) {
		if (mm->path == path) {
			mm->usedIn.push_back(model_renderer_pointer);
			return mm;
		}
	}

	return nullptr;

}

struct ModelRecord* ModelManager::addModelToRecord(std::string path, Model* m, void* model_renderer_pointer)
{
	struct ModelRecord* mr = new struct ModelRecord;
	mr->model = m;
	mr->path = path;
	mr->usedIn.push_back(model_renderer_pointer);
	ModelManagerBlackboard::model_list.push_back(mr);
	return mr;
}

void ModelManager::deleteNotification(struct ModelRecord* modelrec,void* model_renderer_pointer)
{
	if (modelrec == nullptr || model_renderer_pointer == nullptr)return;

	for (int i = 0; i < modelrec->usedIn.size();i++) {
		if (modelrec->usedIn[i] == model_renderer_pointer) {
			modelrec->usedIn.erase(modelrec->usedIn.begin() + i);
		}
	}


	//if not used then delete
	if (modelrec->usedIn.size() == 0)
		deleteUnusedModels();
}

void ModelManager::deleteUnusedModels()
{
	using namespace ModelManagerBlackboard;
	for (int i = 0; i < model_list.size(); i++) {
		if (model_list[i]->usedIn.size() == 0) {

			
			delete model_list[i]->model;
			delete model_list[i];
			model_list.erase(model_list.begin() + i);
			i--;

			continue;
		}
	}
}