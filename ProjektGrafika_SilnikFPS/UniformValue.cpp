#include "Material.h"



UniformValue::UniformValue()
{
	type = UNIFORM_TYPE_FLOAT;
	data = glm::vec3(0, 0, 0);
	sh_pram_name = "";
	texture_name = "";
	tex = nullptr;
}

UniformValue::~UniformValue()
{
	//if (tex != nullptr) { delete tex; }
}

void UniformValue::ImGuiDraw()
{
	ImGui::Text(texture_name.c_str());
}

void Material::ImGuiMaterialEditor(Material* m)
{
	ImGui::Begin("MaterialEditor");


	ImGui::End();
}

void UniformValue::InitAsTexture(std::string path_to_asset, std::string pram_name, std::string tex_name,int textureSamplerID)
{
	struct TextureRecord* texRecord;
	if ((texRecord = TextureManager::isTextureAlreadyLoadet(path_to_asset)) == nullptr) {
		if (tex != nullptr) { delete tex; }
		tex = new Texture();
		if(pram_name=="normalTexture")
			tex->Load(path_to_asset,TEXTURE_FILTER_PIXEL);
		else
			tex->Load(path_to_asset, TEXTURE_FILTER_LINEAR);
		TextureManager::addTextureToRecord(path_to_asset, tex);
		//std::cout << "Loaded new texture:" << path_to_asset << "\n";
	}
	else {
		//std::cout << "Loaded existing texture:" << path_to_asset << "\n";
		tex = texRecord->texture;
	}
	type = UNIFORM_TYPE_TEXTURE;

	data.r = textureSamplerID;
	this->sh_pram_name = pram_name;
	this->texture_name = tex_name;
}

void UniformValue::SendToGPU(Shader* s)
{
	if (type == UNIFORM_TYPE_TEXTURE) {
		
		unsigned int samplerID = (int)data.x;
		if (sh_pram_name == "diffuse") {
			s->SetInt("diffusemap_loadet", 1);
			samplerID = 0;
		}
		if (sh_pram_name == "normalTexture") {
			//std::cout << "Nrm" << "\n";
			s->SetInt("normalmap_loadet", 1);
			samplerID = 1;
		}if (sh_pram_name == "specularTexture") {
			s->SetInt("specularmap_loadet", 1);
			samplerID = 2;
		}
		

		glActiveTexture(GL_TEXTURE0+ samplerID);
		if (tex != nullptr)
			tex->Bind();
		s->SetTextureSampler(sh_pram_name.c_str(), samplerID);
		
	}
	else if (type == UNIFORM_TYPE_FLOAT) {
		s->SetFloat(sh_pram_name.c_str(), data.r);
	}
	else if (type == UNIFORM_TYPE_VECTOR3) {
		s->SetVector3f(sh_pram_name.c_str(), data);
	}


}




Material::Material()
{
	//default values
	AddShadingVector3Value("u_albedoColor", { 1,1,1 });
	AddShadingFloatValue("u_specular_value", 0.3f);
	AddShadingFloatValue("u_roughness_value", 1.0f);
}

Material::~Material()
{
	for (UniformValue* u : uniformList) {
		if (u == nullptr)continue;
			delete u;
			u = nullptr;
	}
	uniformList.clear();
}


void Material::Use(Shader* s)
{

	s->SetInt("diffusemap_loadet", 0);
	s->SetInt("normalmap_loadet", 0);
	s->SetInt("specularmap_loadet", 0);
	for (UniformValue* u : uniformList)
		u->SendToGPU(s);
}



void Material::ImGuiDraw()
{
	ImGui::TreeNode("MaterialUniforms:");
	for (UniformValue* u : uniformList)
		u->ImGuiDraw();
	ImGui::TreePop();
}


void Material::AddTexture(std::string texture_path, std::string uniform_sampler_name, int textureSamplerID)
{
	UniformValue* uval = new UniformValue();

	uval->InitAsTexture(texture_path, uniform_sampler_name, StringTools::getFileName(texture_path), textureSamplerID);
	uniformList.push_back(uval);
}

void Material::AddTexture(std::string texture_path, unsigned int aitype)
{
	if (aitype == aiTextureType_DIFFUSE) {
		UniformValue* uval = new UniformValue();

		uval->InitAsTexture(texture_path, "diffuse", StringTools::getFileName(texture_path),0);
		uniformList.push_back(uval);
	}
	else if (aitype == aiTextureType_NORMALS) {
		UniformValue* uval = new UniformValue();
		//std::cout << "InitNrm" << "\n";
		uval->InitAsTexture(texture_path, "normalTexture", StringTools::getFileName(texture_path),1);
		uniformList.push_back(uval);
	}
	else if (aitype == aiTextureType_SPECULAR) {
		UniformValue* uval = new UniformValue();
		uval->InitAsTexture(texture_path, "specularTexture", StringTools::getFileName(texture_path),2);
		uniformList.push_back(uval);
	}
}


void Material::AddShadingFloatValue(std::string value_name, float value)
{
	//Check for existance of value named value_name
	UniformValue* uval = nullptr;
	for (UniformValue* u : uniformList)
		if (u->sh_pram_name == value_name)
		{
			uval = u;
			break;
		}
	//if not create new one
	if (uval == nullptr) {
		uval = new UniformValue;
		uniformList.push_back(uval);
	}
	//asign new data
	uval->type = UNIFORM_TYPE_FLOAT;
	uval->data.r = value;
	uval->data.g = value;
	uval->data.b = value;
	uval->sh_pram_name = value_name;
}

void Material::AddShadingVector3Value(std::string value_name, glm::vec3 value)
{
	//Check for existance of value named value_name
	UniformValue* uval = nullptr;
	for (UniformValue* u : uniformList)
		if (u->sh_pram_name == value_name)
		{
			uval = u;
			break;
		}
	//if not create new one
	if (uval == nullptr) {
		uval = new UniformValue;
		uniformList.push_back(uval);
	}
	//asign new data
	uval->type = UNIFORM_TYPE_VECTOR3;
	uval->data = value;
	uval->sh_pram_name = value_name;
	
}

void Material::SaveToXML(std::string path) {
	using namespace tinyxml2;
	XMLDocument doc;
	XMLElement* objNode = doc.NewElement("Material");
	doc.InsertEndChild(objNode);
	objNode->InsertNewChildElement("Name")->InsertNewText("MaterialName");



	XMLElement* uniforms = objNode->InsertNewChildElement("UniformValues");
	for (UniformValue* u : this->uniformList)
		u->SaveToXML(uniforms);



	doc.SaveFile(path.c_str());
}

void UniformValue::SaveToXML(tinyxml2::XMLElement* us)
{
	using namespace tinyxml2;
	XMLElement* e = us->InsertNewChildElement("UnivormValue");

	e->InsertNewChildElement("Type")->InsertNewText(StringTools::intToString(type).c_str());
	e->InsertNewChildElement("ParamaterName")->InsertNewText(sh_pram_name.c_str());
	if (type == UNIFORM_TYPE_FLOAT)
		e->InsertNewChildElement("V3FData")->InsertNewText(StringTools::vec3ToString(data).c_str());

	if(type == UNIFORM_TYPE_VECTOR3)
		e->InsertNewChildElement("V3FData")->InsertNewText(StringTools::vec3ToString(data).c_str());

	if (type == UNIFORM_TYPE_TEXTURE && tex != nullptr) {
		e->InsertNewChildElement("TextureName")->InsertNewText(texture_name.c_str());
		e->InsertNewChildElement("TexturePath")->InsertNewText(tex->GetPath().c_str());
		e->InsertNewChildElement("SamplerId")  ->InsertNewText(StringTools::intToString((int)data.r).c_str());
	}
	
}

void Material::Clear() {
	for (UniformValue* uv : uniformList) {
		uv->~UniformValue();
	}
	uniformList.clear();
}

void Material::LoadFromXML(std::string path)
{
	Clear();
	using namespace tinyxml2;
	XMLDocument doc;
	XMLError eResult = doc.LoadFile(path.c_str());
	if (eResult != XML_SUCCESS)
	{
		std::cout << "Error:Can't not open material file. Path:" << path << "\n";
		return;
	}
	auto root = doc.FirstChildElement("Material");
	std::string matName = "Material";
	auto nameNode = root->FirstChildElement("Name");
	if (nameNode != nullptr)
		matName = nameNode->GetText();

	auto UniformValuesArray = root->FirstChildElement("UniformValues");

	if (!UniformValuesArray->NoChildren()) {
		auto node = UniformValuesArray->FirstChildElement("UnivormValue");
		while (true) {
			ProcessXMLUniformValueNode(node);
			if (node == UniformValuesArray->LastChild())
				break;
			else
				node = node->NextSiblingElement("UnivormValue");
		}
	}
}

void Material::ProcessXMLUniformValueNode(tinyxml2::XMLElement* node) 
{
	auto typeNode = node->FirstChildElement("Type");
	std::string type = "";
	if (typeNode != nullptr)
		type = typeNode->GetText();
	else
		return;


	auto paramNameNode = node->FirstChildElement("ParamaterName");
	std::string paramName = "";
	if (paramNameNode != nullptr)
		paramName = paramNameNode->GetText();
	else
		return;

	if ( StringTools::stringToInt(type) == UNIFORM_TYPE_FLOAT) {
		auto dataNode = node->FirstChildElement("V3FData");
		if (dataNode != nullptr) {
			auto data = StringTools::stringToVec3(dataNode->GetText());
			//Add unoform attribute
			this->AddShadingFloatValue(paramName, data.r);
		}
	}

	if (StringTools::stringToInt(type) == UNIFORM_TYPE_VECTOR3 ) {
		auto dataNode = node->FirstChildElement("V3FData");
		if (dataNode != nullptr) {
			auto data = StringTools::stringToVec3(dataNode->GetText());
			//Add unoform attribute
			this->AddShadingVector3Value(paramName, data);
		}
	}

	if (StringTools::stringToInt(type) == UNIFORM_TYPE_TEXTURE) {
		auto dataNode = node->FirstChildElement("TexturePath");
		auto samplerIdNode = node->FirstChildElement("SamplerId");
		if (dataNode != nullptr && samplerIdNode != nullptr) {
			auto texturePath = std::string(dataNode->GetText());
			auto samplerId = StringTools::stringToInt(samplerIdNode->GetText());
			//Add unoform attribute

			this->AddTexture(texturePath, paramName, samplerId);
		}
	}

}