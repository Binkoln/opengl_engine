#pragma once
#include <chrono>
#include <iostream>
#include <vector>

class ProfilerTimerUnit {
public:
	//ProfilerTimerUnit();
	//~ProfilerTimerUnit();


	/// Uruchamiane na pocz�tku i ko�cu metody
	void StartTimer() { if (hasTimerStarted)return; hasTimerStarted = true;  start_time = std::chrono::high_resolution_clock::now(); }
	void StopTimer() { 
		if (!hasTimerStarted)return; hasTimerStarted = false;
		stop_time = std::chrono::high_resolution_clock::now(); 
		auto start = std::chrono::time_point_cast<std::chrono::microseconds>(start_time).time_since_epoch().count();
		auto stop = std::chrono::time_point_cast<std::chrono::microseconds>(stop_time).time_since_epoch().count();

		lastTimeMS = (stop - start)*0.001;
		avreageSumTime += lastTimeMS;
		avregingDenominator ++;
	}

	void Reset() { lastTimeMS = 0; avreageSumTime = 0; avregingDenominator = 0; }

	inline double GetAvreageTimeMS() { return (avregingDenominator !=  0 )? (avreageSumTime / (double)avregingDenominator) : 0; }

	inline double GetTimeMesurement() { return lastTimeMS; }
private:
	bool hasTimerStarted = false;
	std::chrono::time_point<std::chrono::high_resolution_clock> start_time;
	std::chrono::time_point<std::chrono::high_resolution_clock> stop_time;


	double lastTimeMS = 0.0;
	
	double avreageSumTime = 0.0;
	unsigned int avregingDenominator = 0;
};