#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H

#include <vector>
#include <string>
#include "Texture.h"

/// Struktura przechowuje informacje dla TextureManager o za�adowanej teksturze oraz �cie�ce do pliku z kt�rej zosta�a za�adowana
///
/// @param Texture* texture - wska�nik na za�adowan� tekstur�
/// @param string path - �cie�ka do pliku tekstury
/// @see TextureManager Texture Material Model ModelManager
struct TextureRecord {
	Texture* texture;
	std::string path;
};


/// Klasa zarz�dca zapobiegaj�ca wielokrotnemu �adowaniu tej samej testury je�li jest ona urzywana przez wi�cej ni� jeden model. Pozwala ona oszcz�dzi� pami�� oraz zmniejszy� czas �adowania posim�w.
class TextureManager {
public:
	TextureManager();
	/// Usuwa wszystkie zarejestrowane w rejestrze tekstury
	~TextureManager();

	///Sprawdza czy dana tekstura jest ju� za�adowana i je�li tak zwraca struct TextureRecord , a je�li nie to nullptr
	static struct TextureRecord* isTextureAlreadyLoadet(std::string path);
	///Dodaje tekstur� do rejestru za�adowanych tekstur
	static void addTextureToRecord(std::string path, Texture* t);
	
private:
	
};

#endif // !TEXTURE_MANAGER_H
