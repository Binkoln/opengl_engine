#include "Object.h"

Level::Level(bool init_physx)
{
	setLevelObjectIDIterator(0);



	if (init_physx) {
		mFoundation = PxCreateFoundation(PX_PHYSICS_VERSION, mDefaultAllocatorCllback, mDefaulrErrorCallback);
		if (mFoundation == NULL) {
			std::cout << "No foundation! \n";
			throw("PxCreateFoundation failed!");
		}
		else {


			bool PVD = true;
			
			if (PVD) {
				pvd = physx::PxCreatePvd(*mFoundation);
				transport = physx::PxDefaultPvdSocketTransportCreate("127.0.0.1", 5425, 10);
				
				pvd->connect(*transport, physx::PxPvdInstrumentationFlag::eALL);
				
			}


			mTolerancesScale.speed = 10.0f;
			mTolerancesScale.length = 1.0f;

			if(PVD)
				mPhysics = PxCreatePhysics(PX_PHYSICS_VERSION, *(mFoundation), mTolerancesScale,true, pvd);
			else
				mPhysics = PxCreatePhysics(PX_PHYSICS_VERSION, *(mFoundation), mTolerancesScale);

			if (mPhysics == NULL) {
				std::cout << "No mPhysics! \n";
				throw("Creation mPhysics failed!");
			}


			physx::PxCookingParams params(mPhysics->getTolerancesScale());
			params.buildGPUData = true;
			gPxCooking = PxCreateCooking(PX_PHYSICS_VERSION, *(mFoundation), params);

			if (gPxCooking == NULL)
			{
				printf("\nPhysX Error: Unable to initialize the cooking library\n\n");
			}

			//physx::PxCudaContextManager cudaContextManagerDesc;

			physx::PxSceneDesc sceneDesc(mPhysics->getTolerancesScale());
			sceneDesc.gravity = physx::PxVec3(0.0f, -9.81f, 0.0f);
			mDispatcher = physx::PxDefaultCpuDispatcherCreate(16);
			sceneDesc.cpuDispatcher = mDispatcher;
			sceneDesc.broadPhaseType = physx::PxBroadPhaseType::eABP;
			sceneDesc.filterShader = physx::PxDefaultSimulationFilterShader;
			mScene = mPhysics->createScene(sceneDesc);

			characterControllersManager = PxCreateControllerManager(*mScene);
		}//end PHYSX init
	}

}

Level::~Level()
{
	//mScene->fetchResults();
	if (mFoundation != NULL) {
		characterControllersManager->release();
		mScene->release();
		mDispatcher->release();
		mPhysics->release();
		gPxCooking->release();

		if (pvd != nullptr) {
			if (pvd->isConnected())
				pvd->disconnect();

			pvd->release();
		}
		mFoundation->release();
	}
}

namespace BLACKBOARD_LEVEL {
	long id_iterator = 0;
	std::vector<struct SwapObjectPointer> swap_pointers;
	std::vector<Object*> objectToDeletion;
	std::vector<PointLightExtension*> pointLights;
	std::vector<DirectionalLightExtension*> directionalLights;
};

std::vector<PointLightExtension*> Level::GetPointLightsList() {
	return BLACKBOARD_LEVEL::pointLights;
}

void Level::AddPointLight(PointLightExtension* pl) {
	RemovePointLight(pl);
	BLACKBOARD_LEVEL::pointLights.push_back(pl);
}

void Level::RemovePointLight(PointLightExtension* pl) {
	for (int i = 0; i < BLACKBOARD_LEVEL::pointLights.size();i++) {
		PointLightExtension* p = BLACKBOARD_LEVEL::pointLights[i];
		if (p == pl)
		{
			BLACKBOARD_LEVEL::pointLights.erase(BLACKBOARD_LEVEL::pointLights.begin() + i);
			break;
		}
	}
}

void Level::AddDirectionalLight(DirectionalLightExtension* pl) {
	RemoveDirectionalLight(pl);
	BLACKBOARD_LEVEL::directionalLights.push_back(pl);
}

void Level::EnablePlayMode() {
	//zainituj wszystkie obiekty physx
	if (sceneObject == nullptr)return;
	LuaScriptExtension::SetGlobalLevel(this);
	sceneObject->OnPlay();

	playMode = true; /*Utwoz stan sceny*/
}

void Level::RemoveDirectionalLight(DirectionalLightExtension* pl) {
	for (int i = 0; i < BLACKBOARD_LEVEL::directionalLights.size(); i++) {
		DirectionalLightExtension* p = BLACKBOARD_LEVEL::directionalLights[i];
		if (p == pl)
		{
			BLACKBOARD_LEVEL::directionalLights.erase(BLACKBOARD_LEVEL::directionalLights.begin() + i);
			break;
		}
	}
}

std::vector<DirectionalLightExtension*> Level::GetDirectionalLightsList() {
	return BLACKBOARD_LEVEL::directionalLights;
}

void Level::RegisterObjectSwapService(struct SwapObjectPointer s)
{
	BLACKBOARD_LEVEL::swap_pointers.push_back(s);
}

Object* Level::findObjectInLevelByID(long id)
{
	if (sceneObject != nullptr) 
			return sceneObject->findObjectInLevelByID(id);
	
	return nullptr;
}

Object* Level::findFirstObjectWithName(std::string name, Object* start)
{
	if (sceneObject == nullptr || !isPlatModeEnabled())return nullptr;
	return (start != nullptr)? start->findFirstObjectWithName(name) : sceneObject->findFirstObjectWithName(name);
}

long Level::GetObjectID()
{
	return BLACKBOARD_LEVEL::id_iterator++;
}

void Level::setLevelObjectIDIterator(long new_id)
{
	BLACKBOARD_LEVEL::id_iterator = new_id;
}



void Level::Save(std::string path)
{
	if (sceneFilePath != path) sceneFilePath = path;

	using namespace tinyxml2;
	XMLDocument doc;

	XMLElement* e = doc.NewElement("Scene");
	doc.InsertEndChild(e);
	XMLElement* iter = e->InsertNewChildElement("Iterator");
	iter->InsertNewText(StringTools::longToString(BLACKBOARD_LEVEL::id_iterator).c_str());

	if (sceneObject != nullptr)
		sceneObject->SaveScene(e);

	
	doc.SaveFile(path.c_str());
}

void Level::Load(std::string path)
{
	using namespace tinyxml2;
	XMLDocument doc;
		
	XMLError eResult = doc.LoadFile((path).c_str());

	std::cout << "Loading level:" << path << "\n";

	if (eResult != XML_SUCCESS)
	{
		std::cout << "Error:Can't not open scene file. Path:"<< path<<"\n";
		return;
	}

	sceneFilePath = std::string(path);


	XMLElement* e = doc.FirstChildElement("Scene");
	if (sceneObject != nullptr) DeleteLevel();

	XMLElement* it = e->FirstChildElement("Iterator");
	long iterator = StringTools::stringToLong(it->GetText());
	setLevelObjectIDIterator(iterator);

	sceneObject = Object::LoadScene(e->FirstChildElement("Object"));

	if (sceneObject != nullptr) {
		AssignObjectPointersToLuaScriptVariables();
		sceneObject->PushScriptingVariables();
	}
}

void Level::AssignObjectPointersToLuaScriptVariables()
{
	for (struct SwapObjectPointer sp : BLACKBOARD_LEVEL::swap_pointers) {
		Object* o = findObjectInLevelByID(sp.obj_id);
		struct LuaScriptVariable* lvs = (struct LuaScriptVariable*)sp.ptr;
		lvs->ptr = o;
	}
}

void Level::RegisterObjectToDeletion(Object* o) {
	if(std::find(BLACKBOARD_LEVEL::objectToDeletion.begin(), BLACKBOARD_LEVEL::objectToDeletion.end(),o) == BLACKBOARD_LEVEL::objectToDeletion.end())
		BLACKBOARD_LEVEL::objectToDeletion.push_back(o);
}

void Level::Update()
{

	if (mScene == NULL)
	{
		printf("\nError: mScene is not created.\n\n");
	}
	else if(playMode){
		physxProfilerTimer.Reset();
		physxProfilerTimer.StartTimer();
		mScene->simulate(0.016666);
		mScene->fetchResults(true);
		physxProfilerTimer.StopTimer();
	}

	if (isSceneCreated())
		sceneObject->Update(this);

	while (BLACKBOARD_LEVEL::objectToDeletion.size() > 0) {
		deleteObject(BLACKBOARD_LEVEL::objectToDeletion[0]);
		BLACKBOARD_LEVEL::objectToDeletion.erase(BLACKBOARD_LEVEL::objectToDeletion.begin());
	}
}

void Level::Render()
{
	if (isSceneCreated())
		sceneObject->Render(this);
}

void Level::deleteObject(Object* o)
{
	if (o == selectedObjectPointer)
		selectedObjectPointer = nullptr;
	if (o == camera)
		camera = nullptr;

	o->unchild();
	if (isSceneCreated() && o != sceneObject)
		sceneObject->objectActionNotify(o, OBJECT_ACTION_DELETE);

	//while(o->childs.size() > 0)
		//deleteObject(o->childs[0]);

	o->Cleanup();
	delete o;
}

void Level::DeleteLevel()
{
	BLACKBOARD_LEVEL::id_iterator = 0;
	BLACKBOARD_LEVEL::objectToDeletion.clear();
	BLACKBOARD_LEVEL::swap_pointers.clear();
	sceneFilePath = "";
	selectedObjectPointer = nullptr;
	if (sceneObject != nullptr) {
		while(sceneObject->childs.size() > 0)
			deleteObject(sceneObject->childs[0]);
		delete sceneObject;
		sceneObject = nullptr;
	}
}