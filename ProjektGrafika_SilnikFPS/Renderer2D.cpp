#include "Renderer2D.h"

Renderer2D::Renderer2D()
{
	VAO = NULL;
	EBO = NULL;
	VBO = NULL;
}

Renderer2D::~Renderer2D()
{}

void Renderer2D::Init()
{

	const float vertices[] = { -0.5f,0.5f,0.0f,0.0f,
							    0.5f,0.5f,1.0f,0.0f ,
							  - 0.5f,-0.5f,0.0f,1.0f,
							    0.5f,-0.5f,1.0f,1.0f };

	const unsigned int indieces[] = { 0,1,2,1,3,2 };

	//OpenGl Sprite Mesh Init
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indieces), indieces, GL_STATIC_DRAW);

	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(float)*4, (void*)0);

	shader = Shader("EngineProjectFolder/assets/shaders/v_Renderer2D.glsl", "EngineProjectFolder/assets/shaders/f_Renderer2D.glsl");
}

void Renderer2D::Destroy()
{
	if (EBO != NULL) {
		glDeleteBuffers(1, &EBO);
		EBO = NULL;
	}

	if (VBO != NULL) {
		glDeleteBuffers(1, &VBO);
		VBO = NULL;
	}

	if (VAO != NULL) {
		glDeleteVertexArrays(1, &VAO);
		VAO = NULL;
	}

	shader.Delete();
}

void Renderer2D::RenderSprite(
	Texture* texture,//Tekstura
	glm::vec4 posSiz,//Pozycja wycinania z tekstury
	glm::vec3 spriteColor,//Kolor tekstury (mno�ymy ten kolor przez kolor tekstury)
	glm::mat4 model,//Pozycja obliczana z Object (mno�ymy wierzcho�ki przez t� macierz)
	glm::mat4 viewProjection)//Macierz projekcji z kamery
{

	glEnable(GL_BLEND);//W��czamy blendowanie aplha
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	glDisable(GL_DEPTH_TEST);// W��czamy rendering 2D

	shader.Use();
	glBindVertexArray(VAO);//Wysy�amy wierzcho�ki
	bool textureLoadet = (texture != nullptr);

	//Wk�adamy dane do shadera
	shader.SetInt("diffusemap_loadet", (textureLoadet)?1:0);
	shader.SetMatrix4("viewProjection", viewProjection);
	shader.SetMatrix4("model", model);
	shader.SetVector3f("spriteColor", spriteColor);
	shader.SetVector4f("uvs_posSize", posSiz);
	
	//Je�li tekstura jest ustawiona to wysya�my j� do shadera
	if (textureLoadet)
	{
		glActiveTexture(GL_TEXTURE0);
		texture->Bind();
		shader.SetTextureSampler("diffuse", 0);
	}

	//Rysuje na ekran
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

}

void Renderer2D::RenderSprite(
	GLuint texture,//Tekstura
	bool isTextureLoaded,//Czy za�adowana
	glm::vec4 posSiz,//Pozycja wycinania z tekstury
	glm::vec3 spriteColor,//Kolor tekstury (mno�ymy ten kolor przez kolor tekstury)
	glm::mat4 model,//Pozycja obliczana z Object (mno�ymy wierzcho�ki przez t� macierz)
	glm::mat4 viewProjection)//Macierz projekcji z kamery
{
	glEnable(GL_BLEND);//W��czamy blendowanie aplha
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_DEPTH_TEST);// W��czamy rendering 2D

	shader.Use();
	glBindVertexArray(VAO);//Wysy�amy wierzcho�ki
	
	//Wk�adamy dane do shadera
	shader.SetInt("diffusemap_loadet", 1);
	shader.SetMatrix4("viewProjection", viewProjection);
	shader.SetMatrix4("model", model);
	shader.SetVector3f("spriteColor", spriteColor);
	shader.SetVector4f("uvs_posSize", posSiz);

	//Je�li tekstura jest ustawiona to wysya�my j� do shadera
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture);
		shader.SetTextureSampler("diffuse", 0);
	

	//Rysuje na ekran
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

}