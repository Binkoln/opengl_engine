#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include <gl/glew.h>
#include "Texture.h"
#include "InputManager.h"

#define FRAMEBUFFER_TYPE_COLOR 1
#define FRAMEBUFFER_TYPE_DEPTH 2
#define FRAMEBUFFER_TYPE_COLOR_AND_DEPTH 4
#define FRAMEBUFFER_TYPE_DEFFERED_RENDERING_USAGE 8
#define FRAMEBUFFER_TYPE_DEPTH_ONLY 16
#define FRAMEBUFFER_TYPE_COLOR_HDR 32

/// Klasa pozwala utworzy� framebuffer do kt�rego mo�na renderowa� sceny, itd by potem wykorzysta� to do posteffect�w itd. (patrz dokumentacj OpenGl)
class Framebuffer {
public:

	Framebuffer();
	~Framebuffer();

	/// U�ywa framebuffera
	void Bind();
	/// Przestaje u�ywa� framebuffera
	void Unbind();

	/// Inicjalizuje framebuffer
	///
	/// @param unsigned int width - szeroko�� w pikselach
	/// @param unsigned int height - wysoko�� w pikselach
	/// @param unsigned int framebuffer_type - typ framebuffera
	void Init(unsigned int width, unsigned int height,unsigned int framebuffer_type);

	void SetDepthBufferSource(GLuint framebufferDepthID);
	void SetVelocityBufferSource(GLuint framebufferDepthID);
	inline GLuint GetDepthBufferSource() { return textureDepthID; }

	/// Zwraca id textury framebuffera
	inline GLuint getTexture() { return textureColorID; }
	inline unsigned int getWidth() { return width; }
	inline unsigned int getHeight() { return height; }

	GLuint framebufferID, textureColorID, textureDepthID, textureNormalID, texturePositionID,textureMaterialProportiesID,//Deffered rendering
		textureObjectID,//Object picking
		textureVelocityID,//Motion blur
		textureViewSpacePositionID, textureViewSpaceNormalID;//SSAO
private:
	void DeleteFB_TEXTURE(GLuint& id);
	unsigned int framebyffer_type,width,height;
};

#endif // !FRAMEBUFFER_H
