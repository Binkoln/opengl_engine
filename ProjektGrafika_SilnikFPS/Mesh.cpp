#include "Mesh.h"


Mesh::Mesh(std::string mesh_name) {
	this->mesh_name = mesh_name;
}

Mesh::~Mesh() {
	if (loaded) {
		glDeleteBuffers(1, &VBO);
		glDeleteBuffers(1, &EBO);
		glDeleteVertexArrays(1, &VAO);
		material.~Material();
	}
}


bool Mesh::LoadMesh(std::vector<struct MeshVertex> vertices, std::vector<unsigned int> indiecies, unsigned int material_index)
{
	this->vertices = vertices;
	this->indiecies = indiecies;
	indi_count = indiecies.size();

	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	glBufferData(GL_ARRAY_BUFFER, sizeof(struct MeshVertex) * vertices.size(), &vertices[0], GL_STATIC_DRAW);

	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);

	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indiecies.size(), &indiecies[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);//position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(struct MeshVertex), (void*)0);

	glEnableVertexAttribArray(1);//uv
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(struct MeshVertex), (void*)offsetof(MeshVertex,UV));

	glEnableVertexAttribArray(2);//normal
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(struct MeshVertex), (void*)offsetof(MeshVertex, normal));

	glEnableVertexAttribArray(3);//tangent
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(struct MeshVertex), (void*)offsetof(MeshVertex, tangent));

	glEnableVertexAttribArray(4);//bitangent
	glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(struct MeshVertex), (void*)offsetof(MeshVertex, bitangent));

	glEnableVertexAttribArray(5);//bone weights
	glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, sizeof(struct MeshVertex), (void*)offsetof(MeshVertex, boneWeights));

	glEnableVertexAttribArray(6);//bone id's
	glVertexAttribPointer(6, 3, GL_INT, GL_FALSE, sizeof(struct MeshVertex), (void*)offsetof(MeshVertex, boneIDs));

	glBindVertexArray(0);

	loaded = true;
	return true;
}



