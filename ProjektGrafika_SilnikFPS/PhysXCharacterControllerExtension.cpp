#include "Object.h"

PhysXCharacterControllerExtension::PhysXCharacterControllerExtension() :Extension("PhysXCharacterControllerExtension") {
    type = "PhysXCharacterControllerExtension";

}

PhysXCharacterControllerExtension::~PhysXCharacterControllerExtension() {

}

void PhysXCharacterControllerExtension::Init() {

}


tinyxml2::XMLElement* PhysXCharacterControllerExtension::SaveScene(tinyxml2::XMLElement* e)
{
    using namespace tinyxml2;
    XMLElement* extension = Extension::SaveScene(e);
    extension->InsertNewChildElement("Mass")->InsertNewText(StringTools::doubleToString(mass).c_str());
    extension->InsertNewChildElement("ColliderSize")->InsertNewText(StringTools::vec3ToString(collider_size).c_str());
    extension->InsertNewChildElement("Material")->InsertNewText(StringTools::vec3ToString(material_properties).c_str());
    extension->InsertNewChildElement("Origin")->InsertNewText(StringTools::vec3ToString(shape_origin).c_str());

    return extension;
}

Extension* PhysXCharacterControllerExtension::MakeCopy()
{
    PhysXCharacterControllerExtension* mre = new PhysXCharacterControllerExtension();
    mre->enabled = enabled;
    mre->name = name;

    //Copy
  
    mre->mass = mass;
    mre->collider_size = glm::vec3(collider_size);
    mre->material_properties = glm::vec3(material_properties);
    mre->shape_origin = glm::vec3(shape_origin);
    return mre;
}

void PhysXCharacterControllerExtension::Cleanup() {

    if (px_mat != nullptr)
        px_mat->release();
    
    if (controller != nullptr)
        controller->release();


    px_mat = nullptr;
    controller = nullptr;
}

void PhysXCharacterControllerExtension::Update(Object* o, Level* level) {
    if (!level->isPlatModeEnabled())return;
    if (!initialized) {
        initialized = true;
        InitPhysX_Body(level->mScene, level, o);
    }
    else {
        //fetch position
        physx::PxExtendedVec3 pos = controller->getPosition();
        o->position.x = pos.x;
        o->position.y = pos.y;
        o->position.z = pos.z;
    }

        Move(velocity);
        
        velocity.y += -gravity;
}

void PhysXCharacterControllerExtension::Move(glm::vec3 displacment)
{
    if (!initialized)return;
    //0.016666
    physx::PxControllerFilters filters;
    
    physx::PxControllerCollisionFlags cf = controller->move(physx::PxVec3(displacment.x, displacment.y, displacment.z), 0.001, 0.016666,filters);
    
    if (cf.isSet(physx::PxControllerCollisionFlag::eCOLLISION_DOWN)) {
        velocity.y = 0;
        onGround = true;
    }
    
    if (cf.isSet(physx::PxControllerCollisionFlag::eCOLLISION_UP)) {
        velocity.y = -gravity;
    }
}

void  PhysXCharacterControllerExtension::Render(Object* o, Level* level) {
    //Render Colider
}

void PhysXCharacterControllerExtension::InitPhysX_Body(physx::PxScene* scene, Level* level, Object* parent)
{
    Cleanup();
    using namespace physx;
    PxTransform pose;
    pose.p.x = parent->position.x;
    pose.p.y = parent->position.y;
    pose.p.z = parent->position.z;

    glm::quat qrot = parent->getRotation();

    pose.q.x = qrot.x;
    pose.q.y = qrot.y;
    pose.q.z = qrot.z;
    pose.q.w = qrot.w;


    px_mat = scene->getPhysics().createMaterial(material_properties.x, material_properties.y, material_properties.b);

    PxCapsuleControllerDesc desc;
    desc.material = px_mat;
    desc.radius = collider_size.x;
    desc.height = collider_size.y;
    desc.position = PxExtendedVec3(pose.p.x, pose.p.y, pose.p.z);
    

    desc.upDirection = PxVec3(0, 1, 0);
    desc.climbingMode = PxCapsuleClimbingMode::eCONSTRAINED;
    desc.stepOffset = 0.04;
    
    desc.slopeLimit = cosf((float)(glm::radians(70.0f)));
    controller = level->characterControllersManager->createController(desc);

}

void  PhysXCharacterControllerExtension::DrawImGui(Object* parent) {
    if (ImGui::CollapsingHeader(name.c_str(), ImGuiTreeNodeFlags_DefaultOpen)) {
        Extension::DrawImGui(parent);
        ImGui::DragFloat("Capsule radius", &collider_size.x, 0.1f);
        ImGui::DragFloat("Capsule height", &collider_size.y, 0.1f);
        ImGui::Text("PhysX Character Controller");

    }
    ImGui::Separator();
}