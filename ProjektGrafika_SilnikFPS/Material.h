#ifndef MATERIAL_H
#define MATERIAL_H

#include <glm/glm.hpp>
#include <vector>
#include "Texture.h"
#include "Shader.h"
#include <string>
#include "imgui/imgui.h"
#include "TextureManager.h"
#include "StringTools.h"
#include <assimp/scene.h>
#include "tinyxml2/tinyxml2.h"

#define UNIFORM_TYPE_TEXTURE 0
#define UNIFORM_TYPE_VECTOR3 1
#define UNIFORM_TYPE_FLOAT 2

/// Klasa zawiaraj�ca informacje o warto�ci przekazywanej do shadera podczas renderingu model�w 3d
///
/// Parametrami mog� by� liczba float, waktor glm::vec3 lub tekstura Texture
class UniformValue {
public:
	UniformValue();
	~UniformValue();

	/// Wysy�a parametr do shadera
	void SendToGPU(Shader* s);
	void ImGuiDraw();
	/// Inicjalizuje si� jako tekstura
	///
	/// U�ywane w Material:AddTexture
	/// @param string path_to_asset - �cie�ka do tekstury
	/// @param string pram_name - nazwa uniforma w shaderze
	/// @param string tex_name - nazwa tekstury Texture
	void InitAsTexture(std::string path_to_asset,std::string pram_name,std::string tex_name, int textureSamplerID);

	/// Zapisuje parametr do tinyxml2::XMLElement* e
	//virtual tinyxml2::XMLElement* SaveToXML(tinyxml2::XMLElement* e);
	/// Odczytuje parametr
	//static Object* LoadFromXML(tinyxml2::XMLNode* obj);
	void SaveToXML(tinyxml2::XMLElement* uniforms);

	int type;
	std::string sh_pram_name,texture_name;
	glm::vec3 data;
	Texture* tex;
};

/// Klasa opisuj�ca wygl�d danego modelu 3d
///
/// Zawiara ona list� std::vector<UniformValue*> uniformList kt�ra to przechowuje parametry
/// przekazywane do shadera podczas renderingu. 
class Material {
public:
	Material();
	~Material();

	///Ustawia uniformy w shaderze na dane parametry (UniformValue) z uniformList
	void Use(Shader* s);
	void ImGuiDraw();
	///Dodaje parametr tekstury do listy
	///
	/// Metoda u�ywana w Model podczas �adowania modelu
	/// @param string texture_path - �ciezka do tekstury
	/// @param unsigned int aitype - typ tekstury
	void AddTexture(std::string texture_path,unsigned int aitype);
	void AddTexture(std::string texture_path, std::string uniform_sampler_name,int textureSamplerID);

	void AddShadingFloatValue(std::string value_name, float value);
	void AddShadingVector3Value(std::string value_name, glm::vec3 value);
	static void ImGuiMaterialEditor(Material* m);

	void SaveToXML(std::string path);
	void LoadFromXML(std::string path);


	std::vector<UniformValue*> uniformList;
private:
	void Clear();
	void ProcessXMLUniformValueNode(tinyxml2::XMLElement* node);
};

#endif // !MATERIAL_H
