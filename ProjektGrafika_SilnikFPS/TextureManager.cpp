#include "TextureManager.h"

namespace TextureManagerBlackboard {
	std::vector<struct TextureRecord*> textures_list;
}

TextureManager::TextureManager()
{

}

TextureManager::~TextureManager()
{
	for (struct TextureRecord* tr :TextureManagerBlackboard::textures_list) {
		delete tr->texture;
		delete tr;
	}
	TextureManagerBlackboard::textures_list.clear();
}

struct TextureRecord* TextureManager::isTextureAlreadyLoadet(std::string path)
{
	for (struct TextureRecord* tr :TextureManagerBlackboard::textures_list) {
		if (tr->path == path)
			return tr;
	}

	return nullptr;
}

void TextureManager::addTextureToRecord(std::string path, Texture* t)
{
	struct TextureRecord* tr = new struct TextureRecord;
	tr->path = path;
	tr->texture = t;
	TextureManagerBlackboard::textures_list.push_back(tr);
}
